import isf.tools.barcode.ocr.core.Recognized;
import isf.tools.barcode.ocr.core.Recognizer;
import isf.tools.barcode.ocr.core.Settings;
import isf.util.base64.Base64;
import isf.util.codbarras.BarCode2D;
import isf.util.codbarras.BarCodeEncoder;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPOutputStream;


public class Prueba {
	String block;
	String gz64;
	byte gzipped[];

	public static void main(String[] args) {
		String txt = "Paracetamol Texto muy Largo me parece bien 500 mg|Jose Vicente Nieto-Marquez Fernandez de Heredia|Eladio Linares Morcillo 24240585B|1 comprimido cada 8 horas|16/02/2006-18/02/2006|Comentarios del Prescriptor para el Paciente|Comentarios otros que no se|Estoy poniendo datos hasta 400 Bytes de Tama�o|098 cskjb dcjqhbqwe  ewuuywe ewuew yewuyewu dcuowqyfd78as9fn fkjqwefhn kqweufh 876tg 847fgh 8wefbh";
		String sgn = "MIIGXAYJKoZIhvcNAQcCoIIGTTCCBkkCAQExCzAJBgUrDgMCGgUAMAsGCSqGSIb3"
				+ "DQEHAaCCBUIwggU+MIIEp6ADAgECAgQ8f55iMA0GCSqGSIb3DQEBBQUAMDYxCzAJ"
				+ "BgNVBAYTAkVTMQ0wCwYDVQQKEwRGTk1UMRgwFgYDVQQLEw9GTk1UIENsYXNlIDIg"
				+ "Q0EwHhcNMDUwMzE1MTQwMjQ4WhcNMDgwMzE1MTQwMjQ4WjCBgzELMAkGA1UEBhMC"
				+ "RVMxDTALBgNVBAoTBEZOTVQxGDAWBgNVBAsTD0ZOTVQgQ2xhc2UgMiBDQTESMBAG"
				+ "A1UECxMJNTAwMDUzMzY0MTcwNQYDVQQDEy5OT01CUkUgTElOQVJFUyBNT1JDSUxM"
				+ "TyBFTEFESU8gLSBOSUYgMjQyNDA1ODVCMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCB"
				+ "iQKBgQCiKOQ+nSY7LdLpeJ+cZl+fB807KdcvbNg7RJcXHPsJfzyUk59BY6J4+Esx"
				+ "D2LIf6659Jfui0/0WxQX16tSN6lRVedyiMi0g9GXu9tg6HBVKnQJaCyWk9yz52j/"
				+ "9B3L1cy+Fh8Fc+W/GNXQIBT7mKdlTqjY+JmoDwViRWVZOMk00wIDAQABo4IDCTCC"
				+ "AwUwgYgGA1UdEQSBgDB+gRZlbGluYXJlc0Bzb2x1emlvbmEuY29tpGQwYjEYMBYG"
				+ "CSsGAQQBrGYBBBMJMjQyNDA1ODVCMRcwFQYJKwYBBAGsZgEDEwhNT1JDSUxMTzEW"
				+ "MBQGCSsGAQQBrGYBAhMHTElOQVJFUzEVMBMGCSsGAQQBrGYBARMGRUxBRElPMAkG"
				+ "A1UdEwQCMAAwKwYDVR0QBCQwIoAPMjAwNTAzMTUxNDAyNDhagQ8yMDA4MDMxNTE0"
				+ "MDI0OFowCwYDVR0PBAQDAgWgMBEGCWCGSAGG+EIBAQQEAwIFoDAdBgNVHQ4EFgQU"
				+ "jLN3K9PUGbm3TtgnxsxVSerpqr0wHwYDVR0jBBgwFoAUQJp2RJd0B8SsFMsejU86"
				+ "RXww12EwggExBgNVHSAEggEoMIIBJDCCASAGCSsGAQQBrGYDBTCCAREwNAYIKwYB"
				+ "BQUHAgEWKGh0dHA6Ly93d3cuY2VydC5mbm10LmVzL2NvbnZlbmlvL2RwYy5wZGYw"
				+ "gdgGCCsGAQUFBwICMIHLGoHIQ2VydGlmaWNhZG8gUmVjb25vY2lkbyBleHBlZGlk"
				+ "byBzZWf6biBsZWdpc2xhY2nzbiB2aWdlbnRlLlVzbyBsaW1pdGFkbyBhIGxhIENv"
				+ "bXVuaWRhZCBFbGVjdHLzbmljYSBwb3IgdmFsb3IgbeF4aW1vIGRlIDEwMCBlIHNh"
				+ "bHZvIGV4Y2VwY2lvbmVzIGVuIERQQy5Db250YWN0byBGTk1UOkMvSm9yZ2UgSnVh"
				+ "biAxMDYtMjgwMDktTWFkcmlkLUVzcGHxYS4wHQYJKwYBBAGsZgEhBBAWDlBFUlNP"
				+ "TkEgRklTSUNBMC8GCCsGAQUFBwEDBCMwITAIBgYEAI5GAQEwFQYGBACORgECMAsT"
				+ "A0VVUgIBZAIBADBbBgNVHR8EVDBSMFCgTqBMpEowSDELMAkGA1UEBhMCRVMxDTAL"
				+ "BgNVBAoTBEZOTVQxGDAWBgNVBAsTD0ZOTVQgQ2xhc2UgMiBDQTEQMA4GA1UEAxMH"
				+ "Q1JMMTc3MTANBgkqhkiG9w0BAQUFAAOBgQCGJ1kRskUvcCZtHv2f0/e8ArbrfA/o"
				+ "C8Y0hfuqM6PYekgFfLs77J8VQp3niMzXYjgjcOMW6ScKTkhhehraUfQpHqVgfDSd"
				+ "nUvDrdCobw8On1Ct98EhIJHIvOQCN89b/ORHcDBPrjdjFJQRzNJEdsMyfbH5mZcF"
				+ "sdAAU7ewvVjvQzGB4zCB4AIBATA+MDYxCzAJBgNVBAYTAkVTMQ0wCwYDVQQKEwRG"
				+ "Tk1UMRgwFgYDVQQLEw9GTk1UIENsYXNlIDIgQ0ECBDx/nmIwCQYFKw4DAhoFADAN"
				+ "BgkqhkiG9w0BAQEFAASBgANsEI+sM/Jh5I3AkIRjomGEGNeN7gQcItopl3NIyAWJ"
				+ "uVFtQNHEj5glvh65fhHXpPKsyFrEYAB/5DgI95b1FpD1uk4fd9KCNmyjBnZpTR14"
				+ "hjYAWDc2GBQMy3OTSuuvmoW2upr3zKdMDyVtU+96WV94jxiEtDKqRZFfoXdlmPsD";

		try {
			Prueba p=new Prueba(txt,sgn);
			p.generateMacroPDF417_java4less("MacroPDF417");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Prueba(String txt, String firma) {
		block = txt + "BEGSIG" + firma;
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			GZIPOutputStream gzo = new GZIPOutputStream(bos);
			gzo.write(block.getBytes());
			gzo.close();
			gzipped = bos.toByteArray();
			gz64=Base64.encodeBytes(gzipped);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			gzipped = null;
		}
	}

	public void generateMacroPDF417_java4less(String fname) 
		throws UnsupportedEncodingException, IOException {
		BarCode2D bc = new BarCode2D();
		bc.barType = BarCode2D.PDF417;
		bc.PDFMode = BarCode2D.PDF_BINARY;
		bc.codeBinary = gzipped;
		//bc.PDFMacroFileSize = bc.code.length(); // optional macro pdf field
		bc.resolution = 2;
		bc.PDFECLevel = 5;
		bc.setSize(280,450); // set size of the canvas
		
		
		int[] t = { (byte) 0, (byte) 0, (byte) 0 };
		bc.PDFMacroFileId = t; // mandatory macro pdf field (FILE ID)

		bc.PDFColumns=12;
		bc.PDFMaxRows=70;
		
		bc.resetMacroPDF();
		bc.prepareMacroPDF();
		// paint segments one by one
		byte[] result=new byte[0];
		for (int i = 0; i < bc.PDFMacroSegmentCount; i++) {
			// set current segment index
			bc.PDFMacroSegment = (1 == bc.PDFMacroSegmentCount) ? -1 : i;
			bc.setSize(280,450);
			BarCodeEncoder er = new BarCodeEncoder(bc, "JPG", fname+"-"+i+ ".jpg");
			BufferedImage bi=new BufferedImage(
					bc.getSize().width,
					bc.getSize().height,
					java.awt.image.BufferedImage.TYPE_BYTE_INDEXED );
			bc.paint(bi.getGraphics());
			new showImage(bi);
			Pdf417Recognizer rec=new Pdf417Recognizer(bi);
			result = getBinaryResult(result, i, rec);
			System.out.println(rec.getString());
		}
		//resultados(result);
	}

	class Pdf417Recognizer {
		Recognized res[]=null;
		Recognizer rec=null;
		public Pdf417Recognizer(BufferedImage b){
			Settings param=new Settings();
			param.pdf417=true;
			param.rightScanDir=true;
			param.check=true;
			param.barsToRead=1;
			param.scanInterval=1;
			rec=new Recognizer(b);
			try {
				res = rec.recognize(param);
			} catch (IllegalArgumentException ie) {
				ie.printStackTrace();
				return;
			}
			System.out.println(res[0].toString()
					);
		}
		public String getString(){
			return null;
		}
		public byte[] getBytes(){
			return null;
		}
	}
	
	private byte[] getBinaryResult(byte[] result, int i, Pdf417Recognizer rec) {
		byte[] parcial=rec.getBytes();
		System.out.println("Parcial:"+parcial.length);
		byte[] suma=new byte[result.length+parcial.length-(i>0?32:0)];
		for(int j=0;j<suma.length;j++){
			if(i==0)
				suma[j]=(j<result.length ? result[j] : parcial[j-result.length]);
			else
				suma[j]=(j<result.length ? result[j] : parcial[j-result.length+16]);
		}
		result=suma;
		return result;
	}

	private void resultados(byte[] result) {
		System.out.println("Original:"+gzipped.length+" bytes, New:"+result.length+" bytes.");
		int maxl= gzipped.length > result.length ? gzipped.length : result.length ;
		int wrongs=0,wrongs63=0;
		for(int i=0;i<maxl;i++){
			String s1="none",s2="none";
			if(i<gzipped.length)
				s1=""+gzipped[i];
			if(i<result.length)
				s2=""+result[i];
			if(!s1.equals(s2))
				System.out.print("Malamente ");
			else
				System.out.print("          ");
			System.out.println(i+" ("+s1+")("+s2+")");
			if(!s1.equals(s2)){
				wrongs++;
				if(s2.equals("63"))
					wrongs63++;
			}
		}
		System.out.println("Errores:"+wrongs+","+wrongs63);
	}

	
}
