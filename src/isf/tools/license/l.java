/* l - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.license;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class l {
	public static String a(HashMap hashmap) {
		String string = null;
		if (hashmap == null)
			hashmap = new HashMap();
		Set set = hashmap.entrySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry entry = (Map.Entry) iterator.next();
			String string_0_ = ((String) entry.getKey()).concat("=").concat(
					(String) entry.getValue());
			if (string == null)
				string = string_0_;
			else
				string = string.concat(";").concat(string_0_);
		}
		return string;
	}

	public static HashMap a(String string) throws BaseException {
		if (string == null)
			string = "";
		HashMap hashmap = new HashMap();
		StringTokenizer stringtokenizer = new StringTokenizer(string, ";");
		while (stringtokenizer.hasMoreTokens()) {
			String string_1_ = null;
			String string_2_ = null;
			String string_3_ = stringtokenizer.nextToken();
			StringTokenizer stringtokenizer_4_ = new StringTokenizer(string_3_,
					"=");
			if ((stringtokenizer_4_.hasMoreTokens() && (string_1_ = stringtokenizer_4_
					.nextToken()) == null)
					|| (stringtokenizer_4_.hasMoreTokens() && (string_2_ = stringtokenizer_4_
							.nextToken()) == null))
				BaseException.raise(1, null);
			hashmap.put(string_1_, string_2_);
		}
		return hashmap;
	}
}
