/* d - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.license;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;

public class EzLicense implements Serializable {
	public static final int B = 1;

	public static final int _void = 2;

	// public static final int e = 1;
	public static final int h = 2;

	public static final int H = 3;

	public static final int c = 1;

	public static final int _goto = 2;

	public static final int s = 3;

	public static final int K = 4;

	public static final int d = 1;

	public static final int t = 2;

	public static final int _char = 4;

	public static final int b = 8;

	public static final int _new = 16;

	private String k = null;

	private int _case = 0;

	private int nn = 0;

	private int _do = 0;

	private String C = null;

	private String x = null;

	private String _for = null;

	private Date z = null;

	private String _int = null;

	private boolean E = false;

	private long A = -1L;

	private long f = -1L;

	private int L = 0;

	private static int o = -100;

	String q = n.a("dyAuYCM/bV==", "axHtYW==", "ipsic");

	String[] y = { "", "exp", "cpu", "mhz", "cnc", "nmu", "opt", "usr", "hst",
			"net", "qta", "flt", "cst", "cooky", "cksm", "who", "when", "ts",
			"cust", "prod", "enf", "" };

	protected static final int g = 1;

	protected static final int _else = 2;

	protected static final int r = 3;

	protected static final int _if = 4;

	protected static final int J = 5;

	protected static final int G = 6;

	protected static final int _byte = 7;

	protected static final int v = 8;

	protected static final int m = 9;

	protected static final int i = 10;

	protected static final int _try = 11;

	protected static final int _long = 12;

	protected static final int j = 13;

	protected static final int I = 14;

	protected static final int p = 15;

	protected static final int a = 16;

	protected static final int w = 17;

	protected static final int u = 18;

	protected static final int F = 19;

	protected static final int D = 20;

	protected static final int l = 20;

	public String _for() {
		return k;
	}

	public int _if() {
		return _case;
	}

	public int _else() {
		return nn;
	}

	public int _goto() {
		return _do;
	}

	public String _case() {
		return C;
	}

	public String _char() {
		return x;
	}

	public String _new() {
		return _for;
	}

	public Date _try() {
		return z;
	}

	public String _do() {
		return _int;
	}

	public boolean _long() {
		return E;
	}

	public long _int() {
		return A;
	}

	public long a() {
		return f;
	}

	public int _byte() {
		return L;
	}

	public void _if(String string) {
		k = string;
	}

	public void _for(int i) {
		_case = i;
	}

	public void _if(int i) {
		nn = i;
	}

	public void a(int i) {
		_do = i;
	}

	public void _byte(String string) {
		C = string;
	}

	public void a(String string) {
		x = string;
	}

	public void _try(String string) {
		_for = string;
	}

	public void a(Date date) {
		z = date;
	}

	public void _do(String string) {
		_int = string;
	}

	public void a(boolean bool) {
		E = bool;
	}

	public void a(long l) {
		A = l;
	}

	public void _if(long l) {
		f = l;
	}

	public void _do(int i) {
		L = i;
	}

	public static String _int(String string) {
		return a(new Date().getTime(), string);
	}

	public int a(String string, int i, long l, long l_0_, int i_1_, long l_2_,
			String string_3_) throws BaseException {
		return a(string, null, null, i, l, l_0_, i_1_, l_2_, string_3_, 0L);
	}

	public int a(String string, int i, long l, long l_4_, int i_5_, long l_6_,
			String string_7_, long l_8_) throws BaseException {
		return a(string, null, null, i, l, l_4_, i_5_, l_6_, string_7_, l_8_);
	}

	public int a(String string, HolderBase var_o, Object object, int i, long l,
			long l_9_, int i_10_, long l_11_, String string_12_, long l_13_)
			throws BaseException {
		boolean bool = false;
		int i_14_ = -1;
		String string_15_ = n.a("dyAuYCM/bV==", string, "thekey");
		if (_int(0)) {
			System.out
					.println("In EzLicenseInfo.checkLicenseKey.  Decoded license string:");
			System.out.println(string_15_);
		}
		StringTokenizer stringtokenizer = new StringTokenizer(string_15_, ":");
		k = string;
		String string_16_ = n.a("dyAuYCM/bV==", "QtejQ8G/WciuXr==", q);
		while (stringtokenizer.hasMoreTokens()) {
			String string_17_ = null;
			Object object_18_ = null;
			String string_19_ = stringtokenizer.nextToken();
			if (_int(5))
				System.out.println("tok: ".concat(string_19_));
			StringTokenizer stringtokenizer_20_ = new StringTokenizer(
					string_19_, "=");
			String string_21_;
			if ((stringtokenizer_20_.hasMoreTokens() && (string_17_ = stringtokenizer_20_
					.nextToken()) == null)
					|| (stringtokenizer_20_.hasMoreTokens() && (string_21_ = stringtokenizer_20_
							.nextToken()) == null))
				BaseException.raise(1, null);
			int i_22_ = _for(string_17_);
			if (i_22_ == 0)
				BaseException.raise(1, null);
			string_21_ = string_19_.substring(string_19_.indexOf("=") + 1)
					.replace('|', ':');
			if (_int(5)) {
				System.out.println("In EzLicenseInfo.checkLicenseKey.  kwd="
						.concat(string_17_).concat(",val=").concat(string_21_));
				if (_int(8))
					new Throwable().printStackTrace();
			}
			switch (i_22_) {
			case 1:
				a(string_17_, string_21_, (long) i, (long) i_10_);
				_do |= 0x1;
				break;
			case 2:
			case 3:
			case 4:
			case 5:
				if (_case == 1)
					BaseException.raise(1, null);
				f = a(string_17_, string_21_, f, l_13_, 0L);
				switch (i_22_) {
				case 2:
					nn = 2;
					break;
				case 3:
					nn = 3;
					break;
				case 4:
					nn = 1;
					break;
				default:
					nn = 4;
				}
				_case = 2;
				break;
			case 6:
				if (_int != null)
					BaseException.raise(1, null);
				_int = string_21_;
				_do |= 0x4;
				break;
			case 17: {
				long l_23_ = 0L;
				try {
					l_23_ = Long.parseLong(string_21_);
				} catch (Exception exception) {
					BaseException.raise(1, null);
				}
				long l_24_ = new Date().getTime();
				if (l_23_ - l_24_ > 21600000L)
					BaseException.raise(1, null);
				break;
			}
			case 20:
				E = true;
				break;
			case 14:
				if (bool)
					BaseException.raise(1, null);
				try {
					i_14_ = Integer.parseInt(string_21_);
					bool = true;
				} catch (Exception exception) {
					BaseException.raise(1, null);
				}
				break;
			case 7:
				if (_case == 2 || _for != null)
					BaseException.raise(1, null);
				_case = 1;
				nn = 1;
				if (!string_12_.equals(string_16_)
						&& !string_21_.equals(string_12_))
					BaseException.raise(4, null);
				_for = string_21_;
				break;
			case 8:
			case 9:
				if (_for != null)
					BaseException.raise(1, null);
				if (!string_12_.equals(string_16_)
						&& !string_12_.equals(n._if("dyAuYCM/bV==",
								"zapowakah", "foohfah"))
						&& !string_21_.equals(string_12_))
					BaseException.raise(4, null);
				_for = string_21_;
				break;
			case 10:
				A = a(string_17_, string_21_, A, l, l_11_);
				if (A < l)
					L |= 0x8;
				else if (A - l < l_9_)
					L |= 0x2;
				_do |= 0x2;
				break;
			case 11:
				f = a(string_17_, string_21_, f, l_13_, 0L);
				_case = 1;
				nn = 3;
				break;
			case 12:
				if (var_o != null)
					L |= var_o.check(string_21_, object);
				C = string_21_;
				_do |= 0x8;
				break;
			case 13:
				x = string_21_;
				_do |= 0x10;
				break;
			}
		}
		if (bool
				&& i_14_ != _new(string_15_.substring(0, string_15_
						.lastIndexOf(":")))) {
			if (_int(1))
				System.out.println("** Checksum mismatch.  Checksum: "
						.concat(String.valueOf(i_14_)));
			BaseException.raise(1, null);
		}
		if (_case == 0 && _for != null) {
			_case = 1;
			nn = 2;
		}
		if (_for == null)
			BaseException.raise(1, null);
		if (E && !string_12_.equals(string_16_)) {
			String string_25_ = null;
			String string_26_ = null;
			String string_27_ = System.getProperty("user.name");
			if (_int(2))
				System.out.println("OS user name: ".concat(string_27_));
			int i_28_ = string_12_.indexOf("@");
			if (i_28_ > 0 && i_28_ < string_12_.length()) {
				string_25_ = string_12_.substring(i_28_ + 1);
				string_26_ = string_12_.substring(0, i_28_);
			} else if (_case == 1 && nn == 1)
				string_26_ = string_12_;
			else
				string_25_ = string_12_;
			if (string_26_ != null && !string_27_.equals(string_26_))
				BaseException.raise(4, null);
			if (string_25_ != null) {
				Object object_29_ = null;
				String string_30_;
				try {
					string_30_ = InetAddress.getLocalHost().getHostName()
							.trim();
					if (_int(2))
						System.out
								.println("Host name according to name server: ["
										.concat(string_30_).concat("]"));
				} catch (Throwable throwable) {
					if (_int(2))
						System.out
								.println("*** Exception during getLocalHost/getHostName.  Message:\n"
										.concat(throwable.getMessage()));
					string_30_ = "";
				}
				if (!string_30_.equals(string_25_))
					BaseException.raise(4, null);
			}
		}
		if (_int(1))
			System.out.println("License key check ok.  Warning bit map: "
					.concat(String.valueOf(L)));
		return L;
	}

	public String a(String string, String string_31_, BaseException var_e, HolderBase var_o,
			Object object, int i, long l, long l_32_, int i_33_, long l_34_,
			String string_35_, long l_36_) throws BaseException {
		long l_37_ = a(string_31_, string_35_);
		int i_38_ = a(string, var_o, object, i, l, l_32_, i_33_, l_34_,
				string_35_, l_36_);
		String string_39_ = a(l_37_, string_35_);
		if (var_e == null)
			return string_39_;
		var_e.a(string_39_);
		var_e.a(i_38_);
		throw var_e;
	}

	private void a(String string, String string_40_, long l, long l_41_)
			throws BaseException {
		if (z != null)
			BaseException.raise(1, null);
		z = a(string_40_, 1);
		if (z == null)
			BaseException.raise(1, null);
		Date date = new Date();
		long l_42_ = date.getTime();
		long l_43_ = new File(System.getProperty("user.home")).lastModified();
		long l_44_ = new File(System.getProperty("user.dir")).lastModified();
		if (l_42_ < l_43_ || l_42_ < l_44_)
			BaseException.raise(2, "Expired on ".concat(new SimpleDateFormat("yyyy-MM-dd")
					.format(z)));
		long l_45_ = z.getTime() - l_42_;
		long l_46_ = l_45_ / (long) 86400000;
		if (date.after(z)) {
			if (-l_46_ <= l_41_)
				L |= 0x4;
			else
				BaseException.raise(2, "Expired on ".concat(new SimpleDateFormat("yyyy-MM-dd")
						.format(z)));
		} else if (l_46_ < l)
			L |= 0x1;
	}

	protected Date a(String string, int i) {
		Object object = null;
		string = string.trim();
		Date date;
		try {
			SimpleDateFormat simpledateformat = new SimpleDateFormat(
					"yyyy-MM-dd");
			date = simpledateformat.parse(string);
			if (i > 0)
				date = new Date(date.getTime() + (long) (i * 24 * 3600 * 1000));
			if (string.length() != 10 || string.indexOf("-") != 4
					|| string.lastIndexOf("-") != 7) {
				Date date_47_ = null;
				return date_47_;
			}
		} catch (Exception exception) {
			date = null;
		}
		return date;
	}

	protected long a(String string, String string_48_, long l, long l_49_,
			long l_50_) throws BaseException {
		if (l != (long) -1)
			BaseException.raise(1, null);
		try {
			l = Long.parseLong(string_48_);
		} catch (Exception exception) {
			BaseException.raise(1, null);
		}
		if (l < l_49_ && l_49_ - l > l_50_)
			BaseException.raise(3, string.concat(" limit: ").concat(String.valueOf(l)).concat(
					", Current: ").concat(String.valueOf(l_49_)));
		return l;
	}

	protected int _for(String string) {
		boolean bool = false;
		for (int i = 1; i <= 20; i++) {
			if (y[i].equals(string))
				return i;
		}
		return 0;
	}

	protected static String a(long l, String string) {
		if (l == (long) 0)
			l = new Date().getTime();
		long l_51_ = new Date().getTime();
		String string_52_ = String.valueOf(l_51_);
		String string_53_ = string_52_.concat(",").concat(
				n._if("dyAuYCM/bV==", String.valueOf(l), string_52_));
		return n._if("dyAuYCM/bV==", string_53_, string);
	}

	protected long a(String string, String string_54_) throws BaseException {
		long l = 0L;
		long l_55_ = 0L;
		long l_56_ = new Date().getTime();
		String string_57_ = n.a("dyAuYCM/bV==", string, string_54_);
		if (_int(1))
			System.out.println("checkTsString: decoded tsString: "
					.concat(string));
		try {
			int i = string_57_.indexOf(",");
			if (i < 0)
				BaseException.raise(1, null);
			String string_58_ = string_57_.substring(0, i);
			l_55_ = Long.parseLong(string_58_);
			String string_59_ = n.a("dyAuYCM/bV==", string_57_
					.substring(string_57_.indexOf(",")), string_58_);
			if (_int(1))
				System.out.println("checkTsString: decoded oldtsStr: "
						.concat(string_59_));
			l = Long.parseLong(string_59_);
			long l_60_ = new File(System.getProperty("user.home"))
					.lastModified();
			long l_61_ = new File(System.getProperty("user.dir"))
					.lastModified();
			if (l == (long) 0 || l_55_ == (long) 0 || l_55_ < l
					|| l_55_ > l_56_ || l_56_ < l_60_ || l_56_ < l_61_)
				BaseException.raise(1, null);
		} catch (BaseException var_e) {
			throw var_e;
		} catch (Throwable throwable) {
			if (_int(1)) {
				System.out
						.println("checkTsString: caught throwable exception.");
				System.out.println("Message: ".concat(throwable.getMessage()));
				throwable.printStackTrace();
			}
			BaseException.raise(1, null);
		}
		return l_55_;
	}

	protected static int _new(String string) {
		int i = 0;
		for (int i_62_ = 0; i_62_ < string.length(); i_62_++)
			i += string.charAt(i_62_);
		return i;
	}

	protected static boolean _int(int i) {
		if (o == -100) {
			String string = n.a("dyAuYCM/bV==", "HD/aFCHUDCLe", "hoowah");
			String string_63_ = "";
			try {
				InputStream inputstream = Runtime.getRuntime().exec("env")
						.getInputStream();
				BufferedReader bufferedreader = new BufferedReader(
						new InputStreamReader(inputstream));
				while ((string_63_ = bufferedreader.readLine()) != null
						&& !string_63_.startsWith(string)) {
					/* empty */
				}
				if (string_63_ != null && !string_63_.equals(""))
					o = Integer.parseInt(string_63_.substring(
							string_63_.indexOf("=") + 1).trim());
				else
					o = -1;
			} catch (Exception exception) {
				o = -1;
			}
		}
		return o >= i;
	}
}
