/* n - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.license;

public class n {
	private static String _do = "krptptt";

	private static String a = "cqktrzy";

	private static char[] _if = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
			.toCharArray();

	private static byte[] _for = new byte[256];

	private static char[] a(char[] cs, int i) {
		char[] cs_0_ = new char[(cs.length + 2) / 3 * 4];
		int i_1_ = 0;
		int i_2_ = 0;
		while (i_1_ < cs.length) {
			boolean bool = false;
			boolean bool_3_ = false;
			int i_4_ = 0xff & cs[i_1_];
			i_4_ <<= 8;
			if (i_1_ + 1 < cs.length) {
				i_4_ |= 0xff & cs[i_1_ + 1];
				bool_3_ = true;
			}
			i_4_ <<= 8;
			if (i_1_ + 2 < cs.length) {
				i_4_ |= 0xff & cs[i_1_ + 2];
				bool = true;
			}
			int i_5_ = i_4_ & 0x3f;
			cs_0_[i_2_ + 3] = _if[bool ? i_5_ ^ i : 64];
			i_4_ >>= 6;
			i_5_ = i_4_ & 0x3f;
			cs_0_[i_2_ + 2] = _if[bool_3_ ? i_5_ ^ i : 64];
			i_4_ >>= 6;
			i_5_ = i_4_ & 0x3f;
			cs_0_[i_2_ + 1] = _if[i_5_ ^ i];
			i_4_ >>= 6;
			i_5_ = i_4_ & 0x3f;
			cs_0_[i_2_ + 0] = _if[i_5_ ^ i];
			i_1_ += 3;
			i_2_ += 4;
		}
		return cs_0_;
	}

	private static String _if(String string, String string_6_) {
		char[] cs = string.toCharArray();
		int i = string_6_ == null ? 0 : string_6_.hashCode() & 0x1f;
		char[] cs_7_ = a(cs, i);
		return String.valueOf(cs_7_);
	}

	public static String _if(String string, String string_8_, String string_9_) {
		String string_10_ = a(string, _do);
		if (!string_10_.equals(a))
			return null;
		return _if(string_8_, string_9_);
	}

	private static char[] _if(char[] cs, int i) {
		int i_11_ = cs.length;
		for (int i_12_ = 0; i_12_ < cs.length; i_12_++) {
			if (cs[i_12_] > '\u00ff' || _for[cs[i_12_]] < 0)
				i_11_--;
		}
		int i_13_ = i_11_ / 4 * 3;
		if (i_11_ % 4 == 3)
			i_13_ += 2;
		if (i_11_ % 4 == 2)
			i_13_++;
		char[] cs_14_ = new char[i_13_];
		int i_15_ = 0;
		int i_16_ = 0;
		int i_17_ = 0;
		for (int i_18_ = 0; i_18_ < cs.length; i_18_++) {
			char c = cs[i_18_];
			int i_19_ = c > '\u00ff' ? (int) -1 : _for[c];
			if (i > 0 && i_19_ >= 0)
				i_19_ ^= i;
			if (i_19_ >= 0) {
				i_16_ <<= 6;
				i_15_ += 6;
				i_16_ |= i_19_;
				if (i_15_ >= 8) {
					i_15_ -= 8;
					cs_14_[i_17_++] = (char) (i_16_ >> i_15_ & 0xff);
				}
			}
		}
		if (i_17_ != cs_14_.length)
			throw new Error("Miscalculated data length (wrote ".concat(
					String.valueOf(i_17_)).concat(" instead of ").concat(
					String.valueOf(cs_14_.length)).concat(")"));
		return cs_14_;
	}

	private static String a(String string, String string_20_) {
		char[] cs = string.toCharArray();
		int i = string_20_ == null ? 0 : string_20_.hashCode() & 0x1f;
		char[] cs_21_ = _if(cs, i);
		return String.valueOf(cs_21_);
	}

	public static String a(String string, String string_22_, String string_23_) {
		String string_24_ = a(string, _do);
		if (!string_24_.equals(a))
			return null;
		return a(string_22_, string_23_);
	}

	static {
		for (int i = 0; i < 256; i++)
			_for[i] = (byte) -1;
		for (int i = 65; i <= 90; i++)
			_for[i] = (byte) (i - 65);
		for (int i = 97; i <= 122; i++)
			_for[i] = (byte) (26 + i - 97);
		for (int i = 48; i <= 57; i++)
			_for[i] = (byte) (52 + i - 48);
		_for[43] = (byte) 62;
		_for[47] = (byte) 63;
	}
}
