package isf.tools.barcode.ocr;

/* BarDemoPP - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

import isf.tools.barcode.ocr.core.Recognized;
import isf.tools.barcode.ocr.core.Recognizer;
import isf.tools.barcode.ocr.core.Settings;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.Date;
import java.util.Properties;

public class Main extends Frame {
	private Recognizer recognizer;

	private Settings settings;

	private MainCanvas theCanvas;

	private TxtArea txtArea;

	private static String owner = "tasman.bars";

	private static final String version = "v2.69";

	private Dimension dim = new Dimension(240, 320);

	class FNameFilter implements FilenameFilter {
		public boolean accept(File file, String string) {
			String string_0_ = string.toLowerCase();
			if (string_0_.equals("tasman.gif")
					|| string_0_.equals("tasmanbk.gif"))
				return false;
			if (string_0_.endsWith(".jpg") || string_0_.endsWith(".jpeg")
					|| string_0_.endsWith(".gif")
					|| string_0_.endsWith(".giff")
					|| string_0_.endsWith(".png"))
				return true;
			return false;
		}
	}

	class FileChooser implements ActionListener, Runnable {
		private Frame padre;

		private Image imagen;

		private FileDialog fileDialog;

		FileChooser(Frame frame) {
			padre = frame;
			imagen = null;
			fileDialog = new FileDialog(frame);
			fileDialog.setDirectory(System.getProperties().getProperty(
					"user.dir"));
			fileDialog.setFilenameFilter(new FNameFilter());
		}

		public void actionPerformed(ActionEvent actionevent) {
			fileDialog.setVisible(true);
			String fname = fileDialog.getFile();
			String dir = fileDialog.getDirectory();
			if (fname != null) {
				padre.setCursor(Cursor.getPredefinedCursor(3));
				imagen = Main.this.getToolkit().getImage(dir + fname);
				Thread thread = new Thread(this);
				thread.start();
			}
		}

		public void run() {
			MediaTracker mediatracker = new MediaTracker(theCanvas);
			mediatracker.addImage(imagen, 0);
			try {
				mediatracker.waitForID(0);
			} catch (InterruptedException interruptedexception) {
				return;
			}
			theCanvas.setImage(imagen);
			theCanvas.setImage((Image) null);
			theCanvas.repaint();
			recognizer = new Recognizer(imagen);
			padre.setCursor(Cursor.getDefaultCursor());
		}
	}

	class Execute implements ActionListener, Runnable {
		private Container cont;

		Execute(Container container) {
			cont = container;
		}

		public void actionPerformed(ActionEvent actionevent) {
			Thread thread = new Thread(this);
			thread.start();
		}

		public void run() {
			Settings oneDimSettings = null;
			Settings twoDimSettings = null;
			if (settings.code11 || settings.code128 || settings.code32
					|| settings.code39 || settings.code93 || settings.codabar
					|| settings.EAN13 || settings.EAN8
					|| settings.interleaved2of5 || settings.patch
					|| settings.pdf417 || settings.rss14 || settings.rssltd
					|| settings.telepen || settings.upcA || settings.upcE) {
				oneDimSettings = new Settings(settings);
				oneDimSettings.dataMatrix = false;
				oneDimSettings.QRCode = false;
			}
			if (settings.dataMatrix || settings.QRCode) {
				twoDimSettings = new Settings(settings);
				twoDimSettings.code11 = twoDimSettings.code128 = twoDimSettings.code32 = 
					twoDimSettings.code39 = twoDimSettings.code93 = 
					twoDimSettings.codabar = twoDimSettings.EAN13 = twoDimSettings.EAN8 = 
						twoDimSettings.interleaved2of5 = twoDimSettings.patch = 
						twoDimSettings.pdf417 = settings.rss14 = settings.rssltd = 
							twoDimSettings.telepen = twoDimSettings.upcA = twoDimSettings.upcE = false;
			}
			if (oneDimSettings == null && twoDimSettings == null)
				txtArea.append("No symbology specified!");
			else {
				Date inicio = new Date();
				cont.setCursor(Cursor.getPredefinedCursor(3));
				Recognized[] res = null;
				if (oneDimSettings != null) {
					try {
						res = recognizer.recognize(oneDimSettings);
					} catch (IllegalArgumentException ie) {
						txtArea.append(ie.getMessage());
						cont.setCursor(Cursor.getDefaultCursor());
						return;
					}
					String what = twoDimSettings == null ? "" : "1-D/stacked ";
					theCanvas.setRecognized(res);
					theCanvas.repaint();
					Date finalizacion = new Date();
					long intervalo = finalizacion.getTime() - inicio.getTime();
					inicio = finalizacion;
					txtArea.append("Done " + what + intervalo + "ms");
					for (int i = 0; i < res.length; i++) {
						txtArea.append(res[i].getEncoding());
						if (res[i].isSupplementalAvailable())
							txtArea.append("SUPPLEMENTAL: "
									+ res[i].getSupplemental().getEncoding());
					}
				}
				if (twoDimSettings != null) {
					Recognized[] res2D;
					try {
						res2D = recognizer.recognize(twoDimSettings);
					} catch (IllegalArgumentException ie) {
						txtArea.append(ie.getMessage());
						cont.setCursor(Cursor.getDefaultCursor());
						return;
					}
					String string = oneDimSettings == null ? "" : "2-D ";
					Recognized[] all;
					if (res != null) {
						all = new Recognized[res.length + res2D.length];
						for (int i = 0; i < res.length; i++)
							all[i] = res[i];
						for (int i = 0; i < res2D.length; i++)
							all[res.length + i] = res2D[i];
					} else
						all = res2D;
					theCanvas.setRecognized(all);
					theCanvas.repaint();
					Date finalizacion = new Date();
					long intervalo = finalizacion.getTime() - inicio.getTime();
					txtArea.append("Done " + string + intervalo + "ms");
					for (int i = 0; i < res2D.length; i++)
						txtArea.append(res2D[i].getEncoding());
				}
				cont.setCursor(Cursor.getDefaultCursor());
			}
		}
	}

	public static void main(String[] strings) {
		Main m = new Main();
		m.pack();
		m.setVisible(true);
		Image image = m.getTASBAR();
		m.theCanvas.setImage(image);
		m.recognizer = new Recognizer(image);
	}

	private Main() {
		super(owner);
		URL url = this.getClass().getResource("barsicon.gif");
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		setIconImage(toolkit.createImage(url));
		settings = new Settings();
		settings.code128 = true;
		settings.prop = new Properties();
		FileChooser filechooser = this.new FileChooser(this);
		Execute exec = this.new Execute(this);
		ActionListener alInit = new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				txtArea.setText("");
				theCanvas.setImage((Image) null);
				theCanvas.repaint();
			}
		};
		jbInit(filechooser, exec, alInit);
		theCanvas = new MainCanvas();
		Scroller scroll = new Scroller(theCanvas);
		setLayout(new BorderLayout());
		add(scroll, "Center");
		Panel panel = new Panel();
		panel.setLayout(new GridLayout(1, 3));
		Button button = new Button("O");
		button.addActionListener(filechooser);
		panel.add(button);
		button = new Button("R");
		button.addActionListener(exec);
		panel.add(button);
		button = new Button("C");
		button.addActionListener(alInit);
		panel.add(button);
		Panel p1 = new Panel();
		p1.setLayout(new BorderLayout());
		p1.add(panel, "West");
		txtArea = new TxtArea("www.tasman.co.uk\nbars@tasman.co.uk", 2, 50, 1);
		p1.add(txtArea, "Center");
		add(p1, "South");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				Main.this.dispose();
			}
		});
		addWindowListener(new WindowAdapter() {
			public void windowClosed(WindowEvent windowevent) {
				System.exit(0);
			}
		});
	}

	public Dimension getPreferredSize() {
		return dim;
	}

	private Image getTASBAR() {
		URL url = this.getClass().getResource("tasbar.gif");
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Image image = toolkit.createImage(url);
		try {
			MediaTracker mediatracker = new MediaTracker(this);
			mediatracker.addImage(image, 0);
			mediatracker.waitForID(0);
		} catch (Exception exception) {
			/* empty */
		}
		return image;
	}

	private void jbInit(ActionListener actionlistener,
			ActionListener actionlistener_14_, ActionListener actionlistener_15_) {
		MenuItem mnuOpen = new MenuItem("Open");
		MenuItem mnuRead = new MenuItem("Read");
		mnuOpen.addActionListener(actionlistener);
		mnuRead.addActionListener(actionlistener_14_);
		MenuBar menubar = new MenuBar();
		Menu mnuFile = new Menu("File");
		mnuFile.add(mnuOpen);
		mnuFile.add(mnuRead);
		MenuItem mitClear = new MenuItem("Clear");
		mnuFile.add(mitClear);
		mitClear.addActionListener(actionlistener_15_);
		mitClear = new MenuItem("-");
		mnuFile.add(mitClear);
		mitClear = new MenuItem("Exit");
		mnuFile.add(mitClear);
		mitClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				Main.this.dispose();
			}
		});
		menubar.add(mnuFile);
		mnuFile = new Menu("Opts");
		mitClear = new MenuItem("Barcodes...");
		mnuFile.add(mitClear);
		final DialogBarcodes barcodesDialog = new DialogBarcodes(this, settings);
		mitClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				barcodesDialog.setVisible(true);
			}
		});
		mitClear = new MenuItem("Directions...");
		mnuFile.add(mitClear);
		final DialogDirections directionsDialog = new DialogDirections(this,
				settings);
		mitClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				directionsDialog.setVisible(true);
			}
		});
		mitClear = new MenuItem("Settings...");
		mnuFile.add(mitClear);
		final DialogSettings settingsDialog = new DialogSettings(this, settings);
		mitClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				settingsDialog.setVisible(true);
			}
		});
		menubar.add(mnuFile);
		setMenuBar(menubar);
	}
}
