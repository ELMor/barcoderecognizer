/* l - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Rectangle;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;

class QRBarCode {
	private static final boolean _else = false;

	private static final int[] _new = { 31892, 34236, 39577, 42195, 48118,
			51042, 55367, 58893, 63784, 68472, 70749, 76311, 79154, 84390,
			87683, 92361, 96236, 102084, 102881, 110507, 110734, 117786,
			119615, 126325, 127568, 133589, 136944, 141498, 145311, 150283,
			152622, 158308, 161089, 167017 };

	private static final int[] _byte = { 0, 1335, 2670, 3929, 4587, 5340, 7045,
			7858, 9174, 9953, 10680, 11407, 12861, 14090, 14419, 15716, 17051,
			18348, 18677, 19906, 21360, 22087, 22814, 23593, 24909, 25722,
			27427, 28180, 28838, 30097, 31432, 32767 };

	private static final int[] a = { -1, 26, 44, 70, 100, 134, 172, 196, 242,
			292, 346, 404, 466, 532, 581, 655, 733, 815, 901, 991, 1085, 1156,
			1258, 1364, 1474, 1588, 1706, 1828, 1921, 2051, 2185, 2323, 2465,
			2611, 2761, 2876, 3034, 3196, 3362, 3532, 3706 };

	private static final int[][][][] _do = {
			{ { { -1 } }, { { -1 } }, { { -1 } }, { { -1 } } },
			{ { { 1, 26, 19 } }, { { 1, 26, 16 } }, { { 1, 26, 13 } },
					{ { 1, 26, 9 } } },
			{ { { 1, 44, 34 } }, { { 1, 44, 28 } }, { { 1, 44, 22 } },
					{ { 1, 44, 16 } } },
			{ { { 1, 70, 55 } }, { { 1, 70, 44 } }, { { 2, 35, 17 } },
					{ { 2, 35, 13 } } },
			{ { { 1, 100, 80 } }, { { 2, 50, 32 } }, { { 2, 50, 24 } },
					{ { 4, 25, 9 } } },
			{ { { 1, 134, 108 } }, { { 2, 67, 43 } },
					{ { 2, 33, 15 }, { 2, 34, 16 } },
					{ { 2, 33, 11 }, { 2, 34, 12 } } },
			{ { { 2, 86, 68 } }, { { 4, 43, 27 } }, { { 4, 43, 19 } },
					{ { 4, 43, 15 } } },
			{ { { 2, 98, 78 } }, { { 4, 49, 31 } },
					{ { 2, 32, 14 }, { 4, 33, 15 } },
					{ { 4, 39, 13 }, { 1, 40, 14 } } },
			{ { { 2, 121, 97 } }, { { 2, 60, 38 }, { 2, 61, 39 } },
					{ { 4, 40, 18 }, { 2, 41, 19 } },
					{ { 4, 40, 14 }, { 2, 41, 15 } } },
			{ { { 2, 146, 116 } }, { { 3, 58, 36 }, { 2, 59, 37 } },
					{ { 4, 36, 16 }, { 4, 37, 17 } },
					{ { 4, 36, 12 }, { 4, 37, 13 } } },
			{ { { 2, 86, 68 }, { 2, 87, 69 } },
					{ { 4, 69, 43 }, { 1, 70, 44 } },
					{ { 6, 43, 19 }, { 2, 44, 20 } },
					{ { 6, 43, 15 }, { 2, 44, 16 } } },
			{ { { 4, 101, 81 } }, { { 1, 80, 50 }, { 4, 81, 51 } },
					{ { 4, 50, 22 }, { 4, 51, 23 } },
					{ { 3, 36, 12 }, { 8, 37, 13 } } },
			{ { { 2, 116, 92 }, { 2, 117, 93 } },
					{ { 6, 58, 36 }, { 2, 59, 37 } },
					{ { 4, 46, 20 }, { 6, 47, 21 } },
					{ { 7, 42, 14 }, { 4, 43, 15 } } },
			{ { { 4, 133, 107 } }, { { 8, 59, 37 }, { 1, 60, 38 } },
					{ { 8, 44, 20 }, { 4, 45, 21 } },
					{ { 12, 33, 11 }, { 4, 34, 12 } } },
			{ { { 3, 145, 115 }, { 1, 146, 116 } },
					{ { 4, 64, 40 }, { 5, 65, 41 } },
					{ { 11, 36, 16 }, { 5, 37, 17 } },
					{ { 11, 36, 12 }, { 5, 37, 13 } } },
			{ { { 5, 109, 87 }, { 1, 110, 88 } },
					{ { 5, 65, 41 }, { 5, 66, 42 } },
					{ { 5, 54, 24 }, { 7, 55, 25 } },
					{ { 11, 36, 12 }, { 7, 37, 13 } } },
			{ { { 5, 122, 98 }, { 1, 123, 99 } },
					{ { 7, 73, 45 }, { 3, 74, 46 } },
					{ { 15, 43, 19 }, { 2, 44, 20 } },
					{ { 3, 45, 15 }, { 13, 46, 16 } } },
			{ { { 1, 135, 107 }, { 5, 136, 108 } },
					{ { 10, 74, 46 }, { 1, 75, 47 } },
					{ { 1, 50, 22 }, { 15, 51, 23 } },
					{ { 2, 42, 14 }, { 17, 43, 15 } } },
			{ { { 5, 150, 120 }, { 1, 151, 121 } },
					{ { 9, 69, 43 }, { 4, 70, 44 } },
					{ { 17, 50, 22 }, { 1, 51, 23 } },
					{ { 2, 42, 14 }, { 19, 43, 15 } } },
			{ { { 3, 141, 113 }, { 4, 142, 114 } },
					{ { 3, 70, 44 }, { 11, 71, 45 } },
					{ { 17, 47, 21 }, { 4, 48, 22 } },
					{ { 9, 39, 13 }, { 16, 40, 14 } } },
			{ { { 3, 135, 107 }, { 5, 136, 108 } },
					{ { 3, 67, 41 }, { 13, 68, 42 } },
					{ { 15, 54, 24 }, { 5, 55, 25 } },
					{ { 15, 43, 15 }, { 10, 44, 16 } } },
			{ { { 4, 144, 116 }, { 4, 145, 117 } }, { { 17, 68, 42 } },
					{ { 17, 50, 22 }, { 6, 51, 23 } },
					{ { 19, 46, 16 }, { 6, 47, 17 } } },
			{ { { 2, 139, 111 }, { 7, 140, 112 } }, { { 17, 74, 46 } },
					{ { 7, 54, 24 }, { 16, 55, 25 } }, { { 34, 37, 13 } } },
			{ { { 4, 151, 121 }, { 5, 152, 122 } },
					{ { 4, 75, 47 }, { 14, 76, 48 } },
					{ { 11, 54, 24 }, { 14, 55, 25 } },
					{ { 16, 45, 15 }, { 14, 46, 16 } } },
			{ { { 6, 147, 117 }, { 4, 148, 118 } },
					{ { 6, 73, 45 }, { 14, 74, 46 } },
					{ { 11, 54, 24 }, { 16, 55, 25 } },
					{ { 30, 46, 16 }, { 2, 47, 17 } } },
			{ { { 8, 132, 106 }, { 4, 133, 107 } },
					{ { 8, 75, 47 }, { 13, 76, 48 } },
					{ { 7, 54, 24 }, { 22, 55, 25 } },
					{ { 22, 45, 15 }, { 13, 46, 16 } } },
			{ { { 10, 142, 114 }, { 2, 143, 115 } },
					{ { 19, 74, 46 }, { 4, 75, 47 } },
					{ { 28, 50, 22 }, { 6, 51, 23 } },
					{ { 33, 46, 16 }, { 4, 47, 17 } } },
			{ { { 8, 152, 122 }, { 4, 153, 123 } },
					{ { 22, 73, 45 }, { 3, 74, 46 } },
					{ { 8, 53, 23 }, { 26, 54, 24 } },
					{ { 12, 45, 15 }, { 28, 46, 16 } } },
			{ { { 3, 147, 117 }, { 10, 148, 118 } },
					{ { 3, 73, 45 }, { 23, 74, 46 } },
					{ { 4, 54, 24 }, { 31, 55, 25 } },
					{ { 11, 45, 15 }, { 31, 46, 16 } } },
			{ { { 7, 146, 116 }, { 7, 147, 117 } },
					{ { 21, 73, 45 }, { 7, 74, 46 } },
					{ { 1, 53, 23 }, { 37, 54, 24 } },
					{ { 19, 45, 15 }, { 26, 46, 16 } } },
			{ { { 5, 145, 115 }, { 10, 146, 116 } },
					{ { 19, 75, 47 }, { 10, 76, 48 } },
					{ { 15, 54, 24 }, { 25, 55, 25 } },
					{ { 23, 45, 15 }, { 25, 46, 16 } } },
			{ { { 13, 145, 115 }, { 3, 146, 116 } },
					{ { 2, 74, 46 }, { 29, 75, 47 } },
					{ { 42, 54, 24 }, { 1, 55, 25 } },
					{ { 23, 45, 15 }, { 28, 46, 16 } } },
			{ { { 17, 145, 115 } }, { { 10, 74, 46 }, { 23, 75, 47 } },
					{ { 10, 54, 24 }, { 35, 55, 25 } },
					{ { 19, 45, 15 }, { 35, 46, 16 } } },
			{ { { 17, 145, 115 }, { 1, 146, 116 } },
					{ { 14, 74, 46 }, { 21, 75, 47 } },
					{ { 29, 54, 24 }, { 19, 55, 25 } },
					{ { 11, 45, 15 }, { 46, 46, 16 } } },
			{ { { 13, 145, 115 }, { 6, 146, 116 } },
					{ { 14, 74, 46 }, { 23, 75, 47 } },
					{ { 44, 54, 24 }, { 7, 55, 25 } },
					{ { 59, 46, 16 }, { 1, 47, 17 } } },
			{ { { 12, 151, 121 }, { 7, 152, 122 } },
					{ { 12, 75, 47 }, { 26, 76, 48 } },
					{ { 39, 54, 24 }, { 14, 55, 25 } },
					{ { 22, 45, 15 }, { 41, 46, 16 } } },
			{ { { 6, 151, 121 }, { 14, 152, 122 } },
					{ { 6, 75, 47 }, { 34, 76, 48 } },
					{ { 46, 54, 24 }, { 10, 55, 25 } },
					{ { 2, 45, 15 }, { 64, 46, 16 } } },
			{ { { 17, 152, 122 }, { 4, 153, 123 } },
					{ { 29, 74, 46 }, { 14, 75, 47 } },
					{ { 49, 54, 24 }, { 10, 55, 25 } },
					{ { 24, 45, 15 }, { 46, 46, 16 } } },
			{ { { 4, 152, 122 }, { 18, 153, 123 } },
					{ { 13, 74, 46 }, { 32, 75, 47 } },
					{ { 48, 54, 24 }, { 14, 55, 25 } },
					{ { 42, 45, 15 }, { 32, 46, 16 } } },
			{ { { 20, 147, 117 }, { 4, 148, 118 } },
					{ { 40, 75, 47 }, { 7, 76, 48 } },
					{ { 43, 54, 24 }, { 22, 55, 25 } },
					{ { 10, 45, 15 }, { 67, 46, 16 } } },
			{ { { 19, 148, 118 }, { 6, 149, 119 } },
					{ { 18, 75, 47 }, { 31, 76, 48 } },
					{ { 34, 54, 24 }, { 34, 55, 25 } },
					{ { 20, 45, 15 }, { 61, 46, 16 } } } };

	private static final char[] _long = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
			'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
			'X', 'Y', 'Z', ' ', '$', '%', '*', '+', '-', '.', '/', ':' };

	private Recognized _try;

	private Recognizer _if;

	private int _goto;

	private QRFinderPattern _char;

	private QRFinderPattern _case;

	private QRFinderPattern _for;

	private PropertiesHolder _int;

	private ar _void;

	class b {
		String a;

		int[] _if;

		b(String string, int[] is) {
			a = string;
			_if = is;
		}
	}

	private class a {
		private int _for;

		private int _if;

		private int _do;

		private int a;

		private a(int i, int i_0_, int i_1_, int i_2_) {
			switch (i) {
			case 0:
				_for = 1;
				break;
			case 1:
				_for = 0;
				break;
			case 2:
				_for = 3;
				break;
			case 3:
				_for = 2;
				break;
			}
			_if = i_0_;
			_do = i_1_;
			a = i_2_;
		}

		public boolean a(a var_a_3_) {
			return (_for == var_a_3_._for && _if == var_a_3_._if && _do == var_a_3_._do);
		}
	}

	QRBarCode(Recognizer var_a1, int i, QRFinderPattern var_m, QRFinderPattern var_m_7_, QRFinderPattern var_m_8_, PropertiesHolder var_c) throws BarcodeException {
		_if = var_a1;
		_goto = i;
		_char = var_m;
		_case = var_m_7_;
		_for = var_m_8_;
		_int = var_c;
		int[] is = a();
		for (int i_9_ = 0; i_9_ < is.length; i_9_++) {
			int i_10_ = is[i_9_];
			a[] var_as = a(i_10_);
			if (var_as != null) {
				aw var_aw = new aw(i_10_, var_a1, i, var_m, var_m_7_, var_m_8_,
						var_c);
				a4 var_a4 = new a4(i_10_);
				int i_11_ = var_as[0]._if;
				int i_12_ = var_as.length > 1 ? var_as[1]._if : -1;
				int[][] is_13_ = a(i_10_, var_aw, var_a4, i_11_, i_12_, null);
				for (int i_14_ = 0; i_14_ < is_13_.length; i_14_++) {
					if (_void == null)
						_void = new ar(17);
					int i_15_ = var_as[i_14_]._for;
					int[][] is_16_ = _do[i_10_][i_15_];
					int i_17_ = is_16_[0][0];
					int i_18_ = is_16_.length == 2 ? is_16_[1][0] : 0;
					int i_19_ = i_17_ + i_18_;
					int[][] is_20_ = new int[i_19_][];
					for (int i_21_ = 0; i_21_ < i_17_; i_21_++)
						is_20_[i_21_] = new int[is_16_[0][1]];
					for (int i_22_ = 0; i_22_ < i_18_; i_22_++)
						is_20_[i_17_ + i_22_] = new int[is_16_[1][1]];
					for (int i_23_ = 0; i_23_ < i_19_; i_23_++) {
						for (int i_24_ = 0; i_24_ < is_16_[0][2]; i_24_++)
							is_20_[i_23_][i_24_] = is_13_[i_14_][i_24_ * i_19_
									+ i_23_];
					}
					for (int i_25_ = i_17_; i_25_ < i_19_; i_25_++)
						is_20_[i_25_][is_16_[1][2] - 1] = (is_13_[i_14_][is_16_[0][2]
								* i_19_ + i_25_ - i_17_]);
					int i_26_ = i_17_ * is_16_[0][2];
					if (i_18_ > 0)
						i_26_ += i_18_ * is_16_[1][2];
					int[] is_27_ = new int[i_26_];
					int i_28_ = 0;
					int i_29_ = -1;
					for (int i_30_ = 0; i_30_ < i_19_; i_30_++) {
						int i_31_ = is_16_[0][2];
						if (i_30_ >= i_17_)
							i_31_++;
						for (int i_32_ = 0; i_32_ < is_16_[0][1] - is_16_[0][2]; i_32_++)
							is_20_[i_30_][i_31_ + i_32_] = is_13_[i_14_][i_26_
									+ i_32_ * i_19_ + i_30_];
						i_29_ = _void.a(is_20_[i_30_], i_31_,
								is_20_[i_30_].length - i_31_);
						if (i_29_ < 0)
							break;
						System
								.arraycopy(is_20_[i_30_], 0, is_27_, i_28_,
										i_31_);
						i_28_ += i_31_;
					}
					if (i_29_ >= 0) {
						b var_b = a(is_27_, i_10_);
						if (var_b != null) {
							String string = var_b.a;
							Poligono var_i = a(i_10_, var_aw);
							_try = new Recognized(string, var_i, 0, 17, i, var_b._if,
									var_c);
							return;
						}
					}
				}
				if (i_10_ == 1) {
					af var_af = new af();
					for (int i_33_ = 1; i_33_ <= 4; i_33_++) {
						for (int i_34_ = -i_33_; i_34_ <= i_33_; i_34_++) {
							for (int i_35_ = -i_33_; i_35_ <= i_33_; i_35_++) {
								if (Math.abs(i_34_) == i_33_
										|| Math.abs(i_35_) == i_33_) {
									var_af.a = i_34_;
									var_af._if = i_35_;
									is_13_ = a(i_10_, var_aw, var_a4, i_11_,
											i_12_, var_af);
									for (int i_36_ = 0; i_36_ < is_13_.length; i_36_++) {
										if (_void == null)
											_void = new ar(17);
										int i_37_ = var_as[i_36_]._for;
										int i_38_ = _do[1][i_37_][0][2];
										int i_39_ = _void.a(is_13_[i_36_],
												i_38_, 26 - i_38_);
										if (i_39_ >= 0) {
											int[] is_40_ = new int[i_38_];
											System.arraycopy(is_13_[i_36_], 0,
													is_40_, 0, i_38_);
											b var_b = a(is_40_, i_10_);
											if (var_b != null) {
												String string = var_b.a;
												Poligono var_i = a(i_10_, var_aw);
												_try = new Recognized(string, var_i, 0,
														17, i, var_b._if, var_c);
												return;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		throw new BarcodeException("Failed to read possible QR.");
	}

	Recognized _if() {
		return _try;
	}

	private Poligono a(int i, aw var_aw) {
		int[] is = new int[4];
		int[] is_41_ = new int[4];
		int i_42_ = 17 + i * 4 - 1;
		af var_af = new af(0, 0);
		af var_af_43_ = var_aw._if(var_af, null, null);
		is[0] = var_af_43_.a;
		is_41_[0] = var_af_43_._if;
		var_af.a = 1;
		var_af._if = 1;
		var_af_43_ = var_aw._if(var_af, null, null);
		is[0] += (is[0] - var_af_43_.a) / 2;
		is_41_[0] += (is_41_[0] - var_af_43_._if) / 2;
		var_af.a = i_42_;
		var_af._if = 0;
		var_aw._if(var_af, var_af_43_, null);
		is[1] = var_af_43_.a;
		is_41_[1] = var_af_43_._if;
		var_af.a = i_42_ - 1;
		var_af._if = 1;
		var_af_43_ = var_aw._if(var_af, null, null);
		is[1] += (is[1] - var_af_43_.a) / 2;
		is_41_[1] += (is_41_[1] - var_af_43_._if) / 2;
		var_af.a = i_42_;
		var_af._if = i_42_;
		var_aw._if(var_af, var_af_43_, null);
		is[2] = var_af_43_.a;
		is_41_[2] = var_af_43_._if;
		var_af.a = i_42_ - 1;
		var_af._if = i_42_ - 1;
		var_af_43_ = var_aw._if(var_af, null, null);
		is[2] += (is[2] - var_af_43_.a) / 2;
		is_41_[2] += (is_41_[2] - var_af_43_._if) / 2;
		var_af.a = 0;
		var_af._if = i_42_;
		var_aw._if(var_af, var_af_43_, null);
		is[3] = var_af_43_.a;
		is_41_[3] = var_af_43_._if;
		var_af.a = 1;
		var_af._if = i_42_ - 1;
		var_af_43_ = var_aw._if(var_af, null, null);
		is[3] += (is[3] - var_af_43_.a) / 2;
		is_41_[3] += (is_41_[3] - var_af_43_._if) / 2;
		return new Poligono(is, is_41_, 4);
	}

	private int[][] a(int i, aw var_aw, a4 var_a4, int i_44_, int i_45_,
			af var_af) {
		int i_46_ = i_45_ == -1 ? 1 : 2;
		int[][] is = new int[i_46_][a[i]];
		af var_af_47_ = null;
		af var_af_48_ = null;
		int i_49_ = 0;
		int i_50_ = 128;
		for (;;) {
			var_af_47_ = var_a4.a(var_af_47_);
			if (var_af_47_ == null)
				break;
			var_af_48_ = var_aw._if(var_af_47_, var_af_48_, var_af);
			boolean bool = _if.a(var_af_48_.a, var_af_48_._if) < _goto;
			if (bool ^ a(var_af_47_, i_44_))
				is[0][i_49_] |= i_50_;
			if (i_45_ != -1 && bool ^ a(var_af_47_, i_45_))
				is[1][i_49_] |= i_50_;
			i_50_ /= 2;
			if (i_50_ == 0) {
				i_50_ = 128;
				if (++i_49_ == is[0].length)
					break;
			}
		}
		return is;
	}

	private boolean a(af var_af, int i) {
		boolean bool = false;
		switch (i) {
		case 0:
			bool = (var_af.a + var_af._if) % 2 == 0;
			break;
		case 1:
			bool = var_af._if % 2 == 0;
			break;
		case 2:
			bool = var_af.a % 3 == 0;
			break;
		case 3:
			bool = (var_af.a + var_af._if) % 3 == 0;
			break;
		case 4:
			bool = (var_af._if / 2 + var_af.a / 3) % 2 == 0;
			break;
		case 5:
			bool = var_af.a * var_af._if % 2 + var_af.a * var_af._if % 3 == 0;
			break;
		case 6:
			bool = ((var_af.a * var_af._if % 2 + var_af.a * var_af._if % 3) % 2 == 0);
			break;
		case 7:
			bool = ((var_af.a * var_af._if % 3 + (var_af.a + var_af._if) % 2) % 2 == 0);
			break;
		}
		return bool;
	}

	private int[] a() throws BarcodeException {
		int i = a(_char, _case, true);
		int i_51_ = i / 256;
		i %= 256;
		int i_52_ = a(_char, _for, false);
		int i_53_ = i_52_ / 256;
		i_52_ %= 256;
		if (i == 0 && i_52_ == 0)
			throw new BarcodeException("version fail");
		int[] is;
		if (i_52_ == 0 || i == i_52_) {
			is = new int[1];
			is[0] = i;
		} else if (i == 0) {
			is = new int[1];
			is[0] = i_52_;
		} else {
			is = new int[2];
			if (i_51_ < i_53_) {
				is[0] = i;
				is[1] = i_52_;
			} else {
				is[0] = i_52_;
				is[1] = i;
			}
		}
		return is;
	}

	private int a(QRFinderPattern var_m, QRFinderPattern var_m_54_, boolean bool) {
		double d = var_m_54_._do().x - var_m._do().x;
		double d_55_ = var_m_54_._do().y - var_m._do().y;
		boolean bool_56_ = Math.abs(d) > Math.abs(d_55_);
		double d_57_ = bool_56_ ? d_55_ / d : d / d_55_;
		boolean bool_58_ = false;
		int i;
		if (bool_56_)
			i = d > 0.0 ? 1 : -1;
		else
			i = d_55_ > 0.0 ? 1 : -1;
		ArrayList arraylist = var_m_54_._int().getScans();
		Iterator iterator = arraylist.iterator();
		int i_59_ = 0;
		while (iterator.hasNext()) {
			Rectangle rectangle = (Rectangle) iterator.next();
			if (rectangle.width > i_59_)
				i_59_ = rectangle.width;
		}
		arraylist = var_m_54_.a().getScans();
		iterator = arraylist.iterator();
		while (iterator.hasNext()) {
			Rectangle rectangle = (Rectangle) iterator.next();
			if (rectangle.height > i_59_)
				i_59_ = rectangle.height;
		}
		double d_60_ = Math.atan(d_57_);
		double d_61_ = Math.cos(d_60_);
		double d_62_ = Math.sin(d_60_);
		double d_63_ = (double) i_59_ * d_61_;
		double d_64_ = d_63_ / 7.0;
		double d_65_ = var_m_54_._do().mod(var_m._do());
		double d_66_ = (d_65_ / d_64_ - 10.0) / 4.0;
		int i_67_ = (int) (d_66_ + 0.5);
		if (i_67_ <= 6) {
			if (i_67_ < 1)
				i_67_ = 1;
			return i_67_;
		}
		double d_68_ = (double) i_59_ * d_61_ / 7.0;
		double d_69_ = d_68_;
		boolean[] bools = new boolean[18];
		for (int i_70_ = 0; i_70_ < 3; i_70_++) {
			for (int i_71_ = 0; i_71_ < 6; i_71_++) {
				double d_72_ = 0.0;
				double d_73_ = 0.0;
				if (bool) {
					if (bool_56_) {
						d_72_ = ((double) (-5 - i_70_) * d_68_ * d_61_ - (double) (-3 + i_71_)
								* d_69_ * d_62_);
						d_73_ = ((double) (-5 - i_70_) * d_68_ * d_62_ + (double) (-3 + i_71_)
								* d_69_ * d_61_);
					} else {
						d_72_ = ((double) -(-3 + i_71_) * d_68_ * d_61_ + (double) (-5 - i_70_)
								* d_69_ * d_62_);
						d_73_ = ((double) (-3 + i_71_) * d_68_ * d_62_ + (double) (-5 - i_70_)
								* d_69_ * d_61_);
					}
				} else if (bool_56_) {
					d_72_ = ((double) (-5 - i_70_) * d_68_ * d_61_ + (double) (-3 + i_71_)
							* d_69_ * d_62_);
					d_73_ = ((double) (-5 - i_70_) * d_68_ * d_62_ - (double) (-3 + i_71_)
							* d_69_ * d_61_);
				} else {
					d_72_ = ((double) (-3 + i_71_) * d_68_ * d_61_ + (double) (-5 - i_70_)
							* d_69_ * d_62_);
					d_73_ = ((double) -(-3 + i_71_) * d_68_ * d_62_ + (double) (-5 - i_70_)
							* d_69_ * d_61_);
				}
				d_72_ *= (double) i;
				d_73_ *= (double) i;
				double d_74_ = var_m_54_._do().x + d_72_;
				double d_75_ = var_m_54_._do().y + d_73_;
				bools[i_71_ * 3 + 2 - i_70_] = _if.a((int) d_74_, (int) d_75_,
						_goto) == 0;
			}
		}
		int i_76_ = a(bools);
		for (int i_77_ = 0; i_77_ < _new.length; i_77_++) {
			if (i_76_ == _new[i_77_])
				return i_77_ + 7;
		}
		int i_78_ = 2147483647;
		int i_79_ = -1;
		for (int i_80_ = 0; i_80_ < _new.length; i_80_++) {
			int i_81_ = i_76_ ^ _new[i_80_];
			int i_82_ = 131072;
			int i_83_ = 0;
			for (int i_84_ = 0; i_84_ < 18; i_84_++) {
				if ((i_81_ & i_82_) != 0)
					i_83_++;
				i_82_ /= 2;
			}
			if (i_83_ < i_78_) {
				i_78_ = i_83_;
				i_79_ = i_80_;
			}
		}
		if (i_78_ > 3)
			return 0;
		return i_79_ + 7 + 256 * i_78_;
	}

	private a[] a(int i) {
		boolean[][] bools = new boolean[2][15];
		double d = _case._do().x - _char._do().x;
		double d_85_ = _case._do().y - _char._do().y;
		boolean bool = Math.abs(d) > Math.abs(d_85_);
		int i_86_ = 10 + i * 4;
		if (bool) {
			double d_87_ = _case._do().x - _char._do().x;
			double d_88_ = _case._do().y - _char._do().y;
			double d_89_ = _for._do().x - _char._do().x;
			double d_90_ = _for._do().y - _char._do().y;
			for (int i_91_ = 0; i_91_ < 9; i_91_++) {
				double d_92_ = (_char._do().x + d_87_ * 5.0 / (double) i_86_ + (double) (-3 + i_91_)
						* d_89_ / (double) i_86_);
				double d_93_ = (_char._do().y + (double) (-3 + i_91_) * d_90_
						/ (double) i_86_ + d_88_ * 5.0 / (double) i_86_);
				int i_94_ = i_91_ > 6 ? -1 : 0;
				bools[0][i_91_ + i_94_] = _if.a((int) d_92_, (int) d_93_) < _goto;
			}
			for (int i_95_ = 0; i_95_ < 7; i_95_++) {
				double d_96_ = (_for._do().x + d_87_ * 5.0 / (double) i_86_ + (double) (-3 + i_95_)
						* d_89_ / (double) i_86_);
				double d_97_ = (_for._do().y + (double) (-3 + i_95_) * d_90_
						/ (double) i_86_ + d_88_ * 5.0 / (double) i_86_);
				bools[1][i_95_ + 8] = _if.a((int) d_96_, (int) d_97_) < _goto;
			}
			for (int i_98_ = 0; i_98_ < 9; i_98_++) {
				double d_99_ = (_char._do().x + (double) (-3 + i_98_) * d_87_
						/ (double) i_86_ + d_89_ * 5.0 / (double) i_86_);
				double d_100_ = (_char._do().y + d_90_ * 5.0 / (double) i_86_ + (double) (-3 + i_98_)
						* d_88_ / (double) i_86_);
				int i_101_ = i_98_ > 6 ? 1 : 0;
				bools[0][14 - i_98_ + i_101_] = _if
						.a((int) d_99_, (int) d_100_) < _goto;
			}
			for (int i_102_ = 0; i_102_ < 8; i_102_++) {
				double d_103_ = (_case._do().x + (double) (-4 + i_102_) * d_87_
						/ (double) i_86_ + d_89_ * 5.0 / (double) i_86_);
				double d_104_ = (_case._do().y + d_90_ * 5.0 / (double) i_86_ + (double) (-4 + i_102_)
						* d_88_ / (double) i_86_);
				bools[1][7 - i_102_] = _if.a((int) d_103_, (int) d_104_) < _goto;
			}
		} else {
			double d_105_ = _for._do().x - _char._do().x;
			double d_106_ = _for._do().y - _char._do().y;
			double d_107_ = _case._do().x - _char._do().x;
			double d_108_ = _case._do().y - _char._do().y;
			for (int i_109_ = 0; i_109_ < 9; i_109_++) {
				double d_110_ = (_char._do().x + (double) (-3 + i_109_)
						* d_105_ / (double) i_86_ + d_107_ * 5.0
						/ (double) i_86_);
				double d_111_ = (_char._do().y + d_108_ * 5.0
						/ (double) i_86_ + (double) (-3 + i_109_) * d_106_
						/ (double) i_86_);
				int i_112_ = i_109_ > 6 ? -1 : 0;
				bools[0][i_109_ + i_112_] = _if.a((int) d_110_, (int) d_111_) < _goto;
			}
			for (int i_113_ = 0; i_113_ < 7; i_113_++) {
				double d_114_ = (_for._do().x + (double) (-3 + i_113_) * d_105_
						/ (double) i_86_ + d_107_ * 5.0 / (double) i_86_);
				double d_115_ = (_for._do().y + d_108_ * 5.0 / (double) i_86_ + (double) (-3 + i_113_)
						* d_106_ / (double) i_86_);
				bools[1][i_113_ + 8] = _if.a((int) d_114_, (int) d_115_) < _goto;
			}
			for (int i_116_ = 0; i_116_ < 9; i_116_++) {
				double d_117_ = (_char._do().x + d_105_ * 5.0 / (double) i_86_ + (double) (-3 + i_116_)
						* d_107_ / (double) i_86_);
				double d_118_ = (_char._do().y + (double) (-3 + i_116_)
						* d_108_ / (double) i_86_ + d_106_ * 5.0
						/ (double) i_86_);
				int i_119_ = i_116_ > 6 ? 1 : 0;
				bools[0][14 - i_116_ + i_119_] = _if.a((int) d_117_,
						(int) d_118_) < _goto;
			}
			for (int i_120_ = 0; i_120_ < 8; i_120_++) {
				double d_121_ = (_case._do().x + d_105_ * 5.0 / (double) i_86_ + (double) (-4 + i_120_)
						* d_107_ / (double) i_86_);
				double d_122_ = (_case._do().y + (double) (-4 + i_120_)
						* d_108_ / (double) i_86_ + d_106_ * 5.0
						/ (double) i_86_);
				bools[1][7 - i_120_] = _if.a((int) d_121_, (int) d_122_) < _goto;
			}
		}
		int i_123_ = a(bools[0]);
		int i_124_ = a(bools[1]);
		a[] var_as = null;
		if (i_123_ == i_124_) {
			a var_a = _if(i_123_);
			if (var_a != null) {
				var_as = new a[1];
				var_as[0] = var_a;
			}
		} else {
			a var_a = _if(i_123_);
			a var_a_125_ = _if(i_124_);
			if (var_a != null && var_a_125_ != null) {
				if (var_a.a(var_a_125_)) {
					var_as = new a[1];
					var_as[0] = var_a;
				} else {
					var_as = new a[2];
					var_as[0] = var_a;
					var_as[1] = var_a_125_;
				}
			} else if (var_a != null) {
				var_as = new a[1];
				var_as[0] = var_a;
			} else if (var_a_125_ != null) {
				var_as = new a[1];
				var_as[0] = var_a_125_;
			}
		}
		return var_as;
	}

	private a _if(int i) {
		a var_a = a(i, 2);
		return var_a;
	}

	private a a(int i, int i_126_) {
		int i_127_ = i_126_ == 2 ? 21522 : 10277;
		i ^= i_127_;
		for (int i_128_ = 0; i_128_ < _byte.length; i_128_++) {
			if (i == _byte[i_128_]) {
				int i_129_ = (i & 0x6000) >> 13;
				int i_130_ = (i & 0x1c00) >> 10;
				return new a(i_129_, i_130_, i_126_, 0);
			}
		}
		int i_131_ = 2147483647;
		int i_132_ = -1;
		for (int i_133_ = 0; i_133_ < _byte.length; i_133_++) {
			int i_134_ = i ^ _byte[i_133_];
			int i_135_ = 16384;
			int i_136_ = 0;
			for (int i_137_ = 0; i_137_ < 15; i_137_++) {
				if ((i_134_ & i_135_) != 0)
					i_136_++;
				i_135_ /= 2;
			}
			if (i_136_ < i_131_) {
				i_131_ = i_136_;
				i_132_ = i_133_;
			}
		}
		if (i_131_ > 3)
			return null;
		i = _byte[i_132_];
		int i_138_ = (i & 0x6000) >> 13;
		int i_139_ = (i & 0x1c00) >> 10;
		return new a(i_138_, i_139_, i_126_, i_131_);
	}

	private int a(boolean[] bools) {
		int i = 0;
		int i_140_ = 1;
		int i_141_ = bools.length;
		for (int i_142_ = 0; i_142_ < i_141_; i_142_++) {
			if (bools[i_142_])
				i += i_140_;
			i_140_ *= 2;
		}
		return i;
	}

	private b a(int[] is, int i) {
		throw new ArrayIndexOutOfBoundsException("ELM");
	}

	private int a(int i, int i_174_, int[] is) {
		int i_175_ = 0;
		for (int i_176_ = 0; i_176_ < i; i_176_++) {
			i_175_ <<= 1;
			int i_177_ = (i_174_ + i_176_) / 8;
			int i_178_ = 7 - (i_174_ + i_176_) % 8;
			int i_179_ = is[i_177_];
			if ((i_179_ & 1 << i_178_) != 0)
				i_175_ |= 0x1;
		}
		return i_175_;
	}
}
