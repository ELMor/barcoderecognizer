/* af - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

class af {
	int a;

	int _if;

	af() {
		/* empty */
	}

	af(int i, int i_0_) {
		a = i;
		_if = i_0_;
	}

	double a(af var_af_1_) {
		PointDouble var_ay = new PointDouble((double) a, (double) _if);
		PointDouble var_ay_2_ = new PointDouble((double) var_af_1_.a, (double) var_af_1_._if);
		return var_ay.mod(var_ay_2_);
	}

	public String toString() {
		return "(" + a + "," + _if + ")";
	}
}
