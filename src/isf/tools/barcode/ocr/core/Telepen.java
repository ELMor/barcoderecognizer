/* bc - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;

class Telepen extends Barcode {
	private static final boolean cq = false;

	private static final int co = 95;

	private static final int cr = 128;

	private static final long[] cs = { 31313131L, 1131313111L, 33313111L,
			1111313131L, 3111313111L, 11333131L, 13133131L, 111111313111L,
			31333111L, 1131113131L, 33113131L, 1111333111L, 3111113131L,
			1113133111L, 1311133111L, 111111113131L, 3131113111L, 11313331L,
			333331L, 111131113111L, 31113331L, 1133113111L, 1313113111L,
			1111113331L, 31131331L, 113111113111L, 3311113111L, 1111131331L,
			311111113111L, 1113111331L, 1311111331L, 11111111113111L,
			31313311L, 1131311131L, 33311131L, 1111313311L, 3111311131L,
			11333311L, 13313311L, 111111311131L, 31331131L, 1131113311L,
			33113311L, 1111331131L, 3111113311L, 1113131131L, 1311131131L,
			111111113311L, 3131111131L, 1131131311L, 33131311L, 111131111131L,
			3111131311L, 1133111131L, 1313111131L, 111111131311L, 3113111311L,
			113111111131L, 3311111131L, 111113111311L, 311111111131L,
			111311111311L, 131111111311L, 11111111111131L, 3131311111L,
			11313133L, 333133L, 111131311111L, 31113133L, 1133311111L,
			1313311111L, 1111113133L, 313333L, 113111311111L, 3311311111L,
			11113333L, 311111311111L, 11131333L, 13111333L, 11111111311111L,
			31311133L, 1131331111L, 33331111L, 1111311133L, 3111331111L,
			11331133L, 13131133L, 111111331111L, 3113131111L, 1131111133L,
			33111133L, 111113131111L, 3111111133L, 111311131111L,
			131111131111L, 111111111133L, 31311313L, 113131111111L,
			3331111111L, 1111311313L, 311131111111L, 11331313L, 13131313L,
			11111131111111L, 3133111111L, 1131111313L, 33111313L,
			111133111111L, 3111111313L, 111313111111L, 131113111111L,
			111111111313L, 313111111111L, 1131131113L, 33131113L,
			11113111111111L, 3111131113L, 113311111111L, 131311111111L,
			111111131113L, 3113111113L, 11311111111111L, 331111111111L,
			111113111113L, 31111111111111L, 111311111113L, 131111111113L,
			1111111111111111L, 33111111111L };

	private static HashMap cn = null;

	private ArrayList cp = new ArrayList();

	private int cm;

	Telepen(PropertiesHolder var_c, int i) {
		super(var_c);
		cm = i;
	}

	int code() {
		return 13;
	}

	int _if(int i) {
		if (i >= 35)
			return 0;
		return -1;
	}

	boolean isCheck() {
		return true;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_;
		if (is[0] == 0)
			i_4_ = 3;
		else
			i_4_ = 1;
		int i_5_ = is[i_4_];
		for (int i_6_ = 1; i_6_ < 9; i_6_++)
			i_5_ += is[i_4_ + i_6_];
		int i_7_ = 2147483647;
		int i_8_ = 2 * cm;
		while (i_4_ + 9 <= i_1_) {
			if ((i_4_ == 1 || is[i_4_ - 1] > i_5_ / i_8_)
					&& is[i_4_ + 9] < i_5_ / 2) {
				int i_9_ = _try(is, i_4_, 12, 16) % 256;
				if (i_9_ == 95) {
					this.readData(i, i_0_, is, i_4_, 12, i_2_, i_9_, true, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			} else if (is[i_4_ - 1] < i_5_ / 2
					&& (i_4_ + 11 == i_1_ - 1 || is[i_4_ + 11] > i_5_ / i_8_)) {
				int i_10_ = _try(is, i_4_, 11, 15) % 256;
				if (i_10_ == 128) {
					this.readData(i, i_0_, is, i_4_, 11, i_2_, i_10_, false, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			}
			i_5_ -= is[i_4_] + is[i_4_ + 1];
			i_4_ += 2;
			i_5_ += is[i_4_ + 9] + is[i_4_ + 10];
		}
		return i_7_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_11_) {
		int i_12_ = 0;
		int i_13_ = var_d._do();
		while_15_: do {
			for (;;) {
				if (i_12_ == 0) {
					int i_14_ = _try(is, i_12_, 12, 16) % 256;
					i_12_ += 12;
					if (i_14_ != 95)
						break while_15_;
					cp.clear();
					cp.add(new Integer(i_14_));
					i_13_ = 0;
					for (int i_15_ = 0; i_15_ < 12; i_15_++)
						i_13_ += is[i_15_];
				} else {
					if (i_12_ > i - 17)
						break;
					int i_16_ = _try(is, i_12_, -i_13_, 16);
					if (i_16_ == -1)
						break while_15_;
					i_12_ += i_16_ / 256;
					cp.add(new Integer(i_16_ % 256));
				}
			}
			if (i_12_ == i - 11) {
				int i_17_ = _try(is, i_12_, 11, 15) % 256;
				if (i_17_ == 128) {
					cp.add(new Integer(i_17_));
					this.a(cp, -1, var_d, var_d_11_);
					return true;
				}
			}
		} while (false);
		return false;
	}

	private boolean _char(String string) {
		int i = 0;
		for (int i_18_ = 1; i_18_ < string.length() - 2; i_18_++)
			i += string.charAt(i_18_);
		return 127 - i % 127 == string.charAt(string.length() - 2);
	}

	private final int _try(int[] is, int i, int i_19_, int i_20_) {
		StringBuffer stringbuffer = new StringBuffer();
		if (i_19_ > 0) {
			int i_21_ = 0;
			for (int i_22_ = 0; i_22_ < i_19_; i_22_++)
				i_21_ += is[i + i_22_];
			int i_23_ = i_21_ / (i_20_ / 2);
			int i_24_ = 0;
			for (int i_25_ = 0; i_25_ < i_19_; i_25_++) {
				if (is[i + i_25_] < i_23_ / 4)
					return -1;
				if (is[i + i_25_] <= i_23_) {
					stringbuffer.append('1');
					i_24_++;
				} else if (is[i + i_25_] <= i_23_ * 4) {
					stringbuffer.append('3');
					i_24_ += 3;
				} else
					return -1;
			}
		} else {
			int i_26_ = -i_19_;
			i_26_ /= 8;
			int i_27_ = 0;
			int i_28_ = 0;
			while (i_28_ < 16) {
				if (is[i + i_27_] < i_26_ / 4)
					return -1;
				if (is[i + i_27_] <= i_26_) {
					stringbuffer.append('1');
					i_28_++;
				} else if (is[i + i_27_] <= i_26_ * 4) {
					stringbuffer.append('3');
					i_28_ += 3;
				} else
					return -1;
				i_27_++;
			}
			if (i_28_ > 16)
				return -1;
		}
		String string = new String(stringbuffer);
		Object object = cn.get(new Long(Long.parseLong(string)));
		if (object == null)
			return -1;
		int i_29_ = ((Integer) object).intValue();
		return string.length() * 256 + i_29_;
	}

	String a(String string) {
		if (!_char(string))
			return null;
		StringBuffer stringbuffer = new StringBuffer();
		for (int i = 1; i < string.length() - 2; i++) {
			char c = string.charAt(i);
			stringbuffer.append((char) c);
		}
		return new String(stringbuffer);
	}

	static {
		cn = new HashMap();
		for (int i = 0; i < cs.length; i++)
			cn.put(new Long(cs[i]), new Integer(i));
	}
}
