/* bb - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;

class bb implements Runnable {
	private static final boolean _for = true;

	private ab _do;

	private Recognizer reco;

	private int _int;

	private int _if;

	private PropertiesHolder a;

	private Settings _new;

	bb(ab var_ab, Recognizer var_a1, int i, int i_0_, PropertiesHolder var_c, Settings var_y) {
		_do = var_ab;
		reco = var_a1;
		_int = i;
		_if = i_0_;
		a = var_c;
		_new = var_y;
	}

	public void run() {
		Object object = null;
		av var_av;
		try {
			var_av = new av(_int, _if, reco, _do, a);
		} catch (Exception exception) {
			return;
		}
		Recognizer var_a1 = new Recognizer(var_av);
		Settings var_y = new Settings();
		var_y.dataMatrix = _new.dataMatrix;
		var_y.QRCode = _new.QRCode;
		for (int i = 0; i < var_av._do(); i++) {
			if (_do.a() == true) {
				a._do("Aborting Bar2dReaderB.run().");
				return;
			}
			var_av._do(i);
			Recognized[] var_bfs = var_a1.recognize(_new);
			ArrayList arraylist = new ArrayList();
			for (int i_1_ = 0; i_1_ < var_bfs.length; i_1_++)
				arraylist.add(var_bfs[i_1_]);
			int i_2_ = _do.a(arraylist);
			a
					._do("Bar2ReaderB ( bitone = " + i + " )  added " + i_2_
							+ " bars");
		}
		a._do("Finishing Bar2dReaderB.run().");
	}
}
