/* ae - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBuffer;

final class DBImagen extends Imagen {
	private final int[] v;

	private final DataBuffer s;

	private final int q;

	private final int r;

	private final int w;

	private final int t;

	private final int y;

	private final int x;

	private int u;

	DBImagen(BufferedImage bufferedimage) {
		super(bufferedimage);
		s = bufferedimage.getRaster().getDataBuffer();
		ColorModel colormodel = bufferedimage.getColorModel();
		r = colormodel.getPixelSize();
		int i = 2;
		for (int i_0_ = 1; i_0_ < r; i_0_++)
			i *= 2;
		v = new int[i];
		for (int i_1_ = 0; i_1_ < i; i_1_++)
			v[i_1_] = (colormodel.getBlue(i_1_) + colormodel.getGreen(i_1_) + colormodel
					.getRed(i_1_)) / 3;
		q = DataBuffer.getDataTypeSize(s.getDataType());
		w = -tY * slStride * q - tX * r;
		t = q - r;
		y = (1 << r) - 1;
		x = q - 1;
		int i_2_ = q;
		u = 1;
		while ((i_2_ >>= 1) > 1)
			u++;
	}

	public final int getPixel(int i, int i_3_) {
		int i_4_ = w + i * r;
		int i_5_ = s.getElem(i_3_ * slStride + (i_4_ >> u));
		int i_6_ = t - (i_4_ & x);
		int i_7_ = i_5_ >> i_6_ & y;
		return v[i_7_];
	}

	public final int[] getRow(int i) {
		int i_8_ = this.getWidth();
		int i_9_ = i * slStride;
		int i_10_ = w;
		int i_11_ = -1;
		int i_12_ = -1;
		for (int i_13_ = 0; i_13_ < i_8_; i_13_++) {
			int i_14_ = i_9_ + (i_10_ >> u);
			if (i_14_ != i_11_)
				i_12_ = s.getElem(i_14_);
			int i_15_ = t - (i_10_ & x);
			int i_16_ = i_12_ >> i_15_ & y;
			xDim[i_13_] = v[i_16_];
			i_10_ += r;
			i_11_ = i_14_;
		}
		return xDim;
	}

	public int[] getCol(int i) {
		int i_17_ = this.getHeight();
		int i_18_ = w + i * r;
		int i_19_ = i_18_ >> u;
		for (int i_20_ = 0; i_20_ < i_17_; i_20_++) {
			int i_21_ = s.getElem(i_19_);
			int i_22_ = t - (i_18_ & x);
			int i_23_ = i_21_ >> i_22_ & y;
			this.yDim[i_20_] = v[i_23_];
			i_19_ += slStride;
		}
		return this.yDim;
	}
}
