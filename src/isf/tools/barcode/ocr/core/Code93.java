/* ag - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;

class Code93 extends Barcode {
	private static final boolean as = false;

	private static final int an = 6;

	private static final int ak = 47;

	private static final int aj = 48;

	private static final int[] am = { 131112, 111213, 111312, 111411, 121113,
			121212, 121311, 111114, 131211, 141111, 211113, 211212, 211311,
			221112, 221211, 231111, 112113, 112212, 112311, 122112, 132111,
			111123, 111222, 111321, 121122, 131121, 212112, 212211, 211122,
			211221, 221121, 222111, 112122, 112221, 122121, 123111, 121131,
			311112, 311211, 321111, 112131, 113121, 211131, 121221, 312111,
			311121, 122211, 111141, 1111411 };

	private static HashMap ap = null;

	private ArrayList ah;

	private boolean at;

	private int ao;

	private int[] aq = new int[7];

	private int[] ar = new int[7];

	private int[] al = new int[7];

	private static final char[][] ai;

	Code93(PropertiesHolder var_c, boolean bool, int i) {
		super(var_c);
		ah = new ArrayList();
		at = bool;
		ao = i;
	}

	int code() {
		return 4;
	}

	int _if(int i) {
		if (i >= 19 && i % 6 == 1)
			return i / 6;
		return -1;
	}

	boolean isCheck() {
		return at;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_;
		if (is[0] == 0)
			i_4_ = 3;
		else
			i_4_ = 1;
		int i_5_ = is[i_4_];
		for (int i_6_ = 1; i_6_ < 6; i_6_++)
			i_5_ += is[i_4_ + i_6_];
		int i_7_ = 2147483647;
		int i_8_ = 2 * ao;
		while (i_4_ + 6 <= i_1_) {
			if ((i_4_ == 1 || is[i_4_ - 1] > i_5_ / i_8_)
					&& is[i_4_ + 6] < i_5_ / 2) {
				int i_9_ = a(is, i_4_, 6, 9, 47, 47);
				if (i_9_ == 47) {
					this.readData(i, i_0_, is, i_4_, 6, i_2_, i_9_, true, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			} else if (is[i_4_ - 1] < i_5_ / 2
					&& (i_4_ + 7 == i_1_ - 1 || is[i_4_ + 7] > i_5_ / i_8_)) {
				int i_10_ = a(is, i_4_, 7, 10, 48, 48);
				if (i_10_ == 48) {
					this.readData(i, i_0_, is, i_4_, 7, i_2_, i_10_, false, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			}
			i_5_ -= is[i_4_] + is[i_4_ + 1];
			i_4_ += 2;
			i_5_ += is[i_4_ + 4] + is[i_4_ + 5];
		}
		return i_7_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_11_) {
		int i_12_ = 0;
		int i_13_ = 0;
		while_5_: do {
			for (;;) {
				if (i_13_ == 0) {
					int i_14_ = a(is, i_12_, 6, 9, 47, 47);
					i_12_ += 6;
					i_13_ = 1;
					if (i_14_ != 47)
						break while_5_;
					ah.clear();
					ah.add(new Integer(i_14_));
				} else {
					if (i - i_12_ <= 7)
						break;
					int i_15_ = a(is, i_12_, 6, 9, 0, 46);
					i_12_ += 6;
					i_13_++;
					if (i_15_ >= 0 && i_15_ <= 46)
						ah.add(new Integer(i_15_));
					else
						ah.add(new Integer(65535));
				}
			}
			int i_16_ = a(is, i_12_, 7, 10, 48, 48);
			if (i_16_ == 48) {
				ah.add(new Integer(i_16_));
				if (i_12_ == i - 7) {
					this.a(ah, -1, var_d, var_d_11_);
					return true;
				}
			}
		} while (false);
		return false;
	}

	private boolean _for(String string) {
		int i = 1;
		int i_17_ = 0;
		for (int i_18_ = string.length() - 4; i_18_ > 0; i_18_--) {
			int i_19_ = string.charAt(i_18_);
			i_17_ += i_19_ * i;
			if (++i > 20)
				i = 1;
		}
		if (i_17_ % 47 != string.charAt(string.length() - 3))
			return false;
		i = 1;
		i_17_ = 0;
		for (int i_20_ = string.length() - 3; i_20_ > 0; i_20_--) {
			int i_21_ = string.charAt(i_20_);
			i_17_ += i_21_ * i;
			if (++i > 15)
				i = 1;
		}
		if (i_17_ % 47 != string.charAt(string.length() - 2))
			return false;
		return true;
	}

	private final int a(int[] is, int i, int i_22_, int i_23_, int i_24_,
			int i_25_) {
		int i_26_ = 0;
		for (int i_27_ = 0; i_27_ < i_22_; i_27_++) {
			aq[i_27_] = is[i + i_27_] * i_23_;
			i_26_ += is[i + i_27_];
		}
		for (int i_28_ = 0; i_28_ < i_22_; i_28_++)
			ar[i_28_] = aq[i_28_];
		for (int i_29_ = 0; i_29_ < i_22_; i_29_++) {
			int i_30_ = ar[i_29_] / i_26_;
			int i_31_ = ar[i_29_] % i_26_;
			if (i_30_ == 0 || i_31_ > i_26_ / 2) {
				i_30_++;
				i_31_ -= i_26_;
			}
			al[i_29_] = i_31_;
			if (i_29_ < i_22_ - 1)
				ar[i_29_ + 1] += i_31_;
			ar[i_29_] = i_30_;
		}
		int i_32_ = 0;
		int i_33_ = -1;
		for (int i_34_ = 0; i_34_ < i_22_; i_34_++)
			i_32_ = i_32_ * 10 + ar[i_34_];
		Object object = ap.get(new Integer(i_32_));
		if (object != null)
			i_33_ = ((Integer) object).intValue();
		if (i_33_ >= i_24_ && i_33_ <= i_25_)
			return i_33_;
		int i_35_ = 0;
		int i_36_ = Math.abs(al[0]);
		for (int i_37_ = 1; i_37_ < 5; i_37_++) {
			if (Math.abs(al[i_37_]) > i_36_) {
				i_36_ = Math.abs(al[i_37_]);
				i_35_ = i_37_;
			}
		}
		for (int i_38_ = 0; i_38_ < 6; i_38_++)
			ar[i_38_] = aq[i_38_];
		for (int i_39_ = 0; i_39_ < 6; i_39_++) {
			int i_40_ = ar[i_39_] / i_26_;
			int i_41_ = ar[i_39_] % i_26_;
			if (i_40_ == 0) {
				i_40_++;
				i_41_ -= i_26_;
			} else if (i_39_ == i_35_) {
				if (i_41_ <= i_26_ / 2) {
					i_40_++;
					i_41_ -= i_26_;
				}
			} else if (i_41_ > i_26_ / 2) {
				i_40_++;
				i_41_ -= i_26_;
			}
			if (i_39_ < i_22_ - 1)
				ar[i_39_ + 1] += i_41_;
			ar[i_39_] = i_40_;
		}
		i_32_ = 0;
		i_33_ = -1;
		for (int i_42_ = 0; i_42_ < i_22_; i_42_++)
			i_32_ = i_32_ * 10 + ar[i_42_];
		object = ap.get(new Integer(i_32_));
		if (object != null)
			i_33_ = ((Integer) object).intValue();
		if (i_33_ >= i_24_ && i_33_ <= i_25_)
			return i_33_;
		for (int i_43_ = 0; i_43_ < i_22_; i_43_++)
			ar[i_43_] = aq[i_43_];
		for (int i_44_ = i_22_ - 1; i_44_ >= 0; i_44_--) {
			int i_45_ = ar[i_44_] / i_26_;
			int i_46_ = ar[i_44_] % i_26_;
			if (i_45_ == 0 || i_46_ > i_26_ / 2) {
				i_45_++;
				i_46_ -= i_26_;
			}
			al[i_44_] = i_46_;
			if (i_44_ > 0)
				ar[i_44_ - 1] += i_46_;
			ar[i_44_] = i_45_;
		}
		i_32_ = 0;
		i_33_ = -1;
		for (int i_47_ = 0; i_47_ < i_22_; i_47_++)
			i_32_ = i_32_ * 10 + ar[i_47_];
		object = ap.get(new Integer(i_32_));
		if (object != null)
			i_33_ = ((Integer) object).intValue();
		if (i_33_ >= i_24_ && i_33_ <= i_25_)
			return i_33_;
		i_35_ = 5;
		i_36_ = Math.abs(al[5]);
		for (int i_48_ = 5; i_48_ > 0; i_48_--) {
			if (Math.abs(al[i_48_]) > i_36_) {
				i_36_ = Math.abs(al[i_48_]);
				i_35_ = i_48_;
			}
		}
		for (int i_49_ = 0; i_49_ < 6; i_49_++)
			ar[i_49_] = aq[i_49_];
		for (int i_50_ = 5; i_50_ >= 0; i_50_--) {
			int i_51_ = ar[i_50_] / i_26_;
			int i_52_ = ar[i_50_] % i_26_;
			if (i_51_ == 0) {
				i_51_++;
				i_52_ -= i_26_;
			} else if (i_50_ == i_35_) {
				if (i_52_ <= i_26_ / 2) {
					i_51_++;
					i_52_ -= i_26_;
				}
			} else if (i_52_ > i_26_ / 2) {
				i_51_++;
				i_52_ -= i_26_;
			}
			if (i_50_ > 0)
				ar[i_50_ - 1] += i_52_;
			ar[i_50_] = i_51_;
		}
		i_32_ = 0;
		i_33_ = -1;
		for (int i_53_ = 0; i_53_ < i_22_; i_53_++)
			i_32_ = i_32_ * 10 + ar[i_53_];
		object = ap.get(new Integer(i_32_));
		if (object != null)
			i_33_ = ((Integer) object).intValue();
		if (i_33_ >= i_24_ && i_33_ <= i_25_)
			return i_33_;
		return -1;
	}

	String a(String string) {
		if (at && !_for(string))
			return null;
		StringBuffer stringbuffer = new StringBuffer();
		int i = -1;
		int i_54_ = at ? 3 : 1;
		for (int i_55_ = 1; i_55_ < string.length() - i_54_; i_55_++) {
			int i_56_ = string.charAt(i_55_);
			if (i != -1) {
				if (i_56_ >= 10 && i_56_ < 36) {
					i_56_ -= 10;
					stringbuffer.append(ai[i_56_][i]);
				}
				i = -1;
			} else if (i_56_ < 10)
				stringbuffer.append((char) (48 + i_56_));
			else if (i_56_ < 36)
				stringbuffer.append((char) (65 + i_56_ - 10));
			else if (i_56_ == 36)
				stringbuffer.append('-');
			else if (i_56_ == 37)
				stringbuffer.append('.');
			else if (i_56_ == 38)
				stringbuffer.append(' ');
			else if (i_56_ == 39)
				stringbuffer.append('$');
			else if (i_56_ == 40)
				stringbuffer.append('/');
			else if (i_56_ == 41)
				stringbuffer.append('+');
			else if (i_56_ == 42)
				stringbuffer.append('%');
			else if (i_56_ == 43)
				i = 3;
			else if (i_56_ == 44)
				i = 2;
			else if (i_56_ == 45)
				i = 1;
			else if (i_56_ == 46)
				i = 0;
		}
		return new String(stringbuffer);
	}

	static {
		ap = new HashMap();
		for (int i = 0; i < am.length; i++)
			ap.put(new Integer(am[i]), new Integer(i));
		ai = (new char[][] { { 'a', '!', '\033', '\001' },
				{ 'b', '\"', '\034', '\002' }, { 'c', '#', '\035', '\003' },
				{ 'd', '$', '\036', '\004' }, { 'e', '%', '\037', '\005' },
				{ 'f', '&', ';', '\006' }, { 'g', '\'', '<', '\007' },
				{ 'h', '(', '=', '\010' }, { 'i', ')', '>', '\t' },
				{ 'j', '*', '?', '\n' }, { 'k', '+', '[', '\013' },
				{ 'l', ',', '\\', '\014' }, { 'm', '-', ']', '\r' },
				{ 'n', '.', '^', '\016' }, { 'o', '/', '_', '\017' },
				{ 'p', ' ', '{', '\020' }, { 'q', ' ', '|', '\021' },
				{ 'r', ' ', '}', '\022' }, { 's', ' ', '~', '\023' },
				{ 't', ' ', '\u007f', '\024' }, { 'u', ' ', '\0', '\025' },
				{ 'v', ' ', '@', '\026' }, { 'w', ' ', '`', '\027' },
				{ 'x', ' ', '\u007f', '\030' }, { 'y', ' ', '\u007f', '\031' },
				{ 'z', ':', '\u007f', '\032' } });
	}
}
