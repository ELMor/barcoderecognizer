/* aj - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

final class DataBufferImagen extends Imagen {
	private final byte[] z;

	DataBufferImagen(BufferedImage bufferedimage) {
		super(bufferedimage);
		java.awt.image.DataBuffer databuffer = bufferedimage.getRaster()
				.getDataBuffer();
		z = ((DataBufferByte) databuffer).getData();
	}

	public final int getPixel(int i, int i_0_) {
		int i_1_ = g + i_0_ * slStride + 3 * i;
		int i_2_ = z[i_1_];
		if (i_2_ < 0)
			i_2_ += 256;
		int i_3_ = z[i_1_ + 1];
		if (i_3_ < 0)
			i_3_ += 256;
		int i_4_ = z[i_1_ + 2];
		if (i_4_ < 0)
			i_4_ += 256;
		return (i_2_ + i_3_ + i_4_) / 3;
	}

	public int[] getRow(int i) {
		int i_5_ = this.getWidth();
		int i_6_ = g + i * slStride;
		for (int i_7_ = 0; i_7_ < i_5_; i_7_++) {
			int i_8_ = z[i_6_++];
			if (i_8_ < 0)
				i_8_ += 256;
			int i_9_ = z[i_6_++];
			if (i_9_ < 0)
				i_9_ += 256;
			int i_10_ = z[i_6_++];
			if (i_10_ < 0)
				i_10_ += 256;
			xDim[i_7_] = (i_8_ + i_9_ + i_10_) / 3;
		}
		return xDim;
	}

	public int[] getCol(int i) {
		int i_11_ = this.getHeight();
		int i_12_ = g + i * 3;
		for (int i_13_ = 0; i_13_ < i_11_; i_13_++) {
			int i_14_ = z[i_12_];
			if (i_14_ < 0)
				i_14_ += 256;
			int i_15_ = z[i_12_ + 1];
			if (i_15_ < 0)
				i_15_ += 256;
			int i_16_ = z[i_12_ + 2];
			if (i_16_ < 0)
				i_16_ += 256;
			this.yDim[i_13_] = (i_14_ + i_15_ + i_16_) / 3;
			i_12_ += slStride;
		}
		return this.yDim;
	}
}
