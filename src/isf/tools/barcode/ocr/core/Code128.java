/* be - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

class Code128 extends Barcode {
	private static final boolean cG = false;

	private static final int cx = 103;

	private static final int cv = 104;

	private static final int ct = 105;

	private static final int cA = 106;

	private static final int[] cz = { 212222, 222122, 222221, 121223, 121322,
			131222, 122213, 122312, 132212, 221213, 221312, 231212, 112232,
			122132, 122231, 113222, 123122, 123221, 223211, 221132, 221231,
			213212, 223112, 312131, 311222, 321122, 321221, 312212, 322112,
			322211, 212123, 212321, 232121, 111323, 131123, 131321, 112313,
			132113, 132311, 211313, 231113, 231311, 112133, 112331, 132131,
			113123, 113321, 133121, 313121, 211331, 231131, 213113, 213311,
			213131, 311123, 311321, 331121, 312113, 312311, 332111, 314111,
			221411, 431111, 111224, 111422, 121124, 121421, 141122, 141221,
			112214, 112412, 122114, 122411, 142112, 142211, 241211, 221114,
			413111, 241112, 134111, 111242, 121142, 121241, 114212, 124112,
			124211, 411212, 421112, 421211, 212141, 214121, 412121, 111143,
			111341, 131141, 114113, 114311, 411113, 411311, 113141, 114131,
			311141, 411131, 211412, 211214, 211232, 2331112 };

	private static HashMap cD = null;

	private ArrayList cu;

	private int cC;

	private ArrayList cB = null;

	private HashMap cw;

	private int[] cE = new int[7];

	private int[] cF = new int[7];

	private int[] cy = new int[7];

	Code128(PropertiesHolder var_c, int i) {
		super(var_c);
		cu = new ArrayList();
		cC = i;
	}

	int code() {
		return 2;
	}

	int _if(int i) {
		if (i > 24 && i % 6 == 1)
			return i / 6;
		return -1;
	}

	boolean isCheck() {
		return true;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_;
		if (is[0] == 0)
			i_4_ = 3;
		else
			i_4_ = 1;
		int i_5_ = is[i_4_];
		for (int i_6_ = 1; i_6_ < 6; i_6_++)
			i_5_ += is[i_4_ + i_6_];
		int i_7_ = 2147483647;
		int i_8_ = 2 * cC;
		while (i_4_ + 6 <= i_1_) {
			if ((i_4_ == 1 || is[i_4_ - 1] > i_5_ / i_8_)
					&& is[i_4_ + 6] < i_5_ / 2) {
				int i_9_ = _do(is, i_4_, 6, 11, 103, 105, 0);
				if (i_9_ >= 103 && i_9_ <= 105) {
					SepInten var_d = this.readData(i, i_0_, is, i_4_, 6, i_2_, i_9_, true,
							i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			}
			if (is[i_4_ - 1] < i_5_
					&& (i_4_ + 7 == i_1_ - 1 || is[i_4_ + 7] > (i_5_ + is[i_4_ + 6])
							/ i_8_)) {
				int i_10_ = _do(is, i_4_, 7, 13, 106, 106, 0);
				if (i_10_ == 106) {
					SepInten var_d = this.readData(i, i_0_, is, i_4_, 7, i_2_, i_10_, false,
							i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			}
			i_5_ -= is[i_4_] + is[i_4_ + 1];
			i_4_ += 2;
			i_5_ += is[i_4_ + 4] + is[i_4_ + 5];
		}
		return i_7_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_11_) {
		if (cB == null)
			cB = new ArrayList();
		if (cw == null)
			cw = new HashMap();
		if (is != null) {
			Iterator iterator = cB.iterator();
			int[] is_12_ = null;
			do {
				if (!iterator.hasNext())
					break;
				is_12_ = (int[]) iterator.next();
			} while (is_12_.length != i);
			if (is_12_ == null || is_12_.length != i) {
				is_12_ = new int[i];
				cB.add(is_12_);
				cw.put(is_12_, new Integer(0));
			}
			for (int i_13_ = 0; i_13_ < i; i_13_++) {
				int i_14_ = is[i_13_];
				is_12_[i_13_] += i_14_;
			}
			cw.put(is_12_, new Integer(
					((Integer) cw.get(is_12_)).intValue() + 1));
			return true;
		}
		Iterator iterator = cB.iterator();
		Object object = null;
		while (iterator.hasNext()) {
			int[] is_15_ = (int[]) iterator.next();
			int i_16_ = ((Integer) cw.get(is_15_)).intValue();
			_if(is_15_, var_d, var_d_11_, i_16_);
		}
		cB.clear();
		return false;
	}

	protected boolean _char() {
		return true;
	}

	private boolean _if(int[] is, SepInten var_d, SepInten var_d_17_, int i) {
		int i_18_ = is.length;
		int i_19_ = 0;
		int i_20_ = 0;
		int i_21_ = 0;
		while_2_: do {
			for (;;) {
				if (i_20_ == 0) {
					int i_22_ = _do(is, i_19_, 6, 11, 103, 105, 0);
					i_19_ += 6;
					i_20_ = 1;
					if (i_22_ < 103 || i_22_ > 105)
						break while_2_;
					cu.clear();
					cu.add(new Integer(i_22_));
					i_21_ = _if(is, i_22_, 0);
				} else {
					if (i_18_ - i_19_ <= 7)
						break;
					int i_23_ = _do(is, i_19_, 6, 11, 0, 102, i_21_);
					if (i_23_ >= 0 && i_23_ <= 102) {
						cu.add(new Integer(i_23_));
						i_21_ = _if(is, i_23_, i_19_);
					} else
						cu.add(new Integer(65535));
					i_19_ += 6;
					i_20_++;
				}
			}
			int i_24_ = 106;
			if (i_24_ == 106) {
				cu.add(new Integer(i_24_));
				Recognized var_bf = this.a(cu, -1, var_d, var_d_17_);
				var_bf.setNumberOfProps(i);
				return true;
			}
		} while (false);
		return false;
	}

	private int _if(int[] is, int i, int i_25_) {
		int i_26_ = cz[i];
		int i_27_ = 0;
		for (int i_28_ = 0; i_28_ < 6; i_28_++) {
			if (i_28_ % 2 == 1)
				i_27_ += i_26_ % 10;
			i_26_ /= 10;
		}
		int i_29_ = 11;
		int i_30_ = 0;
		int i_31_ = 0;
		for (int i_32_ = 0; i_32_ < 6; i_32_++) {
			i_31_ += is[i_25_ + i_32_];
			if (i_32_ % 2 == 0)
				i_30_ += is[i_25_ + i_32_];
		}
		int i_33_ = i_30_ - i_31_ * i_27_ / i_29_;
		return i_33_;
	}

	private boolean _else(String string) {
		int i = string.charAt(0);
		if (i != 103 && i != 104 && i != 105)
			return false;
		if (string.charAt(string.length() - 1) != 'j')
			return false;
		for (int i_34_ = 1; i_34_ < string.length() - 2; i_34_++)
			i += string.charAt(i_34_) * i_34_;
		return i % 103 == string.charAt(string.length() - 2);
	}

	private final int _do(int[] is, int i, int i_35_, int i_36_, int i_37_,
			int i_38_, int i_39_) {
		int i_40_ = 0;
		int i_41_ = i_39_ * i_36_ / 3;
		for (int i_42_ = 0; i_42_ < i_35_; i_42_++) {
			cE[i_42_] = is[i + i_42_] * i_36_;
			i_40_ += is[i + i_42_];
			if (i_41_ != 0) {
				if (i_42_ % 2 == 0 && cE[i_42_] > i_41_)
					cE[i_42_] -= i_41_;
				if (i_42_ % 2 == 1)
					cE[i_42_] += i_41_;
			}
		}
		int i_43_ = 0;
		int i_44_ = -2147483648;
		int i_45_ = -1;
		int i_46_ = 2147483647;
		int i_47_ = -1;
		for (int i_48_ = 0; i_48_ < i_35_; i_48_++)
			cF[i_48_] = cE[i_48_];
		for (int i_49_ = 0; i_49_ < i_35_; i_49_++) {
			int i_50_ = cF[i_49_] / i_40_;
			int i_51_ = cF[i_49_] % i_40_;
			if (i_50_ == 0 || i_51_ > i_40_ / 2) {
				i_50_++;
				i_51_ -= i_40_;
			}
			cF[i_49_] = i_50_;
			i_43_ += i_50_;
			if (i_51_ > i_44_) {
				i_44_ = i_51_;
				i_45_ = i_49_;
			}
			if (i_51_ < i_46_) {
				i_46_ = i_51_;
				i_47_ = i_49_;
			}
		}
		if (Math.abs(i_36_ - i_43_) <= 1) {
			if (i_43_ == i_36_ - 1)
				cF[i_45_]++;
			else if (i_43_ == i_36_ + 1)
				cF[i_47_]--;
			int i_52_ = 0;
			int i_53_ = -1;
			for (int i_54_ = 0; i_54_ < i_35_; i_54_++)
				i_52_ = i_52_ * 10 + cF[i_54_];
			Object object = cD.get(new Integer(i_52_));
			if (object != null)
				i_53_ = ((Integer) object).intValue();
			if ((cF[0] + cF[2] + cF[4]) % 2 == 0 && i_53_ >= i_37_
					&& i_53_ <= i_38_)
				return i_53_;
		}
		for (int i_55_ = 0; i_55_ < i_35_; i_55_++)
			cF[i_55_] = cE[i_55_];
		for (int i_56_ = 0; i_56_ < i_35_; i_56_++) {
			int i_57_ = cF[i_56_] / i_40_;
			int i_58_ = cF[i_56_] % i_40_;
			if (i_57_ == 0 || i_58_ > i_40_ / 2) {
				i_57_++;
				i_58_ -= i_40_;
			}
			cy[i_56_] = i_58_;
			if (i_56_ < i_35_ - 1)
				cF[i_56_ + 1] += i_58_;
			cF[i_56_] = i_57_;
		}
		int i_59_ = 0;
		int i_60_ = -1;
		for (int i_61_ = 0; i_61_ < i_35_; i_61_++)
			i_59_ = i_59_ * 10 + cF[i_61_];
		Object object = cD.get(new Integer(i_59_));
		if (object != null)
			i_60_ = ((Integer) object).intValue();
		if ((cF[0] + cF[2] + cF[4]) % 2 == 0 && i_60_ >= i_37_
				&& i_60_ <= i_38_)
			return i_60_;
		int i_62_ = 0;
		i_44_ = Math.abs(cy[0]);
		for (int i_63_ = 1; i_63_ < 5; i_63_++) {
			if (Math.abs(cy[i_63_]) > i_44_) {
				i_44_ = Math.abs(cy[i_63_]);
				i_62_ = i_63_;
			}
		}
		for (int i_64_ = 0; i_64_ < 6; i_64_++)
			cF[i_64_] = cE[i_64_];
		for (int i_65_ = 0; i_65_ < 6; i_65_++) {
			int i_66_ = cF[i_65_] / i_40_;
			int i_67_ = cF[i_65_] % i_40_;
			if (i_66_ == 0) {
				i_66_++;
				i_67_ -= i_40_;
			} else if (i_65_ == i_62_) {
				if (i_67_ <= i_40_ / 2) {
					i_66_++;
					i_67_ -= i_40_;
				}
			} else if (i_67_ > i_40_ / 2) {
				i_66_++;
				i_67_ -= i_40_;
			}
			if (i_65_ < 5)
				cF[i_65_ + 1] += i_67_;
			cF[i_65_] = i_66_;
		}
		i_59_ = 0;
		i_60_ = -1;
		for (int i_68_ = 0; i_68_ < i_35_; i_68_++)
			i_59_ = i_59_ * 10 + cF[i_68_];
		object = cD.get(new Integer(i_59_));
		if (object != null)
			i_60_ = ((Integer) object).intValue();
		if ((cF[0] + cF[2] + cF[4]) % 2 == 0 && i_60_ >= i_37_
				&& i_60_ <= i_38_)
			return i_60_;
		for (int i_69_ = 0; i_69_ < i_35_; i_69_++)
			cF[i_69_] = cE[i_69_];
		for (int i_70_ = i_35_ - 1; i_70_ >= 0; i_70_--) {
			int i_71_ = cF[i_70_] / i_40_;
			int i_72_ = cF[i_70_] % i_40_;
			if (i_71_ == 0 || i_72_ > i_40_ / 2) {
				i_71_++;
				i_72_ -= i_40_;
			}
			cy[i_70_] = i_72_;
			if (i_70_ > 0)
				cF[i_70_ - 1] += i_72_;
			cF[i_70_] = i_71_;
		}
		i_59_ = 0;
		i_60_ = -1;
		for (int i_73_ = 0; i_73_ < i_35_; i_73_++)
			i_59_ = i_59_ * 10 + cF[i_73_];
		object = cD.get(new Integer(i_59_));
		if (object != null)
			i_60_ = ((Integer) object).intValue();
		if ((cF[0] + cF[2] + cF[4]) % 2 == 0 && i_60_ >= i_37_
				&& i_60_ <= i_38_)
			return i_60_;
		i_62_ = 5;
		i_44_ = Math.abs(cy[5]);
		for (int i_74_ = 5; i_74_ > 0; i_74_--) {
			if (Math.abs(cy[i_74_]) > i_44_) {
				i_44_ = Math.abs(cy[i_74_]);
				i_62_ = i_74_;
			}
		}
		for (int i_75_ = 0; i_75_ < 6; i_75_++)
			cF[i_75_] = cE[i_75_];
		for (int i_76_ = 5; i_76_ >= 0; i_76_--) {
			int i_77_ = cF[i_76_] / i_40_;
			int i_78_ = cF[i_76_] % i_40_;
			if (i_77_ == 0) {
				i_77_++;
				i_78_ -= i_40_;
			} else if (i_76_ == i_62_) {
				if (i_78_ <= i_40_ / 2) {
					i_77_++;
					i_78_ -= i_40_;
				}
			} else if (i_78_ > i_40_ / 2) {
				i_77_++;
				i_78_ -= i_40_;
			}
			if (i_76_ > 0)
				cF[i_76_ - 1] += i_78_;
			cF[i_76_] = i_77_;
		}
		i_59_ = 0;
		i_60_ = -1;
		for (int i_79_ = 0; i_79_ < i_35_; i_79_++)
			i_59_ = i_59_ * 10 + cF[i_79_];
		object = cD.get(new Integer(i_59_));
		if (object != null)
			i_60_ = ((Integer) object).intValue();
		if ((cF[0] + cF[2] + cF[4]) % 2 == 0 && i_60_ >= i_37_
				&& i_60_ <= i_38_)
			return i_60_;
		int i_80_ = 0;
		int i_81_ = 0;
		for (int i_82_ = 0; i_82_ < i_35_; i_82_++) {
			int i_83_ = cE[i_82_] / i_40_;
			int i_84_ = cE[i_82_] % i_40_;
			if (i_83_ == 0 || i_84_ > i_40_ / 2) {
				i_83_++;
				i_84_ -= i_40_;
			}
			cF[i_82_] = i_83_;
			cy[i_82_] = i_84_;
			if (i_82_ % 2 == 0)
				i_80_ += i_83_;
			else
				i_81_ += i_83_;
		}
		int i_85_ = i_36_ - i_80_ - i_81_;
		if (i_85_ == 1 || i_85_ == -1) {
			int i_86_ = -1;
			if (i_85_ == 1) {
				if (i_80_ % 2 == 1) {
					i_44_ = cy[0];
					i_86_ = 0;
					for (int i_87_ = 2; i_87_ < i_35_; i_87_ += 2) {
						if (cy[i_87_] > i_44_) {
							i_44_ = cy[i_87_];
							i_86_ = i_87_;
						}
					}
				} else if (i_81_ % 2 == 0) {
					i_44_ = cy[1];
					i_86_ = 1;
					for (int i_88_ = 1; i_88_ < i_35_; i_88_ += 2) {
						if (cy[i_88_] > i_44_) {
							i_44_ = cy[i_88_];
							i_86_ = i_88_;
						}
					}
				}
				cF[i_86_]++;
			} else if (i_85_ == -1) {
				if (i_80_ % 2 == 1) {
					i_44_ = cy[0];
					i_86_ = 0;
					for (int i_89_ = 2; i_89_ < i_35_; i_89_ += 2) {
						if (cy[i_89_] < i_44_) {
							i_44_ = cy[i_89_];
							i_86_ = i_89_;
						}
					}
				} else if (i_81_ % 2 == 0) {
					i_44_ = cy[1];
					i_86_ = 1;
					for (int i_90_ = 1; i_90_ < i_35_; i_90_ += 2) {
						if (cy[i_90_] < i_44_) {
							i_44_ = cy[i_90_];
							i_86_ = i_90_;
						}
					}
				}
				cF[i_86_]--;
			}
			i_59_ = 0;
			i_60_ = -1;
			for (int i_91_ = 0; i_91_ < i_35_; i_91_++)
				i_59_ = i_59_ * 10 + cF[i_91_];
			object = cD.get(new Integer(i_59_));
			if (object != null)
				i_60_ = ((Integer) object).intValue();
			if (i_60_ >= i_37_ && i_60_ <= i_38_)
				return i_60_;
		}
		return -1;
	}

	String a(String string) {
		if (!_else(string))
			return null;
		StringBuffer stringbuffer = new StringBuffer();
		char c = string.charAt(0);
		boolean bool = false;
		for (int i = 1; i < string.length() - 2; i++) {
			char c_92_ = string.charAt(i);
			if (c == 'g' || bool && c == 'h') {
				bool = false;
				if (c_92_ < '@')
					stringbuffer.append((char) (c_92_ + ' '));
				else if (c_92_ < '`')
					stringbuffer.append((char) (c_92_ - '@'));
				else if (c_92_ == '`')
					stringbuffer.append('?');
				else if (c_92_ == 'a')
					stringbuffer.append('?');
				else if (c_92_ == 'b')
					bool = true;
				else if (c_92_ == 'c')
					c = 'i';
				else if (c_92_ == 'd')
					c = 'h';
				else if (c_92_ == 'e')
					stringbuffer.append('?');
				else if (c_92_ == 'f')
					stringbuffer.append('?');
			} else if (c == 'h' || bool && c == 'g') {
				bool = false;
				if (c_92_ < '`')
					stringbuffer.append((char) (c_92_ + ' '));
				else if (c_92_ == '`')
					stringbuffer.append('?');
				else if (c_92_ == 'a')
					stringbuffer.append('?');
				else if (c_92_ == 'b')
					bool = true;
				else if (c_92_ == 'c')
					c = 'i';
				else if (c_92_ == 'd')
					stringbuffer.append('?');
				else if (c_92_ == 'e')
					c = 'g';
				else if (c_92_ == 'f')
					stringbuffer.append('?');
			} else if (c == 'i') {
				if (c_92_ < 'd') {
					stringbuffer.append((char) ('0' + c_92_ / '\n'));
					stringbuffer.append((char) ('0' + c_92_ % '\n'));
				} else if (c_92_ == 'd')
					c = 'h';
				else if (c_92_ == 'e')
					c = 'g';
				else if (c_92_ == 'f')
					stringbuffer.append('?');
			}
		}
		return new String(stringbuffer);
	}

	static {
		cD = new HashMap();
		for (int i = 0; i < 107; i++)
			cD.put(new Integer(cz[i]), new Integer(i));
	}
}
