/* at - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.DataBufferByte;

final class RGBMediumImagen extends Imagen {
	private final byte[] dataBuffer;

	private final int[] medium;

	RGBMediumImagen(BufferedImage bufferedimage) {
		super(bufferedimage);
		java.awt.image.DataBuffer db = bufferedimage.getRaster()
				.getDataBuffer();
		dataBuffer = ((DataBufferByte) db).getData();
		medium = new int[256];
		ColorModel cm = bufferedimage.getColorModel();
		for (int i = 0; i < 256; i++)
			medium[i] = (cm.getBlue(i) + cm.getGreen(i) + cm
					.getRed(i)) / 3;
	}

	public final int getPixel(int offset, int row) {
		int i = dataBuffer[g + row * slStride + offset];
		if (i < 0)
			i += 256;
		return medium[i];
	}

	public int[] getRow(int i) {
		int i_2_ = this.getWidth();
		int i_3_ = g + i * slStride;
		for (int i_4_ = 0; i_4_ < i_2_; i_4_++) {
			int i_5_ = dataBuffer[i_3_++];
			if (i_5_ < 0)
				i_5_ += 256;
			xDim[i_4_] = medium[i_5_];
		}
		return xDim;
	}

	public int[] getCol(int i) {
		int i_6_ = this.getHeight();
		int i_7_ = g + i;
		for (int i_8_ = 0; i_8_ < i_6_; i_8_++) {
			int i_9_ = dataBuffer[i_7_];
			if (i_9_ < 0)
				i_9_ += 256;
			this.yDim[i_8_] = medium[i_9_];
			i_7_ += slStride;
		}
		return this.yDim;
	}
}
