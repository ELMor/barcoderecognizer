/* m - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Rectangle;
import java.util.ArrayList;

class QRFinderPattern {
	private static final boolean _do = false;

	private PropertiesHolder _if;

	private SepInten si1;

	private SepInten si2;

	private PointDouble _new;

	private boolean _int;

	QRFinderPattern(SepInten var_d, SepInten var_d_0_, PropertiesHolder var_c) {
		Rectangle rectangle = var_d._char();
		Rectangle rectangle_1_ = var_d_0_._char();
		Rectangle rectangle_2_ = rectangle.intersection(rectangle_1_);
		_new = new PointDouble(
				((double) rectangle_2_.x + (double) rectangle_2_.width / 2.0),
				((double) rectangle_2_.y + (double) rectangle_2_.height / 2.0));
		_int = false;
		si1 = var_d;
		si2 = var_d_0_;
		_if = var_c;
	}

	void _if() {
		_int = true;
	}

	boolean _for() {
		return _int;
	}

	PointDouble _do() {
		return _new;
	}

	SepInten _int() {
		return si1;
	}

	SepInten a() {
		return si2;
	}

	int a(QRFinderPattern var_m_3_, QRFinderPattern var_m_4_) {
		int i = (si1._do() + si1._do()) / 4;
		double d = _new.mod(var_m_3_._new);
		double d_5_ = _new.mod(var_m_4_._new);
		if (Math.abs(d - d_5_) < 2.0 * (double) i) {
			double d_6_ = var_m_4_._new.mod(var_m_3_._new);
			if (Math.abs(d_6_ - 1.4 * d) < 2.0 * (double) i) {
				double d_7_ = var_m_3_._new.x - _new.x;
				double d_8_ = var_m_3_._new.y - _new.y;
				double d_9_ = var_m_4_._new.x - _new.x;
				double d_10_ = var_m_4_._new.y - _new.y;
				boolean bool = Math.abs(d_7_) > Math.abs(d_8_);
				if (bool)
					return d_7_ * d_10_ > 0.0 ? 1 : -1;
				return d_8_ * d_9_ > 0.0 ? -1 : 1;
			}
			return 0;
		}
		return 0;
	}

	boolean a(QRFinderPattern var_m_11_, ArrayList arraylist) {
		if (arraylist.size() <= 3)
			return true;
		double d = _new.x;
		double d_12_ = _new.y;
		double d_13_ = var_m_11_._new.x;
		double d_14_ = var_m_11_._new.y;
		double d_15_ = (d - d_13_) * 2.0 / 3.0;
		double d_16_ = (d_12_ - d_14_) * 2.0 / 3.0;
		int[] is = new int[4];
		int[] is_17_ = new int[4];
		is[0] = (int) d;
		is_17_[0] = (int) d_12_;
		is[1] = (int) d_13_;
		is_17_[1] = (int) d_14_;
		is[2] = (int) (d_13_ - d_16_);
		is_17_[2] = (int) (d_14_ + d_15_);
		is[3] = (int) (d - d_16_);
		is_17_[3] = (int) (d_12_ + d_15_);
		Poligono var_i = new Poligono(is, is_17_, 4);
		is = new int[4];
		is_17_ = new int[4];
		is[0] = (int) d;
		is_17_[0] = (int) d_12_;
		is[1] = (int) d_13_;
		is_17_[1] = (int) d_14_;
		is[2] = (int) (d_13_ + d_16_);
		is_17_[2] = (int) (d_14_ - d_15_);
		is[3] = (int) (d + d_16_);
		is_17_[3] = (int) (d_12_ - d_15_);
		Poligono var_i_18_ = new Poligono(is, is_17_, 4);
		boolean bool = false;
		boolean bool_19_ = false;
		for (int i = 0; i < arraylist.size(); i++) {
			QRFinderPattern var_m_20_ = (QRFinderPattern) arraylist.get(i);
			if (!var_m_20_.equals(this) && !var_m_20_.equals(var_m_11_)) {
				if (bool == true || var_i.contiene(var_m_20_._new))
					bool = true;
				if (bool_19_ == true || var_i_18_.contiene(var_m_20_._new))
					bool_19_ = true;
				if (bool && bool_19_)
					return false;
			}
		}
		return true;
	}

	public String toString() {
		return ("QrFinderPattern (" + _new.x + "," + _new.y
				+ ")   bwBound = " + si1.getBWBound());
	}
}
