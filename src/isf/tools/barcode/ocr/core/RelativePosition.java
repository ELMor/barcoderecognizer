/* j - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

class RelativePosition {
	private ArrayList lst;

	RelativePosition(ArrayList arraylist) {
		lst = arraylist;
	}

	void comparePlus(int top, int[] data) {
		for (int j = 0; j < lst.size(); j++) {
			Recognized var_bf = (Recognized) lst.get(j);
			Rectangle r = var_bf.getRectangle();
			if (top >= r.y && top < r.y + r.height) {
				Poligono var_i = var_bf._int();
				int k;
				for (k = r.x; (k < r.x + r.width && !var_i
						.contains(k, top)); k++) {
					/* empty */
				}
				int l;
				for (l = r.x + r.width - 1; l >= r.x
						&& !var_i.contains(l, top); l--) {
					/* empty */
				}
				while (k < l)
					data[k++] = 255;
			}
		}
	}

	void compareMinus(int top, int[] data) {
		for (int j = 0; j < lst.size(); j++) {
			Recognized var_bf = (Recognized) lst.get(j);
			Rectangle r = var_bf.getRectangle();
			if (top >= r.x && top < r.x + r.width) {
				Poligono var_i = var_bf._int();
				int k;
				for (k = r.y; (k < r.y + r.height && !var_i
						.contains(top, k)); k++) {
					/* empty */
				}
				int l;
				for (l = r.y + r.height - 1; l >= r.y
						&& !var_i.contains(top, l); l--) {
					/* empty */
				}
				while (k < l)
					data[k++] = 255;
			}
		}
	}

	boolean isInto(SepInten var_d) {
		while_19_: for (int i = 0; i < lst.size(); i++) {
			Recognized var_bf = (Recognized) lst.get(i);
			Poligono var_i = var_bf._int();
			ArrayList arraylist = var_d.getScans();
			Iterator iterator = arraylist.iterator();
			Rectangle rectangle;
			do {
				if (!iterator.hasNext())
					continue while_19_;
				rectangle = (Rectangle) iterator.next();
			} while (!var_i.contiene(rectangle));
			return true;
		}
		return false;
	}
}
