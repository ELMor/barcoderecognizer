/* z - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferUShort;
import java.awt.image.DirectColorModel;

final class RGBImagen extends Imagen {
	private final short[] data;

	private final int[] red;

	private final int[] green;

	private final int[] blue;

	RGBImagen(BufferedImage bufferedimage) {
		super(bufferedimage);
		java.awt.image.DataBuffer databuffer = bufferedimage.getRaster()
				.getDataBuffer();
		data = ((DataBufferUShort) databuffer).getData();
		red = new int[32];
		green = new int[32];
		blue = new int[32];
		DirectColorModel directcolormodel = (DirectColorModel) bufferedimage
				.getColorModel();
		for (int i = 0; i < 32; i++) {
			blue[i] = directcolormodel.getBlue(i);
			green[i] = directcolormodel.getGreen(i << 5);
			red[i] = directcolormodel.getRed(i << 10);
		}
	}

	public final int getPixel(int i, int i_0_) {
		int i_1_ = data[g + i_0_ * slStride + i];
		int i_2_ = blue[i_1_ & 0x1f];
		i_1_ >>= 5;
		int i_3_ = green[i_1_ & 0x1f];
		i_1_ >>= 5;
		int i_4_ = red[i_1_ & 0x1f];
		return (i_4_ + i_3_ + i_2_) / 3;
	}

	public int[] getRow(int i) {
		int i_5_ = this.getWidth();
		int i_6_ = g + i * slStride;
		for (int i_7_ = 0; i_7_ < i_5_; i_7_++) {
			int i_8_ = data[i_6_++];
			int i_9_ = blue[i_8_ & 0x1f];
			i_8_ >>= 5;
			int i_10_ = green[i_8_ & 0x1f];
			i_8_ >>= 5;
			int i_11_ = red[i_8_ & 0x1f];
			xDim[i_7_] = (i_11_ + i_10_ + i_9_) / 3;
		}
		return xDim;
	}

	public int[] getCol(int i) {
		int i_12_ = this.getHeight();
		int i_13_ = g + i;
		for (int i_14_ = 0; i_14_ < i_12_; i_14_++) {
			int i_15_ = data[i_13_];
			int i_16_ = blue[i_15_ & 0x1f];
			i_15_ >>= 5;
			int i_17_ = green[i_15_ & 0x1f];
			i_15_ >>= 5;
			int i_18_ = red[i_15_ & 0x1f];
			this.yDim[i_14_] = (i_18_ + i_17_ + i_16_) / 3;
			i_13_ += slStride;
		}
		return this.yDim;
	}
}
