/* a1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

public class Recognizer {
	private static final boolean _if = false;

	private static final boolean c = false;

	private static LicenseManager licMgr = null;

	private static final String[] messages = { "", "", "",
			"License is for Code39 only.", "License is for 1D only.",
			"META-INF/LADING.MF", "eval.ini",
			"Expired evaluation period or invalid license key. ",
			"No symbology specified.", "No read direction specified.",
			"ReadOptions.scanInterval out of allowed range (>=1)." };

	private static boolean g = true;

	private int width;

	private int height;

	private Normalize norm;

	private RelativePosition relPos;

	private PropertiesHolder prop;

	private boolean code39NoGuard = false;

	private boolean code39NoGuardFFNX = false;

	private int quietZone;

	private boolean despeckleMore = false;

	private String prefix = null;

	private boolean dm2 = false;

	private ImgManager intensityGetter;

	private ImgManager getter;

	private ImgManager imagen;

	private static final int[] _long;

	private static final void startLicenseManager() {
		licMgr = new LicenseManager(getMessage(5), getMessage(6), getMessage(7));
		Thread thread = new Thread(licMgr);
		thread.start();
	}

	private static final String getMessage(int i) {
		return messages[i];
	}

	public Recognizer(Image image) {
		norm = new Normalize();
		if (!g) {
			imagen = new ImagenConsumer(image);
			width = imagen.getWidth();
			height = imagen.getHeight();
		} else {
			BufferedImage bi;
			if (image instanceof BufferedImage) {
				width = image.getWidth(null);
				height = image.getHeight(null);
				bi = (BufferedImage) image;
			} else {
				Panel panel = new Panel();
				MediaTracker mediatracker = new MediaTracker(panel);
				mediatracker.addImage(image, 0);
				try {
					mediatracker.waitForID(0);
				} catch (InterruptedException interruptedexception) {
					/* empty */
				}
				width = image.getWidth(panel);
				height = image.getHeight(panel);
				bi = new BufferedImage(width, height, 2);
				Graphics2D graphics2d = bi.createGraphics();
				graphics2d.drawImage(image, 0, 0, panel);
			}
			getter = new BIImagen(bi);
			switch (bi.getType()) {
			case 5:
				imagen = new DataBufferImagen(bi);
				break;
			case 6:
			case 7:
				imagen = new DataBuffer02Imagen(bi);
				break;
			case 10:
				imagen = new ByteImagen(bi);
				break;
			case 13:
				imagen = new RGBMediumImagen(bi);
				break;
			case 12:
				imagen = new DBImagen(bi);
				break;
			case 1:
			case 2:
			case 3:
			case 4:
				imagen = new IntDataImagen(bi);
				break;
			case 11:
				imagen = new DataImagen(bi);
				break;
			case 9:
				imagen = new RGBImagen(bi);
				break;
			case 8:
				imagen = new ShortImagen(bi);
				break;
			default:
				imagen = new BIImagen(bi);
			}
		}
	}

	public Recognizer(ImgManager imgr) {
		width = imgr.getWidth();
		height = imgr.getHeight();
		norm = new Normalize();
		getter = imagen = imgr;
	}

	public Recognized[] recognize(Settings settings) throws IllegalArgumentException {
		if (settings.properties == null)
			prop = null;
		else if (settings.properties instanceof HashMap)
			prop = new PropertiesHolder((HashMap) settings.properties);
		else if (settings.properties instanceof StringBuffer)
			prop = new PropertiesHolder((StringBuffer) settings.properties);
		else
			prop = null;
		norm.prop = prop;
		intensityGetter = imagen;
		quietZone = 1;
		boolean bitone2d = false;
		if (settings.prop != null) {
			Properties properties = settings.prop;
			String val = properties.getProperty("code39NoGuard", "false");
			code39NoGuard = val.equals("true");
			val = properties.getProperty("code39NoGuardFFNX", "false");
			code39NoGuardFFNX = val.equals("true");
			val = properties.getProperty("smallQuietZone", "false");
			boolean smallQuietZone = val.equals("true");
			val = properties.getProperty("tinyQuietZone", "false");
			boolean tinyQuietZone = val.equals("true");
			val = properties.getProperty("noQuietZone", "false");
			boolean noQuietZone = val.equals("true");
			val = properties.getProperty("despeckleMore", "false");
			despeckleMore = val.equals("true");
			prefix = properties.getProperty("prefix", "");
			if (smallQuietZone && tinyQuietZone)
				quietZone = 4;
			else if (tinyQuietZone)
				quietZone = 3;
			else if (smallQuietZone)
				quietZone = 2;
			if (noQuietZone)
				quietZone = 100;
			val = properties.getProperty("bitone2d", "false");
			bitone2d = val.equals("true");
			val = properties.getProperty("defaultIntensityGetter", "false");
			if (val.equals("true"))
				intensityGetter = getter;
			val = properties.getProperty("dm2", "false");
			dm2 = val.equals("true");
		}
		ArrayList cb1D = new ArrayList();
		ArrayList cb2D = new ArrayList();
		if (settings.code128)
			cb1D.add(new Code128(prop, quietZone));
		if (settings.code32)
			cb1D.add(new Code32(prop, quietZone));
		if (settings.code39)
			cb1D.add(new Code39(prop, settings.check, code39NoGuard, code39NoGuardFFNX, quietZone));
		if (settings.code93)
			cb1D.add(new Code93(prop, settings.check, quietZone));
		if (settings.upcA)
			cb1D.add(new UPC_A(prop, quietZone));
		if (settings.upcE)
			cb1D.add(new UPC_E(prop, quietZone));
		if (settings.EAN13)
			cb1D.add(new EAN13(prop, quietZone));
		if (settings.EAN8)
			cb1D.add(new EAN8(prop, quietZone));
		if (settings.interleaved2of5)
			cb1D.add(new I2OF5(prop, settings.check, quietZone));
		if (settings.codabar)
			cb1D.add(new CodaBar(prop, quietZone));
		if (settings.pdf417)
			cb1D.add(new PDF417(prop, quietZone));
		if (settings.rss14)
			cb1D.add(new RSS14(prop));
		if (settings.rssltd)
			cb1D.add(new RSSLTD(prop));
		if (settings.telepen)
			cb1D.add(new Telepen(prop, quietZone));
		if (settings.code11)
			cb1D.add(new Code11(prop, settings.check, quietZone));
		if (settings.patch)
			cb1D.add(new Patch(prop, quietZone));
		if (settings.dataMatrix) {
			if (dm2)
				cb2D.add(new DataMatrixDM2(width, height, settings.scanInterval, prop));
			else
				cb2D.add(new DataMatrix(width, height, settings.scanInterval, prop));
		}
		if (settings.QRCode)
			cb2D.add(new QRCode(width, height, settings.scanInterval, prop));
		if (cb1D.size() == 0 && cb2D.size() == 0)
			throw new IllegalArgumentException(getMessage(8));
		ArrayList al1 = new ArrayList();
		relPos = null;
		if (cb1D.size() != 0) {
			al1.addAll(a(cb1D, settings, 0));
			if (!norm.isTwoNonZeroes() && al1.size() < settings.barsToRead) {
				relPos = new RelativePosition(al1);
				al1.addAll(a(cb1D, settings, 1));
				if (al1.size() < settings.barsToRead)
					al1.addAll(a(cb1D, settings, 2));
			}
			a(cb1D);
		}
		if (cb2D.size() != 0) {
			int i = settings.barsToRead - al1.size();
			if (i <= 0)
				i = 1;
			ArrayList arraylist_5_ = new ArrayList();
			arraylist_5_ = read(cb2D, 128, i, settings.scanInterval);
			norm.init();
			if (!norm.isTwoNonZeroes() && arraylist_5_.size() < i) {
				ab var_ab = new ab(i, arraylist_5_);
				int[] is = norm.normalize();
				Object object = null;
				Thread thread;
				if (bitone2d == true && is.length > 1) {
					bb var_bb = new bb(var_ab, this, width, height, prop, settings);
					thread = new Thread(var_bb);
				} else {
					ak var_ak = new ak(var_ab, this, cb2D, is, i,
							settings.scanInterval, prop);
					thread = new Thread(var_ak);
				}
				thread.start();
				try {
					thread.join();
				} catch (InterruptedException interruptedexception) {
					/* empty */
				}
			}
			al1.addAll(arraylist_5_);
		}
		Recognized[] ret = new Recognized[al1.size()];
		for (int i = 0; i < al1.size(); i++)
			ret[i] = (Recognized) al1.get(i);
		return ret;
	}

	private ArrayList a(ArrayList barcodes, Settings settings, int i)
			throws IllegalArgumentException {
		if (!(settings.rightScanDir || settings.downScanDir || settings.leftScanDir || settings.upScanDir))
			throw new IllegalArgumentException(getMessage(9));
		if (settings.scanInterval < 1)
			throw new IllegalArgumentException(getMessage(10));
		int[] is = new int[width + height > 100 ? width + height : 100];
		int i_6_ = settings._int;
		if (norm.isInitialized() && norm.isTwoNonZeroes())
			i_6_ = norm.between(i_6_);
		ArrayList retVal = new ArrayList();
		boolean isMobile = false;
		if (settings.prop != null) {
			Properties properties = settings.prop;
			String string = properties.getProperty("Mobile", "false");
			isMobile = string.equals("true");
		}
		if (isMobile == true) {
			if (i == 0) {
				if (settings.rightScanDir)
					i_6_ = _if(barcodes, is, i_6_, 90, settings.scanInterval);
				if (settings.downScanDir)
					i_6_ = _if(barcodes, is, i_6_, 180, settings.scanInterval);
				if (settings.leftScanDir)
					i_6_ = _if(barcodes, is, i_6_, 270, settings.scanInterval);
				if (settings.upScanDir)
					i_6_ = _if(barcodes, is, i_6_, 360, settings.scanInterval);
			} else if (i == 1) {
				if (settings.rightScanDir)
					_if(barcodes, is, -1, 90, settings.scanInterval);
				if (settings.downScanDir)
					_if(barcodes, is, -1, 180, settings.scanInterval);
				if (settings.leftScanDir)
					_if(barcodes, is, -1, 270, settings.scanInterval);
				if (settings.upScanDir)
					_if(barcodes, is, -1, 360, settings.scanInterval);
			}
		} else if (i == 0) {
			if (settings.rightScanDir)
				i_6_ = a(barcodes, is, i_6_, 90, settings.scanInterval);
			if (settings.downScanDir)
				i_6_ = a(barcodes, is, i_6_, 180, settings.scanInterval);
			if (settings.leftScanDir)
				i_6_ = a(barcodes, is, i_6_, 270, settings.scanInterval);
			if (settings.upScanDir)
				i_6_ = a(barcodes, is, i_6_, 360, settings.scanInterval);
		} else if (i == 1) {
			if (settings.rightScanDir)
				a(barcodes, is, 90, settings.scanInterval);
			if (settings.downScanDir)
				a(barcodes, is, 180, settings.scanInterval);
			if (settings.leftScanDir)
				a(barcodes, is, 270, settings.scanInterval);
			if (settings.upScanDir)
				a(barcodes, is, 360, settings.scanInterval);
		}
		for (int k = 0; k < barcodes.size(); k++) {
			Barcode barcode = (Barcode) barcodes.get(k);
			barcode.clear();
			if (i == 2) {
				ArrayList a1 = new ArrayList();
				Iterator t = barcode.getAL1().iterator();
				while (t.hasNext()) {
					SepInten var_d = (SepInten) t.next();
					if (!relPos.isInto(var_d))
						a1.add(var_d);
				}
				barcode.al01 = a1;
				a1 = new ArrayList();
				t = barcode.getAL2().iterator();
				while (t.hasNext()) {
					SepInten var_d = (SepInten) t.next();
					if (!relPos.isInto(var_d))
						a1.add(var_d);
				}
				barcode.al02 = a1;
			}
			barcode.init(barcode.code());
			if (barcode.code() == 10)
				((PDF417) barcode).a(this, is);
			else if (barcode.code() == 18)
				((RSS14) barcode).a(this);
			else if (barcode.code() == 19)
				((RSSLTD) barcode)._if(this);
			barcode._do(i);
			Iterator iterator = barcode.getAL1().iterator();
			while (iterator.hasNext()) {
				SepInten var_d = (SepInten) iterator.next();
				if (!var_d.p) {
					ArrayList arraylist_10_ = var_d.a();
					if (arraylist_10_ != null) {
						Iterator iterator_11_ = arraylist_10_.iterator();
						while (iterator_11_.hasNext()) {
							SepInten var_d_12_ = (SepInten) iterator_11_.next();
							if (!var_d_12_.p) {
								if (barcode.code() == 10)
									((PDF417) barcode).a(0, is, var_d, var_d_12_);
								else if (barcode.code() == 18)
									((RSS14) barcode).a(0, null, var_d, var_d_12_);
								else if (barcode.code() == 19)
									((RSSLTD) barcode).a(0, null, var_d, var_d_12_);
								else {
									int i_13_ = a(barcode, var_d, var_d_12_, is);
								}
							}
						}
					}
				}
			}
			iterator = barcode.getAL1().iterator();
			while (iterator.hasNext()) {
				SepInten var_d = (SepInten) iterator.next();
				var_d._for();
			}
			iterator = barcode.getAL2().iterator();
			while (iterator.hasNext()) {
				SepInten var_d = (SepInten) iterator.next();
				var_d._for();
			}
			barcode._try();
			iterator = barcode.getAL3().iterator();
			while (iterator.hasNext()) {
				Recognized var_bf = (Recognized) iterator.next();
				SepInten var_d = var_bf.getSepIntensity();
				var_d.a(this, is, prop);
				SepInten var_d_14_ = var_bf._new();
				var_d_14_.a(this, is, prop);
				var_bf.a();
			}
			barcode.a();
			for (int i_15_ = 0; i_15_ < barcode.getAL3().size(); i_15_++) {
				Recognized var_bf = (Recognized) barcode.getAL3().get(i_15_);
				if (var_bf != null
						&& (prefix == null || prefix.length() == 0 || var_bf.getEncoding()
								.startsWith(prefix)))
					retVal.add(var_bf);
			}
		}
		if (settings.plus2 || settings.plus5) {
			Iterator iterator = retVal.iterator();
			while (iterator.hasNext()) {
				Recognized var_bf = (Recognized) iterator.next();
				int i_16_ = var_bf.getBarcodeCode();
				if (i_16_ == 6 || i_16_ == 7 || i_16_ == 14 || i_16_ == 15) {
					if (settings.plus2) {
						w var_w = new w(2, prop);
						var_w.a(var_bf, this, is, prop);
					}
					if (settings.plus5) {
						w var_w = new w(5, prop);
						var_w.a(var_bf, this, is, prop);
					}
				}
			}
		}
		return retVal;
	}

	ArrayList read(ArrayList ls, int i, int i_17_, int i_18_) {
		if (norm.isInitialized() && norm.isTwoNonZeroes())
			i = norm.between(i);
		ArrayList lst = new ArrayList();
		for (int k = 0; k < ls.size(); k++) {
			Barcode bc = (Barcode) ls.get(k);
			if (bc.code() == 5) {
				if (dm2)
					((DataMatrixDM2) bc).read(this, i, lst, norm, i_17_, prefix);
				else
					((DataMatrix) bc).read(this, i, lst, norm, i_17_, prefix);
			}
			if (bc.code() == 17)
				((QRCode) bc).read(this, i, lst, norm, i_17_, prefix);
		}
		return lst;
	}

	private void a(ArrayList arraylist) {
		for (int i = 0; i < arraylist.size(); i++) {
			Barcode var_aa = (Barcode) arraylist.get(i);
			var_aa.clear();
			Iterator iterator = var_aa.getAL1().iterator();
			while (iterator.hasNext()) {
				SepInten var_d = (SepInten) iterator.next();
				var_d.clear();
			}
			iterator = var_aa.getAL2().iterator();
			while (iterator.hasNext()) {
				SepInten var_d = (SepInten) iterator.next();
				var_d.clear();
			}
		}
	}

	private int a(Barcode var_aa, SepInten var_d, SepInten var_d_21_, int[] is) {
		int i = var_d.getDirection();
		int i_22_ = var_d.getBWBound();
		int i_23_ = var_d_21_.getBWBound();
		int i_24_ = 0;
		int i_25_ = 0;
		int i_26_;
		int i_27_;
		int i_28_;
		int i_29_;
		if (i == 90 || i == 270) {
			i_26_ = (int) var_d.getP1().getY();
			i_27_ = (int) var_d.getP2().getY();
			i_28_ = (int) var_d_21_.getP1().getY();
			i_29_ = (int) var_d_21_.getP2().getY();
		} else {
			i_26_ = (int) var_d.getP1().getX();
			i_27_ = (int) var_d.getP2().getX();
			i_28_ = (int) var_d_21_.getP1().getX();
			i_29_ = (int) var_d_21_.getP2().getX();
		}
		int i_30_ = i_27_ - i_26_;
		int i_31_ = i_29_ - i_28_;
		if (i_30_ < 0) {
			int i_32_ = i_27_;
			i_27_ = i_26_;
			i_26_ = i_32_;
			i_30_ = -i_30_;
		}
		if (i_31_ < 0) {
			int i_33_ = i_29_;
			i_29_ = i_28_;
			i_28_ = i_33_;
			i_31_ = -i_31_;
		}
		int i_34_ = Math.min(i_30_, i_31_);
		for (int i_35_ = 0; i_35_ <= i_34_; i_35_++) {
			boolean bool = false;
			boolean bool_36_ = false;
			boolean bool_37_ = false;
			boolean bool_38_ = false;
			int i_39_;
			int i_40_;
			int i_41_;
			int i_42_;
			if (i == 90 || i == 270) {
				i_40_ = i_26_ + i_35_;
				i_39_ = (int) (var_d._else() * (double) i_40_ + var_d._if());
				i_42_ = i_28_ + i_35_;
				i_41_ = (int) (var_d_21_._else() * (double) i_42_ + var_d_21_
						._if());
			} else {
				i_39_ = i_26_ + i_35_;
				i_40_ = (int) (var_d._else() * (double) i_39_ + var_d._if());
				i_41_ = i_28_ + i_35_;
				i_42_ = (int) (var_d_21_._else() * (double) i_41_ + var_d_21_
						._if());
			}
			if (i == 90) {
				if (a(i_39_, i_40_, i_22_) != 0) {
					while (++i_39_ < width && a(i_39_, i_40_, i_22_) != 0) {
						/* empty */
					}
				}
				if (a(i_41_, i_42_, i_23_) != 0) {
					while (--i_41_ >= 0 && a(i_41_, i_42_, i_23_) != 0) {
						/* empty */
					}
				}
			} else if (i == 180) {
				if (a(i_39_, i_40_, i_22_) != 0) {
					while (++i_40_ < height && a(i_39_, i_40_, i_22_) != 0) {
						/* empty */
					}
				}
				if (a(i_41_, i_42_, i_23_) != 0) {
					while (--i_42_ >= 0 && a(i_41_, i_42_, i_23_) != 0) {
						/* empty */
					}
				}
			} else if (i == 270) {
				if (a(i_39_, i_40_, i_22_) != 0) {
					while (--i_39_ >= 0 && a(i_39_, i_40_, i_22_) != 0) {
						/* empty */
					}
				}
				if (a(i_41_, i_42_, i_23_) != 0) {
					while (++i_41_ < width && a(i_41_, i_42_, i_23_) != 0) {
						/* empty */
					}
				}
			} else if (i == 360) {
				if (a(i_39_, i_40_, i_22_) != 0) {
					while (--i_40_ >= 0 && a(i_39_, i_40_, i_22_) != 0) {
						/* empty */
					}
				}
				if (a(i_41_, i_42_, i_23_) != 0) {
					while (++i_42_ < height && a(i_41_, i_42_, i_23_) != 0) {
						/* empty */
					}
				}
			}
			int i_43_ = _if(i_39_, i_40_, i_41_, i_42_, is, i_22_, i_23_);
			if (var_aa._if(i_43_) == -1) {
				int i_44_ = 0;
				for (int i_45_ = 1; i_45_ < i_43_ - 1; i_45_++) {
					if (is[i_45_] == 1 && is[i_45_ - 1] > 1
							&& is[i_45_ + 1] > 1) {
						is[i_45_] = -1;
						i_44_++;
					}
				}
				if (var_aa._if(i_43_ - 2 * i_44_) != -1) {
					int i_46_ = 0;
					for (int i_47_ = 1; i_47_ < i_43_; i_47_++) {
						if (is[i_47_] == -1) {
							is[i_47_ - 1 - i_46_] += 1 + is[i_47_ + 1];
							i_47_++;
							i_46_ += 2;
						} else
							is[i_47_ - i_46_] = is[i_47_];
					}
					i_43_ -= 2 * i_44_;
				} else {
					i_43_ = a(i_39_, i_40_, i_41_, i_42_, is, i_22_, i_23_);
					if (var_aa._if(i_43_) == -1)
						continue;
				}
			}
			i_24_++;
			if (var_aa.a(i_43_, is, var_d, var_d_21_))
				i_25_++;
		}
		if (var_aa._char())
			var_aa.a(-1, null, var_d, var_d_21_);
		if (i_24_ == 0)
			return 0;
		if (i_25_ == 0)
			return -i_24_;
		return i_25_;
	}

	private int _if(Barcode var_aa, SepInten var_d, SepInten var_d_48_, int[] is) {
		int i = var_d.getDirection();
		int i_49_ = var_d.getBWBound();
		int i_50_ = var_d_48_.getBWBound();
		boolean bool = false;
		boolean bool_51_ = false;
		int i_52_;
		int i_53_;
		int i_54_;
		int i_55_;
		if (i == 90 || i == 270) {
			i_52_ = (int) var_d.getP1().getY();
			i_53_ = (int) var_d.getP2().getY();
			i_54_ = (int) var_d_48_.getP1().getY();
			i_55_ = (int) var_d_48_.getP2().getY();
		} else {
			i_52_ = (int) var_d.getP1().getX();
			i_53_ = (int) var_d.getP2().getX();
			i_54_ = (int) var_d_48_.getP1().getX();
			i_55_ = (int) var_d_48_.getP2().getX();
		}
		int i_56_ = 0;
		int i_57_ = 0;
		if (i_54_ >= i_52_ && i_54_ <= i_53_) {
			i_56_ = i_54_;
			i_57_ = Math.min(i_53_, i_55_);
		} else if (i_55_ >= i_52_ && i_55_ <= i_53_) {
			i_56_ = i_52_;
			i_57_ = i_55_;
		} else if (i_54_ < i_52_ && i_55_ > i_53_) {
			i_56_ = i_52_;
			i_57_ = i_53_;
		}
		int i_58_ = i_57_ - i_56_;
		if (i_58_ > 10) {
			int[] is_59_ = null;
			int[] is_60_ = null;
			int i_61_ = -1;
			for (int i_62_ = 0; i_62_ <= i_58_ + 1; i_62_++) {
				boolean bool_63_ = false;
				boolean bool_64_ = false;
				boolean bool_65_ = false;
				boolean bool_66_ = false;
				int i_67_ = -1;
				if (i == 90 || i == 270) {
					int i_68_ = i_56_ + i_62_;
					int i_69_ = (int) (var_d._else() * (double) i_68_ + var_d
							._if());
					int i_70_ = i_68_;
					int i_71_ = (int) (var_d_48_._else() * (double) i_70_ + var_d_48_
							._if());
					i_67_ = i == 90 ? i_71_ - i_69_ : i_69_ - i_71_;
					if (i_62_ == 0 || i_67_ != i_61_ || i_62_ == i_58_ + 1) {
						if (i_62_ != 0) {
							for (int i_72_ = 0; i_72_ <= i_61_; i_72_++)
								is_59_[i_72_] /= is_60_[i_72_];
							int i_73_ = 0;
							is[0] = 0;
							boolean bool_74_ = false;
							for (int i_75_ = 0; i_75_ <= i_61_; i_75_++) {
								int i_76_ = i_49_ + (i_50_ - i_49_) * i_75_
										/ i_61_;
								boolean bool_77_ = is_59_[i_75_] >= i_76_;
								if (bool_77_ != bool_74_) {
									bool_74_ = !bool_74_;
									is[++i_73_] = 0;
								}
								is[i_73_]++;
							}
							i_73_++;
							if (var_aa._if(i_73_) == -1
									&& !var_aa.a(i_73_, is, var_d, var_d_48_)) {
								/* empty */
							}
							if (i_62_ == i_58_ + 1)
								break;
						}
						is_59_ = new int[i_67_ + 1];
						is_60_ = new int[i_67_ + 1];
					}
					int i_78_ = i == 90 ? 1 : -1;
					for (int i_79_ = 0; i_79_ <= i_67_; i_79_ += i_78_) {
						int i_80_ = a(i_69_ + i_79_, i_68_);
						is_59_[i_79_] += i_80_;
						is_60_[i_79_]++;
					}
				} else {
					int i_81_ = i_56_ + i_62_;
					int i_82_ = (int) (var_d._else() * (double) i_81_ + var_d
							._if());
					int i_83_ = i_81_;
					int i_84_ = (int) (var_d_48_._else() * (double) i_83_ + var_d_48_
							._if());
				}
				i_61_ = i_67_;
			}
		}
		return 0;
	}

	void a(ArrayList arraylist, int[] is, int i, int i_85_) {
		for (int i_86_ = 32; i_86_ <= 224; i_86_ += 16) {
			if (i_86_ != 128)
				a(arraylist, is, i_86_, i, i_85_);
		}
	}

	private int a(ArrayList arraylist, int[] is, int i, int i_87_, int i_88_) {
		int i_89_ = 0;
		int i_90_ = 0;
		boolean bool = true;
		if (i_87_ == 90) {
			i_89_ = 0;
			bool = true;
			i_90_ = height;
		} else if (i_87_ == 270) {
			i_89_ = width - 1;
			bool = true;
			i_90_ = height;
		} else if (i_87_ == 180) {
			i_89_ = 0;
			bool = false;
			i_90_ = width;
		} else if (i_87_ == 360) {
			i_89_ = height - 1;
			bool = false;
			i_90_ = width;
		}
		int i_91_ = i_88_;
		int i_92_ = 0;
		for (int i_93_ = 0; i_93_ < i_90_; i_93_ += i_91_) {
			int i_94_ = a(bool ? i_89_ : i_93_, bool ? i_93_ : i_89_, is, i,
					i_87_);
			int i_95_ = 2147483647;
			for (int i_96_ = 0; i_96_ < arraylist.size(); i_96_++) {
				Barcode var_aa = (Barcode) arraylist.get(i_96_);
				int i_97_ = var_aa.a(bool ? i_89_ : i_93_,
						bool ? i_93_ : i_89_, i_94_, is, i_87_, i);
				if (i_97_ < i_95_)
					i_95_ = i_97_;
			}
			i_95_ /= 8;
			if (i_95_ == 0)
				i_95_ = 1;
			if (i_95_ < i_88_) {
				i_92_ = 8;
				if (i_95_ < i_91_)
					i_91_ = i_95_;
			} else if (i_92_ > 0 && --i_92_ == 0)
				i_91_ = i_88_;
		}
		if (!norm.isInitialized()) {
			norm.init();
			if (norm.isTwoNonZeroes() && norm.between(i) != i) {
				i = norm.between(i);
				i = a(arraylist, is, i, i_87_, i_88_);
			}
		}
		return i;
	}

	private int _if(ArrayList arraylist, int[] is, int i, int i_98_, int i_99_) {
		boolean bool = false;
		int i_100_ = 0;
		boolean bool_101_ = true;
		boolean bool_102_ = false;
		boolean bool_103_ = false;
		if (i_98_ == 90) {
			bool = false;
			bool_101_ = true;
			bool_102_ = true;
			i_100_ = height;
		} else if (i_98_ == 270) {
			int i_104_ = width - 1;
			bool_101_ = true;
			bool_103_ = true;
			i_100_ = height;
		} else if (i_98_ == 180) {
			bool = false;
			bool_101_ = false;
			bool_102_ = true;
			i_100_ = width;
		} else if (i_98_ == 360) {
			int i_105_ = height - 1;
			bool_101_ = false;
			bool_103_ = true;
			i_100_ = width;
		}
		int i_106_ = i_99_;
		int i_107_ = 0;
		for (int i_108_ = 0; i_108_ < i_100_; i_108_ += i_106_) {
			int[] is_109_ = bool_101_ ? intensityGetter.getRow(i_108_) : intensityGetter.getCol(i_108_);
			int[] is_110_;
			if (i != -1) {
				is_110_ = new int[1];
				is_110_[0] = i;
				if (!norm.isInitialized()) {
					int i_111_ = is_109_.length;
					for (int i_112_ = 0; i_112_ < i_111_; i_112_++)
						norm.data[is_109_[i_112_]]++;
				}
			} else
				is_110_ = _long;
			int i_113_ = 2147483647;
			if (bool_102_) {
				for (int i_114_ = 0; i_114_ < is_110_.length; i_114_++) {
					int i_115_ = a(is_109_, is, is_110_[i_114_], true);
					for (int i_116_ = 0; i_116_ < arraylist.size(); i_116_++) {
						Barcode var_aa = (Barcode) arraylist.get(i_116_);
						int i_117_ = var_aa.a(bool_101_ ? 0 : i_108_,
								bool_101_ ? i_108_ : 0, i_115_, is,
								bool_101_ ? 90 : 180, is_110_[i_114_]);
						if (i_117_ < i_113_)
							i_113_ = i_117_;
					}
				}
			}
			if (bool_103_) {
				for (int i_118_ = 0; i_118_ < is_110_.length; i_118_++) {
					int i_119_ = a(is_109_, is, is_110_[i_118_], false);
					for (int i_120_ = 0; i_120_ < arraylist.size(); i_120_++) {
						Barcode var_aa = (Barcode) arraylist.get(i_120_);
						int i_121_ = var_aa.a(bool_101_ ? width - 1 : i_108_,
								bool_101_ ? i_108_ : height - 1, i_119_, is,
								bool_101_ ? 270 : 360, is_110_[i_118_]);
						if (i_121_ < i_113_)
							i_113_ = i_121_;
					}
				}
			}
			i_113_ /= 8;
			if (i_113_ == 0)
				i_113_ = 1;
			if (i_113_ < i_99_) {
				i_107_ = 8;
				if (i_113_ < i_106_)
					i_106_ = i_113_;
			} else if (i_107_ > 0 && --i_107_ == 0)
				i_106_ = i_99_;
		}
		if (!norm.isInitialized()) {
			norm.init();
			if (norm.isTwoNonZeroes() && norm.between(i) != i) {
				i = norm.between(i);
				i = _if(arraylist, is, i, i_98_, i_99_);
			}
		}
		return i;
	}

	final int a(int i, int i_122_, int[] is, int i_123_, int i_124_) {
		is[0] = 0;
		int i_125_ = 0;
		boolean bool = true;
		boolean bool_126_ = norm.isInitialized();
		if (i_124_ == 90) {
			int[] is_127_ = intensityGetter.getRow(i_122_);
			if (relPos != null)
				relPos.comparePlus(i_122_, is_127_);
			for (int i_128_ = 0; i_128_ < width; i_128_++) {
				int i_129_ = is_127_[i_128_];
				if (!bool_126_)
					norm.data[i_129_]++;
				if (i_129_ >= i_123_ != bool) {
					bool = !bool;
					is[++i_125_] = 0;
				}
				is[i_125_]++;
			}
		} else if (i_124_ == 270) {
			int[] is_130_ = intensityGetter.getRow(i_122_);
			if (relPos != null)
				relPos.comparePlus(i_122_, is_130_);
			for (int i_131_ = 0; i_131_ < width; i_131_++) {
				int i_132_ = is_130_[width - 1 - i_131_];
				if (!bool_126_)
					norm.data[i_132_]++;
				if (i_132_ >= i_123_ != bool) {
					bool = !bool;
					is[++i_125_] = 0;
				}
				is[i_125_]++;
			}
		} else if (i_124_ == 180) {
			int[] is_133_ = intensityGetter.getCol(i);
			if (relPos != null)
				relPos.compareMinus(i, is_133_);
			for (int i_134_ = 0; i_134_ < height; i_134_++) {
				int i_135_ = is_133_[i_134_];
				if (!bool_126_)
					norm.data[i_135_]++;
				if (i_135_ >= i_123_ != bool) {
					bool = !bool;
					is[++i_125_] = 0;
				}
				is[i_125_]++;
			}
		} else {
			int[] is_136_ = intensityGetter.getCol(i);
			if (relPos != null)
				relPos.compareMinus(i, is_136_);
			for (int i_137_ = 0; i_137_ < height; i_137_++) {
				int i_138_ = is_136_[height - 1 - i_137_];
				if (!bool_126_)
					norm.data[i_138_]++;
				if (i_138_ >= i_123_ != bool) {
					bool = !bool;
					is[++i_125_] = 0;
				}
				is[i_125_]++;
			}
		}
		is[++i_125_] = 0;
		if (despeckleMore) {
			int i_139_ = 0;
			int i_140_ = 0;
			while (i_139_ <= i_125_) {
				if (is[i_139_ + 1] == 1) {
					is[i_140_] = is[i_139_] + 1 + is[i_139_ + 2];
					i_139_ += 3;
				} else {
					is[i_140_] = is[i_139_];
					i_139_++;
				}
				i_140_++;
			}
			i_125_ = i_140_;
		}
		return i_125_;
	}

	final int a(int[] is, int[] is_141_, int i, boolean bool) {
		is_141_[0] = 0;
		int i_142_ = 0;
		boolean bool_143_ = true;
		int i_144_ = is.length;
		if (bool) {
			for (int i_145_ = 0; i_145_ < i_144_; i_145_++) {
				if (is[i_145_] >= i != bool_143_) {
					bool_143_ = !bool_143_;
					is_141_[++i_142_] = 0;
				}
				is_141_[i_142_]++;
			}
		} else {
			for (int i_146_ = 0; i_146_ < i_144_; i_146_++) {
				if (is[i_144_ - 1 - i_146_] >= i != bool_143_) {
					bool_143_ = !bool_143_;
					is_141_[++i_142_] = 0;
				}
				is_141_[i_142_]++;
			}
		}
		is_141_[++i_142_] = 0;
		return i_142_;
	}

	final int _if(int i, int i_147_, int i_148_, int i_149_, int[] is,
			int i_150_, int i_151_) {
		return a(i, i_147_, i_148_, i_149_, is, i_150_, i_151_, true);
	}

	final int a(int i, int i_152_, int i_153_, int i_154_, int[] is,
			int i_155_, int i_156_, boolean bool) {
		int i_157_ = i_155_;
		boolean bool_158_;
		if (i_155_ == i_156_ || i_153_ - i + i_154_ - i_152_ == 0)
			bool_158_ = true;
		else
			bool_158_ = false;
		int i_159_ = Math.abs(i_153_ - i);
		int i_160_ = Math.abs(i_154_ - i_152_);
		boolean bool_161_ = i_159_ > i_160_;
		int i_162_;
		int i_163_;
		if (bool_161_) {
			i_162_ = i_159_;
			i_163_ = i_160_;
		} else {
			i_162_ = i_160_;
			i_163_ = i_159_;
		}
		int i_164_ = i_162_ / 2;
		int i_165_ = i;
		int i_166_ = i_152_;
		is[0] = 0;
		int i_167_ = 0;
		int i_168_ = 0;
		int i_169_ = 0;
		int i_170_ = 0;
		for (;;) {
			if (!bool_158_)
				i_157_ = i_156_
						+ ((i_155_ - i_156_)
								* (i_153_ - i_165_ + i_154_ - i_166_) / (i_153_
								- i + i_154_ - i_152_));
			int i_171_;
			if (i_165_ < 0 || i_166_ < 0 || i_165_ >= width || i_166_ >= height)
				i_171_ = 1;
			else
				i_171_ = a(i_165_, i_166_, i_157_);
			if (i_171_ != i_168_) {
				i_168_ = i_171_;
				if (is[i_167_] == 1 && i_167_ > 0 && bool
						&& _if(i_169_, i_170_, i_157_)) {
					i_167_--;
					is[i_167_]++;
				} else {
					i_167_++;
					is[i_167_] = 0;
				}
			}
			is[i_167_]++;
			i_169_ = i_165_;
			i_170_ = i_166_;
			i_164_ -= i_163_;
			boolean bool_172_ = false;
			if (i_164_ < 0) {
				bool_172_ = true;
				i_164_ += i_162_;
			}
			if (bool_161_) {
				if (i_165_ == i_153_)
					break;
				if (i_153_ > i)
					i_165_++;
				else
					i_165_--;
				if (bool_172_) {
					if (i_165_ < 0 || i_166_ < 0 || i_165_ >= width
							|| i_166_ >= height)
						i_171_ = 1;
					else
						i_171_ = a(i_165_, i_166_, i_157_);
					if (i_171_ != i_168_) {
						i_168_ = i_171_;
						if (is[i_167_] == 1 && i_167_ > 0 && bool
								&& _if(i_169_, i_170_, i_157_)) {
							i_167_--;
							is[i_167_]++;
						} else {
							i_167_++;
							is[i_167_] = 0;
						}
					}
					is[i_167_]++;
					i_169_ = i_165_;
					i_170_ = i_166_;
					if (i_154_ > i_152_)
						i_166_++;
					else
						i_166_--;
				}
			} else {
				if (i_166_ == i_154_)
					break;
				if (i_154_ > i_152_)
					i_166_++;
				else
					i_166_--;
				if (bool_172_) {
					if (i_165_ < 0 || i_166_ < 0 || i_165_ >= width
							|| i_166_ >= height)
						i_171_ = 1;
					else
						i_171_ = a(i_165_, i_166_, i_157_);
					if (i_171_ != i_168_) {
						i_168_ = i_171_;
						if (is[i_167_] == 1 && i_167_ > 0 && bool
								&& _if(i_169_, i_170_, i_157_)) {
							i_167_--;
							is[i_167_]++;
						} else {
							i_167_++;
							is[i_167_] = 0;
						}
					}
					is[i_167_]++;
					i_169_ = i_165_;
					i_170_ = i_166_;
					if (i_153_ > i)
						i_165_++;
					else
						i_165_--;
				}
			}
		}
		is[i_167_ + 1] = 0;
		return ++i_167_;
	}

	final int a(int i, int i_173_, int i_174_, int i_175_, int[] is,
			int i_176_, int i_177_) {
		int i_178_ = i_176_;
		boolean bool;
		if (i_176_ == i_177_ || i_174_ - i + i_175_ - i_173_ == 0)
			bool = true;
		else
			bool = false;
		int i_179_ = Math.abs(i_174_ - i);
		int i_180_ = Math.abs(i_175_ - i_173_);
		boolean bool_181_ = i_179_ > i_180_;
		int i_182_;
		int i_183_;
		if (bool_181_) {
			i_182_ = i_179_;
			i_183_ = i_180_;
		} else {
			i_182_ = i_180_;
			i_183_ = i_179_;
		}
		int i_184_ = i_182_ / 2;
		int i_185_ = i;
		int i_186_ = i_173_;
		is[0] = 0;
		int i_187_ = 0;
		int i_188_ = 0;
		boolean bool_189_ = false;
		boolean bool_190_ = false;
		for (;;) {
			if (!bool)
				i_178_ = i_177_
						+ ((i_176_ - i_177_)
								* (i_174_ - i_185_ + i_175_ - i_186_) / (i_174_
								- i + i_175_ - i_173_));
			int i_191_;
			if (i_185_ < 1 || i_186_ < 1 || i_185_ >= width - 1
					|| i_186_ >= height - 1)
				i_191_ = 1;
			else {
				i_191_ = a(i_185_, i_186_, i_178_);
				if (bool_181_) {
					i_191_ += a(i_185_, i_186_ - 1, i_178_);
					i_191_ += a(i_185_, i_186_ + 1, i_178_);
				} else {
					i_191_ += a(i_185_ - 1, i_186_, i_178_);
					i_191_ += a(i_185_ + 1, i_186_, i_178_);
				}
				if (i_191_ > 0)
					i_191_ = 1;
			}
			if (i_191_ != i_188_) {
				i_188_ = i_191_;
				i_187_++;
				is[i_187_] = 0;
			}
			is[i_187_]++;
			int i_192_ = i_185_;
			int i_193_ = i_186_;
			i_184_ -= i_183_;
			boolean bool_194_ = false;
			if (i_184_ < 0) {
				bool_194_ = true;
				i_184_ += i_182_;
			}
			if (bool_181_) {
				if (i_185_ == i_174_)
					break;
				if (i_174_ > i)
					i_185_++;
				else
					i_185_--;
				if (bool_194_) {
					if (i_175_ > i_173_)
						i_186_++;
					else
						i_186_--;
				}
			} else {
				if (i_186_ == i_175_)
					break;
				if (i_175_ > i_173_)
					i_186_++;
				else
					i_186_--;
				if (bool_194_) {
					if (i_174_ > i)
						i_185_++;
					else
						i_185_--;
				}
			}
		}
		is[i_187_ + 1] = 0;
		return ++i_187_;
	}

	private boolean _if(int i, int i_195_, int i_196_) {
		if (i <= 0 || i_195_ <= 0 || i >= width - 1 || i_195_ >= height - 1)
			return false;
		int i_197_ = a(i, i_195_, i_196_);
		if (a(i - 1, i_195_ - 1, i_196_) == i_197_)
			return false;
		if (a(i, i_195_ - 1, i_196_) == i_197_)
			return false;
		if (a(i + 1, i_195_ - 1, i_196_) == i_197_)
			return false;
		if (a(i - 1, i_195_, i_196_) == i_197_)
			return false;
		if (a(i + 1, i_195_, i_196_) == i_197_)
			return false;
		if (a(i - 1, i_195_ + 1, i_196_) == i_197_)
			return false;
		if (a(i, i_195_ + 1, i_196_) == i_197_)
			return false;
		if (a(i + 1, i_195_ + 1, i_196_) == i_197_)
			return false;
		return true;
	}

	final int a(int i, int i_198_, int i_199_) {
		return intensityGetter.getPixel(i, i_198_) < i_199_ ? 0 : 1;
	}

	final int a(int i, int i_200_) {
		if (i < 0 || i_200_ < 0 || i >= width || i_200_ >= height)
			return 255;
		return intensityGetter.getPixel(i, i_200_);
	}

	ImgManager a() {
		return intensityGetter;
	}

	protected void finalize() throws Throwable {
		norm.data = null;
		norm = null;
		super.finalize();
	}

	static {
		try {
			Class.forName("java.awt.image.Raster");
		} catch (ClassNotFoundException classnotfoundexception) {
			g = false;
		} catch (NoClassDefFoundError noclassdeffounderror) {
			g = false;
		}
		_long = new int[] { 32, 48, 64, 80, 96, 112, 144, 160, 176, 192, 208,
				224 };
	}
}
