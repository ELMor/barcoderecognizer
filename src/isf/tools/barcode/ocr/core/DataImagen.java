/* r - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferUShort;

final class DataImagen extends Imagen {
	private final short[] data;

	DataImagen(BufferedImage bufferedimage) {
		super(bufferedimage);
		java.awt.image.DataBuffer databuffer = bufferedimage.getRaster()
				.getDataBuffer();
		data = ((DataBufferUShort) databuffer).getData();
	}

	public final int getPixel(int i, int i_0_) {
		int i_1_ = data[g + i_0_ * slStride + i];
		if (i_1_ < 0)
			i_1_ += 65536;
		return i_1_ >> 8;
	}

	public int[] getRow(int i) {
		int i_2_ = this.getWidth();
		int i_3_ = g + i * slStride;
		for (int i_4_ = 0; i_4_ < i_2_; i_4_++) {
			int i_5_ = data[i_3_++];
			if (i_5_ < 0)
				i_5_ += 65536;
			xDim[i_4_] = i_5_ >> 8;
		}
		return xDim;
	}

	public int[] getCol(int i) {
		int i_6_ = this.getHeight();
		int i_7_ = g + i;
		for (int i_8_ = 0; i_8_ < i_6_; i_8_++) {
			int i_9_ = data[i_7_];
			if (i_9_ < 0)
				i_9_ += 65536;
			this.yDim[i_8_] = i_9_ >> 8;
			i_7_ += slStride;
		}
		return this.yDim;
	}
}
