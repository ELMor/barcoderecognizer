/* ap - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.image.BufferedImage;
import java.awt.image.ComponentSampleModel;
import java.awt.image.MultiPixelPackedSampleModel;
import java.awt.image.SinglePixelPackedSampleModel;

abstract class Imagen implements ImgManager {
	protected final int slStride;

	protected final int tX;

	protected final int tY;

	protected final int g;

	protected int[] yDim;

	protected int[] xDim;

	private BufferedImage bImage;

	Imagen(BufferedImage bufferedimage) {
		bImage = bufferedimage;
		java.awt.image.WritableRaster writableraster = bufferedimage
				.getRaster();
		tX = writableraster.getSampleModelTranslateX();
		tY = writableraster.getSampleModelTranslateY();
		java.awt.image.SampleModel sm = bufferedimage.getSampleModel();
		if (sm instanceof SinglePixelPackedSampleModel)
			slStride = ((SinglePixelPackedSampleModel) sm)
					.getScanlineStride();
		else if (sm instanceof ComponentSampleModel)
			slStride = ((ComponentSampleModel) sm).getScanlineStride();
		else if (sm instanceof MultiPixelPackedSampleModel)
			slStride = ((MultiPixelPackedSampleModel) sm).getScanlineStride();
		else
			slStride = 0;
		int i = 1;
		int biType = bufferedimage.getType();
		if (biType == 6 || biType == 7)
			i = 4;
		else if (biType == 5)
			i = 3;
		g = -tY * slStride - tX * i;
		this.yDim = new int[getHeight()];
		xDim = new int[getWidth()];
	}

	Imagen(BufferedImage bufferedimage, boolean bool) {
		bImage = bufferedimage;
		slStride = 0;
		tX = tY = 0;
		g = 0;
		yDim = new int[getHeight()];
		xDim = new int[getWidth()];
	}

	public abstract int getPixel(int i, int i_1_);

	public int getWidth() {
		return bImage.getWidth();
	}

	public int getHeight() {
		return bImage.getHeight();
	}

	public int[] getRow(int x) {
		int w = getWidth();
		for (int i = 0; i < w; i++)
			xDim[i] = getPixel(i, x);
		return xDim;
	}

	public int[] getCol(int i) {
		int i_4_ = getHeight();
		for (int i_5_ = 0; i_5_ < i_4_; i_5_++)
			this.yDim[i_5_] = getPixel(i, i_5_);
		return this.yDim;
	}
}
