/* w - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Rectangle;
import java.util.HashMap;

class w extends Barcode {

	private static final int J = 112;

	private static final int G = 1100;

	private static final int[] L = { 3211, 2221, 2122, 1411, 1132, 1231, 1114,
			1312, 1213, 3112 };

	private static final int[] K = { 1123, 1222, 2212, 1141, 2311, 1321, 4111,
			2131, 3121, 2113 };

	private static final String[] I = { "EEOOO", "EOEOO", "EOOEO", "EOOOE",
			"OEEOO", "OOEEO", "OOOEE", "OEOEO", "OEOOE", "OOEOE" };

	private static HashMap F = null;

	private int E;

	w(int i, PropertiesHolder var_c) {
		super(var_c);
		E = i;
	}

	int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		return 2147483647;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_4_) {
		return false;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	int code() {
		return E == 2 ? 11 : 12;
	}

	int _if(int i) {
		if (E == 2 && i == 13)
			return 2;
		if (E == 5 && i == 31)
			return 5;
		return -1;
	}

	boolean isCheck() {
		return false;
	}

	void a(Recognized var_bf, Recognizer var_a1, int[] is, PropertiesHolder var_c) {
		char[] cs = new char[E];
		int i = var_bf.direction();
		PointDouble var_ay = var_bf._new().getP1();
		PointDouble var_ay_5_ = var_bf._new().getP2();
		double d = var_bf._new()._else();
		double d_6_ = var_bf._new()._if();
		int i_7_ = var_bf._new()._do();
		Rectangle rectangle = var_bf.getRectangle();
		int i_8_ = Math.max(rectangle.width, rectangle.height);
		double d_9_ = Math.sqrt(d * d + 1.0);
		double d_10_ = d / d_9_;
		double d_11_ = 1.0 / d_9_;
		int i_12_ = (int) ((double) i_8_ * d_11_ * d_11_);
		int i_13_ = (int) ((double) i_8_ * d_11_ * d_10_);
		boolean bool = true;
		i_7_ *= d_11_;
		int i_14_ = var_bf._new().getBWBound();
		switch (i) {
		case 90:
			i_13_ = -i_13_;
			break;
		case 270:
			i_12_ = -i_12_;
			break;
		case 180: {
			bool = false;
			int i_15_ = i_12_;
			i_12_ = -i_13_;
			i_13_ = i_15_;
			break;
		}
		case 360: {
			bool = false;
			int i_16_ = i_12_;
			i_12_ = i_13_;
			i_13_ = -i_16_;
			break;
		}
		}
		int i_17_;
		int i_18_;
		if (bool) {
			i_17_ = (int) var_ay.getY();
			i_18_ = (int) var_ay_5_.getY();
		} else {
			i_17_ = (int) var_ay.getX();
			i_18_ = (int) var_ay_5_.getX();
		}
		for (int i_19_ = i_17_; i_19_ <= i_18_; i_19_++) {
			int i_20_;
			int i_21_;
			if (bool) {
				i_21_ = i_19_;
				i_20_ = (int) (d * (double) i_21_ + d_6_);
			} else {
				i_20_ = i_19_;
				i_21_ = (int) (d * (double) i_20_ + d_6_);
			}
			boolean bool_22_ = false;
			int i_23_ = var_a1._if(i_20_, i_21_, i_20_ + i_12_, i_21_ + i_13_,
					is, i_14_, i_14_);
			int i_24_ = E == 2 ? 13 : 31;
			if (i_23_ >= i_24_ + 1) {
				int i_25_ = 0;
				int i_26_;
				for (i_26_ = 0; i_26_ < i_23_; i_26_++) {
					i_25_ += is[i_26_];
					if (i_25_ >= i_7_ / 2)
						break;
				}
				if (i_26_ != i_23_ && i_26_ % 2 != 0 && is[i_26_] >= i_7_ / 3
						&& i_26_ + i_24_ + 2 <= i_23_
						&& is[i_26_ + i_24_ + 1] >= i_7_ / 4) {
					int i_27_;
					for (i_27_ = 1; i_27_ < i_24_ + 1
							&& is[i_26_ + i_27_] <= i_7_; i_27_++) {
						/* empty */
					}
					int i_28_;
					if (i_27_ >= i_24_ + 1
							&& a(0, is[i_26_], is, i_26_ + 1) == J
							&& (i_28_ = a(1, is[i_26_], is, i_26_ + 4)) >= 0) {
						int i_29_ = i_28_ < 10 ? i_28_ : i_28_ - 10;
						cs[0] = i_28_ < 10 ? 'O' : 'E';
						if (a(2, is[i_26_], is, i_26_ + 8) == G
								&& ((i_28_ = a(3, is[i_26_], is, i_26_ + 10)) >= 0)) {
							i_29_ = i_29_ * 10
									+ (i_28_ < 10 ? i_28_ : i_28_ - 10);
							cs[1] = i_28_ < 10 ? 'O' : 'E';
							if (E == 5) {
								if (a(4, is[i_26_], is, i_26_ + 14) != G
										|| (i_28_ = a(5, is[i_26_], is,
												i_26_ + 16)) < 0)
									continue;
								i_29_ = i_29_ * 10
										+ (i_28_ < 10 ? i_28_ : i_28_ - 10);
								cs[2] = i_28_ < 10 ? 'O' : 'E';
								if (a(6, is[i_26_], is, i_26_ + 20) != G
										|| (i_28_ = a(7, is[i_26_], is,
												i_26_ + 22)) < 0)
									continue;
								i_29_ = i_29_ * 10
										+ (i_28_ < 10 ? i_28_ : i_28_ - 10);
								cs[3] = i_28_ < 10 ? 'O' : 'E';
								if (a(8, is[i_26_], is, i_26_ + 26) != G
										|| (i_28_ = a(9, is[i_26_], is,
												i_26_ + 28)) < 0)
									continue;
								i_29_ = i_29_ * 10
										+ (i_28_ < 10 ? i_28_ : i_28_ - 10);
								cs[4] = i_28_ < 10 ? 'O' : 'E';
							}
							boolean bool_30_ = false;
							if (E == 2) {
								int i_31_ = i_29_ % 4;
								if (i_31_ == 0
										&& cs[0] == 'O'
										&& cs[1] == 'O'
										|| (i_31_ == 1 && cs[0] == 'O' && cs[1] == 'E')
										|| (i_31_ == 2 && cs[0] == 'E' && cs[1] == 'O')
										|| (i_31_ == 3 && cs[0] == 'E' && cs[1] == 'E'))
									bool_30_ = true;
							} else {
								int i_32_ = i_29_;
								int i_33_ = i_32_ % 10;
								i_32_ /= 10;
								int i_34_ = i_32_ % 10;
								i_32_ /= 10;
								i_33_ += i_32_ % 10;
								i_32_ /= 10;
								i_34_ += i_32_ % 10;
								i_32_ /= 10;
								i_33_ += i_32_ % 10;
								i_32_ /= 10;
								if (I[(3 * i_33_ + 9 * i_34_) % 10]
										.equals(new String(cs)))
									bool_30_ = true;
							}
							if (bool_30_ == true) {
								boolean bool_35_ = false;
								boolean bool_36_ = false;
								int i_37_;
								int i_38_;
								if (bool) {
									i_38_ = (int) ((double) i_25_ / (1.0 + Math
											.abs(d)));
									i_37_ = i_25_ - i_38_;
								} else {
									i_37_ = (int) ((double) i_25_ / (1.0 + Math
											.abs(d)));
									i_38_ = i_25_ - i_37_;
								}
								if (i_12_ < 0)
									i_38_ = -i_38_;
								if (i_13_ < 0)
									i_37_ = -i_37_;
								this.readData(i_20_ + i_38_, i_21_ + i_37_, is,
										i_26_ + 1, 7, i, i_29_, true, i_14_);
							}
						}
					}
				}
			}
		}
		this.init(-1);
		if (getAL1().size() != 0) {
			int i_39_ = 0;
			int i_40_ = 0;
			for (int i_41_ = 0; i_41_ < getAL1().size(); i_41_++) {
				int i_42_ = ((SepInten) getAL1().get(i_41_)).getScans().size();
				if (i_42_ > i_39_) {
					i_39_ = i_42_;
					i_40_ = i_41_;
				}
			}
			SepInten var_d = (SepInten) getAL1().get(i_40_);
			getAL1().clear();
			getAL1().add(var_d);
			var_d._for();
			var_d.a(var_a1, is, var_c);
			SepInten var_d_43_ = new SepInten(E, var_d);
			String string = a("" + var_d._try());
			Recognized var_bf_44_ = new Recognized(string, -1, var_d, var_d_43_, code(), var_c);
			var_bf_44_.a(string);
			var_bf_44_.setNumberOfProps(i_39_);
			var_bf_44_.a();
			if (var_bf.getSupplemental() == null
					|| var_bf.getSupplemental().getNumberOfProps() <= var_bf_44_.getNumberOfProps())
				var_bf.setSupplemental(var_bf_44_);
		}
	}

	private final int a(int i, int i_45_, int[] is, int i_46_) {
		int i_47_;
		int i_48_;
		if (i == 0) {
			i_47_ = 3;
			i_48_ = 4;
		} else if (i % 2 == 0) {
			i_47_ = 2;
			i_48_ = 2;
		} else {
			i_47_ = 4;
			i_48_ = 7;
		}
		int i_49_ = 0;
		int[] is_50_ = new int[i_47_];
		for (int i_51_ = 0; i_51_ < i_47_; i_51_++) {
			is_50_[i_51_] = is[i_46_ + i_51_] * i_48_;
			i_49_ += is[i_46_ + i_51_];
			if (i_49_ > i_45_)
				return -1;
		}
		int[] is_52_ = new int[i_47_];
		int[] is_53_ = new int[i_47_];
		for (int i_54_ = 0; i_54_ < i_47_; i_54_++)
			is_52_[i_54_] = is_50_[i_54_];
		for (int i_55_ = 0; i_55_ < i_47_; i_55_++) {
			int i_56_ = is_52_[i_55_] / i_49_;
			int i_57_ = is_52_[i_55_] % i_49_;
			if (i_56_ == 0 || i_57_ > i_49_ / 2) {
				i_56_++;
				i_57_ -= i_49_;
			}
			is_53_[i_55_] = i_57_;
			if (i_55_ < i_47_ - 1)
				is_52_[i_55_ + 1] += i_57_;
			is_52_[i_55_] = i_56_;
		}
		if (i == 0) {
			int i_58_ = is_52_[0] * 100 + is_52_[1] * 10 + is_52_[2];
			return i_58_ == J ? J : -1;
		}
		if (i % 2 == 0) {
			int i_59_ = is_52_[0] * 10 + is_52_[1];
			return i_59_ == 11 ? G : -1;
		}
		Object object = F.get(new Integer(is_52_[0] * 1000 + is_52_[1] * 100
				+ is_52_[2] * 10 + is_52_[3]));
		if (object != null)
			return ((Integer) object).intValue();
		int i_60_ = 0;
		int i_61_ = Math.abs(is_53_[0]);
		for (int i_62_ = 1; i_62_ < i_47_ - 1; i_62_++) {
			if (Math.abs(is_53_[i_62_]) > i_61_) {
				i_61_ = Math.abs(is_53_[i_62_]);
				i_60_ = i_62_;
			}
		}
		for (int i_63_ = 0; i_63_ < i_47_; i_63_++)
			is_52_[i_63_] = is_50_[i_63_];
		for (int i_64_ = 0; i_64_ < i_47_; i_64_++) {
			int i_65_ = is_52_[i_64_] / i_49_;
			int i_66_ = is_52_[i_64_] % i_49_;
			if (i_65_ == 0) {
				i_65_++;
				i_66_ -= i_49_;
			} else if (i_64_ == i_60_) {
				if (i_66_ <= i_49_ / 2) {
					i_65_++;
					i_66_ -= i_49_;
				}
			} else if (i_66_ > i_49_ / 2) {
				i_65_++;
				i_66_ -= i_49_;
			}
			if (i_64_ < i_47_ - 1)
				is_52_[i_64_ + 1] += i_66_;
			is_52_[i_64_] = i_65_;
		}
		object = F.get(new Integer(is_52_[0] * 1000 + is_52_[1] * 100
				+ is_52_[2] * 10 + is_52_[3]));
		if (object != null)
			return ((Integer) object).intValue();
		for (int i_67_ = 0; i_67_ < i_47_; i_67_++)
			is_52_[i_67_] = is_50_[i_67_];
		for (int i_68_ = i_47_ - 1; i_68_ >= 0; i_68_--) {
			int i_69_ = is_52_[i_68_] / i_49_;
			int i_70_ = is_52_[i_68_] % i_49_;
			if (i_69_ == 0 || i_70_ > i_49_ / 2) {
				i_69_++;
				i_70_ -= i_49_;
			}
			is_53_[i_68_] = i_70_;
			if (i_68_ > 0)
				is_52_[i_68_ - 1] += i_70_;
			is_52_[i_68_] = i_69_;
		}
		object = F.get(new Integer(is_52_[0] * 1000 + is_52_[1] * 100
				+ is_52_[2] * 10 + is_52_[3]));
		if (object != null)
			return ((Integer) object).intValue();
		i_60_ = i_47_ - 1;
		i_61_ = Math.abs(is_53_[i_47_ - 1]);
		for (int i_71_ = i_47_ - 1; i_71_ > 0; i_71_--) {
			if (Math.abs(is_53_[i_71_]) > i_61_) {
				i_61_ = Math.abs(is_53_[i_71_]);
				i_60_ = i_71_;
			}
		}
		for (int i_72_ = 0; i_72_ < i_47_; i_72_++)
			is_52_[i_72_] = is_50_[i_72_];
		for (int i_73_ = i_47_ - 1; i_73_ >= 0; i_73_--) {
			int i_74_ = is_52_[i_73_] / i_49_;
			int i_75_ = is_52_[i_73_] % i_49_;
			if (i_74_ == 0) {
				i_74_++;
				i_75_ -= i_49_;
			} else if (i_73_ == i_60_) {
				if (i_75_ <= i_49_ / 2) {
					i_74_++;
					i_75_ -= i_49_;
				}
			} else if (i_75_ > i_49_ / 2) {
				i_74_++;
				i_75_ -= i_49_;
			}
			if (i_73_ > 0)
				is_52_[i_73_ - 1] += i_75_;
			is_52_[i_73_] = i_74_;
		}
		object = F.get(new Integer(is_52_[0] * 1000 + is_52_[1] * 100
				+ is_52_[2] * 10 + is_52_[3]));
		if (object != null)
			return ((Integer) object).intValue();
		return -1;
	}

	String a(String string) {
		for (/**/; string.length() < E; string = "0" + string) {
			/* empty */
		}
		return string;
	}

	static {
		F = new HashMap();
		for (int i = 0; i < 10; i++)
			F.put(new Integer(L[i]), new Integer(i));
		for (int i = 0; i < 10; i++)
			F.put(new Integer(K[i]), new Integer(i + 10));
	}
}
