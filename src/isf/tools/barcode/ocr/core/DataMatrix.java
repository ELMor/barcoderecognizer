/* ao - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

class DataMatrix extends Barcode {
	private static final boolean aX = false;

	private static final int aT = 100;

	private static final int aR = 1;

	private static final double[] aP = new double[200];

	private static final double[] a1 = new double[200];

	private static final double[] a2 = new double[200];

	private int aV;

	private int aO;

	private int a0;

	private int aZ;

	static final int aN = 3;

	static final int aU = 0;

	static final int aQ = 1;

	static final int aW = 2;

	static final int aS = 4;

	private static final int aY = 50;

	class a implements Iterator {
		private int a;

		private Iterator _if;

		a(int[][] is, Point point) {
			int i = -1;
			int i_0_ = -1;
			int i_1_ = -1;
			for (int i_2_ = 0; i_2_ < 200; i_2_++) {
				for (int i_3_ = 0; i_3_ < is[0].length; i_3_++) {
					if (is[i_2_][i_3_] > i)
						i = is[i_0_ = i_2_][i_1_ = i_3_];
				}
			}
			int i_4_ = i_0_ - 50;
			if (i_4_ < 0)
				i_4_ += 200;
			int i_5_ = i_0_ + 50;
			if (i_5_ >= 200)
				i_5_ -= 200;
			int i_6_ = -1;
			int i_7_ = -1;
			for (int i_8_ = 0; i_8_ < is[i_4_].length; i_8_++) {
				if (is[i_4_][i_8_] > i_6_)
					i_6_ = is[i_4_][i_7_ = i_8_];
			}
			int i_9_ = -1;
			int i_10_ = -1;
			for (int i_11_ = 0; i_11_ < is[i_5_].length; i_11_++) {
				if (is[i_5_][i_11_] > i_9_)
					i_9_ = is[i_5_][i_10_ = i_11_];
			}
			int i_12_;
			int i_13_;
			int i_14_;
			if (i_6_ > i_9_) {
				i_12_ = i_7_;
				i_13_ = i_1_;
				i_14_ = i_4_;
			} else {
				i_12_ = i_1_;
				i_13_ = i_10_;
				i_14_ = i_0_;
			}
			int i_15_ = i_14_ / 50;
			a = i_14_ % 50;
			int i_16_ = i_14_ + 100;
			int i_17_ = i_16_ + 50;
			if (i_16_ >= 200)
				i_16_ -= 200;
			if (i_17_ >= 200)
				i_17_ -= 200;
			int[] is_18_ = is[i_16_];
			int[] is_19_ = is[i_17_];
			int i_20_ = -1;
			for (int i_21_ = 0; i_21_ < is_18_.length; i_21_++) {
				if (is_18_[i_21_] > i_20_)
					i_20_ = is_18_[i_21_];
			}
			int i_22_ = -1;
			for (int i_23_ = 0; i_23_ < is_19_.length; i_23_++) {
				if (is_19_[i_23_] > i_22_)
					i_22_ = is_19_[i_23_];
			}
			ArrayList arraylist = new ArrayList();
			int i_24_ = 6;
			do {
				for (int i_25_ = 1; i_25_ < is_18_.length; i_25_++) {
					if (is_18_[i_25_ - 1] > 1
							&& is_18_[i_25_ - 1] >= i_20_ / i_24_
							&& is_18_[i_25_] < i_20_ / i_24_) {
						int i_26_ = i_12_ + i_25_;
						for (int i_27_ = 1; i_27_ < is_19_.length; i_27_++) {
							if (is_19_[i_27_ - 1] > 1
									&& is_19_[i_27_ - 1] >= i_22_ / i_24_
									&& is_19_[i_27_] < i_22_ / i_24_) {
								int i_28_ = i_13_ + i_27_;
								if (Math.abs(i_28_ - i_26_) < (i_28_ + i_26_) / 20) {
									int[] is_29_ = new int[4];
									int i_30_ = i_15_;
									is_29_[i_30_] = i_12_;
									if (++i_30_ == 4)
										i_30_ = 0;
									is_29_[i_30_] = i_13_;
									if (++i_30_ == 4)
										i_30_ = 0;
									is_29_[i_30_] = i_25_;
									if (++i_30_ == 4)
										i_30_ = 0;
									is_29_[i_30_] = i_27_;
									if (++i_30_ == 4) {
										boolean bool = false;
									}
									int i_31_;
									for (i_31_ = 0; i_31_ < arraylist.size(); i_31_++) {
										int[] is_32_ = (int[]) arraylist
												.get(i_31_);
										if (Arrays.equals(is_32_, is_29_))
											break;
									}
									if (i_31_ >= arraylist.size())
										arraylist.add(is_29_);
								}
							}
						}
					}
				}
			} while (--i_24_ > 3);
			_if = arraylist.iterator();
		}

		int a() {
			return a;
		}

		public boolean hasNext() {
			return _if.hasNext();
		}

		public Object next() {
			return _if.next();
		}

		public void remove() {
			_if.remove();
		}
	}

	DataMatrix(int i, int i_33_, int i_34_, PropertiesHolder var_c) {
		super(var_c);
		aV = i;
		aO = i_33_;
		a0 = aZ = 20;
		if (i_34_ > 10)
			a0 = aZ = 2 * i_34_;
		if (i < 300 && i_33_ < 300)
			a0 = aZ = 10;
	}

	int code() {
		return 5;
	}

	boolean isCheck() {
		return true;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	int _if(int i) {
		return -1;
	}

	String a(String string) {
		return "";
	}

	final int a(int i, int i_35_, int i_36_, int[] is, int i_37_, int i_38_) {
		int i_39_ = 2147483647;
		return i_39_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_40_) {
		return false;
	}

	void read(Recognizer var_a1, int i, ArrayList arraylist, Normalize var_a8, int i_41_,
			String string) {
		if (aV >= a0 && aO >= aZ) {
			int i_42_ = aO / aZ;
			if (aO % aZ > 1)
				i_42_++;
			int i_43_ = aV / a0;
			if (aV % a0 > 1)
				i_43_++;
			int[][] is = new int[i_42_][i_43_];
			i = a(var_a1, i, i_42_, i_43_, is, var_a8);
			int[][] is_44_ = new int[i_42_][i_43_];
			int i_45_ = Math.min(i_43_, i_42_);
			if (i_45_ > 10)
				i_45_ = 10;
			for (int i_46_ = 1; i_46_ <= i_45_; i_46_++)
				a(is, i_46_, is_44_);
			Rectangle rectangle = new Rectangle(0, 0, aV, aO);
			int i_47_ = -1;
			int i_48_ = -1;
			ArrayList arraylist_49_ = new ArrayList();
			int i_50_ = 0;
			ArrayList arraylist_51_ = new ArrayList();
			for (;;) {
				if (i_47_ != -1)
					is_44_[i_48_][i_47_] = 0;
				Point point = a(is_44_);
				i_47_ = point.x;
				i_48_ = point.y;
				if (is_44_[i_48_][i_47_] == 0)
					break;
				Rectangle rectangle_52_ = a(i_47_, i_48_, is, var_a8);
				if (rectangle_52_ != null) {
					if (rectangle_52_.width == 0 && rectangle_52_.height == 0)
						break;
					Iterator iterator = arraylist_51_.iterator();
					while_6_: do {
						Rectangle rectangle_53_;
						do {
							if (!iterator.hasNext())
								break while_6_;
							rectangle_53_ = (Rectangle) iterator.next();
						} while (!rectangle_53_.equals(rectangle_52_));
						iterator = null;
					} while (false);
					if (iterator != null) {
						arraylist_51_.add(rectangle_52_);
						Rectangle rectangle_54_ = new Rectangle(rectangle_52_.x
								* a0, rectangle_52_.y * aZ, rectangle_52_.width
								* a0, rectangle_52_.height * aZ);
						rectangle_54_ = rectangle_54_.intersection(rectangle);
						Point point_55_ = new Point(
								(rectangle_54_.x + rectangle_54_.width / 2),
								(rectangle_54_.y + rectangle_54_.height / 2));
						int i_56_ = _do(var_a1, i, rectangle_54_, point_55_);
						int[][] is_57_ = a(var_a1, i, i_56_, rectangle_54_,
								point_55_);
						a3 var_a3 = null;
						Poligono var_i = null;
						Rectangle rectangle_58_ = null;
						for (int i_59_ = 2; i_59_ < 4; i_59_++) {
							var_a3 = null;
							Object object = null;
							Object object_60_ = null;
							int[] is_61_ = new int[4];
							for (int i_62_ = 0; i_62_ < 4; i_62_++)
								is_61_[i_62_] = a(is_57_[i_62_], i_59_);
							boolean bool = false;
							for (int i_63_ = 0; i_63_ < 4; i_63_++) {
								for (int i_64_ = is_61_[i_63_]; i_64_ < is_57_[i_63_].length; i_64_++) {
									int i_65_ = is_57_[i_63_][i_64_];
									if (i_65_ > 0) {
										bool = true;
										break;
									}
								}
								if (bool)
									break;
							}
							var_i = a(point_55_, i_56_, is_61_);
							if (bool) {
								int[][] is_66_ = a(var_a1, i, i_56_, var_i,
										point_55_);
								for (int i_67_ = 0; i_67_ < 4; i_67_++)
									is_61_[i_67_] = a(is_66_[i_67_], i_59_);
								var_i = a(point_55_, i_56_, is_61_);
							}
							rectangle_58_ = new Rectangle(0, 0, a0, aZ);
							for (int i_68_ = 0; i_68_ < i_43_; i_68_++) {
								for (int i_69_ = 0; i_69_ < i_42_; i_69_++) {
									rectangle_58_.x = i_68_ * a0;
									rectangle_58_.y = i_69_ * aZ;
									if (var_i.contiene(rectangle_58_))
										is_44_[i_69_][i_68_] = 0;
								}
							}
							int i_70_ = is_61_[0] + is_61_[2];
							int i_71_ = is_61_[1] + is_61_[3];
							if (i_70_ >= 10 && i_71_ >= 10) {
								double d = ((double) Math.max(i_70_, i_71_) / (double) Math
										.min(i_70_, i_71_));
								if ((!(d > 1.3) || !(d < 1.7)) && !(d > 4.8)) {
									for (int i_72_ = 0; i_72_ < arraylist_49_
											.size(); i_72_++) {
										Polygon polygon = ((Polygon) arraylist_49_
												.get(i_72_));
										if (a(polygon, var_i)) {
											var_i = null;
											break;
										}
									}
									if (var_i != null) {
										Rectangle rectangle_73_ = var_i
												.getBounds();
										var_a3 = a(var_a1, i, var_i);
										if (var_a3 != null
												&& (!var_a3._if() || var_a3.n
														.length() == 0)) {
											var_a3._do();
											var_a3 = null;
										}
									}
								}
							}
							if (var_a3 != null)
								break;
						}
						if (var_a3 != null
								&& (string == null || string.length() == 0 || var_a3.n
										.startsWith(string))) {
							arraylist.add(new Recognized(var_a3.n, var_i, var_a3._void,
									5, i, var_a3.l, prop));
							if (i_41_ > 0 && ++i_50_ >= i_41_)
								break;
							arraylist_49_.add(var_i);
							for (int i_74_ = 0; i_74_ < i_43_; i_74_++) {
								for (int i_75_ = 0; i_75_ < i_42_; i_75_++) {
									rectangle_58_.x = i_74_ * a0;
									rectangle_58_.y = i_75_ * aZ;
									if (var_i.contiene(rectangle_58_))
										is[i_75_][i_74_] = 0;
								}
							}
							var_a3._do();
							Object object = null;
						}
					}
				}
			}
		}
	}

	void a(Recognizer var_a1, int i, ArrayList arraylist, Normalize var_a8, int i_76_) {
		if (aV >= a0 && aO >= aZ) {
			int i_77_ = aO / aZ;
			if (aO % aZ > 1)
				i_77_++;
			int i_78_ = aV / a0;
			if (aV % a0 > 1)
				i_78_++;
			int[][] is = new int[i_77_][i_78_];
			i = a(var_a1, i, i_77_, i_78_, is, var_a8);
			ArrayList arraylist_79_ = new ArrayList();
			int i_80_ = 0;
			Rectangle rectangle = new Rectangle(0, 0, aV, aO);
			Rectangle rectangle_81_ = rectangle;
			Point point = new Point(rectangle_81_.x + rectangle_81_.width / 2,
					rectangle_81_.y + rectangle_81_.height / 2);
			int[][] is_82_ = _if(var_a1, i, rectangle_81_, point);
			a var_a = new a(is_82_, point);
			int i_83_ = var_a.a();
			while (var_a.hasNext()) {
				int[] is_84_ = (int[]) var_a.next();
				if (is_84_[0] != 0 && is_84_[1] != 0 && is_84_[2] != 0
						&& is_84_[3] != 0) {
					Poligono var_i = a(point, i_83_, is_84_);
					int i_85_ = is_84_[0] + is_84_[2];
					int i_86_ = is_84_[1] + is_84_[3];
					if (i_85_ >= 10 && i_86_ >= 10) {
						double d = ((double) Math.max(i_85_, i_86_) / (double) Math
								.min(i_85_, i_86_));
						if ((!(d > 1.2) || !(d < 1.7)) && !(d > 4.8)) {
							Rectangle rectangle_87_ = var_i.getBounds();
							Object object = null;
							a3 var_a3 = a(var_a1, i, var_i);
							if (var_a3 != null) {
								if (!var_a3._if()) {
									var_a3._do();
									object = null;
								} else if (var_a3.n.length() == 10) {
									arraylist.add(new Recognized(var_a3.n, var_i,
											var_a3._void, 5, i, var_a3.l, prop));
									if (i_76_ > 0 && ++i_80_ >= i_76_)
										break;
									arraylist_79_.add(var_i);
									var_a3._do();
									object = null;
								}
							}
						}
					}
				}
			}
		}
	}

	private int[][] _if(Recognizer var_a1, int i, Rectangle rectangle, Point point) {
		int i_88_ = rectangle.x;
		int i_89_ = rectangle.y;
		int i_90_ = rectangle.width;
		int i_91_ = rectangle.height;
		int i_92_ = i_88_ + rectangle.width;
		int i_93_ = i_89_ + rectangle.height;
		int i_94_ = point.x;
		int i_95_ = point.y;
		int i_96_ = i_94_ - i_88_;
		if (i_92_ - i_94_ > i_96_)
			i_96_ = i_92_ - i_94_;
		if (i_95_ - i_89_ > i_96_)
			i_96_ = i_95_ - i_89_;
		if (i_93_ - i_95_ > i_96_)
			i_96_ = i_93_ - i_95_;
		i_96_++;
		int[][] is = new int[200][i_96_];
		int i_97_ = -1;
		for (int i_98_ = i_89_ + 1; i_98_ < i_93_ - 1; i_98_++) {
			if (i_98_ == i_95_)
				i_97_ = 1;
			int i_99_ = -1;
			int i_100_ = i_88_ + 1;
			int i_101_ = i_92_;
			for (int i_102_ = i_100_; i_102_ < i_101_ - 1; i_102_++) {
				if (i_102_ == i_94_)
					i_99_ = 1;
				if (var_a1.a(i_102_, i_98_) < i) {
					boolean bool = var_a1.a(i_102_ + i_99_, i_98_) >= i;
					boolean bool_103_ = var_a1.a(i_102_, i_98_ + i_97_) >= i;
					if (bool || bool_103_) {
						for (int i_104_ = 0; i_104_ < 100; i_104_++) {
							double d = ((double) (i_102_ - i_94_) * a1[i_104_] + (double) (i_98_ - i_95_)
									* aP[i_104_]);
							int i_105_ = i_104_;
							if (d < 0.0) {
								d = -d;
								i_105_ += 100;
							}
							int i_106_ = (int) d;
							if (i_106_ < i_96_)
								is[i_105_ / 1][i_106_]++;
						}
					}
				}
			}
		}
		return is;
	}

	private int _do(Recognizer var_a1, int i, Rectangle rectangle, Point point) {
		int[][][] is = a(var_a1, i, rectangle, point);
		int i_107_ = a(is, rectangle, false) * 1;
		return i_107_;
	}

	private int a(int[][][] is, Rectangle rectangle, boolean bool) {
		int i = is[0].length;
		int[] is_108_ = new int[i];
		for (int i_109_ = 0; i_109_ < i; i_109_++) {
			for (int i_110_ = 0; i_110_ < 4; i_110_++) {
				int i_111_ = 0;
				for (int i_112_ = 0; i_112_ < is[i_110_][0].length; i_112_++) {
					if (is[i_110_][i_109_][i_112_] > i_111_)
						i_111_ = is[i_110_][i_109_][i_112_];
				}
				is_108_[i_109_] += i_111_;
			}
		}
		int i_113_ = 0;
		int i_114_ = 0;
		if (bool) {
			for (int i_115_ = 0; i_115_ < i; i_115_++) {
				if (is_108_[i_115_] > i_114_) {
					i_114_ = is_108_[i_115_];
					i_113_ = i_115_;
				}
			}
			return i_113_;
		}
		int[] is_116_ = new int[i / 4];
		for (int i_117_ = 0; i_117_ < i / 4; i_117_++)
			is_116_[i_117_] = (is_108_[i_117_] + is_108_[i_117_ + i / 4]
					+ is_108_[i_117_ + i / 2] + is_108_[i_117_ + 3 * i / 4]);
		for (int i_118_ = 0; i_118_ < i / 4; i_118_++) {
			if (is_116_[i_118_] > i_114_)
				i_114_ = is_116_[i_113_ = i_118_];
		}
		return i_113_;
	}

	private boolean a(Polygon polygon, Polygon polygon_119_) {
		if (!polygon.getBounds().intersects(polygon_119_.getBounds()))
			return false;
		for (int i = 0; i < polygon.npoints; i++) {
			if (polygon_119_.contains(polygon.xpoints[i], polygon.ypoints[i]))
				return true;
		}
		for (int i = 0; i < polygon_119_.npoints; i++) {
			if (polygon.contains(polygon_119_.xpoints[i], polygon.ypoints[i]))
				return true;
		}
		return false;
	}

	private int a(int[] is, int i) {
		int i_120_ = is[0];
		int i_121_;
		for (i_121_ = 1; i_121_ < is.length; i_121_++) {
			int i_122_ = is[i_121_];
			if (i_122_ <= i_120_ / i_121_ / 25
					&& (i_121_ >= is.length - 1 || is[i_121_ + 1] < i_120_
							/ i_121_ / 25))
				break;
			i_120_ += i_122_;
		}
		i_120_ /= i_121_;
		do {
			if (--i_121_ < 0)
				break;
		} while (is[i_121_] <= i_120_ / i);
		return i_121_ + 1;
	}

	private Poligono a(Point point, int i, int[] is) {
		int[] is_123_ = new int[4];
		int[] is_124_ = new int[4];
		double d = a1[i];
		double d_125_ = aP[i];
		double d_126_ = (double) is[0] * d - (double) is[1] * d_125_;
		int i_127_;
		if (d_126_ < 0.0)
			i_127_ = (int) Math.floor(d_126_);
		else
			i_127_ = (int) Math.ceil(d_126_);
		is_123_[0] = point.x + i_127_;
		d_126_ = (double) is[0] * d_125_ + (double) is[1] * d;
		if (d_126_ < 0.0)
			i_127_ = (int) Math.floor(d_126_);
		else
			i_127_ = (int) Math.ceil(d_126_);
		is_124_[0] = point.y + i_127_;
		d_126_ = (double) -is[1] * d_125_ - (double) is[2] * d;
		if (d_126_ < 0.0)
			i_127_ = (int) Math.floor(d_126_);
		else
			i_127_ = (int) Math.ceil(d_126_);
		is_123_[1] = point.x + i_127_;
		d_126_ = (double) is[1] * d - (double) is[2] * d_125_;
		if (d_126_ < 0.0)
			i_127_ = (int) Math.floor(d_126_);
		else
			i_127_ = (int) Math.ceil(d_126_);
		is_124_[1] = point.y + i_127_;
		d_126_ = (double) -is[2] * d + (double) is[3] * d_125_;
		if (d_126_ < 0.0)
			i_127_ = (int) Math.floor(d_126_);
		else
			i_127_ = (int) Math.ceil(d_126_);
		is_123_[2] = point.x + i_127_;
		d_126_ = (double) -is[2] * d_125_ - (double) is[3] * d;
		if (d_126_ < 0.0)
			i_127_ = (int) Math.floor(d_126_);
		else
			i_127_ = (int) Math.ceil(d_126_);
		is_124_[2] = point.y + i_127_;
		d_126_ = (double) is[3] * d_125_ + (double) is[0] * d;
		if (d_126_ < 0.0)
			i_127_ = (int) Math.floor(d_126_);
		else
			i_127_ = (int) Math.ceil(d_126_);
		is_123_[3] = point.x + i_127_;
		d_126_ = (double) -is[3] * d + (double) is[0] * d_125_;
		if (d_126_ < 0.0)
			i_127_ = (int) Math.floor(d_126_);
		else
			i_127_ = (int) Math.ceil(d_126_);
		is_124_[3] = point.y + i_127_;
		return new Poligono(is_123_, is_124_, 4);
	}

	private int a(Recognizer var_a1, int i, int i_128_, int i_129_, int[][] is,
			Normalize var_a8) {
		int i_130_ = 0;
		boolean bool = var_a8.isInitialized();
		for (int i_131_ = 0; i_131_ < i_128_; i_131_++) {
			for (int i_132_ = 0; i_132_ < i_129_; i_132_++) {
				int i_133_ = 0;
				int i_134_ = 0;
				for (int i_135_ = 0; i_135_ < aZ; i_135_ += 3) {
					if (i_135_ == 0)
						i_134_ = i_131_ * aZ;
					else
						i_134_ += 3;
					if (i_134_ >= aO - 1)
						break;
					for (int i_136_ = 0; i_136_ < a0; i_136_ += 3) {
						if (i_136_ == 0)
							i_130_ = i_132_ * a0;
						else
							i_130_ += 3;
						if (i_130_ >= aV - 1)
							break;
						i_133_++;
						int i_137_ = var_a1.a(i_130_, i_134_);
						if (!bool)
							var_a8.data[i_137_]++;
						if (i_137_ >= i)
							is[i_131_][i_132_] += 255;
					}
				}
				is[i_131_][i_132_] /= i_133_;
			}
		}
		if (!var_a8.isInitialized()) {
			var_a8.init();
			if (var_a8.isTwoNonZeroes() && var_a8.between(i) != i) {
				i = var_a8.between(i);
				for (int i_138_ = 0; i_138_ < i_128_; i_138_++) {
					for (int i_139_ = 0; i_139_ < i_129_; i_139_++)
						is[i_138_][i_139_] = 0;
				}
				i = a(var_a1, i, i_128_, i_129_, is, var_a8);
			}
		}
		return i;
	}

	private void a(int[][] is, int i, int[][] is_140_) {
		int i_141_ = is.length;
		int i_142_ = is[0].length;
		int[][] is_143_ = new int[i_141_ - i + 1][i_142_ - i + 1];
		int[] is_144_ = new int[4];
		int[] is_145_ = new int[4];
		int i_146_ = i * i;
		int i_147_ = i / 2;
		for (int i_148_ = 0; i_148_ <= i_141_ - i; i_148_++) {
			for (int i_149_ = 0; i_149_ <= i_142_ - i; i_149_++) {
				for (int i_150_ = 0; i_150_ < 4; i_150_++) {
					is_144_[i_150_] = 0;
					is_145_[i_150_] = 0;
				}
				for (int i_151_ = 0; i_151_ < i_147_; i_151_++) {
					for (int i_152_ = 0; i_152_ < i_147_; i_152_++) {
						is_144_[0] += is[i_148_ + i_151_][i_149_ + i_152_];
						is_145_[0]++;
					}
					for (int i_153_ = i_147_; i_153_ < i; i_153_++) {
						is_144_[1] += is[i_148_ + i_151_][i_149_ + i_153_];
						is_145_[1]++;
					}
				}
				for (int i_154_ = i_147_; i_154_ < i; i_154_++) {
					for (int i_155_ = 0; i_155_ < i / 2; i_155_++) {
						is_144_[2] += is[i_148_ + i_154_][i_149_ + i_155_];
						is_145_[2]++;
					}
					for (int i_156_ = i_147_; i_156_ < i; i_156_++) {
						is_144_[3] += is[i_148_ + i_154_][i_149_ + i_156_];
						is_145_[3]++;
					}
				}
				boolean bool = false;
				for (int i_157_ = 0; i_157_ < 4; i_157_++) {
					if (i != 1 || is_145_[i_157_] != 0) {
						int i_158_ = is_144_[i_157_] / is_145_[i_157_];
						if (i_158_ < 50 || i_158_ > 205) {
							bool = true;
							break;
						}
						is_143_[i_148_][i_149_] += is_144_[i_157_];
					}
				}
				if (bool)
					is_143_[i_148_][i_149_] = 0;
				else {
					is_143_[i_148_][i_149_] /= i_146_;
					if (is_143_[i_148_][i_149_] > 127)
						is_143_[i_148_][i_149_] = 255 - is_143_[i_148_][i_149_];
					int i_159_ = is_143_[i_148_][i_149_] / i_146_;
					for (int i_160_ = 0; i_160_ < i; i_160_++) {
						for (int i_161_ = 0; i_161_ < i; i_161_++)
							is_140_[i_148_ + i_160_][i_149_ + i_161_] += i_159_;
					}
				}
			}
		}
	}

	private Point a(int[][] is) {
		int i = is[0].length;
		int i_162_ = is.length;
		int i_163_ = 0;
		int i_164_ = 0;
		int i_165_ = 0;
		for (int i_166_ = 0; i_166_ < i_162_; i_166_++) {
			for (int i_167_ = 0; i_167_ < i; i_167_++) {
				if (is[i_166_][i_167_] > i_163_)
					i_163_ = is[i_165_ = i_166_][i_164_ = i_167_];
			}
		}
		return new Point(i_164_, i_165_);
	}

	private Rectangle a(int i, int i_168_, int[][] is, Normalize var_a8) {
		int i_169_ = is.length;
		int i_170_ = is[0].length;
		int i_171_ = 1;
		int i_172_ = 1;
		int i_173_;
		for (;;) {
			i_173_ = 0;
			for (int i_174_ = i_168_; i_174_ < i_168_ + i_172_; i_174_++) {
				for (int i_175_ = i; i_175_ < i + i_171_; i_175_++)
					i_173_ += is[i_174_][i_175_];
			}
			i_173_ /= i_172_ * i_171_;
			if (i_173_ > 127)
				i_173_ = 255 - i_173_;
			int[] is_176_ = new int[4];
			for (int i_177_ = i; i_177_ < i + i_171_; i_177_++) {
				if (i_168_ > 0)
					is_176_[3] += is[i_168_ - 1][i_177_];
				if (i_168_ + i_172_ < i_169_)
					is_176_[1] += is[i_168_ + i_172_][i_177_];
			}
			is_176_[3] /= i_171_;
			is_176_[1] /= i_171_;
			for (int i_178_ = i_168_; i_178_ < i_168_ + i_172_; i_178_++) {
				if (i > 0)
					is_176_[2] += is[i_178_][i - 1];
				if (i + i_171_ < i_170_)
					is_176_[0] += is[i_178_][i + i_171_];
			}
			is_176_[2] /= i_172_;
			is_176_[0] /= i_172_;
			for (int i_179_ = 0; i_179_ < 4; i_179_++) {
				if (is_176_[i_179_] > 127)
					is_176_[i_179_] = 255 - is_176_[i_179_];
			}
			int i_180_ = 0;
			int i_181_ = -1;
			for (int i_182_ = 0; i_182_ < 4; i_182_++) {
				if (is_176_[i_182_] > i_180_)
					i_180_ = is_176_[i_181_ = i_182_];
			}
			if (i_181_ == -1 || is_176_[i_181_] < 50)
				break;
			switch (i_181_) {
			case 0:
				i_171_++;
				break;
			case 1:
				i_172_++;
				break;
			case 2:
				i--;
				i_171_++;
				break;
			case 3:
				i_168_--;
				i_172_++;
				break;
			}
		}
		if (i_173_ < 64)
			return new Rectangle(0, 0, 0, 0);
		if (i_171_ <= 5 || i_172_ <= 5) {
			if (i_173_ < 80)
				return null;
		} else if (i_171_ <= 10 || i_172_ <= 10) {
			if (i_173_ < 85)
				return null;
		} else if (var_a8.isTwoNonZeroes()) {
			if (i_173_ < 100)
				return null;
		} else if (i_173_ < 90)
			return null;
		if (i > 0) {
			i--;
			i_171_++;
		}
		if (i_168_ > 0) {
			i_168_--;
			i_172_++;
		}
		if (i + i_171_ < i_170_)
			i_171_++;
		if (i_168_ + i_172_ < i_169_)
			i_172_++;
		return new Rectangle(i, i_168_, i_171_, i_172_);
	}

	private Polygon a(Recognizer var_a1, int i, Polygon polygon, int i_183_) {
		int[] is = new int[4];
		int[] is_184_ = new int[4];
		for (int i_185_ = 0; i_185_ < 4; i_185_++) {
			int i_186_;
			if (i_183_ < 300)
				i_186_ = 3;
			else
				i_186_ = i_183_ / 100;
			int[] is_187_ = new int[4];
			int[] is_188_ = new int[4];
			int[] is_189_ = new int[4];
			int[] is_190_ = new int[4];
			int[] is_191_ = new int[4];
			int[] is_192_ = new int[4];
			int i_193_ = i_185_ == 3 ? 0 : i_185_ + 1;
			int i_194_ = i_193_ == 3 ? 0 : i_193_ + 1;
			int i_195_ = i_194_ == 3 ? 0 : i_194_ + 1;
			is_187_[0] = polygon.xpoints[i_185_];
			is_187_[1] = polygon.xpoints[i_185_]
					+ ((polygon.xpoints[i_193_] - polygon.xpoints[i_185_]) / i_186_);
			is_187_[2] = polygon.xpoints[i_185_]
					+ ((polygon.xpoints[i_194_] - polygon.xpoints[i_185_]) / i_186_);
			is_187_[3] = polygon.xpoints[i_185_]
					+ ((polygon.xpoints[i_195_] - polygon.xpoints[i_185_]) / i_186_);
			is_190_[0] = polygon.ypoints[i_185_];
			is_190_[1] = polygon.ypoints[i_185_]
					+ ((polygon.ypoints[i_193_] - polygon.ypoints[i_185_]) / i_186_);
			is_190_[2] = polygon.ypoints[i_185_]
					+ ((polygon.ypoints[i_194_] - polygon.ypoints[i_185_]) / i_186_);
			is_190_[3] = polygon.ypoints[i_185_]
					+ ((polygon.ypoints[i_195_] - polygon.ypoints[i_185_]) / i_186_);
			int i_196_ = (is_187_[2] - is_187_[0]) / 10;
			int i_197_ = (is_190_[2] - is_190_[0]) / 10;
			for (int i_198_ = 0; i_198_ < 4; i_198_++) {
				is_187_[i_198_] -= i_196_;
				is_190_[i_198_] -= i_197_;
			}
			is_188_[0] = is_187_[0];
			is_191_[0] = is_190_[0];
			is_188_[3] = (is_187_[0] + is_187_[1]) / 2;
			is_191_[3] = (is_190_[0] + is_190_[1]) / 2;
			is_188_[2] = (is_187_[3] + is_187_[2]) / 2;
			is_191_[2] = (is_190_[3] + is_190_[2]) / 2;
			is_188_[1] = is_187_[3];
			is_191_[1] = is_190_[3];
			is_189_[0] = is_187_[0];
			is_192_[0] = is_190_[0];
			is_189_[1] = is_187_[1];
			is_192_[1] = is_190_[1];
			is_189_[2] = (is_187_[1] + is_187_[2]) / 2;
			is_192_[2] = (is_190_[1] + is_190_[2]) / 2;
			is_189_[3] = (is_187_[0] + is_187_[3]) / 2;
			is_192_[3] = (is_190_[0] + is_190_[3]) / 2;
			Poligono var_i = new Poligono(is_188_, is_191_, 4);
			int i_199_ = (var_i.xpoints[2] + var_i.xpoints[3]) / 2;
			int i_200_ = (var_i.ypoints[2] + var_i.ypoints[3]) / 2;
			PointDouble var_ay = a(var_a1, i, var_i, i_199_, i_200_);
			Poligono var_i_201_ = new Poligono(is_189_, is_192_, 4);
			PointDouble var_ay_202_ = a(var_a1, i, var_i_201_, i_199_, i_200_);
			is[i_185_] = i_199_ + (int) (var_ay.x + var_ay_202_.x);
			is_184_[i_185_] = i_200_ + (int) (var_ay.y + var_ay_202_.y);
		}
		return new Polygon(is, is_184_, 4);
	}

	private a3 a(Recognizer var_a1, int i, Polygon polygon) {
		PointDouble var_ay = new PointDouble((double) polygon.xpoints[0],
				(double) polygon.ypoints[0]);
		PointDouble var_ay_203_ = new PointDouble((double) polygon.xpoints[1],
				(double) polygon.ypoints[1]);
		PointDouble var_ay_204_ = new PointDouble((double) polygon.xpoints[2],
				(double) polygon.ypoints[2]);
		double d = var_ay.mod(var_ay_203_);
		double d_205_ = var_ay_203_.mod(var_ay_204_);
		double d_206_ = Math.max(d, d_205_) / Math.min(d, d_205_);
		boolean bool;
		if (d_206_ < 1.3)
			bool = false;
		else {
			if (d_206_ < 1.7 || d_206_ > 4.8)
				return null;
			bool = true;
		}
		int i_207_ = Math.max((int) d, (int) d_205_);
		if (i_207_ > 100)
			polygon = a(var_a1, i, polygon, i_207_);
		Rectangle rectangle = polygon.getBounds();
		byte[][] is = new byte[rectangle.width][];
		for (int i_208_ = 0; i_208_ < is.length; i_208_++)
			is[i_208_] = new byte[rectangle.height];
		for (int i_209_ = 0; i_209_ < rectangle.height; i_209_++) {
			for (int i_210_ = 0; i_210_ < rectangle.width; i_210_++) {
				if (var_a1.a(rectangle.x + i_210_, rectangle.y + i_209_) >= i
						|| !polygon.contains(rectangle.x + i_210_, rectangle.y
								+ i_209_))
					is[i_210_][i_209_] = (byte) 1;
				else
					is[i_210_][i_209_] = (byte) 0;
			}
		}
		a3 var_a3 = new a3(polygon, is, bool, prop);
		return var_a3;
	}

	private int[][][] a(Recognizer var_a1, int i, Rectangle rectangle, Point point) {
		int i_211_ = rectangle.x;
		int i_212_ = rectangle.y;
		int i_213_ = rectangle.width;
		int i_214_ = rectangle.height;
		int i_215_ = i_211_ + rectangle.width;
		int i_216_ = i_212_ + rectangle.height;
		int i_217_ = point.x;
		int i_218_ = point.y;
		int i_219_ = ((int) Math.sqrt((double) (i_213_ * i_213_ + i_214_
				* i_214_)) + 1);
		i_219_ = i_219_ / 2 + 1;
		int[][][] is = new int[4][200][i_219_];
		boolean bool = false;
		boolean bool_220_ = false;
		int i_221_ = 50 + (i_214_ / 2 - 50) / 12;
		int i_222_ = 50 + (i_213_ / 2 - 50) / 12;
		boolean bool_223_ = i_214_ > 100 && i_213_ > 100;
		int i_224_ = -1;
		int i_225_ = 3;
		for (int i_226_ = i_212_; i_226_ < i_216_; i_226_++) {
			int i_227_ = i_226_ - i_218_;
			if (i_227_ == 0) {
				i_224_ = 1;
				i_225_ = 1;
			}
			int i_228_ = i_226_ + i_224_;
			int i_229_ = -1;
			int i_230_ = 2;
			int i_231_;
			int i_232_;
			if (bool_223_
					&& (i_226_ < i_218_ - i_221_ || i_226_ > i_218_ + i_221_)) {
				i_231_ = i_217_ - i_222_;
				i_232_ = i_217_ + i_222_;
			} else {
				i_231_ = i_211_;
				i_232_ = i_215_;
			}
			for (int i_233_ = i_231_; i_233_ < i_232_; i_233_++) {
				int i_234_ = i_233_ - i_217_;
				if (i_234_ == 0) {
					i_229_ = 1;
					i_230_ = 0;
				}
				if (var_a1.a(i_233_, i_226_) < i) {
					boolean bool_235_ = var_a1.a(i_233_ + i_229_, i_226_) >= i;
					boolean bool_236_ = var_a1.a(i_233_, i_228_) >= i;
					if (bool_235_ || bool_236_) {
						for (int i_237_ = 0; i_237_ < 100; i_237_++) {
							double d = ((double) i_234_ * a1[i_237_] + (double) i_227_
									* aP[i_237_]);
							int i_238_ = i_237_;
							if (d < 0.0) {
								d = -d;
								i_238_ += 100;
							}
							int i_239_ = (int) d;
							if (i_239_ < i_219_) {
								if (bool_235_)
									is[i_230_][i_238_ / 1][i_239_]++;
								if (bool_236_)
									is[i_225_][i_238_ / 1][i_239_]++;
							}
						}
					}
				}
			}
		}
		return is;
	}

	private PointDouble a(Recognizer var_a1, int i, Poligono var_i, int i_240_, int i_241_) {
		int i_242_ = var_i.xpoints[0] - var_i.xpoints[1];
		int i_243_ = var_i.ypoints[0] - var_i.ypoints[1];
		int i_244_;
		if (Math.abs(i_243_) < Math.abs(i_242_)) {
			if (var_i.ypoints[3] > var_i.ypoints[0])
				i_244_ = 3;
			else
				i_244_ = 1;
		} else if (var_i.xpoints[3] > var_i.xpoints[0])
			i_244_ = 2;
		else
			i_244_ = 0;
		Object object = null;
		Rectangle rectangle = var_i.getBounds();
		int[] is = new int[Math.max(rectangle.width, rectangle.height)];
		double d = 0.0;
		double d_245_ = 0.0;
		if (i_244_ == 2 || i_244_ == 0) {
			int i_246_ = (i_244_ == 2 ? rectangle.x : rectangle.x
					+ rectangle.width - 1);
			int i_247_ = (i_244_ == 0 ? rectangle.x : rectangle.x
					+ rectangle.width - 1);
			int i_248_ = i_244_ == 2 ? 1 : -1;
			double d_249_ = Math.atan((double) i_242_ / (double) i_243_);
			d = Math.cos(d_249_);
			d_245_ = Math.sin(d_249_);
			for (int i_250_ = rectangle.y; i_250_ < rectangle.y
					+ rectangle.height; i_250_++) {
				int i_251_ = 255;
				for (int i_252_ = i_246_; i_252_ != i_247_; i_252_ += i_248_) {
					if (var_i.contains(i_252_, i_250_)) {
						int i_253_ = var_a1.a(i_252_, i_250_);
						if (i_253_ < i && i_251_ >= i) {
							double d_254_ = ((double) (i_252_ - i_240_) * d - (double) (i_250_ - i_241_)
									* d_245_);
							if (d_254_ < 0.0)
								d_254_ = -d_254_;
							if ((int) d_254_ < is.length)
								is[(int) d_254_]++;
							break;
						}
						i_251_ = i_253_;
					}
				}
			}
		} else {
			int i_255_ = (i_244_ == 3 ? rectangle.y : rectangle.y
					+ rectangle.height - 1);
			int i_256_ = (i_244_ == 1 ? rectangle.y : rectangle.y
					+ rectangle.height - 1);
			int i_257_ = i_244_ == 3 ? 1 : -1;
			double d_258_ = Math.atan((double) i_243_ / (double) i_242_);
			d = Math.cos(d_258_);
			d_245_ = Math.sin(d_258_);
			for (int i_259_ = rectangle.x; i_259_ < rectangle.x
					+ rectangle.width; i_259_++) {
				int i_260_ = 255;
				for (int i_261_ = i_255_; i_261_ != i_256_; i_261_ += i_257_) {
					if (var_i.contains(i_259_, i_261_)) {
						int i_262_ = var_a1.a(i_259_, i_261_);
						if (i_262_ < i && i_260_ >= i) {
							double d_263_ = ((double) -(i_259_ - i_240_)
									* d_245_ + (double) (i_261_ - i_241_) * d);
							if (d_263_ < 0.0)
								d_263_ = -d_263_;
							if ((int) d_263_ < is.length)
								is[(int) d_263_]++;
							break;
						}
						i_260_ = i_262_;
					}
				}
			}
		}
		int i_264_ = this.maxValueIndex(is);
		int i_265_ = is[i_264_];
		int i_266_ = i_265_ * 35 / 100;
		for (int i_267_ = i_264_ + 1; i_267_ < is.length; i_267_++) {
			if (is[i_267_] > i_266_)
				i_264_ = i_267_;
		}
		PointDouble var_ay = null;
		if (i_244_ == 2)
			var_ay = new PointDouble((double) -i_264_ * d, (double) i_264_ * d_245_);
		else if (i_244_ == 0)
			var_ay = new PointDouble((double) i_264_ * d, (double) -i_264_ * d_245_);
		else if (i_244_ == 3)
			var_ay = new PointDouble((double) i_264_ * d_245_, (double) -i_264_ * d);
		else if (i_244_ == 1)
			var_ay = new PointDouble((double) -i_264_ * d_245_, (double) i_264_ * d);
		return var_ay;
	}

	private int[][] a(Recognizer var_a1, int i, int i_268_, Shape shape, Point point) {
		Rectangle rectangle;
		boolean bool;
		if (shape instanceof Polygon) {
			rectangle = ((Polygon) shape).getBounds();
			bool = true;
		} else {
			rectangle = (Rectangle) shape;
			bool = false;
		}
		int i_269_ = rectangle.x;
		int i_270_ = rectangle.y;
		int i_271_ = rectangle.width;
		int i_272_ = rectangle.height;
		int i_273_ = point.x;
		int i_274_ = point.y;
		int i_275_ = ((int) Math.sqrt((double) (i_271_ * i_271_ + i_272_
				* i_272_)) + 1);
		i_275_ = i_275_ / 2 + 1;
		int[][] is = new int[4][i_275_];
		int i_276_ = i_269_;
		int i_277_ = i_269_ + i_271_;
		int i_278_ = i_270_;
		int i_279_ = i_270_ + i_272_;
		int i_280_ = i_268_;
		double d = a1[i_280_];
		double d_281_ = aP[i_280_];
		i_280_ = i_268_ + 50;
		double d_282_ = a1[i_280_];
		double d_283_ = aP[i_280_];
		for (int i_284_ = i_278_; i_284_ < i_279_; i_284_++) {
			int i_285_ = i_284_ - i_274_;
			double d_286_ = (double) i_285_ * d_281_;
			double d_287_ = (double) i_285_ * d_283_;
			for (int i_288_ = i_276_; i_288_ < i_277_; i_288_++) {
				if (var_a1.a(i_288_, i_284_) < i
						&& (!bool || ((Polygon) shape).contains(i_288_, i_284_))) {
					int i_289_ = 0;
					double d_290_ = (double) (i_288_ - i_273_) * d + d_286_;
					if (d_290_ < 0.0) {
						d_290_ = -d_290_;
						i_280_ += 100;
						i_289_ = 2;
					}
					int i_291_;
					if (Math.abs(i_291_ = (int) d_290_) < i_275_)
						is[i_289_][i_291_]++;
					i_289_ = 1;
					d_290_ = (double) (i_288_ - i_273_) * d_282_ + d_287_;
					if (d_290_ < 0.0) {
						d_290_ = -d_290_;
						i_280_ += 100;
						i_289_ = 3;
					}
					if (Math.abs(i_291_ = (int) d_290_) < i_275_)
						is[i_289_][i_291_]++;
				}
			}
		}
		return is;
	}

	static {
		for (int i = 0; i < 200; i++) {
			double d = 3.141592653589793 * (double) i / 100.0;
			aP[i] = Math.sin(d);
			a1[i] = Math.cos(d);
			a2[i] = Math.tan(d);
		}
		a3._if = 100;
		a3.m = aP;
		a3._new = a1;
		a3.j = a2;
	}
}
