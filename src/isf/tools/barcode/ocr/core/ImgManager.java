/* k - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

public interface ImgManager {
	public int getWidth();

	public int getHeight();

	public int getPixel(int x, int y);

	public int[] getCol(int i);

	public int[] getRow(int i);
}
