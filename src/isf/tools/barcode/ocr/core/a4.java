/* a4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

class a4 {
	private static final byte _do = 1;

	private static final byte _try = 2;

	private static final byte _new = 3;

	private static final int[][] _int = { { -1 }, { -1 }, { 6, 18 }, { 6, 22 },
			{ 6, 26 }, { 6, 30 }, { 6, 34 }, { 6, 22, 38 }, { 6, 24, 42 },
			{ 6, 26, 46 }, { 6, 28, 50 }, { 6, 30, 54 }, { 6, 32, 58 },
			{ 6, 34, 62 }, { 6, 26, 46, 66 }, { 6, 26, 48, 70 },
			{ 6, 26, 50, 74 }, { 6, 30, 54, 78 }, { 6, 30, 56, 82 },
			{ 6, 30, 58, 86 }, { 6, 34, 62, 90 }, { 6, 28, 50, 72, 94 },
			{ 6, 26, 50, 74, 98 }, { 6, 30, 54, 78, 102 },
			{ 6, 28, 54, 80, 106 }, { 6, 32, 58, 84, 110 },
			{ 6, 30, 58, 86, 114 }, { 6, 34, 62, 90, 118 },
			{ 6, 26, 50, 74, 98, 122 }, { 6, 30, 54, 78, 102, 126 },
			{ 6, 26, 52, 78, 104, 130 }, { 6, 30, 56, 82, 108, 134 },
			{ 6, 34, 60, 86, 112, 138 }, { 6, 30, 58, 86, 114, 142 },
			{ 6, 34, 62, 90, 118, 146 }, { 6, 30, 54, 78, 102, 126, 150 },
			{ 6, 24, 50, 76, 102, 128, 154 }, { 6, 28, 54, 80, 106, 132, 158 },
			{ 6, 32, 58, 84, 110, 136, 162 }, { 6, 26, 54, 82, 110, 138, 166 },
			{ 6, 30, 58, 86, 114, 142, 170 } };

	private int a;

	private int _if;

	private byte[][] _for;

	static int _if(int i) {
		return _int[i].length;
	}

	static int[] a(int i) {
		return _int[i];
	}

	static af a(int i, int i_0_, int i_1_) {
		if (i_0_ == 0 && i_1_ == 0)
			return new af(3, 3);
		int[] is = _int[i];
		if (i_1_ == 0 && i_0_ == is.length - 1)
			return new af(13 + 4 * i, 3);
		if (i_0_ == 0 && i_1_ == is.length - 1)
			return new af(3, 13 + 4 * i);
		return new af(is[i_0_], is[i_1_]);
	}

	a4(int i) {
		a = i;
		_if = 17 + i * 4;
		_for = new byte[_if][_if];
		for (int i_2_ = 0; i_2_ <= 8; i_2_++) {
			for (int i_3_ = 0; i_3_ <= 8; i_3_++)
				_for[i_3_][i_2_] = (byte) 1;
		}
		for (int i_4_ = _if - 8; i_4_ < _if; i_4_++) {
			for (int i_5_ = 0; i_5_ <= 8; i_5_++)
				_for[i_5_][i_4_] = (byte) 1;
		}
		for (int i_6_ = 0; i_6_ <= 8; i_6_++) {
			for (int i_7_ = _if - 8; i_7_ < _if; i_7_++)
				_for[i_7_][i_6_] = (byte) 1;
		}
		for (int i_8_ = 9; i_8_ < _if - 8; i_8_++)
			_for[6][i_8_] = (byte) 2;
		for (int i_9_ = 9; i_9_ < _if - 8; i_9_++)
			_for[i_9_][6] = (byte) 2;
		if (i > 1) {
			int i_10_ = _int[i].length;
			for (int i_11_ = 0; i_11_ < i_10_; i_11_++) {
				for (int i_12_ = 0; i_12_ < i_10_; i_12_++) {
					if ((i_11_ != 0 || i_12_ != 0)
							&& (i_11_ != 0 || i_12_ != i_10_ - 1)
							&& (i_11_ != i_10_ - 1 || i_12_ != 0)) {
						int i_13_ = _int[i][i_11_];
						int i_14_ = _int[i][i_12_];
						for (int i_15_ = -2; i_15_ <= 2; i_15_++) {
							for (int i_16_ = -2; i_16_ <= 2; i_16_++)
								_for[i_14_ + i_16_][i_13_ + i_15_] = (byte) 3;
						}
					}
				}
			}
		}
		if (i > 6) {
			for (int i_17_ = _if - 11; i_17_ <= _if - 9; i_17_++) {
				for (int i_18_ = 0; i_18_ <= 6; i_18_++)
					_for[i_18_][i_17_] = (byte) 1;
			}
			_for[6][_if - 11] = (byte) 2;
			for (int i_19_ = 0; i_19_ <= 6; i_19_++) {
				for (int i_20_ = _if - 11; i_20_ <= _if - 9; i_20_++)
					_for[i_20_][i_19_] = (byte) 1;
			}
		}
	}

	af a(af var_af) {
		if (var_af == null)
			return new af(_if - 1, _if - 1);
		int i = var_af.a;
		if (i > 6)
			i--;
		boolean bool = i % 2 == 0;
		boolean bool_21_ = i / 2 % 2 == 1;
		if (!bool) {
			var_af.a--;
			return var_af;
		}
		if (bool_21_) {
			if (var_af._if == 0)
				var_af.a--;
			else {
				var_af._if--;
				var_af.a++;
			}
			byte i_22_ = _for[var_af._if][var_af.a];
			if (i_22_ == 2)
				var_af._if--;
			else if (i_22_ == 1) {
				var_af._if++;
				var_af.a -= 2;
				if (var_af.a == 6)
					var_af.a--;
				if (var_af._if == 7) {
					var_af.a--;
					var_af._if = 0;
				}
			} else if (i_22_ == 3) {
				do {
					if (var_af.a % 2 == 0)
						var_af.a--;
					else {
						var_af._if--;
						var_af.a++;
					}
				} while (_for[var_af._if][var_af.a] == 3
						|| _for[var_af._if][var_af.a] == 2);
			}
		} else {
			if (var_af._if == _if - 1)
				var_af.a--;
			else {
				var_af._if++;
				var_af.a++;
			}
			byte i_23_ = _for[var_af._if][var_af.a];
			if (i_23_ == 2)
				var_af._if++;
			else if (i_23_ == 1) {
				if (var_af.a == 8)
					var_af._if = _if - 9;
				else if (var_af._if < 7)
					var_af.a--;
				else {
					var_af._if--;
					var_af.a -= 2;
					if (var_af.a < 0)
						var_af = null;
				}
			} else if (i_23_ == 3) {
				do {
					i = var_af.a;
					if (i < 6)
						i++;
					if (i % 2 == 0)
						var_af.a--;
					else {
						var_af._if++;
						var_af.a++;
					}
				} while (_for[var_af._if][var_af.a] == 3
						|| _for[var_af._if][var_af.a] == 2);
			}
		}
		return var_af;
	}
}
