/* ai - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;

class CodaBar extends Barcode {
	private static final boolean aC = false;

	private static final String[] aw = { "0000011", "0000110", "0001001",
			"1100000", "0010010", "1000010", "0100001", "0100100", "0110000",
			"1001000", "0001100", "0011000", "1000101", "1010001", "1010100",
			"0010101", "0011010", "0101001", "0001011", "0001110" };

	private static final char[] av = { '0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', '-', '$', ':', '/', '.', '+', 'A', 'B', 'C', 'D' };

	private static HashMap aA = null;

	private ArrayList au;

	private int aB;

	private char[] az = new char[7];

	private int[] ax = new int[7];

	private int[] ay = new int[7];

	CodaBar(PropertiesHolder var_c, int i) {
		super(var_c);
		au = new ArrayList();
		aB = i;
	}

	int code() {
		return 0;
	}

	int _if(int i) {
		if (i >= 23 && i % 8 == 7)
			return i / 8 - 1;
		return -1;
	}

	boolean isCheck() {
		return false;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_;
		if (is[0] == 0)
			i_4_ = 3;
		else
			i_4_ = 1;
		int i_5_ = is[i_4_];
		for (int i_6_ = 1; i_6_ < 7; i_6_++)
			i_5_ += is[i_4_ + i_6_];
		int i_7_ = 2147483647;
		int i_8_ = 2 * aB;
		while (i_4_ + 7 < i_1_) {
			if ((i_4_ == 1 || is[i_4_ - 1] > i_5_ / i_8_)
					&& is[i_4_ + 7] < i_5_ / 2) {
				int i_9_ = a(is, i_4_, i_5_, 16, 19);
				if (i_9_ != -1) {
					this.readData(i, i_0_, is, i_4_, 7, i_2_, i_9_, true, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			} else if (is[i_4_ - 1] < i_5_ / 2
					&& (i_4_ + 7 == i_1_ - 1 || is[i_4_ + 7] > i_5_ / i_8_)) {
				int i_10_ = a(is, i_4_, i_5_, 16, 19);
				if (i_10_ != -1) {
					this.readData(i, i_0_, is, i_4_, 7, i_2_, i_10_, false, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			}
			i_5_ -= is[i_4_] + is[i_4_ + 1];
			i_4_ += 2;
			i_5_ += is[i_4_ + 5] + is[i_4_ + 6];
		}
		return i_7_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_11_) {
		int i_12_ = 0;
		for (;;) {
			if (i_12_ == 0) {
				int i_13_ = a(is, i_12_, -1, 16, 19);
				i_12_ += 8;
				if (i_13_ < 16 || i_13_ > 19)
					break;
				au.clear();
				au.add(new Integer(i_13_));
			} else {
				if (i_12_ + 7 == i) {
					int i_14_ = a(is, i_12_, -1, 16, 19);
					if (i_14_ >= 16 && i_14_ <= 19) {
						au.add(new Integer(i_14_));
						this.a(au, -1, var_d, var_d_11_);
						return true;
					}
					break;
				}
				int i_15_ = a(is, i_12_, -1, 0, 15);
				i_12_ += 8;
				if ((i_15_ < 0 || i_15_ > 15) && i_15_ != 65535)
					break;
				au.add(new Integer(i_15_));
			}
		}
		return false;
	}

	private final int a(int[] is, int i, int i_16_, int i_17_, int i_18_) {
		if (i_16_ == -1) {
			i_16_ = 0;
			for (int i_19_ = 0; i_19_ < 7; i_19_++)
				i_16_ += is[i + i_19_];
		}
		int i_20_ = i_16_ / 7;
		for (int i_21_ = 0; i_21_ < 7; i_21_++) {
			ax[i_21_] = is[i + i_21_];
			if (ax[i_21_] < i_20_ / 5)
				return -1;
			if (ax[i_21_] > i_20_ * 3)
				return -1;
		}
		for (int i_22_ = 0; i_22_ < 7; i_22_++) {
			int i_23_ = 0;
			int i_24_ = 0;
			for (int i_25_ = 0; i_25_ < 7; i_25_++) {
				if (ax[i_25_] > i_23_) {
					i_23_ = ax[i_25_];
					i_24_ = i_25_;
				}
			}
			ay[i_22_] = i_24_;
			ax[i_24_] = -1;
		}
		for (int i_26_ = 0; i_26_ < 7; i_26_++)
			az[i_26_] = '0';
		for (int i_27_ = 0; i_27_ < 3; i_27_++)
			az[ay[i_27_]] = '1';
		if (i_17_ == 16 && i_18_ == 19) {
			String string = new String(az);
			Object object;
			if ((object = aA.get(string)) != null) {
				int i_28_ = ((Integer) object).intValue();
				if (i_28_ >= 16 && i_28_ <= 19)
					return i_28_;
			}
			return -1;
		}
		if (ay[0] % 2 == 0 && ay[1] % 2 == 0 && ay[2] % 2 == 0) {
			String string = new String(az);
			return ((Integer) aA.get(string)).intValue();
		}
		for (int i_29_ = 0; i_29_ < 7; i_29_++)
			az[i_29_] = '0';
		if (ay[0] % 2 == 0 && ay[1] % 2 == 0) {
			if (ay[2] % 2 == 1)
				ay[1] = ay[2];
			else if (ay[3] % 2 == 1)
				ay[1] = ay[3];
			else
				return 65535;
		} else if (ay[0] % 2 == 1 && ay[1] % 2 == 1) {
			if (ay[2] % 2 == 0)
				ay[1] = ay[2];
			else if (ay[3] % 2 == 0)
				ay[1] = ay[3];
			else
				return 65535;
		}
		az[ay[0]] = '1';
		az[ay[1]] = '1';
		String string = new String(az);
		Object object;
		if ((object = aA.get(string)) != null) {
			int i_30_ = ((Integer) object).intValue();
			if (i_30_ >= 0 && i_30_ <= 15)
				return i_30_;
		}
		return 65535;
	}

	String a(String string) {
		StringBuffer stringbuffer = new StringBuffer();
		for (int i = 0; i < string.length(); i++)
			stringbuffer.append(av[string.charAt(i)]);
		return new String(stringbuffer);
	}

	static {
		aA = new HashMap();
		for (int i = 0; i < aw.length; i++)
			aA.put(aw[i], new Integer(i));
	}
}
