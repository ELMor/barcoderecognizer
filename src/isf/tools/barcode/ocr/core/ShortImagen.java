/* b - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferUShort;
import java.awt.image.DirectColorModel;

final class ShortImagen extends Imagen {
	private final short[] F;

	private final int[] H;

	private final int[] G;

	private final int[] E;

	ShortImagen(BufferedImage bufferedimage) {
		super(bufferedimage);
		java.awt.image.DataBuffer databuffer = bufferedimage.getRaster()
				.getDataBuffer();
		F = ((DataBufferUShort) databuffer).getData();
		H = new int[32];
		G = new int[64];
		E = new int[32];
		DirectColorModel directcolormodel = (DirectColorModel) bufferedimage
				.getColorModel();
		for (int i = 0; i < 32; i++) {
			E[i] = directcolormodel.getBlue(i);
			H[i] = directcolormodel.getRed(i << 11);
		}
		for (int i = 0; i < 64; i++)
			G[i] = directcolormodel.getGreen(i << 5);
	}

	public final int getPixel(int i, int i_0_) {
		int i_1_ = F[g + i_0_ * slStride + i];
		if (i_1_ < 0)
			i_1_ += 65536;
		int i_2_ = E[i_1_ & 0x1f];
		i_1_ >>= 5;
		int i_3_ = G[i_1_ & 0x3f];
		i_1_ >>= 6;
		int i_4_ = H[i_1_ & 0x1f];
		return (i_4_ + i_3_ + i_2_) / 3;
	}

	public int[] getRow(int i) {
		int i_5_ = this.getWidth();
		int i_6_ = g + i * slStride;
		for (int i_7_ = 0; i_7_ < i_5_; i_7_++) {
			int i_8_ = F[i_6_++];
			int i_9_ = E[i_8_ & 0x1f];
			i_8_ >>= 5;
			int i_10_ = G[i_8_ & 0x1f];
			i_8_ >>= 6;
			int i_11_ = H[i_8_ & 0x1f];
			xDim[i_7_] = (i_11_ + i_10_ + i_9_) / 3;
		}
		return xDim;
	}

	public int[] getCol(int i) {
		int i_12_ = this.getHeight();
		int i_13_ = g + i;
		for (int i_14_ = 0; i_14_ < i_12_; i_14_++) {
			int i_15_ = F[i_13_];
			int i_16_ = E[i_15_ & 0x1f];
			i_15_ >>= 5;
			int i_17_ = G[i_15_ & 0x1f];
			i_15_ >>= 6;
			int i_18_ = H[i_15_ & 0x1f];
			this.yDim[i_14_] = (i_18_ + i_17_ + i_16_) / 3;
			i_13_ += slStride;
		}
		return this.yDim;
	}
}
