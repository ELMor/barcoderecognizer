/* u - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

class QRCode extends Barcode {
	private static final boolean z = false;

	private static final int B = 1;

	private int A;

	private int C;

	private int[] y = new int[9];

	private int[] D = new int[4];

	QRCode(int i, int i_0_, int i_1_, PropertiesHolder var_c) {
		super(var_c);
		A = i;
		C = i_0_;
	}

	int code() {
		return 17;
	}

	boolean isCheck() {
		return true;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	int _if(int i) {
		return -1;
	}

	String a(String string) {
		return "";
	}

	final int a(int i, int i_2_, int i_3_, int[] is, int i_4_, int i_5_) {
		int i_6_ = 2147483647;
		int i_7_;
		if (is[0] == 0)
			i_7_ = 3;
		else
			i_7_ = 1;
		int i_8_ = is[i_7_];
		for (int i_9_ = 1; i_9_ < 5; i_9_++)
			i_8_ += is[i_7_ + i_9_];
		while (i_7_ + 5 <= i_3_) {
			if (i_7_ == 1 || is[i_7_ - 1] >= i_8_ * 2 / 7
					|| i_7_ + 5 == i_3_ - 1 || is[i_7_ + 5] >= i_8_ * 2 / 7) {
				int i_10_ = _if(is, i_7_, 5, 7);
				if (i_10_ == 1) {
					this.readData(i, i_2_, is, i_7_, 5, i_4_, i_10_, i_4_ == 90, i_5_);
					if (i_8_ < i_6_)
						i_6_ = i_8_;
				}
			}
			i_8_ -= is[i_7_] + is[i_7_ + 1];
			i_7_ += 2;
			i_8_ += is[i_7_ + 3] + is[i_7_ + 4];
		}
		return i_6_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_11_) {
		return false;
	}

	void read(Recognizer var_a1, int i, ArrayList arraylist, Normalize var_a8, int i_12_,
			String string) {
		throw new ArrayIndexOutOfBoundsException("ELM");
	}

	private final int _if(int[] is, int i, int i_51_, int i_52_) {
		int i_53_ = 0;
		for (int i_54_ = 0; i_54_ < 5; i_54_++) {
			y[i_54_] = is[i + i_54_];
			i_53_ += is[i + i_54_];
		}
		int i_55_ = 0;
		for (int i_56_ = 0; i_56_ < 4; i_56_++) {
			for (int i_57_ = 1; i_57_ < 9; i_57_++) {
				if (y[i_57_] > y[i_55_])
					i_55_ = i_57_;
			}
		}
		if (i_55_ != 2)
			return -1;
		int i_58_ = i_53_ * 2 / 7;
		if (y[2] < i_58_ || y[2] > i_58_ * 2)
			return -1;
		if (y[0] > i_58_ || y[1] > i_58_ || y[3] > i_58_ || y[4] > i_58_)
			return -1;
		int i_59_ = i_53_ / 14;
		if (y[0] < i_59_ || y[1] < i_59_ || y[3] < i_59_ || y[4] < i_59_)
			return -1;
		return 1;
	}
}
