/* c - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

class PropertiesHolder {
	private JTextArea txtArea;

	private HashMap map;

	private BufferedImage bImage;

	private Date fecha;

	PropertiesHolder(HashMap hashmap) {
		txtArea = (JTextArea) hashmap.get("ta");
		map = hashmap;
		txtArea.setTabSize(3);
		for (int i = 1; i < 8; i++)
			hashmap.put(String.valueOf(i), new ArrayList());
		bImage = null;
	}

	PropertiesHolder(StringBuffer stringbuffer) {
		map = new HashMap();
		map.put("on", stringbuffer);
	}

	void a(String string) {
		if (map.get("on").equals(Boolean.TRUE))
			txtArea.append(string);
	}

	void _do(String string) {
		a(string + "\n");
	}

	void a() {
		a("\n");
	}

	void a(String string, int i) {
		for (int i_0_ = 0; i_0_ < string.length(); i_0_++) {
			char c = string.charAt(i_0_);
			if (c == '\uffff')
				a("\t?");
			else
				a("\t" + (int) c);
		}
		_do("   scans = " + i);
	}

	void debug(String string) {
		if (string == null)
			fecha = new Date();
		else {
			Date date = new Date();
			long l = date.getTime() - fecha.getTime();
			_do("TIMER -> " + string + ": " + l + " millisec");
			fecha = date;
		}
	}

	void rectangles(int i, ArrayList arraylist) {
		if (map.get("on").equals(Boolean.TRUE)) {
			Color[] colors = new Color[3];
			colors[0] = new Color(0, 255, 0, 127);
			colors[1] = new Color(0, 0, 255, 127);
			colors[2] = new Color(255, 0, 0, 127);
			int i_1_ = 0;
			Graphics2D graphics2d = bImage.createGraphics();
			graphics2d.setStroke(new BasicStroke(1.0F));
			if (arraylist != null) {
				Iterator iterator = arraylist.iterator();
				while (iterator.hasNext()) {
					graphics2d.setColor(colors[i_1_++ % 3]);
					SepInten var_d = (SepInten) iterator.next();
					Iterator iterator_2_ = var_d.getScans().iterator();
					while (iterator_2_.hasNext()) {
						Rectangle rectangle = (Rectangle) iterator_2_.next();
						if (rectangle.height == 1)
							graphics2d.drawLine(rectangle.x, rectangle.y,
									rectangle.x + rectangle.width, rectangle.y);
						else if (rectangle.width == 1)
							graphics2d.drawLine(rectangle.x, rectangle.y,
									rectangle.x,
									(rectangle.y + rectangle.height));
					}
				}
			}
		}
	}

	void line(PointDouble p, Color color) {
		if (map.get("on").equals(Boolean.TRUE)) {
			Graphics2D graphics2d = bImage.createGraphics();
			graphics2d.setPaintMode();
			graphics2d.setColor(color);
			graphics2d.drawLine((int) p.x, (int) p.y,
					(int) p.x, (int) p.y);
		}
	}

	void a(int i, String string, String string_3_, ArrayList arraylist) {
		if (map.get("on").equals(Boolean.TRUE)) {
			Graphics2D graphics2d = bImage.createGraphics();
			graphics2d.setStroke(new BasicStroke(3.0F));
			graphics2d.setColor(new Color(0, 0, 255, 127));
			int i_4_ = arraylist.size();
			int i_5_ = 0;
			Iterator iterator = arraylist.iterator();
			while (iterator.hasNext()) {
				SepInten var_d = (SepInten) iterator.next();
				UserRect var_al = var_d.getUserRect();
				graphics2d.drawLine((int) var_al.getP1x(), (int) var_al.getP1y(),
						(int) var_al.getP2x(), (int) var_al.getP2y());
				_do(string + var_d);
				ArrayList arraylist_6_ = var_d.getScans();
				i_5_ += arraylist_6_.size();
			}
			_do(string_3_ + arraylist.size());
		}
	}

	void a(af var_af, af var_af_7_) {
		if (map.get("on").equals(Boolean.TRUE)) {
			Graphics2D graphics2d = bImage.createGraphics();
			graphics2d.setStroke(new BasicStroke(1.0F));
			graphics2d.setColor(new Color(0, 0, 0, 127));
			graphics2d.drawLine(var_af.a, var_af._if, var_af_7_.a,
					var_af_7_._if);
		}
	}

	void a(af var_af) {
		if (map.get("on").equals(Boolean.TRUE)) {
			Graphics2D graphics2d = bImage.createGraphics();
			graphics2d.setStroke(new BasicStroke(1.0F));
			graphics2d.setColor(new Color(255, 0, 0, 127));
			graphics2d.drawLine(var_af.a + 1, var_af._if, var_af.a - 1,
					var_af._if);
			graphics2d.drawLine(var_af.a, var_af._if + 1, var_af.a,
					var_af._if - 1);
		}
	}

	void a(int i, int i_8_) {
		if (map.get("on").equals(Boolean.TRUE)) {
			bImage = new BufferedImage(i, i_8_, 2);
			map.put("overlay", bImage);
			for (int i_9_ = 0; i_9_ < i; i_9_++) {
				for (int i_10_ = 0; i_10_ < i_8_; i_10_++)
					bImage.setRGB(i_9_, i_10_, 0);
			}
		}
	}

	void a(int i, int i_11_, int i_12_) {
		if (map.get("on").equals(Boolean.TRUE)
				&& (i >= 0 && i_11_ >= 0 && i < bImage.getWidth() && i_11_ < bImage
						.getHeight()))
			bImage.setRGB(i, i_11_, i_12_);
	}

	void _if(int i, int i_13_, Color color) {
		int i_14_ = (color.getAlpha() * 16777216 + color.getRed() * 65536
				+ color.getGreen() * 256 + color.getBlue());
		if (map.get("on").equals(Boolean.TRUE)
				&& (i >= 0 && i_13_ >= 0 && i < bImage.getWidth() && i_13_ < bImage
						.getHeight()))
			bImage.setRGB(i, i_13_, i_14_);
	}

	void a(Rectangle rectangle) {
		if (map.get("on").equals(Boolean.TRUE)) {
			int i = rectangle.x;
			int i_15_ = rectangle.y;
			int i_16_ = rectangle.width;
			int i_17_ = rectangle.height;
			for (int i_18_ = i_15_; i_18_ < i_15_ + 10; i_18_++) {
				for (int i_19_ = i; i_19_ < i + i_16_; i_19_++)
					a(i_19_, i_18_, 2147483392);
			}
			for (int i_20_ = i_15_ + 10; i_20_ < i_15_ + i_17_ - 10; i_20_++) {
				for (int i_21_ = i; i_21_ < i + 10; i_21_++)
					a(i_21_, i_20_, 2147483392);
			}
			for (int i_22_ = i_15_ + 10; i_22_ < i_15_ + i_17_ - 10; i_22_++) {
				for (int i_23_ = i + i_16_ - 10; i_23_ < i + i_16_; i_23_++)
					a(i_23_, i_22_, 2147483392);
			}
			for (int i_24_ = i_15_ + i_17_ - 10; i_24_ < i_15_ + i_17_; i_24_++) {
				for (int i_25_ = i; i_25_ < i + i_16_; i_25_++)
					a(i_25_, i_24_, 2147483392);
			}
		}
	}

	void a(Poligono var_i, Color color) {
		if (map.get("on").equals(Boolean.TRUE)) {
			Graphics2D graphics2d = bImage.createGraphics();
			graphics2d.setColor(color);
			graphics2d.draw(var_i);
		}
	}

	void a(Polygon polygon, Color color) {
		if (map.get("on").equals(Boolean.TRUE)) {
			Graphics2D graphics2d = bImage.createGraphics();
			graphics2d.setColor(color);
			graphics2d.draw(polygon);
		}
	}

	void a(int i, int i_26_, Color color) {
		if (map.get("on").equals(Boolean.TRUE)) {
			int i_27_ = color.getRGB();
			a(i, i_26_, i_27_);
		}
	}

	void a(Polygon polygon, int[] is, int[] is_28_, double d, double d_29_) {
		if (map.get("on").equals(Boolean.TRUE)) {
			Rectangle rectangle = polygon.getBounds();
			for (int i = 0; i < is.length; i++) {
				for (int i_30_ = rectangle.x; i_30_ < rectangle.x
						+ rectangle.width; i_30_++) {
					int i_31_ = (polygon.ypoints[2] + (int) ((double) is[i] / d + ((double) (i_30_ - polygon.xpoints[2]) * d_29_)));
					if (polygon.contains(i_30_, i_31_))
						a(i_30_, i_31_, -1);
				}
			}
			for (int i = 0; i < is_28_.length; i++) {
				for (int i_32_ = rectangle.y; i_32_ < rectangle.y
						+ rectangle.height; i_32_++) {
					int i_33_ = (polygon.xpoints[1] + (int) ((double) is_28_[i]
							/ d - ((double) (i_32_ - polygon.ypoints[1]) * d_29_)));
					if (polygon.contains(i_33_, i_32_))
						a(i_33_, i_32_, -1);
				}
			}
		}
	}

	void a(int i, int i_34_, double d, int i_35_, int i_36_, int i_37_,
			int i_38_, int i_39_, int i_40_, int i_41_) {
		if (map.get("on").equals(Boolean.TRUE)) {
			double d_42_ = 3.141592653589793 * (double) i_34_ / d;
			if (Math.abs(Math.sin(d_42_)) < 0.5) {
				for (int i_43_ = i_36_; i_43_ < i_36_ + i_38_; i_43_++) {
					int i_44_ = i_39_
							+ (int) ((double) i / Math.cos(d_42_) - ((double) (i_43_ - i_40_) * Math
									.tan(d_42_)));
					if (i_44_ >= i_35_ && i_44_ < i_35_ + i_37_) {
						a(i_44_ - 1, i_43_, i_41_);
						a(i_44_, i_43_, i_41_);
						a(i_44_ + 1, i_43_, i_41_);
					}
				}
			} else {
				for (int i_45_ = i_35_; i_45_ < i_35_ + i_37_; i_45_++) {
					int i_46_ = i_40_
							+ (int) ((double) i / Math.sin(d_42_) - ((double) (i_45_ - i_39_) / Math
									.tan(d_42_)));
					if (i_46_ >= i_36_ && i_46_ < i_36_ + i_38_) {
						a(i_45_, i_46_ - 1, i_41_);
						a(i_45_, i_46_, i_41_);
						a(i_45_, i_46_ + 1, i_41_);
					}
				}
			}
		}
	}

	void _if(int[][] is, int i, int i_47_) {
		if (map.get("on").equals(Boolean.TRUE)) {
			int i_48_ = is[0].length;
			int i_49_ = is.length;
			for (int i_50_ = 0; i_50_ < i_49_; i_50_++) {
				for (int i_51_ = 0; i_51_ < i_48_; i_51_++) {
					int i_52_ = -1;
					if (is[i_50_][i_51_] < 50)
						i_52_ = 2139029504;
					else {
						if (is[i_50_][i_51_] <= 200)
							continue;
						i_52_ = 2130738944;
					}
					for (int i_53_ = 0; i_53_ < i_47_; i_53_++) {
						int i_54_ = i_50_ * i_47_ + i_53_;
						if (i_54_ == bImage.getHeight() - 1)
							break;
						for (int i_55_ = 0; i_55_ < i; i_55_++) {
							int i_56_ = i_51_ * i + i_55_;
							if (i_56_ == bImage.getWidth() - 1)
								break;
							a(i_56_, i_54_, i_52_);
						}
					}
				}
			}
		}
	}

	void a(int[][] is, int i, int i_57_) {
		if (map.get("on").equals(Boolean.TRUE)) {
			int i_58_ = is[0].length;
			int i_59_ = is.length;
			int i_60_ = 0;
			for (int i_61_ = 0; i_61_ < i_59_; i_61_++) {
				for (int i_62_ = 0; i_62_ < i_58_; i_62_++) {
					if (is[i_61_][i_62_] > i_60_)
						i_60_ = is[i_61_][i_62_];
				}
			}
			int[] is_63_ = new int[7];
			is_63_[6] = 2147418112;
			is_63_[5] = 2139029504;
			is_63_[4] = 2130771712;
			is_63_[3] = 2130738944;
			is_63_[2] = 2130706687;
			is_63_[1] = 2130706559;
			is_63_[0] = 2147483647;
			for (int i_64_ = 0; i_64_ < i_59_; i_64_++) {
				for (int i_65_ = 0; i_65_ < i_58_; i_65_++) {
					for (int i_66_ = 0; i_66_ < i_57_; i_66_++) {
						int i_67_ = i_64_ * i_57_ + i_66_;
						if (i_67_ == bImage.getHeight() - 1)
							break;
						for (int i_68_ = 0; i_68_ < i; i_68_++) {
							int i_69_ = i_65_ * i + i_68_;
							if (i_69_ == bImage.getWidth() - 1)
								break;
							if (i_60_ > 0)
								a(i_69_, i_67_, is_63_[is[i_64_][i_65_] * 6
										/ i_60_]);
						}
					}
				}
			}
		}
	}

	void a(int i, int i_70_, int i_71_, int i_72_) {
		if (map.get("on").equals(Boolean.TRUE)) {
			for (int i_73_ = 0; i_73_ < i_71_; i_73_++) {
				int i_74_ = i * i_71_ + i_73_;
				if (i_74_ == bImage.getHeight() - 1)
					break;
				for (int i_75_ = 0; i_75_ < i_71_; i_75_++) {
					int i_76_ = i_70_ * i_71_ + i_75_;
					if (i_76_ == bImage.getWidth() - 1)
						break;
					a(i_76_, i_74_, i_72_);
				}
			}
		}
	}

	void a(int i, final int x, final int y, final int[][] houghSpace) {
		final char directionChar;
		if (i == 4)
			directionChar = ' ';
		else if (i == 3)
			directionChar = 'N';
		else if (i == 0)
			directionChar = 'E';
		else if (i == 1)
			directionChar = 'S';
		else if (i == 2)
			directionChar = 'W';
		else
			directionChar = '?';
		final JDialog houghWind = new JDialog((Frame) txtArea.getParent().getParent()
				.getParent().getParent().getParent().getParent().getParent(),
				directionChar + "(" + x + "," + y + ")", false);
		houghWind.setResizable(false);
		Container container = houghWind.getContentPane();
		container.setLayout(new GridLayout(0, 1));
		int i_77_ = 0;
		boolean bool = false;
		boolean bool_78_ = false;
		int i_79_ = 0;
		int i_80_ = 0;
		for (int i_81_ = 0; i_81_ < houghSpace.length; i_81_++) {
			for (int i_82_ = 0; i_82_ < houghSpace[0].length - 1; i_82_++) {
				if (houghSpace[i_81_][i_82_] > i_77_) {
					i_77_ = houghSpace[i_81_][i_82_];
					int i_83_ = i_82_;
					int i_84_ = i_81_;
				}
				int i_85_ = Math.abs(houghSpace[i_81_][i_82_]
						- houghSpace[i_81_][i_82_ + 1]);
				if (i_85_ > i_79_)
					i_79_ = i_85_;
				int i_86_ = i_85_ * houghSpace[i_81_][i_82_];
				if (i_86_ > i_80_)
					i_80_ = i_86_;
			}
		}
		final BufferedImage bi = new BufferedImage(2 * houghSpace[0].length,
				2 * houghSpace.length, 1);
		for (int i_87_ = 0; i_87_ < houghSpace.length; i_87_++) {
			for (int i_88_ = 0; i_88_ < houghSpace[0].length - 1; i_88_++) {
				int i_89_ = houghSpace[i_87_][i_88_] * 255 / i_77_;
				i_89_ = i_89_ * 256 * 256 + i_89_ * 256 + i_89_;
				for (int i_90_ = 0; i_90_ < 2; i_90_++) {
					for (int i_91_ = 0; i_91_ < 2; i_91_++)
						bi.setRGB(2 * i_88_ + i_90_, 2 * i_87_ + i_91_, i_89_);
				}
			}
		}
		Canvas canvas;
		container.add(canvas = new Canvas() {
			public void paint(Graphics graphics) {
				graphics.drawImage(bi, 0, 0, this);
			}

			public Dimension getPreferredSize() {
				return new Dimension(2 * houghSpace[0].length,
						2 * houghSpace.length);
			}
		});
		canvas.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent mouseevent) {
				int i_96_ = mouseevent.getX() / 2;
				int i_97_ = mouseevent.getY() / 2;
				int i_98_ = houghSpace[i_97_][i_96_];
				houghWind.setTitle(directionChar + "(" + x + "," + y + ") "
						+ i_97_ + "," + i_98_);
			}
		});
		houghWind.setDefaultCloseOperation(2);
		houghWind.pack();
		houghWind.setLocation(x, y);
		houghWind.setVisible(true);
	}

	void a(int i, final int[][] cornerSpace, final Rectangle rect) {
		final JDialog cornerWind = new JDialog((Frame) txtArea.getParent()
				.getParent().getParent().getParent().getParent().getParent()
				.getParent(), "corner " + i, false);
		Container container = cornerWind.getContentPane();
		container.setLayout(new BorderLayout());
		int i_99_ = 0;
		int i_100_ = 0;
		int i_101_ = 0;
		for (int i_102_ = 0; i_102_ < cornerSpace[0].length; i_102_++) {
			for (int i_103_ = 0; i_103_ < cornerSpace.length; i_103_++) {
				if (cornerSpace[i_103_][i_102_] > i_99_)
					i_99_ = cornerSpace[i_100_ = i_103_][i_101_ = i_102_];
			}
		}
		int[] is = new int[256];
		final int theMax = i_99_;
		cornerWind.setTitle("corner " + i + "  max at (" + i_100_ + ","
				+ i_101_ + ")");
		final BufferedImage bi = new BufferedImage(cornerSpace.length,
				cornerSpace[0].length, 1);
		for (int i_104_ = 0; i_104_ < cornerSpace[0].length; i_104_++) {
			for (int i_105_ = 0; i_105_ < cornerSpace.length; i_105_++) {
				int i_106_ = cornerSpace[i_105_][i_104_] * 255 / i_99_;
				is[i_106_]++;
				i_106_ = i_106_ * 256 * 256 + i_106_ * 256 + i_106_;
				bi.setRGB(i_105_, i_104_, i_106_);
			}
		}
		final BufferedImage biHisto = new BufferedImage(256,
				cornerSpace[0].length, 1);
		for (int i_107_ = 0; i_107_ < 256; i_107_++) {
			for (int i_108_ = 0; i_108_ < is[i_107_]
					&& i_108_ < biHisto.getHeight(); i_108_++)
				biHisto.setRGB(i_107_, biHisto.getHeight() - 1 - i_108_,
						16777215);
		}
		final JSlider slider = new JSlider(0, 250, 0);
		final Canvas canv;
		container.add(canv = new Canvas() {
			public void paint(Graphics graphics) {
				int i_113_ = slider.getValue();
				for (int i_114_ = 0; i_114_ < cornerSpace[0].length; i_114_++) {
					for (int i_115_ = 0; i_115_ < cornerSpace.length; i_115_++) {
						int i_116_ = cornerSpace[i_115_][i_114_] * 255 / theMax;
						if (i_116_ >= i_113_)
							i_116_ = i_116_ * 256 * 256 + i_116_ * 256 + i_116_;
						else
							i_116_ = 0;
						bi.setRGB(i_115_, i_114_, i_116_);
					}
				}
				graphics.drawImage(bi, 0, 0, this);
				graphics.drawImage(biHisto, bi.getWidth() + 10, 0, this);
			}

			public Dimension getPreferredSize() {
				return new Dimension(cornerSpace.length, cornerSpace[0].length);
			}
		}, "Center");
		canv.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent mouseevent) {
				int i_119_ = mouseevent.getX();
				int i_120_ = mouseevent.getY();
				int i_121_ = cornerSpace[i_119_][i_120_];
				i_119_ += rect.x;
				i_120_ += rect.y;
				cornerWind
						.setTitle("(" + i_119_ + "," + i_120_ + ") " + i_121_);
			}
		});
		JPanel jpanel = new JPanel();
		jpanel.setLayout(new BorderLayout());
		int i_122_ = 0;
		jpanel.add(slider, "Center");
		Hashtable hashtable = new Hashtable();
		hashtable.put(new Integer(0), new JLabel("0"));
		hashtable.put(new Integer(50), new JLabel("50"));
		hashtable.put(new Integer(100), new JLabel("100"));
		hashtable.put(new Integer(150), new JLabel("150"));
		hashtable.put(new Integer(200), new JLabel("200"));
		hashtable.put(new Integer(250), new JLabel("250"));
		slider.setLabelTable(hashtable);
		slider.setMinorTickSpacing(25);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setSnapToTicks(true);
		final JTextField granField = new JTextField("" + i_122_, 3);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent changeevent) {
				JSlider jslider = (JSlider) changeevent.getSource();
				int i_124_ = jslider.getValue();
				granField.setText("" + i_124_);
				canv.repaint();
			}
		});
		JPanel jpanel_125_ = new JPanel();
		jpanel_125_.add(granField);
		granField.setEditable(false);
		granField.setHorizontalAlignment(0);
		jpanel.add(jpanel_125_, "North");
		container.add(jpanel, "South");
		cornerWind.setDefaultCloseOperation(2);
		cornerWind.pack();
		cornerWind.setLocation(0, 0);
		cornerWind.setVisible(true);
	}

	void a(final int x, final int y, final int[] angles) {
		final JDialog anglesWind = new JDialog((Frame) txtArea.getParent()
				.getParent().getParent().getParent().getParent().getParent()
				.getParent(), "Angles (" + x + "," + y + ")", false);
		anglesWind.setResizable(false);
		Container container = anglesWind.getContentPane();
		container.setLayout(new GridLayout(0, 1));
		int i = 0;
		for (int i_126_ = 0; i_126_ < angles.length; i_126_++) {
			if (angles[i_126_] > i)
				i = angles[i_126_];
		}
		final BufferedImage bi = new BufferedImage(256, 2 * angles.length, 1);
		for (int i_127_ = 0; i_127_ < angles.length; i_127_++) {
			for (int i_128_ = 0; i_128_ < angles[i_127_] * 256 / i; i_128_++) {
				for (int i_129_ = 0; i_129_ < 2; i_129_++)
					bi.setRGB(i_128_, 2 * i_127_ + i_129_, 16777215);
			}
		}
		Canvas canvas;
		container.add(canvas = new Canvas() {
			public void paint(Graphics graphics) {
				graphics.drawImage(bi, 0, 0, this);
			}

			public Dimension getPreferredSize() {
				return new Dimension(256, 2 * angles.length);
			}
		});
		canvas.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent mouseevent) {
				int i_134_ = mouseevent.getY() / 2;
				anglesWind.setTitle("Angles (" + x + "," + y + ") " + i_134_);
			}
		});
		anglesWind.setDefaultCloseOperation(2);
		anglesWind.pack();
		anglesWind.setLocation(x, y);
		anglesWind.setVisible(true);
	}

	void a(final BufferedImage bi, final Recognizer parser, final int[][] av,
			final int threshold, final Rectangle rect) {
		JDialog jdialog = new JDialog((Frame) txtArea.getParent().getParent()
				.getParent().getParent().getParent().getParent().getParent(),
				"hi", false);
		jdialog.setResizable(false);
		Container container = jdialog.getContentPane();
		container.setLayout(new GridLayout(0, 1));
		final af clickPoint = new af(-1, -1);
		final Canvas canv;
		container.add(canv = new Canvas() {
			public void paint(Graphics graphics) {
				graphics.drawImage(bi, 0, 0, this);
				if (clickPoint._if != -1) {
					graphics.setColor(Color.red);
					graphics.drawLine(0, clickPoint._if, bi.getWidth(),
							clickPoint._if);
				}
				if (rect != null) {
					graphics.setColor(Color.red);
					graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
				}
			}

			public Dimension getPreferredSize() {
				return new Dimension(bi.getWidth(), bi.getHeight());
			}
		});
		canv.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent mouseevent) {
				int i = mouseevent.getX();
				int i_137_ = mouseevent.getY();
				clickPoint._if = i_137_;
				canv.repaint();
				ImgManager var_k = parser.a();
				int[] is = var_k.getRow(i_137_);
				a(is, is.length, av[i_137_], threshold, null);
			}
		});
		jdialog.setDefaultCloseOperation(2);
		jdialog.pack();
		jdialog.setLocation(0, 0);
		jdialog.setVisible(true);
	}

	void a(final int[][] av, int i, int i_138_, final Rectangle rect) {
		int i_139_ = av[0].length;
		int i_140_ = av.length;
		final BufferedImage bi = new BufferedImage(i_139_, i_140_, 10);
		for (int i_141_ = 0; i_141_ < i_140_; i_141_++) {
			for (int i_142_ = 0; i_142_ < i_139_; i_142_++) {
				int i_143_ = av[i_141_][i_142_] * 255 / i;
				bi.setRGB(i_142_, i_141_, i_143_ * 65793);
			}
		}
		final JDialog monoWind = new JDialog((Frame) txtArea.getParent().getParent()
				.getParent().getParent().getParent().getParent().getParent(),
				"hi", false);
		monoWind.setResizable(false);
		Container container = monoWind.getContentPane();
		container.setLayout(new GridLayout(0, 1));
		Canvas canvas;
		container.add(canvas = new Canvas() {
			public void paint(Graphics graphics) {
				graphics.drawImage(bi, 0, 0, this);
				if (rect != null) {
					graphics.setColor(Color.white);
					graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
				}
			}

			public Dimension getPreferredSize() {
				return new Dimension(bi.getWidth(), bi.getHeight());
			}
		});
		canvas.addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseMoved(MouseEvent mouseevent) {
				int i_146_ = mouseevent.getX();
				int i_147_ = mouseevent.getY();
				int i_148_ = av[i_147_][i_146_];
				monoWind.setTitle("(" + i_146_ + "," + i_147_ + ") " + i_148_);
			}
		});
		monoWind.setDefaultCloseOperation(2);
		monoWind.pack();
		monoWind.setLocation(0, 0);
		monoWind.setVisible(true);
	}

	void a(final byte[][] av, int i) {
		if (map.get("on").equals(Boolean.TRUE)) {
			int i_149_ = av[0].length;
			int i_150_ = av.length;
			final BufferedImage bi = new BufferedImage(i_149_, i_150_, 10);
			for (int i_151_ = 0; i_151_ < i_150_; i_151_++) {
				for (int i_152_ = 0; i_152_ < i_149_; i_152_++) {
					int i_153_ = (av[i_151_][i_152_] & i) == 0 ? 0 : 16777215;
					bi.setRGB(i_152_, i_151_, i_153_);
				}
			}
			final JDialog monoWind = new JDialog((Frame) txtArea.getParent()
					.getParent().getParent().getParent().getParent()
					.getParent().getParent(), "hi", false);
			monoWind.setResizable(false);
			Container container = monoWind.getContentPane();
			container.setLayout(new GridLayout(0, 1));
			Canvas canvas;
			container.add(canvas = new Canvas() {
				public void paint(Graphics graphics) {
					graphics.drawImage(bi, 0, 0, this);
				}

				public Dimension getPreferredSize() {
					return new Dimension(bi.getWidth(), bi.getHeight());
				}
			});
			canvas.addMouseMotionListener(new MouseMotionAdapter() {
				public void mouseMoved(MouseEvent mouseevent) {
					int i_156_ = mouseevent.getX();
					int i_157_ = mouseevent.getY();
					byte i_158_ = av[i_157_][i_156_];
					monoWind.setTitle("(" + i_156_ + "," + i_157_ + ") "
							+ i_158_);
				}
			});
			monoWind.setDefaultCloseOperation(2);
			monoWind.pack();
			monoWind.setLocation(0, 0);
			monoWind.setVisible(true);
		}
	}

	void a(int i, int i_159_, final int angle, final int[] val, int i_160_,
			int[] is, int i_161_, int i_162_) {
		if (map.get("on").equals(Boolean.TRUE)) {
			int i_163_ = 0;
			for (int i_164_ = 0; i_164_ < val.length; i_164_++) {
				if (val[i_164_] > i_163_)
					i_163_ = val[i_164_];
			}
			if (i_163_ != 0) {
				final int max = i_163_;
				i_160_ = i_163_ / 2;
				final JDialog histoWind = new JDialog((Frame) txtArea.getParent()
						.getParent().getParent().getParent().getParent()
						.getParent().getParent(), "angle=" + angle + "   max="
						+ i_163_, false);
				Container container = histoWind.getContentPane();
				container.setLayout(new GridLayout(0, 1));
				final BufferedImage bi = new BufferedImage(2 * val.length, 256,
						1);
				int i_165_ = 0;
				for (int i_166_ = 0; i_166_ < val.length; i_166_++) {
					if (val[i_166_] >= 0) {
						for (int i_167_ = 0; i_167_ < val[i_166_] * 256
								/ i_163_; i_167_++) {
							for (int i_168_ = 0; i_168_ < 2; i_168_++)
								bi.setRGB(i_166_ * 2 + i_168_, 255 - i_167_,
										-65536);
						}
						i_165_ += val[i_166_];
						int i_169_ = i_165_ / (i_166_ + 1);
						int i_170_ = 255 - i_169_ * 256 / i_163_;
						if (i_170_ >= 0)
							bi.setRGB(i_166_ * 2, i_170_, -1);
						if (i_166_ > i_161_) {
							for (int i_171_ = val[i_166_] * 256 / i_163_; i_171_ < 256; i_171_++) {
								for (int i_172_ = 0; i_172_ < 2; i_172_++)
									bi.setRGB(i_166_ * 2 + i_172_,
											255 - i_171_, i_162_);
							}
						}
					}
				}
				if (i_160_ != -1) {
					for (int i_173_ = 0; i_173_ < val.length; i_173_++) {
						for (int i_174_ = 0; i_174_ < 2; i_174_++)
							bi.setRGB(i_173_ * 2 + i_174_, 256 - 256 * i_160_
									/ i_163_, -16777216);
					}
				}
				if (is != null) {
					for (int i_175_ = 1; i_175_ <= is[0]; i_175_++) {
						_do("i=" + i_175_ + "   peaks[ i ] " + is[i_175_]
								+ "   val.length=" + val.length);
						for (int i_176_ = 0; i_176_ < 10; i_176_++)
							bi.setRGB(is[i_175_] * 2, 255 - i_176_, 16777215);
					}
				}
				Canvas canvas;
				container.add(canvas = new Canvas() {
					public void paint(Graphics graphics) {
						graphics.drawImage(bi, 0, 0, this);
					}

					public Dimension getPreferredSize() {
						return new Dimension(2 * val.length, 256);
					}
				});
				canvas.addMouseMotionListener(new MouseMotionAdapter() {
					public void mouseMoved(MouseEvent mouseevent) {
						int i_182_ = mouseevent.getX() / 2;
						histoWind.setTitle("angle=" + angle + "   max=" + max
								+ "   r=" + i_182_);
					}
				});
				histoWind.setDefaultCloseOperation(2);
				histoWind.pack();
				histoWind.setLocation(i, i_159_);
				histoWind.setVisible(true);
			}
		}
	}

	void a(int[] is, final int xWidth, int[] is_183_, int i, int[] is_184_) {
		if (map.get("on").equals(Boolean.TRUE)) {
			JDialog jdialog = new JDialog((Frame) txtArea.getParent().getParent()
					.getParent().getParent().getParent().getParent()
					.getParent(), "intensities", false);
			Container container = jdialog.getContentPane();
			container.setLayout(new GridLayout(0, 1));
			final BufferedImage bi = new BufferedImage(xWidth, 256, 1);
			for (int i_185_ = 0; i_185_ < xWidth; i_185_++) {
				for (int i_186_ = 0; i_186_ <= is[i_185_]; i_186_++)
					bi.setRGB(i_185_, 255 - i_186_, 16777215);
			}
			if (is_183_ != null) {
				for (int i_187_ = 0; i_187_ < xWidth; i_187_++) {
					bi.setRGB(i_187_, 255 - is_183_[i_187_], 16711680);
					bi.setRGB(i_187_, 255 - is_183_[i_187_] * i / 100, 65280);
				}
			}
			if (is_184_ != null) {
				for (int i_188_ = 0; i_188_ < xWidth; i_188_++)
					bi.setRGB(i_188_, 255 - is_184_[i_188_], 255);
			}
			Canvas canvas;
			container.add(canvas = new Canvas() {
				public void paint(Graphics graphics) {
					graphics.drawImage(bi, 0, 0, this);
				}

				public Dimension getPreferredSize() {
					return new Dimension(xWidth, 255);
				}
			});
			jdialog.setDefaultCloseOperation(2);
			jdialog.pack();
			jdialog.setLocation(0, 0);
			jdialog.setVisible(true);
		}
	}

	void a(long[] ls) {
		if (map.get("on").equals(Boolean.TRUE)) {
			final JDialog histoWind = new JDialog((Frame) txtArea.getParent()
					.getParent().getParent().getParent().getParent()
					.getParent().getParent(), "intensities", false);
			Container container = histoWind.getContentPane();
			container.setLayout(new GridLayout(0, 1));
			long l = -1L;
			for (int i = 0; i < ls.length; i++) {
				if (ls[i] > l)
					l = ls[i];
			}
			final BufferedImage bi = new BufferedImage(256, 1024, 1);
			for (int i = 0; i < ls.length; i++) {
				long l_191_ = ls[i] * 1024L;
				l_191_ /= l;
				for (int i_192_ = 0; i_192_ < (int) l_191_; i_192_++)
					bi.setRGB(i, 1023 - i_192_, 16777215);
			}
			Canvas canvas;
			container.add(canvas = new Canvas() {
				public void paint(Graphics graphics) {
					graphics.drawImage(bi, 0, 0, this);
				}

				public Dimension getPreferredSize() {
					return new Dimension(256, 1024);
				}
			});
			canvas.addMouseMotionListener(new MouseMotionAdapter() {
				public void mouseMoved(MouseEvent mouseevent) {
					int i = mouseevent.getX();
					histoWind.setTitle("intense = " + i);
				}
			});
			histoWind.setDefaultCloseOperation(2);
			histoWind.pack();
			histoWind.setLocation(0, 0);
			histoWind.setVisible(true);
		}
	}
}
