/* a5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.HashMap;

abstract class BarcodeMix extends Barcode {
	private static final boolean bR = false;

	private HashMap bQ;

	private int[] bP = new int[10];

	private int[] bS = new int[10];

	private int[] bT = new int[10];

	BarcodeMix(HashMap hashmap, PropertiesHolder var_c) {
		super(var_c);
		bQ = hashmap;
	}

	protected final int _do(int[] is, int i, int i_0_, int i_1_, int i_2_,
			int i_3_) {
		int i_4_ = 0;
		for (int i_5_ = 0; i_5_ < i_0_; i_5_++) {
			bP[i_5_] = is[i + i_5_] * i_1_;
			i_4_ += is[i + i_5_];
		}
		for (int i_6_ = 0; i_6_ < i_0_; i_6_++)
			bS[i_6_] = bP[i_6_];
		for (int i_7_ = 0; i_7_ < i_0_; i_7_++) {
			int i_8_ = bS[i_7_] / i_4_;
			int i_9_ = bS[i_7_] % i_4_;
			if (i_8_ == 0 || i_9_ > i_4_ / 2) {
				i_8_++;
				i_9_ -= i_4_;
			}
			bT[i_7_] = i_9_;
			if (i_7_ < i_0_ - 1)
				bS[i_7_ + 1] += i_9_;
			bS[i_7_] = i_8_;
		}
		Object object = null;
		boolean bool = false;
		int i_10_ = 1;
		int i_11_ = 0;
		for (int i_12_ = 0; i_12_ < i_0_; i_12_++) {
			i_11_ += bS[i_0_ - i_12_ - 1] * i_10_;
			i_10_ *= 10;
		}
		object = bQ.get(new Integer(i_11_));
		int i_13_;
		if (object != null)
			i_13_ = ((Integer) object).intValue();
		else
			i_13_ = -1;
		if (i_13_ >= i_2_ && i_13_ <= i_3_)
			return i_13_;
		int i_14_ = 0;
		int i_15_ = Math.abs(bT[0]);
		for (int i_16_ = 1; i_16_ < i_0_ - 1; i_16_++) {
			if (Math.abs(bT[i_16_]) > i_15_) {
				i_15_ = Math.abs(bT[i_16_]);
				i_14_ = i_16_;
			}
		}
		for (int i_17_ = 0; i_17_ < i_0_; i_17_++)
			bS[i_17_] = bP[i_17_];
		for (int i_18_ = 0; i_18_ < i_0_; i_18_++) {
			int i_19_ = bS[i_18_] / i_4_;
			int i_20_ = bS[i_18_] % i_4_;
			if (i_19_ == 0) {
				i_19_++;
				i_20_ -= i_4_;
			} else if (i_18_ == i_14_) {
				if (i_20_ <= i_4_ / 2) {
					i_19_++;
					i_20_ -= i_4_;
				}
			} else if (i_20_ > i_4_ / 2) {
				i_19_++;
				i_20_ -= i_4_;
			}
			if (i_18_ < i_0_ - 1)
				bS[i_18_ + 1] += i_20_;
			bS[i_18_] = i_19_;
		}
		i_10_ = 1;
		i_11_ = 0;
		for (int i_21_ = 0; i_21_ < i_0_; i_21_++) {
			i_11_ += bS[i_0_ - i_21_ - 1] * i_10_;
			i_10_ *= 10;
		}
		object = bQ.get(new Integer(i_11_));
		if (object != null)
			i_13_ = ((Integer) object).intValue();
		else
			i_13_ = -1;
		if (i_13_ >= i_2_ && i_13_ <= i_3_)
			return i_13_;
		for (int i_22_ = 0; i_22_ < i_0_; i_22_++)
			bS[i_22_] = bP[i_22_];
		for (int i_23_ = i_0_ - 1; i_23_ >= 0; i_23_--) {
			int i_24_ = bS[i_23_] / i_4_;
			int i_25_ = bS[i_23_] % i_4_;
			if (i_24_ == 0 || i_25_ > i_4_ / 2) {
				i_24_++;
				i_25_ -= i_4_;
			}
			bT[i_23_] = i_25_;
			if (i_23_ > 0)
				bS[i_23_ - 1] += i_25_;
			bS[i_23_] = i_24_;
		}
		i_10_ = 1;
		i_11_ = 0;
		for (int i_26_ = 0; i_26_ < i_0_; i_26_++) {
			i_11_ += bS[i_0_ - i_26_ - 1] * i_10_;
			i_10_ *= 10;
		}
		object = bQ.get(new Integer(i_11_));
		if (object != null)
			i_13_ = ((Integer) object).intValue();
		else
			i_13_ = -1;
		if (i_13_ >= i_2_ && i_13_ <= i_3_)
			return i_13_;
		i_14_ = i_0_ - 1;
		i_15_ = Math.abs(bT[i_0_ - 1]);
		for (int i_27_ = i_0_ - 1; i_27_ > 0; i_27_--) {
			if (Math.abs(bT[i_27_]) > i_15_) {
				i_15_ = Math.abs(bT[i_27_]);
				i_14_ = i_27_;
			}
		}
		for (int i_28_ = 0; i_28_ < i_0_; i_28_++)
			bS[i_28_] = bP[i_28_];
		for (int i_29_ = i_0_ - 1; i_29_ >= 0; i_29_--) {
			int i_30_ = bS[i_29_] / i_4_;
			int i_31_ = bS[i_29_] % i_4_;
			if (i_30_ == 0) {
				i_30_++;
				i_31_ -= i_4_;
			} else if (i_29_ == i_14_) {
				if (i_31_ <= i_4_ / 2) {
					i_30_++;
					i_31_ -= i_4_;
				}
			} else if (i_31_ > i_4_ / 2) {
				i_30_++;
				i_31_ -= i_4_;
			}
			if (i_29_ > 0)
				bS[i_29_ - 1] += i_31_;
			bS[i_29_] = i_30_;
		}
		i_10_ = 1;
		i_11_ = 0;
		for (int i_32_ = 0; i_32_ < i_0_; i_32_++) {
			i_11_ += bS[i_0_ - i_32_ - 1] * i_10_;
			i_10_ *= 10;
		}
		object = bQ.get(new Integer(i_11_));
		if (object != null)
			i_13_ = ((Integer) object).intValue();
		else
			i_13_ = -1;
		if (i_13_ >= i_2_ && i_13_ <= i_3_)
			return i_13_;
		return -1;
	}
}
