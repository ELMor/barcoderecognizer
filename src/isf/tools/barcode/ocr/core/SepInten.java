/* d - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

class SepInten {
	private static final boolean _case = false;

	static final int l = 0;

	static final int _try = 1;

	static final int r = 2;

	static final int k = 3;

	private PropertiesHolder d;

	private int _for;

	int c;

	private int direction;

	private boolean isStartEdge;

	private double gradient;

	private double intercept;

	private boolean i;

	private double f;

	private int _do;

	private int _goto;

	private int _if;

	private ArrayList scans;

	private Rectangle rectangle;

	private UserRect userRect;

	boolean p;

	private double o = 0.0;

	private double n = 0.0;

	private double g = 0.0;

	private double _long = 0.0;

	private double m = 0.0;

	private int bwBound;

	private int e = 0;

	private int _byte = 0;

	private ad j;

	private ArrayList a = null;

	int e() {
		if (_byte == 0)
			return 0;
		return e / _byte;
	}

	void a(int i) {
		e += i;
		_byte++;
	}

	SepInten(int dir, int i_0_, Rectangle r, boolean stEdge, double d, int i_1_,
			int bwbound) {
		_for = i_0_;
		direction = dir;
		isStartEdge = stEdge;
		f = d;
		_do = i_1_;
		bwBound = bwbound;
		p = false;
		this.i = false;
		scans = new ArrayList();
		_if = 0;
		_goto = -1;
		a(r);
	}

	SepInten(int i, int i_3_, Rectangle rectangle, boolean bool, double d, int i_4_,
			int i_5_, PropertiesHolder var_c) {
		this(i, i_3_, rectangle, bool, d, i_4_, i_5_);
		this.d = var_c;
	}

	SepInten(int i, SepInten sep) {
		direction = sep.direction;
		isStartEdge = false;
		int i_7_ = i == 2 ? 20 : 47;
		double d = (double) (sep._goto * i_7_ / 11);
		double d_8_ = 0.0;
		double d_9_ = 0.0;
		switch (direction) {
		case 90:
			d_8_ = d / (1.0 + Math.abs(sep.gradient));
			d_9_ = d - d_8_;
			if (sep.gradient > 0.0)
				d_9_ = -d_9_;
			break;
		case 270:
			d_8_ = d / (1.0 + Math.abs(sep.gradient));
			d_9_ = d - d_8_;
			d_8_ = -d_8_;
			if (sep.gradient < 0.0)
				d_9_ = -d_9_;
			break;
		case 180:
			d_9_ = d / (1.0 + Math.abs(sep.gradient));
			d_8_ = d - d_9_;
			if (sep.gradient > 0.0)
				d_8_ = -d_8_;
			break;
		case 360:
			d_9_ = d / (1.0 + Math.abs(sep.gradient));
			d_8_ = d - d_9_;
			d_9_ = -d_9_;
			if (sep.gradient < 0.0)
				d_8_ = -d_8_;
			break;
		}
		userRect = new UserRect(sep.userRect.getP1x() + d_8_, sep.userRect.getP1y() + d_9_,
				sep.userRect.getP2x() + d_8_, sep.userRect.getP2y() + d_9_);
		this.i = false;
	}

	SepInten(SepInten var_d_10_, UserRect usrRect, int bwbound) {
		isStartEdge = false;
		userRect = usrRect;
		_goto = var_d_10_._do();
		p = true;
		this.i = false;
		bwBound = bwbound;
	}

	SepInten(UserRect usrRect, int bwbound) {
		userRect = usrRect;
		bwBound = bwbound;
	}

	void _for() {
		_goto = _do();
	}

	void clear() {
		scans.clear();
		scans = null;
		if (a != null) {
			a.clear();
			a = null;
		}
	}

	int getBWBound() {
		return bwBound;
	}

	int getDirection() {
		return direction;
	}

	int _try() {
		return _for;
	}

	ArrayList getScans() {
		return scans;
	}

	Rectangle _char() {
		if (rectangle == null) {
			rectangle = (Rectangle) scans.get(0);
			Iterator t = scans.iterator();
			while (t.hasNext())
				rectangle = rectangle.union((Rectangle) t.next());
		}
		return rectangle;
	}

	UserRect getUserRect() {
		return userRect;
	}

	PointDouble getP1() {
		return userRect.getP1();
	}

	PointDouble getP2() {
		return userRect.getP2();
	}

	Rectangle getRectangle() {
		return userRect.getRectangle();
	}

	void a(ad var_ad) {
		j = var_ad;
	}

	ad b() {
		return j;
	}

	public String toString() {
		String string = ((isStartEdge ? "startEdge" : "endEdge") + " >> "
				+ "  direction=" + direction + "  gradient=" + (int) (gradient * 100.0)
				+ "  intercept=" + (int) intercept);
		if (userRect != null)
			string += ("  line=(" + (int) userRect.getP1x() + " , " + (int) userRect.getP1y()
					+ " )  (" + (int) userRect.getP2x() + " , " + (int) userRect.getP2y() + " )");
		string = string + "  bwBound = " + bwBound
				+ (scans == null ? "" : "  scans=" + scans.size());
		if (j != null)
			string += "\r\n" + j;
		return string;
	}

	void a(Rectangle rectangle) {
		SepInten var_d_11_ = this;
		var_d_11_._if = var_d_11_._if
				+ (direction == 90 || direction == 270 ? rectangle.width
						: rectangle.height);
		int i = 0;
		int i_12_ = 0;
		switch (direction) {
		case 90:
			i = rectangle.x;
			if (!isStartEdge)
				i += rectangle.width - 1;
			i_12_ = rectangle.y;
			break;
		case 270:
			i = rectangle.x;
			if (isStartEdge)
				i += rectangle.width - 1;
			i_12_ = rectangle.y;
			break;
		case 180:
			i = rectangle.x;
			i_12_ = rectangle.y;
			if (!isStartEdge)
				i_12_ += rectangle.height - 1;
			break;
		case 360:
			i = rectangle.x;
			i_12_ = rectangle.y;
			if (isStartEdge)
				i_12_ += rectangle.height - 1;
			break;
		}
		o += (double) i;
		n += (double) i_12_;
		g += (double) (i * i_12_);
		_long += (double) (i * i);
		m += (double) (i_12_ * i_12_);
		scans.add(rectangle);
	}

	int _do() {
		return _goto == -1 ? _if / scans.size() : _goto;
	}

	boolean a(Rectangle rectangle, boolean bool) {
		boolean bool_13_ = direction == 90 || direction == 270;
		Rectangle rectangle_14_ = (Rectangle) scans.get(scans.size() - 1);
		int i = bool ? 6 : 2;
		if (bool_13_) {
			if (Math.abs(rectangle.y - rectangle_14_.y) > _do() / i)
				return false;
		} else if (Math.abs(rectangle.x - rectangle_14_.x) > _do() / i)
			return false;
		int i_15_ = scans.size();
		if (i_15_ == 1) {
			if (!bool) {
				if (bool_13_)
					return ((rectangle.x + rectangle.width - 1 >= rectangle_14_.x) && (rectangle.x <= rectangle_14_.x
							+ rectangle_14_.width - 1));
				return (rectangle.y + rectangle.height - 1 >= rectangle_14_.y && (rectangle.y <= rectangle_14_.y
						+ rectangle_14_.height - 1));
			}
			if (bool_13_)
				return (Math.abs(rectangle.x - rectangle_14_.x) < _do() / i && (Math
						.abs(rectangle.width - rectangle_14_.width) < _do() / i));
			return (Math.abs(rectangle.y - rectangle_14_.y) < _do() / i && (Math
					.abs(rectangle.height - rectangle_14_.height) < _do() / i));
		}
		double d = (double) i_15_ * g - o * n;
		double d_16_;
		double d_17_;
		if (bool_13_) {
			d_16_ = (double) i_15_ * m - n * n;
			d_17_ = m * o - g * n;
		} else {
			d_16_ = (double) i_15_ * _long - o * o;
			d_17_ = _long * n - g * o;
		}
		double d_18_ = d / d_16_;
		double d_19_ = d_17_ / d_16_;
		if (bool_13_) {
			double d_20_ = d_18_ * (double) rectangle.y + d_19_;
			if (direction == 90 && isStartEdge || direction == 270 && !isStartEdge)
				return (Math.abs((double) rectangle.x - d_20_) < (double) (_do() / i));
			return (Math.abs((double) (rectangle.x + rectangle.width - 1)
					- d_20_) < (double) (_do() / i));
		}
		double d_21_ = d_18_ * (double) rectangle.x + d_19_;
		if (direction == 180 && isStartEdge || direction == 360 && !isStartEdge)
			return (Math.abs((double) rectangle.y - d_21_) < (double) (_do() / i));
		return (Math.abs((double) (rectangle.y + rectangle.height - 1) - d_21_) < (double) (_do() / i));
	}

	void a(int i, int i_22_) {
		getUserRect().sumVector(i, i_22_);
		if (direction == 90 || direction == 270)
			intercept += (double) i;
		else
			intercept += (double) i_22_;
	}

	final boolean _if(int i) {
		if (this.i)
			return true;
		this.i = true;
		int i_23_ = i == 9 ? 8 : 3;
		int i_24_;
		int i_25_;
		int i_26_;
		int i_27_;
		for (;;) {
			int size = scans.size();
			if (size < i_23_)
				return false;
			double d = (double) size * g - o * n;
			double d_29_;
			double d_30_;
			if (direction == 90 || direction == 270) {
				d_29_ = (double) size * m - n * n;
				d_30_ = m * o - g * n;
			} else {
				d_29_ = (double) size * _long - o * o;
				d_30_ = _long * n - g * o;
			}
			gradient = d / d_29_;
			intercept = d_30_ / d_29_;
			double d_31_ = 0.0;
			double d_32_ = 0.0;
			int i_33_ = -1;
			int i_34_ = -1;
			int i_35_ = -1;
			int w = -1;
			i_24_ = 2147483647;
			i_25_ = 2147483647;
			i_26_ = -2147483648;
			i_27_ = -2147483648;
			for (int k = 0; k < size; k++) {
				int posX = 0;
				int posY = 0;
				int width = 0;
				Rectangle rectangle = (Rectangle) scans.get(k);
				switch (direction) {
				case 90:
					posX = rectangle.x;
					width = rectangle.width;
					if (!isStartEdge)
						posX += width - 1;
					posY = rectangle.y;
					d_32_ = (double) posX - gradient * (double) posY - intercept;
					if (posY < i_25_)
						i_25_ = posY;
					if (posY > i_27_)
						i_27_ = posY;
					break;
				case 270:
					posX = rectangle.x;
					width = rectangle.width;
					if (isStartEdge)
						posX += width - 1;
					posY = rectangle.y;
					d_32_ = (double) posX - gradient * (double) posY - intercept;
					if (posY < i_25_)
						i_25_ = posY;
					if (posY > i_27_)
						i_27_ = posY;
					break;
				case 180:
					posX = rectangle.x;
					posY = rectangle.y;
					width = rectangle.height;
					if (!isStartEdge)
						posY += width - 1;
					d_32_ = (double) posY - gradient * (double) posX - intercept;
					if (posX < i_24_)
						i_24_ = posX;
					if (posX > i_26_)
						i_26_ = posX;
					break;
				case 360:
					posX = rectangle.x;
					posY = rectangle.y;
					width = rectangle.height;
					if (isStartEdge)
						posY += width - 1;
					d_32_ = (double) posY - gradient * (double) posX - intercept;
					if (posX < i_24_)
						i_24_ = posX;
					if (posX > i_26_)
						i_26_ = posX;
					break;
				}
				if (Math.abs(d_32_) > d_31_) {
					d_31_ = Math.abs(d_32_);
					i_33_ = k;
					i_34_ = posX;
					i_35_ = posY;
					w = width;
				}
			}
			if (!(d_31_ > (double) (_do() / 4)))
				break;
			scans.remove(i_33_);
			o -= (double) i_34_;
			n -= (double) i_35_;
			g -= (double) (i_34_ * i_35_);
			_long -= (double) (i_34_ * i_34_);
			m -= (double) (i_35_ * i_35_);
			_if -= w;
		}
		if (direction == 90 || direction == 270)
			userRect = new UserRect(gradient * (double) i_25_ + intercept, (double) i_25_, gradient
					* (double) i_27_ + intercept, (double) i_27_);
		else
			userRect = new UserRect((double) i_24_, gradient * (double) i_24_ + intercept,
					(double) i_26_, gradient * (double) i_26_ + intercept);
		return true;
	}

	double _else() {
		return gradient;
	}

	double _if() {
		return intercept;
	}

	boolean a(SepInten var_d_41_, int i, PropertiesHolder var_c) {
		if (var_c != null) {
			if (scans.size() > 40 && var_d_41_.scans.size() > 40)
				var_c
						._do("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			var_c._do("In Edge.matches()");
			var_c._do("start: " + toString());
			var_c._do("start: " + var_d_41_.toString());
		}
		if (direction != var_d_41_.getDirection())
			return false;
		int i_42_ = var_d_41_.getBWBound();
		if (i == 1) {
			if (bwBound != i_42_)
				return false;
			if (bwBound == 128)
				return false;
		} else if (i == 2 && bwBound == i_42_)
			return false;
		if (_do() < var_d_41_._do() * 2 / 3 || _do() > var_d_41_._do() * 2) {
			if (var_c != null)
				var_c._do("avSymbWidth no match, start = " + _do() + "  end = "
						+ var_d_41_._do());
			return false;
		}
		if (Math.abs(_else() - var_d_41_._else()) > 0.3) {
			if (var_c != null)
				var_c._do("gradient no match");
			return false;
		}
		if (f != 0.0 && var_d_41_.f != 0.0) {
			double d = 0.0;
			if (direction == 90 || direction == 270)
				d = (_else() * getP1().getY() + _if() - var_d_41_._else() * getP1().getY() - var_d_41_
						._if());
			else
				d = (_else() * getP1().getX() + _if() - var_d_41_._else()
						* getP1().getX() - var_d_41_._if());
			d = Math.abs(d);
			if (Math.abs(d - (double) _do() * f) > (double) (4 * _do())) {
				if (var_c != null)
					var_c._do("distance1 no match, delta = " + d
							+ "   avSymbWidth = " + _do());
				return false;
			}
			if (Math.abs(d - (double) var_d_41_._do() * var_d_41_.f) > (double) (4 * var_d_41_
					._do())) {
				if (var_c != null)
					var_c._do("distance2 no match");
				return false;
			}
		}
		if (b() != null && var_d_41_.b() != null) {
			ad var_ad = b();
			ad var_ad_43_ = var_d_41_.b();
			if (var_ad == null || var_ad_43_ == null)
				return false;
			if (!var_ad.a(var_ad_43_))
				return false;
		}
		double d;
		double d_44_;
		if (direction == 90 || direction == 270) {
			d_44_ = (((getP1().getY() + getP2().getY()) / 2.0 + _else()
					* (getP1().getX() + getP2().getX()) / 2.0 - _else()
					* var_d_41_._if()) / (1.0 + _else() * var_d_41_._else()));
			d = var_d_41_._else() * d_44_ + var_d_41_._if();
		} else {
			d = (((getP1().getX() + getP2().getX()) / 2.0 + _else()
					* (getP1().getY() + getP2().getY()) / 2.0 - _else() * var_d_41_._if()) / (1.0 + _else()
					* var_d_41_._else()));
			d_44_ = var_d_41_._else() * d + var_d_41_._if();
		}
		double d_45_ = var_d_41_.getP1().mod(var_d_41_.getP2());
		double d_46_ = var_d_41_.getP1().mod(d, d_44_);
		double d_47_ = var_d_41_.getP2().mod(d, d_44_);
		if (d_46_ < d_45_ && d_47_ < d_45_)
			return true;
		if (Math.abs(d_46_ - d_45_) < (double) _do()
				|| Math.abs(d_47_ - d_45_) < (double) _do())
			return true;
		if (var_c != null)
			var_c._do("align no match");
		return false;
	}

	final void a(SepInten var_d_48_) {
		if (a == null)
			a = new ArrayList();
		a.add(var_d_48_);
	}

	void _goto() {
		if (a != null)
			a.clear();
	}

	ArrayList a() {
		return a;
	}

	boolean _if(SepInten var_d_49_) {
		if (this == var_d_49_)
			return true;
		double d = (double) _goto / 4.0;
		if (Math.abs(userRect.getP1().getX() - var_d_49_.userRect.getP1().getX()) < d
				&& Math.abs(userRect.getP1().getY() - var_d_49_.userRect.getP1().getY()) < d
				&& Math.abs(userRect.getP2().getX() - var_d_49_.userRect.getP2().getX()) < d
				&& Math.abs(userRect.getP2().getY() - var_d_49_.userRect.getP2().getY()) < d)
			return true;
		double d_50_ = userRect._if(var_d_49_.userRect.getP1());
		double d_51_ = userRect._if(var_d_49_.userRect.getP2());
		double d_52_ = var_d_49_.userRect._if(userRect.getP1());
		double d_53_ = var_d_49_.userRect._if(userRect.getP2());
		double d_54_ = Math.min(Math.min(d_50_, d_51_), Math.min(d_52_, d_53_));
		return d_54_ < d;
	}

	void a(Recognizer var_a1, int[] is, PropertiesHolder var_c) {
		if (!p) {
			double d = Math.sqrt(gradient * gradient + 1.0);
			double d_55_ = gradient / d;
			double d_56_ = 1.0 / d;
			int i = (int) ((double) _goto * d_56_ * d_56_);
			int i_57_ = i;
			int i_58_ = (int) ((double) _goto * d_56_ * d_55_);
			boolean bool = true;
			switch (direction) {
			case 90:
				i_58_ = -i_58_;
				break;
			case 270:
				i_57_ = -i_57_;
				break;
			case 180: {
				bool = false;
				int i_59_ = i_57_;
				i_57_ = -i_58_;
				i_58_ = i_59_;
				break;
			}
			case 360: {
				bool = false;
				int i_60_ = i_57_;
				i_57_ = i_58_;
				i_58_ = -i_60_;
				break;
			}
			}
			if (!isStartEdge) {
				i_57_ = -i_57_;
				i_58_ = -i_58_;
			}
			ad var_ad = b();
			if (var_ad != null) {
				if (!isStartEdge) {
					i_57_ *= 2;
					i_58_ *= 2;
				}
				var_ad.a(userRect, -i_57_, -i_58_);
				p = true;
			} else {
				int i_61_;
				int i_62_;
				if (bool) {
					i_61_ = (int) userRect.getP1().getY();
					i_62_ = (int) userRect.getP2().getY();
				} else {
					i_61_ = (int) userRect.getP1().getX();
					i_62_ = (int) userRect.getP2().getX();
				}
				double[] ds = new double[_do - 3];
				int i_63_ = 0;
				for (int i_64_ = 0; i_64_ < _do - 3; i_64_++)
					ds[i_64_] = 0.0;
				for (int i_65_ = i_61_; i_65_ <= i_62_; i_65_++) {
					int i_66_;
					int i_67_;
					if (bool) {
						i_67_ = i_65_;
						i_66_ = (int) (gradient * (double) i_67_ + intercept);
					} else {
						i_66_ = i_65_;
						i_67_ = (int) (gradient * (double) i_66_ + intercept);
					}
					boolean bool_68_ = false;
					int i_69_ = var_a1._if(i_66_ - i_57_, i_67_ - i_58_, i_66_
							+ i_57_, i_67_ + i_58_, is, bwBound, bwBound);
					if (a(-1, ds, i_69_, is, i, var_c))
						i_63_++;
				}
				if (i_63_ > 0) {
					for (int i_70_ = 0; i_70_ < _do - 3; i_70_++)
						ds[i_70_] /= (double) i_63_;
				}
				an var_an = new an(i_61_, false);
				int i_71_ = i_61_;
				for (;;) {
					int i_72_;
					int i_73_;
					if (bool) {
						i_73_ = i_71_;
						i_72_ = (int) (gradient * (double) i_73_ + intercept);
					} else {
						i_72_ = i_71_;
						i_73_ = (int) (gradient * (double) i_72_ + intercept);
					}
					boolean bool_74_ = false;
					int i_75_ = var_a1._if(i_72_ - i_57_, i_73_ - i_58_, i_72_
							+ i_57_, i_73_ + i_58_, is, bwBound, bwBound);
					if (a(i_63_, ds, i_75_, is, i, var_c))
						var_an.a(i_71_);
					else if (var_an.a() - i_71_ > i / 2)
						break;
					i_71_--;
				}
				if (var_an.a() < i_61_) {
					if (bool) {
						userRect.setP1x(gradient * (double) var_an.a() + intercept);
						userRect.setP1y((double) var_an.a());
					} else {
						userRect.setP1y(gradient * (double) var_an.a() + intercept);
						userRect.setP1x((double) var_an.a());
					}
				}
				var_an = new an(i_62_, true);
				i_71_ = i_62_;
				for (;;) {
					int i_76_;
					int i_77_;
					if (bool) {
						i_77_ = i_71_;
						i_76_ = (int) (gradient * (double) i_77_ + intercept);
					} else {
						i_76_ = i_71_;
						i_77_ = (int) (gradient * (double) i_76_ + intercept);
					}
					boolean bool_78_ = false;
					int i_79_ = var_a1._if(i_76_ - i_57_, i_77_ - i_58_, i_76_
							+ i_57_, i_77_ + i_58_, is, bwBound, bwBound);
					if (a(i_63_, ds, i_79_, is, i, var_c))
						var_an.a(i_71_);
					else if (i_71_ - var_an.a() > i / 2)
						break;
					i_71_++;
				}
				if (var_an.a() > i_62_) {
					if (bool) {
						userRect.setP2x(gradient * (double) var_an.a() + intercept);
						userRect.setP2y((double) var_an.a());
					} else {
						userRect.setP2y(gradient * (double) var_an.a() + intercept);
						userRect.setP2x((double) var_an.a());
					}
				}
				p = true;
			}
		}
	}

	private final boolean a(int i, double[] ds, int i_80_, int[] is, int i_81_,
			PropertiesHolder var_c) {
		int i_82_ = 0;
		int i_83_;
		for (i_83_ = 0; i_83_ < i_80_; i_83_++) {
			i_82_ += is[i_83_];
			if (i_82_ >= i_81_ * 3 / 4)
				break;
		}
		if (i_83_ == i_80_)
			return false;
		if (i_83_ % 2 == 0)
			return false;
		if (is[i_83_] < i_81_ / 2)
			return false;
		int i_84_ = i_80_ - i_83_ - 1;
		if (i_84_ < _do - 2 || i_84_ > _do + 2)
			return false;
		if (i == 0)
			return true;
		if (i > 0) {
			boolean bool = true;
			for (int i_85_ = 0; i_85_ < _do - 3; i_85_++) {
				if (Math.abs((double) is[i_85_ + i_83_ + 1] - ds[i_85_] / 2.0) > 2.0) {
					if ((double) is[i_85_ + i_83_ + 1] < ds[i_85_] / 2.0)
						bool = false;
					else if ((double) is[i_85_ + i_83_ + 1] > ds[i_85_] * 2.0)
						bool = false;
				}
			}
			return bool;
		}
		for (int i_86_ = i_83_ + 1; i_86_ < i_83_ + _do - 2; i_86_++)
			ds[i_86_ - i_83_ - 1] += (double) is[i_86_];
		return true;
	}
}
