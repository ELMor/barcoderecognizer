/* av - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

class av implements ImgManager {
	private static final boolean _char = true;

	private int _long;

	private int _case;

	private byte[][] _else;

	private byte _void;

	private int[] _goto;

	private int[] b;

	av(int i, int i_0_, Recognizer var_a1, ab var_ab, PropertiesHolder var_c) throws Exception {
		var_c.debug("starting to make Bitonal.");
		_long = i;
		_case = i_0_;
		_else = new byte[_case][_long];
		short[][] is = new short[_case + 30][_long + 30];
		for (int i_1_ = 0; i_1_ < _long; i_1_++) {
			if (var_ab.a() == true) {
				var_c._do("Aborting (1) make of Bitonal.");
				throw new Exception();
			}
			int i_2_ = i_1_ + 15;
			for (int i_3_ = 0; i_3_ < _case; i_3_++) {
				short i_4_ = (short) var_a1.a(i_1_, i_3_);
				int i_5_ = i_3_ + 15;
				for (int i_6_ = -15; i_6_ <= 15; i_6_++) {
					is[i_5_][i_2_ + i_6_] += i_4_;
					is[i_5_ + i_6_][i_2_] += i_4_;
					is[i_5_ + i_6_][i_2_ + i_6_] += i_4_;
					is[i_5_ - i_6_][i_2_ + i_6_] += i_4_;
				}
			}
		}
		short i_7_ = 31;
		i_7_ *= 4;
		for (int i_8_ = 0; i_8_ < _long; i_8_++) {
			if (var_ab.a() == true) {
				var_c._do("Aborting (2) make of Bitonal.");
				throw new Exception();
			}
			int i_9_ = i_8_ + 15;
			for (int i_10_ = 0; i_10_ < _case; i_10_++) {
				int i_11_ = var_a1.a(i_8_, i_10_);
				int i_12_ = is[i_10_ + 15][i_9_] / i_7_;
				if (i_11_ >= i_12_)
					_else[i_10_][i_8_] = (byte) 15;
				else if (i_11_ >= i_12_ - 5)
					_else[i_10_][i_8_] = (byte) 7;
				else if (i_11_ >= i_12_ - 10)
					_else[i_10_][i_8_] = (byte) 3;
				else if (i_11_ >= i_12_ - 15)
					_else[i_10_][i_8_] = (byte) 1;
			}
		}
		_goto = new int[_long];
		b = new int[_case];
		var_c.debug("made Bitonal ");
		var_c.a(_else, 8);
		var_c.a(_else, 4);
		var_c.a(_else, 2);
		var_c.a(_else, 1);
	}

	int _do() {
		return 4;
	}

	void _do(int i) {
		if (i == 0)
			_void = (byte) 8;
		else if (i == 1)
			_void = (byte) 4;
		else if (i == 2)
			_void = (byte) 2;
		else if (i == 3)
			_void = (byte) 1;
	}

	public int getWidth() {
		return _long;
	}

	public int getHeight() {
		return _case;
	}

	public int getPixel(int i, int i_13_) {
		return (_else[i_13_][i] & _void) == 0 ? 0 : 255;
	}

	public int[] getRow(int i) {
		for (int i_14_ = 0; i_14_ < _long; i_14_++)
			_goto[i_14_] = getPixel(i_14_, i);
		return _goto;
	}

	public int[] getCol(int i) {
		for (int i_15_ = 0; i_15_ < _case; i_15_++)
			b[i_15_] = getPixel(i, i_15_);
		return b;
	}
}
