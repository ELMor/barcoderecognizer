/* aq - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;

class UPC_E extends Barcode {
	private static final boolean be = false;

	private static final long[] a8 = { 3211L, 2221L, 2122L, 1411L, 1132L,
			1231L, 1114L, 1312L, 1213L, 3112L };

	private static final long[] bg = { 1113211L, 1112221L, 1112122L, 1111411L,
			1111132L, 1111231L, 1111114L, 1111312L, 1111213L, 1113112L };

	private static final long[] a9 = { 3211111111L, 2221111111L, 2122111111L,
			1411111111L, 1132111111L, 1231111111L, 1114111111L, 1312111111L,
			1213111111L, 3112111111L };

	private static final long[] a6 = { 1123L, 1222L, 2212L, 1141L, 2311L,
			1321L, 4111L, 2131L, 3121L, 2113L };

	private static final long[] a5 = { 1111123L, 1111222L, 1112212L, 1111141L,
			1112311L, 1111321L, 1114111L, 1112131L, 1113121L, 1112113L };

	private static final long[] a4 = { 1123111111L, 1222111111L, 2212111111L,
			1141111111L, 2311111111L, 1321111111L, 4111111111L, 2131111111L,
			3121111111L, 2113111111L };

	private static final String[] bb = { "EEEOOO", "EEOEOO", "EEOOEO",
			"EEOOOE", "EOEEOO", "EOOEEO", "EOOOEE", "EOEOEO", "EOEOOE",
			"EOOEOE" };

	private static final String[] ba = { "OOOEEE", "OOEOEE", "OOEEOE",
			"OOEEEO", "OEOOEE", "OEEOOE", "OEEEOO", "OEOEOE", "OEOEEO",
			"OEEOEO" };

	private static HashMap bd = null;

	private static HashMap bf = null;

	private static HashMap a7 = null;

	private ArrayList a3 = new ArrayList();

	private int bc;

	UPC_E(PropertiesHolder var_c, int i) {
		super(var_c);
		bc = i;
	}

	int code() {
		return 15;
	}

	int _if(int i) {
		if (i == 33)
			return 33;
		return -1;
	}

	boolean isCheck() {
		return true;
	}

	double _else() {
		return 5.1;
	}

	double _for() {
		return 3.923076923076923;
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_ = 0;
		int i_5_;
		if (is[0] == 0)
			i_5_ = 3;
		else
			i_5_ = 1;
		int i_6_ = is[i_5_];
		for (int i_7_ = 1; i_7_ < 7; i_7_++)
			i_6_ += is[i_5_ + i_7_];
		int i_8_ = 2147483647;
		while (i_5_ + 7 <= i_1_) {
			if (i_5_ == 3) {
				i_4_ = 0;
				for (int i_9_ = -3; i_9_ < 7; i_9_++)
					i_4_ += is[i_5_ + i_9_];
			}
			int i_10_ = i_6_ * 7 / 10;
			int i_11_ = i_4_ * 7 / 13;
			if ((i_5_ == 1 || is[i_5_ - 1] > i_10_ / bc)
					&& is[i_5_ + 7] < i_10_) {
				int i_12_ = _if(is, i_5_, 7, 10, 20, 39);
				if (i_12_ >= 0) {
					this.readData(i, i_0_, is, i_5_, 7, i_2_, i_12_, true, i_3_);
					if (i_6_ < i_8_)
						i_8_ = i_6_;
				}
			} else if (i_5_ > 3 && i_5_ + 7 <= i_1_ && is[i_5_ - 4] < i_11_
					&& (i_5_ + 7 == i_1_ - 1 || is[i_5_ + 7] > i_11_ / bc)) {
				int i_13_ = _if(is, i_5_ - 3, 10, 13, 40, 59);
				if (i_13_ >= 0) {
					this.readData(i, i_0_, is, i_5_ - 3, 10, i_2_, i_13_, false, i_3_);
					if (i_6_ < i_8_)
						i_8_ = i_6_;
				}
			}
			i_6_ -= is[i_5_] + is[i_5_ + 1];
			if (i_5_ >= 3)
				i_4_ -= is[i_5_ - 3] + is[i_5_ - 2];
			i_5_ += 2;
			i_6_ += is[i_5_ + 5] + is[i_5_ + 6];
			if (i_5_ >= 5)
				i_4_ += is[i_5_ + 5] + is[i_5_ + 6];
		}
		return i_8_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_14_) {
		int i_15_ = 0;
		int i_16_ = 0;
		while_17_: do {
			for (;;) {
				if (i_16_ == 0) {
					int i_17_ = _if(is, i_15_, 7, 10, 20, 39);
					if (i_17_ < 20 || i_17_ >= 40)
						break while_17_;
					i_15_ += 7;
					i_16_ = 1;
					a3.clear();
					ArrayList arraylist = a3;
					i_17_ -= 20;
					arraylist.add(new Integer(i_17_));
				} else if (i_16_ > 0 && i_16_ < 5) {
					int i_18_ = _if(is, i_15_, 4, 7, 0, 19);
					if (i_18_ < 0 || i_18_ >= 20)
						break while_17_;
					i_15_ += 4;
					i_16_++;
					a3.add(new Integer(i_18_));
				} else if (i_16_ == 5)
					break;
			}
			int i_19_ = _if(is, i_15_, 10, 13, 40, 59);
			if (i_19_ >= 40) {
				ArrayList arraylist = a3;
				i_19_ -= 40;
				arraylist.add(new Integer(i_19_));
				this.a(a3, 11, var_d, var_d_14_);
				return true;
			}
		} while (false);
		return false;
	}

	private boolean _new(String string) {
		int i = 0;
		int i_20_ = 0;
		for (int i_21_ = 0; i_21_ <= 10; i_21_++) {
			int i_22_ = string.charAt(i_21_) - 48;
			if (i_21_ % 2 == 0)
				i += i_22_;
			else
				i_20_ += i_22_;
		}
		int i_23_ = i * 3 + i_20_;
		if (i_23_ % 10 == 0)
			i_23_ = 0;
		else
			i_23_ = 10 - i_23_ % 10;
		return string.charAt(11) == 48 + i_23_;
	}

	String a(String string) {
		StringBuffer stringbuffer = new StringBuffer();
		StringBuffer stringbuffer_24_ = new StringBuffer();
		for (int i = 0; i < 6; i++) {
			int i_25_ = string.charAt(i);
			if (i_25_ >= 10) {
				i_25_ -= 10;
				stringbuffer_24_.append('E');
			} else
				stringbuffer_24_.append('O');
			stringbuffer.append(i_25_);
		}
		Object object = a7.get(new String(stringbuffer_24_));
		if (object == null)
			return null;
		int i = ((Integer) object).intValue();
		StringBuffer stringbuffer_26_ = new StringBuffer();
		stringbuffer_26_.append(i / 10);
		stringbuffer_26_.append(stringbuffer.charAt(0));
		stringbuffer_26_.append(stringbuffer.charAt(1));
		char c = stringbuffer.charAt(5);
		if (c == '0' || c == '1' || c == '2') {
			stringbuffer_26_.append(c);
			stringbuffer_26_.append("0000");
			stringbuffer_26_.append(stringbuffer.charAt(2));
			stringbuffer_26_.append(stringbuffer.charAt(3));
			stringbuffer_26_.append(stringbuffer.charAt(4));
		} else if (c == '3') {
			stringbuffer_26_.append(stringbuffer.charAt(2));
			stringbuffer_26_.append("00000");
			stringbuffer_26_.append(stringbuffer.charAt(3));
			stringbuffer_26_.append(stringbuffer.charAt(4));
		} else if (c == '4') {
			stringbuffer_26_.append(stringbuffer.charAt(2));
			stringbuffer_26_.append(stringbuffer.charAt(3));
			stringbuffer_26_.append("00000");
			stringbuffer_26_.append(stringbuffer.charAt(4));
		} else {
			stringbuffer_26_.append(stringbuffer.charAt(2));
			stringbuffer_26_.append(stringbuffer.charAt(3));
			stringbuffer_26_.append(stringbuffer.charAt(4));
			stringbuffer_26_.append("0000");
			stringbuffer_26_.append(c);
		}
		stringbuffer_26_.append(i % 10);
		String string_27_ = new String(stringbuffer_26_);
		return _new(string_27_) ? string_27_ : null;
	}

	protected final int _if(int[] is, int i, int i_28_, int i_29_, int i_30_,
			int i_31_) {
		int i_32_ = 0;
		int[] is_33_ = new int[i_28_];
		for (int i_34_ = 0; i_34_ < i_28_; i_34_++) {
			is_33_[i_34_] = is[i + i_34_] * i_29_;
			i_32_ += is[i + i_34_];
		}
		int[] is_35_ = new int[i_28_];
		int[] is_36_ = new int[i_28_];
		for (int i_37_ = 0; i_37_ < i_28_; i_37_++)
			is_35_[i_37_] = is_33_[i_37_];
		for (int i_38_ = 0; i_38_ < i_28_; i_38_++) {
			int i_39_ = is_35_[i_38_] / i_32_;
			int i_40_ = is_35_[i_38_] % i_32_;
			if (i_39_ == 0 || i_40_ > i_32_ / 2) {
				i_39_++;
				i_40_ -= i_32_;
			}
			is_36_[i_38_] = i_40_;
			if (i_38_ < i_28_ - 1)
				is_35_[i_38_ + 1] += i_40_;
			is_35_[i_38_] = i_39_;
		}
		Object object = null;
		boolean bool = false;
		long l = 1L;
		long l_41_ = 0L;
		for (int i_42_ = 0; i_42_ < i_28_; i_42_++) {
			l_41_ += (long) is_35_[i_28_ - i_42_ - 1] * l;
			l *= 10L;
		}
		object = bd.get(new Long(l_41_));
		int i_43_;
		if (object != null)
			i_43_ = ((Integer) object).intValue();
		else
			i_43_ = -1;
		if (i_43_ >= i_30_ && i_43_ <= i_31_)
			return i_43_;
		int i_44_ = 0;
		int i_45_ = Math.abs(is_36_[0]);
		for (int i_46_ = 1; i_46_ < i_28_ - 1; i_46_++) {
			if (Math.abs(is_36_[i_46_]) > i_45_) {
				i_45_ = Math.abs(is_36_[i_46_]);
				i_44_ = i_46_;
			}
		}
		for (int i_47_ = 0; i_47_ < i_28_; i_47_++)
			is_35_[i_47_] = is_33_[i_47_];
		for (int i_48_ = 0; i_48_ < i_28_; i_48_++) {
			int i_49_ = is_35_[i_48_] / i_32_;
			int i_50_ = is_35_[i_48_] % i_32_;
			if (i_49_ == 0) {
				i_49_++;
				i_50_ -= i_32_;
			} else if (i_48_ == i_44_) {
				if (i_50_ <= i_32_ / 2) {
					i_49_++;
					i_50_ -= i_32_;
				}
			} else if (i_50_ > i_32_ / 2) {
				i_49_++;
				i_50_ -= i_32_;
			}
			if (i_48_ < i_28_ - 1)
				is_35_[i_48_ + 1] += i_50_;
			is_35_[i_48_] = i_49_;
		}
		l = 1L;
		l_41_ = 0L;
		for (int i_51_ = 0; i_51_ < i_28_; i_51_++) {
			l_41_ += (long) is_35_[i_28_ - i_51_ - 1] * l;
			l *= 10L;
		}
		object = bd.get(new Long(l_41_));
		if (object != null)
			i_43_ = ((Integer) object).intValue();
		else
			i_43_ = -1;
		if (i_43_ >= i_30_ && i_43_ <= i_31_)
			return i_43_;
		for (int i_52_ = 0; i_52_ < i_28_; i_52_++)
			is_35_[i_52_] = is_33_[i_52_];
		for (int i_53_ = i_28_ - 1; i_53_ >= 0; i_53_--) {
			int i_54_ = is_35_[i_53_] / i_32_;
			int i_55_ = is_35_[i_53_] % i_32_;
			if (i_54_ == 0 || i_55_ > i_32_ / 2) {
				i_54_++;
				i_55_ -= i_32_;
			}
			is_36_[i_53_] = i_55_;
			if (i_53_ > 0)
				is_35_[i_53_ - 1] += i_55_;
			is_35_[i_53_] = i_54_;
		}
		l = 1L;
		l_41_ = 0L;
		for (int i_56_ = 0; i_56_ < i_28_; i_56_++) {
			l_41_ += (long) is_35_[i_28_ - i_56_ - 1] * l;
			l *= 10L;
		}
		object = bd.get(new Long(l_41_));
		if (object != null)
			i_43_ = ((Integer) object).intValue();
		else
			i_43_ = -1;
		if (i_43_ >= i_30_ && i_43_ <= i_31_)
			return i_43_;
		i_44_ = i_28_ - 1;
		i_45_ = Math.abs(is_36_[i_28_ - 1]);
		for (int i_57_ = i_28_ - 1; i_57_ > 0; i_57_--) {
			if (Math.abs(is_36_[i_57_]) > i_45_) {
				i_45_ = Math.abs(is_36_[i_57_]);
				i_44_ = i_57_;
			}
		}
		for (int i_58_ = 0; i_58_ < i_28_; i_58_++)
			is_35_[i_58_] = is_33_[i_58_];
		for (int i_59_ = i_28_ - 1; i_59_ >= 0; i_59_--) {
			int i_60_ = is_35_[i_59_] / i_32_;
			int i_61_ = is_35_[i_59_] % i_32_;
			if (i_60_ == 0) {
				i_60_++;
				i_61_ -= i_32_;
			} else if (i_59_ == i_44_) {
				if (i_61_ <= i_32_ / 2) {
					i_60_++;
					i_61_ -= i_32_;
				}
			} else if (i_61_ > i_32_ / 2) {
				i_60_++;
				i_61_ -= i_32_;
			}
			if (i_59_ > 0)
				is_35_[i_59_ - 1] += i_61_;
			is_35_[i_59_] = i_60_;
		}
		l = 1L;
		l_41_ = 0L;
		for (int i_62_ = 0; i_62_ < i_28_; i_62_++) {
			l_41_ += (long) is_35_[i_28_ - i_62_ - 1] * l;
			l *= 10L;
		}
		object = bd.get(new Long(l_41_));
		if (object != null)
			i_43_ = ((Integer) object).intValue();
		else
			i_43_ = -1;
		if (i_43_ >= i_30_ && i_43_ <= i_31_)
			return i_43_;
		return -1;
	}

	static {
		bd = new HashMap();
		bf = new HashMap();
		a7 = new HashMap();
		for (int i = 0; i < 10; i++)
			bd.put(new Long(a8[i]), new Integer(i));
		for (int i = 0; i < 10; i++)
			bd.put(new Long(a6[i]), new Integer(i + 10));
		for (int i = 0; i < 10; i++)
			bd.put(new Long(bg[i]), new Integer(i + 20));
		for (int i = 0; i < 10; i++)
			bd.put(new Long(a5[i]), new Integer(i + 30));
		for (int i = 0; i < 10; i++)
			bd.put(new Long(a9[i]), new Integer(i + 40));
		for (int i = 0; i < 10; i++)
			bd.put(new Long(a4[i]), new Integer(i + 50));
		for (int i = 0; i < 10; i++)
			a7.put(bb[i], new Integer(i));
		for (int i = 0; i < 10; i++)
			a7.put(ba[i], new Integer(i + 10));
	}
}
