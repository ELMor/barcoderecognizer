/* a9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.image.BufferedImage;

final class BIImagen extends Imagen {
	private final BufferedImage D;

	BIImagen(BufferedImage bufferedimage) {
		super(bufferedimage, true);
		D = bufferedimage;
	}

	public final int getPixel(int i, int i_0_) {
		int i_1_ = D.getRGB(i, i_0_);
		int i_2_ = (i_1_ & 0xff0000) >> 16;
		int i_3_ = (i_1_ & 0xff00) >> 8;
		int i_4_ = i_1_ & 0xff;
		return (i_2_ + i_3_ + i_4_) / 3;
	}
}
