/* as - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;

class UPC_A extends BarcodeMix {
	private static final boolean b5 = false;

	private static final int b7 = 11111;

	private static final int[] b8 = { 3211, 2221, 2122, 1411, 1132, 1231, 1114,
			1312, 1213, 3112 };

	private static final int[] b6 = { 1113211, 1112221, 1112122, 1111411,
			1111132, 1111231, 1111114, 1111312, 1111213, 1113112 };

	private static final int[] b9 = { 3211111, 2221111, 2122111, 1411111,
			1132111, 1231111, 1114111, 1312111, 1213111, 3112111 };

	private static HashMap b3 = null;

	private ArrayList b4 = new ArrayList();

	private int b2;

	UPC_A(PropertiesHolder var_c, int i) {
		super(b3, var_c);
		b2 = i;
	}

	int code() {
		return 14;
	}

	int _if(int i) {
		if (i == 59)
			return 59;
		return -1;
	}

	boolean isCheck() {
		return true;
	}

	double _else() {
		return 9.5;
	}

	double _for() {
		return 9.5;
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_;
		if (is[0] == 0)
			i_4_ = 3;
		else
			i_4_ = 1;
		int i_5_ = is[i_4_];
		for (int i_6_ = 1; i_6_ < 7; i_6_++)
			i_5_ += is[i_4_ + i_6_];
		int i_7_ = 2147483647;
		while (i_4_ + 7 <= i_1_) {
			int i_8_ = i_5_ * 7 / 10;
			if ((i_4_ == 1 || is[i_4_ - 1] > i_8_ / b2) && is[i_4_ + 7] < i_8_) {
				int i_9_ = this._do(is, i_4_, 7, 10, 10, 19);
				if (i_9_ >= 0) {
					this.readData(i, i_0_, is, i_4_, 7, i_2_, i_9_, true, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			} else if (is[i_4_ - 1] < i_8_
					&& (i_4_ + 7 == i_1_ - 1 || is[i_4_ + 7] > i_8_ / b2)) {
				int i_10_ = this._do(is, i_4_, 7, 10, 20, 29);
				if (i_10_ >= 0) {
					this.readData(i, i_0_, is, i_4_, 7, i_2_, i_10_, false, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			}
			i_5_ -= is[i_4_] + is[i_4_ + 1];
			i_4_ += 2;
			i_5_ += is[i_4_ + 5] + is[i_4_ + 6];
		}
		return i_7_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_11_) {
		int i_12_ = 0;
		int i_13_ = 0;
		while_16_: do {
			for (;;) {
				if (i_13_ == 0) {
					int i_14_ = this._do(is, i_12_, 7, 10, 10, 19);
					if (i_14_ < 10 || i_14_ >= 20)
						break while_16_;
					i_12_ += 7;
					i_13_ = 1;
					b4.clear();
					b4.add(new Integer(i_14_ - 10));
				} else if (i_13_ > 0 && i_13_ < 6) {
					int i_15_ = this._do(is, i_12_, 4, 7, 0, 9);
					if (i_15_ < 0 || i_15_ >= 10)
						break while_16_;
					i_12_ += 4;
					i_13_++;
					b4.add(new Integer(i_15_));
				} else if (i_13_ == 6) {
					int i_16_ = this._do(is, i_12_, 5, 5, 30, 30);
					if (i_16_ != 30)
						break while_16_;
					i_12_ += 5;
					i_13_ = 7;
				} else if (i_13_ > 6 && i_13_ < 12) {
					int i_17_ = this._do(is, i_12_, 4, 7, 0, 9);
					if (i_17_ < 0 || i_17_ >= 10)
						break while_16_;
					i_12_ += 4;
					i_13_++;
					b4.add(new Integer(i_17_));
				} else if (i_13_ == 12)
					break;
			}
			int i_18_ = this._do(is, i_12_, 7, 10, 20, 29);
			if (i_18_ >= 20) {
				b4.add(new Integer(i_18_ - 20));
				this.a(b4, 11, var_d, var_d_11_);
				return true;
			}
		} while (false);
		return false;
	}

	private boolean _byte(String string) {
		int i = string.length() - 1;
		char c = string.charAt(i);
		int i_19_ = 0;
		for (int i_20_ = i - 1; i_20_ >= 0; i_20_ -= 2) {
			int i_21_ = string.charAt(i_20_);
			i_19_ += i_21_ * 3;
		}
		for (int i_22_ = i - 2; i_22_ >= 0; i_22_ -= 2) {
			int i_23_ = string.charAt(i_22_);
			i_19_ += i_23_;
		}
		if (10 - i_19_ % 10 == c || c == 0 && i_19_ % 10 == 0)
			return true;
		return false;
	}

	String a(String string) {
		if (!_byte(string))
			return null;
		StringBuffer stringbuffer = new StringBuffer();
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			stringbuffer.append((char) ('0' + c));
		}
		return new String(stringbuffer);
	}

	static {
		b3 = new HashMap();
		for (int i = 0; i < 10; i++)
			b3.put(new Integer(b8[i]), new Integer(i));
		for (int i = 0; i < 10; i++)
			b3.put(new Integer(b6[i]), new Integer(i + 10));
		for (int i = 0; i < 10; i++)
			b3.put(new Integer(b9[i]), new Integer(i + 20));
		b3.put(new Integer(11111), new Integer(30));
	}
}
