/* e - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.StringTokenizer;

class LicenseManager extends IllegalArgumentException implements Runnable {
	private static final int _do = 256;

	private static final int _if = 97;

	private static final String a = "|";

	private String _for = "";

	private String resourceName;

	LicenseManager(String ressourceFileName, String string_0_, String string_1_) {
		super(string_1_);
		resourceName = ressourceFileName;
	}

	public void printStackTrace() {
		System.out.println(getMessage());
	}

	public void printStackTrace(PrintStream printstream) {
		printstream.println(getMessage());
	}

	public void printStackTrace(PrintWriter printwriter) {
		printwriter.println(getMessage());
	}

	boolean _if() {
		return getMessage().length() == 0;
	}

	public String getMessage() {
		return _for;
	}

	public void run() {
		Object object = null;
		boolean bool = false;
		String string;
		try {
			int i = a();
			string = i == 0 ? "" : super.getMessage() + "[error: " + i + "]";
		} catch (Exception exception) {
			string = super.getMessage();
		}
		_for = string;
	}

	private final int a() {
		long l = new Date().getTime();
		long l_2_ = 86400000L;
		InputStream inst = ClassLoader.getSystemResourceAsStream(resourceName);
		if (inst == null) {
			String tmpDir;
			String fileSep;
			try {
				tmpDir = System.getProperty("java.io.tmpdir");
				fileSep = System.getProperty("file.separator");
			} catch (Exception exception) {
				return 8;
			}
			if (!tmpDir.endsWith(fileSep))
				tmpDir += (String) fileSep;
			tmpDir += resourceName;
			try {
				inst = new FileInputStream(new File(tmpDir));
			} catch (FileNotFoundException filenotfoundexception) {
				System.out
						.println("See: http://www.tasman.co.uk/bars/error10.html");
				return 10;
			}
		}
		byte[] is;
		try {
			DataInputStream datainputstream = new DataInputStream(inst);
			int i = datainputstream.available();
			is = new byte[i];
			datainputstream.readFully(is);
			datainputstream.close();
		} catch (IOException ioexception) {
			return 20;
		}
		int i = 97;
		for (int i_4_ = 256; i_4_ < is.length; i_4_++) {
			int i_5_ = is[i] - 32;
			int i_6_ = is[i_4_] - 32;
			int i_7_ = i_6_ - i_5_;
			if (i_7_ < 0)
				i_7_ += 94;
			i_7_ += 32;
			is[i_4_] = (byte) i_7_;
			if (++i == 256)
				i = 97;
		}
		String string = new String(is, 256, is.length - 256);
		int i_8_ = string.lastIndexOf("|") + 1;
		long l_9_ = Long.parseLong(string.substring(i_8_));
		long l_10_ = 0L;
		for (int i_11_ = 0; i_11_ < 256; i_11_++)
			l_10_ += (long) ((i_11_ % 11 + 1) * is[i_11_]);
		for (int i_12_ = 0; i_12_ < i_8_; i_12_++)
			l_10_ += (long) ((i_12_ % 11 + 1) * is[i_12_ + 256]);
		if (l_9_ != l_10_)
			return 30;
		StringTokenizer stringtokenizer = new StringTokenizer(string, "|");
		String string_13_ = System.getProperty(stringtokenizer.nextToken());
		String string_14_ = System.getProperty(stringtokenizer.nextToken());
		for (int i_15_ = 0; i_15_ < 7; i_15_++) {
			String string_16_ = stringtokenizer.nextToken();
			String string_17_ = System.getProperty(string_16_);
			if (string_17_ != null) {
				StringTokenizer stringtokenizer_18_ = new StringTokenizer(
						string_17_, string_14_);
				while (stringtokenizer_18_.hasMoreTokens()) {
					String string_19_ = stringtokenizer_18_.nextToken();
					File file = new File(string_19_);
					if (file.isDirectory()) {
						File[] files = file.listFiles();
						for (int i_20_ = 0; i_20_ < files.length; i_20_++) {
							long l_21_ = files[i_20_].lastModified();
							if (l_21_ > l + l_2_)
								return 40;
						}
					}
				}
			}
		}
		String string_22_ = stringtokenizer.nextToken();
		String string_23_ = stringtokenizer.nextToken();
		String string_24_ = stringtokenizer.nextToken();
		String string_25_ = stringtokenizer.nextToken();
		long l_26_ = Long.parseLong(string_25_);
		long l_27_ = l_26_ + l_2_ * 62L;
		Date date = new Date(l_27_);
		if (l < l_26_ - l_2_ || l > l_27_)
			return 50;
		System.out.println(string_22_ + string_24_);
		System.out.println(string_23_ + date);
		return 0;
	}
}
