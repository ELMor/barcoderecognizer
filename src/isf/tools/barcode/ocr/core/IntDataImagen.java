/* s - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

final class IntDataImagen extends Imagen {
	private final int[] data;

	IntDataImagen(BufferedImage bufferedimage) {
		super(bufferedimage);
		java.awt.image.DataBuffer databuffer = bufferedimage.getRaster()
				.getDataBuffer();
		data = ((DataBufferInt) databuffer).getData();
	}

	public final int getPixel(int i, int i_0_) {
		int i_1_ = data[g + i_0_ * slStride + i];
		int i_2_ = (i_1_ & 0xff0000) >> 16;
		int i_3_ = (i_1_ & 0xff00) >> 8;
		int i_4_ = i_1_ & 0xff;
		return (i_2_ + i_3_ + i_4_) / 3;
	}

	public int[] getRow(int i) {
		int i_5_ = this.getWidth();
		int i_6_ = g + i * slStride;
		for (int i_7_ = 0; i_7_ < i_5_; i_7_++) {
			int i_8_ = data[i_6_++];
			int i_9_ = (i_8_ & 0xff0000) >> 16;
			int i_10_ = (i_8_ & 0xff00) >> 8;
			int i_11_ = i_8_ & 0xff;
			xDim[i_7_] = (i_9_ + i_10_ + i_11_) / 3;
		}
		return xDim;
	}

	public int[] getCol(int i) {
		int i_12_ = this.getHeight();
		int i_13_ = g + i;
		for (int i_14_ = 0; i_14_ < i_12_; i_14_++) {
			int i_15_ = data[i_13_];
			int i_16_ = (i_15_ & 0xff0000) >> 16;
			int i_17_ = (i_15_ & 0xff00) >> 8;
			int i_18_ = i_15_ & 0xff;
			this.yDim[i_14_] = (i_16_ + i_17_ + i_18_) / 3;
			i_13_ += slStride;
		}
		return this.yDim;
	}
}
