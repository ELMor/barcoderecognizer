/* ay - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

class PointDouble {
	double x;

	double y;

	PointDouble(double d, double d_0_) {
		x = d;
		y = d_0_;
	}

	private double sqMod(PointDouble pd2) {
		double d1 = x - pd2.x;
		double d2 = y - pd2.y;
		return d1 * d1 + d2 * d2;
	}

	double mod(PointDouble var_ay_3_) {
		return Math.sqrt(sqMod(var_ay_3_));
	}

	double mod(double d, double d_4_) {
		double d_5_ = d - x;
		double d_6_ = d_4_ - y;
		return Math.sqrt(d_5_ * d_5_ + d_6_ * d_6_);
	}

	double getX() {
		return x;
	}

	double getY() {
		return y;
	}

	public String toString() {
		return "PointDouble  x = " + x + "  y = " + y;
	}
}
