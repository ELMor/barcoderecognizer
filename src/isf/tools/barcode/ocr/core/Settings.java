/* y - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.io.Serializable;
import java.util.Properties;

public class Settings implements Serializable {
	public boolean code128;

	public boolean code32;

	public boolean code39;

	public boolean code93;

	public boolean codabar;

	public boolean dataMatrix;

	public boolean EAN13;

	public boolean EAN8;

	public boolean interleaved2of5;

	public boolean patch;

	public boolean pdf417;

	public boolean QRCode;

	public boolean rss14;

	public boolean rssltd;

	public boolean telepen;

	public boolean code11;

	public boolean upcA;

	public boolean upcE;

	public boolean check;

	public boolean plus2;

	public boolean plus5;

	public boolean rightScanDir = true;

	public boolean upScanDir;

	public boolean downScanDir;

	public boolean leftScanDir;

	public Object properties;

	public int scanInterval = 5;

	public int barsToRead = 1;

	int _int = 128;

	public Properties prop = null;

	public Settings() {
		/* empty */
	}

	public Settings(Settings set2) {
		this();
		code11 = set2.code11;
		code128 = set2.code128;
		code32 = set2.code32;
		code39 = set2.code39;
		code93 = set2.code93;
		codabar = set2.codabar;
		dataMatrix = set2.dataMatrix;
		EAN13 = set2.EAN13;
		EAN8 = set2.EAN8;
		interleaved2of5 = set2.interleaved2of5;
		patch = set2.patch;
		pdf417 = set2.pdf417;
		QRCode = set2.QRCode;
		rss14 = set2.rss14;
		rssltd = set2.rssltd;
		telepen = set2.telepen;
		upcA = set2.upcA;
		upcE = set2.upcE;
		check = set2.check;
		plus2 = set2.plus2;
		plus5 = set2.plus5;
		rightScanDir = set2.rightScanDir;
		upScanDir = set2.upScanDir;
		downScanDir = set2.downScanDir;
		leftScanDir = set2.leftScanDir;
		properties = set2.properties;
		scanInterval = set2.scanInterval;
		barsToRead = set2.barsToRead;
		_int = set2._int;
		prop = set2.prop;
	}
}
