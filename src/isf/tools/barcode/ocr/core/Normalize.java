/* a8 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;

class Normalize {
	private static final boolean _byte = false;

	private boolean initialized;

	long[] data;

	private long maxValue;

	private int nonZeroIndex;

	PropertiesHolder prop;

	private int firstNonZero = 0;

	private int secondNonZero = 0;

	private int media = -1;

	Normalize() {
		initialized = false;
		data = new long[256];
		nonZeroIndex = 0;
	}

	boolean isInitialized() {
		return initialized;
	}

	boolean isTwoNonZeroes() {
		return initialized && nonZeroIndex == 2;
	}

	int between(int i) {
		if (!isTwoNonZeroes())
			return i;
		if (i > firstNonZero && i < secondNonZero)
			return i;
		return (firstNonZero + secondNonZero) / 2;
	}

	void init() {
		if (!initialized) {
			nonZeroIndex = 0;
			maxValue = -1L;
			long sumAcumPos = 0L;
			long acum = 0L;
			for (int i = 0; i < 256; i++) {
				if (data[i] != 0L) {
					if (nonZeroIndex == 0)
						firstNonZero = i;
					if (nonZeroIndex == 1)
						secondNonZero = i;
					nonZeroIndex++;
					if (data[i] > maxValue)
						maxValue = data[i];
					sumAcumPos += (long) i * data[i];
					acum += data[i];
				}
			}
			media = (int) (sumAcumPos / acum);
			initialized = true;
		}
	}

	int getMedia() {
		if (!initialized)
			init();
		return media;
	}

	private boolean isOver(int beg, int end, long tope) {
		if (beg > end) {
			for (int k = end + 1; k <= beg; k++) {
				if (data[k] > tope)
					return true;
			}
		} else {
			for (int k = end - 1; k >= beg; k--) {
				if (data[k] > tope)
					return true;
			}
		}
		return false;
	}

	int[] normalize() {
		if (!initialized)
			init();
		long l = maxValue / 1024L;
		ArrayList lst = new ArrayList();
		int len = 16;
		if (isOver(media, 128, l))
			lst.add(new Integer(media));
		int[] over = new int[128 / len];
		for (int k = 0; k < over.length; k++)
			over[k] = 128 + len * k;
		int[] below = new int[over.length];
		for (int k = 0; k < over.length; k++)
			below[k] = 128 - len * k;
		for (int k = 1; k < over.length; k++) {
			if (isOver(over[k], over[k - 1], l))
				lst.add(new Integer(over[k]));
			if (isOver(below[k], below[k - 1], l))
				lst.add(new Integer(below[k]));
		}
		int[] ret = new int[lst.size()];
		for (int k = 0; k < lst.size(); k++)
			ret[k] = ((Integer) lst.get(k)).intValue();
		return ret;
	}

	int[] normalize10() {
		if (!initialized)
			init();
		long l = maxValue / 100L;
		ArrayList lst = new ArrayList();
		int i = 16;
		for (int k = i; k < 256; k += i) {
			if (isOver(k, k - i, l))
				lst.add(new Integer(k));
		}
		int[] ret = new int[lst.size()];
		for (int k = 0; k < lst.size(); k++)
			ret[k] = ((Integer) lst.get(k)).intValue();
		return ret;
	}
}
