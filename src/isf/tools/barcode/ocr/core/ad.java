/* ad - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

class ad {
	private static final boolean _goto = false;

	private PropertiesHolder _int;

	private SepInten _for;

	private int _new;

	private int _do;

	private int _long;

	private int a;

	double _void;

	private double _else;

	private double _try;

	private int _case;

	private double _byte;

	private double _char;

	private Recognizer _if;

	ad(PropertiesHolder var_c, SepInten var_d, int i, int i_0_, int i_1_, double d, double d_2_,
			int i_3_, double d_4_, double d_5_, Recognizer var_a1) {
		_int = var_c;
		_for = var_d;
		_new = var_d.getDirection();
		_do = i;
		_long = i_0_;
		a = i_1_;
		_else = d;
		_try = d_2_;
		_case = i_3_;
		_byte = d_4_;
		_char = d_5_;
		_if = var_a1;
	}

	ad(double d, double d_6_, double d_7_, double d_8_) {
		_else = d;
		_try = d_6_;
		_case = _case;
		_byte = d_7_;
		_char = d_8_;
	}

	int _int() {
		return _do;
	}

	int _do() {
		return _long;
	}

	int _byte() {
		return a;
	}

	double _new() {
		return _else;
	}

	double _try() {
		return _try;
	}

	double _for() {
		return _byte;
	}

	double _case() {
		return _char;
	}

	Recognizer _if() {
		return _if;
	}

	int a() {
		return _for.getBWBound();
	}

	boolean a(ad var_ad_9_) {
		if (_new != var_ad_9_._new || _do != var_ad_9_._do)
			return false;
		if (_long == var_ad_9_._long)
			return _if(var_ad_9_) == 0;
		int i = _long;
		int i_10_ = var_ad_9_._long;
		var_ad_9_._long = _long = -1;
		int i_11_ = _if(var_ad_9_);
		if (i_11_ != -1) {
			var_ad_9_._long = _long = i_11_;
			return true;
		}
		_long = i;
		var_ad_9_._long = i_10_;
		return false;
	}

	private int _if(ad var_ad_12_) {
		double d = (double) (_do / 2);
		double d_13_ = (d - _char) / _byte;
		double d_14_ = _else * d_13_ + _try;
		double d_15_ = (d - var_ad_12_._char) / var_ad_12_._byte;
		double d_16_ = var_ad_12_._else * d_15_ + var_ad_12_._try;
		PointDouble var_ay = new PointDouble(d_13_, d_14_);
		PointDouble var_ay_17_ = new PointDouble(d_15_, d_16_);
		double d_18_ = var_ay.mod(var_ay_17_);
		double d_19_ = Math.sqrt(_else * _else + 1.0);
		double d_20_ = 1.0 / d_19_;
		int i = 0;
		double d_21_ = Math.abs((double) _case * d_20_);
		if (_long == -1) {
			i = (int) (d_18_ / (double) _case * d_20_ - 0.5);
			if (i < 1 || i > 30)
				i = -1;
		} else {
			d_21_ *= (double) (_long + 1);
			if (d_18_ < d_21_ * 3.0 / 4.0 || d_18_ > d_21_ * 4.0 / 3.0)
				return -1;
		}
		if (_new == 90 && d_16_ < d_14_)
			return -1;
		if (_new == 270 && d_14_ < d_16_)
			return -1;
		if (_new == 180 && d_16_ < d_14_)
			return -1;
		if (_new == 360 && d_14_ < d_16_)
			return -1;
		return i;
	}

	boolean _do(ad var_ad_22_) {
		if (_new != var_ad_22_._new || _do != var_ad_22_._do
				|| _long != var_ad_22_._long || a != var_ad_22_.a
				|| a() != var_ad_22_.a())
			return false;
		if (Math.abs(_case) < Math.abs(_case * 3 / 4)
				|| Math.abs(_case) > Math.abs(_case * 4 / 3))
			return false;
		double d = (double) (_do / 2);
		double d_23_ = (d - _char) / _byte;
		double d_24_ = _else * d_23_ + _try;
		double d_25_ = (d - var_ad_22_._char) / var_ad_22_._byte;
		double d_26_ = var_ad_22_._else * d_25_ + var_ad_22_._try;
		if (Math.abs(d_23_ - d_25_) < Math.abs((double) (_case / 2))
				&& Math.abs(d_24_ - d_26_) < Math.abs((double) (_case / 2)))
			return true;
		return false;
	}

	public String toString() {
		String string = ("PDF417 row indicator info: rows = " + _do
				+ "  data columns = " + _long + "  error correction level = " + a);
		return string;
	}

	void a(UserRect var_al, int i, int i_27_) {
		double d = (0.5 - _char) / _byte;
		double d_28_ = _else * d + _try;
		double d_29_ = ((double) _do + 0.5 - _char) / _byte;
		double d_30_ = _else * d_29_ + _try;
		double d_31_;
		double d_32_;
		double d_33_;
		double d_34_;
		if (_new == 90 || _new == 270) {
			d_31_ = d;
			d_32_ = d_28_;
			d_33_ = d_29_;
			d_34_ = d_30_;
		} else {
			d_31_ = d_28_;
			d_32_ = d;
			d_33_ = d_30_;
			d_34_ = d_29_;
		}
		var_al.setP1y(d_31_ + (double) i_27_);
		var_al.setP1x(d_32_ + (double) i);
		var_al.setP2y(d_33_ + (double) i_27_);
		var_al.setP2x(d_34_ + (double) i);
	}
}
