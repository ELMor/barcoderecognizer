/* az - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;

class Patch extends Barcode {
	private static final boolean bk = false;

	private static final int bj = 7;

	private static final String bl = "12346T";

	private static final int[] bm = { 2121111, 2111112, 2111211, 1121211,
			1111212, 1121112 };

	private static HashMap bi = null;

	private int bh;

	Patch(PropertiesHolder var_c, int i) {
		super(var_c);
		bh = i;
	}

	int code() {
		return 9;
	}

	int _if(int i) {
		if (i == 7)
			return 1;
		return -1;
	}

	boolean isCheck() {
		return false;
	}

	double _else() {
		return 1.0;
	}

	double _for() {
		return 1.0;
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_;
		if (is[0] == 0)
			i_4_ = 3;
		else
			i_4_ = 1;
		int i_5_ = is[i_4_];
		for (int i_6_ = 1; i_6_ < 7; i_6_++)
			i_5_ += is[i_4_ + i_6_];
		int i_7_ = 2147483647;
		int i_8_ = 2 * bh;
		while (i_4_ + 7 <= i_1_) {
			if ((i_4_ == 1 || is[i_4_ - 1] > i_5_ / i_8_)
					&& (i_4_ + 7 == i_1_ - 1 || is[i_4_ + 7] > i_5_ / 2)) {
				int i_9_ = _int(is, i_4_, 7, 12);
				if (i_9_ != -1) {
					this.readData(i, i_0_, is, i_4_, 7, i_2_, i_9_, true, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
					this.readData(i, i_0_, is, i_4_, 7, i_2_, i_9_, false, i_3_);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			}
			i_5_ -= is[i_4_] + is[i_4_ + 1];
			i_4_ += 2;
			i_5_ += is[i_4_ + 5] + is[i_4_ + 6];
		}
		return i_7_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_10_) {
		int i_11_ = _int(is, 0, 7, 12);
		if (i_11_ != -1) {
			ArrayList arraylist = new ArrayList(1);
			arraylist.add(new Integer(i_11_));
			this.a(arraylist, -1, var_d, var_d_10_);
			return true;
		}
		return false;
	}

	private final int _int(int[] is, int i, int i_12_, int i_13_) {
		int i_14_ = 0;
		for (int i_15_ = 0; i_15_ < i_12_; i_15_++)
			i_14_ += is[i + i_15_];
		if (i_14_ < 40)
			return -1;
		int i_16_ = i_14_ / (i_12_ - 1);
		int i_17_ = 0;
		for (int i_18_ = 0; i_18_ < i_12_; i_18_++) {
			i_17_ *= 10;
			if (is[i + i_18_] < i_16_ / 4)
				return -1;
			if (is[i + i_18_] <= i_16_)
				i_17_++;
			else if (is[i + i_18_] <= i_16_ * 4)
				i_17_ += 2;
			else
				return -1;
		}
		Object object = bi.get(new Integer(i_17_));
		if (object == null)
			return -1;
		int i_19_ = ((Integer) object).intValue();
		return i_19_;
	}

	String a(String string) {
		return "Patch " + "12346T".charAt(string.charAt(0));
	}

	static {
		bi = new HashMap();
		for (int i = 0; i < bm.length; i++)
			bi.put(new Integer(bm[i]), new Integer(i));
	}
}
