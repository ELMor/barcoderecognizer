/* o - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Image;
import java.awt.image.ColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.ImageProducer;
import java.awt.image.IndexColorModel;
import java.util.Hashtable;

class ImagenConsumer implements ImgManager, ImageConsumer {
	private ImageProducer imgProd;

	private int height;

	private int width;

	private byte[] pizels;

	private int[] col;

	private int[] row;

	private byte[] colorMap;

	private Thread thread = Thread.currentThread();

	ImagenConsumer(Image image) {
		imgProd = image.getSource();
		imgProd.startProduction(this);
		try {
			Thread.sleep(9223372036854775807L);
		} catch (InterruptedException interruptedexception) {
			/* empty */
		}
	}

	public void imageComplete(int i) {
		imgProd.removeConsumer(this);
		thread.interrupt();
	}

	public void setColorModel(ColorModel colormodel) {
		if (colormodel instanceof IndexColorModel) {
			int mapSize = ((IndexColorModel) colormodel).getMapSize();
			colorMap = new byte[mapSize];
			for (int k = 0; k < mapSize; k++) {
				int blue = ((IndexColorModel) colormodel).getBlue(k);
				int green = ((IndexColorModel) colormodel).getGreen(k);
				int red = ((IndexColorModel) colormodel).getRed(k);
				colorMap[k] = (byte) ((red + green + blue) / 3);
			}
		}
	}

	public void setDimensions(int h, int w) {
		height = h;
		width = w;
		pizels = new byte[h * w];
		col = new int[h];
		row = new int[w];
	}

	public void setHints(int i) {
		/* empty */
	}

	public void setPixels(int x1, int y1, int x2, int y2,
			ColorModel cm, byte[] newColor, int colSize, int rowSize) {
		int sumy = y1 + y2;
		int sumx = x1 + x2;
		for (int j = y1; j < sumy; j++) {
			int p1 = colSize + (j - y1) * rowSize;
			int p2 = j * height;
			for (int k = x1; k < sumx; k++) {
				int q1 = p1 + k - x1;
				int q2 = p2 + k;
				int r = newColor[q1];
				if (r < 0)
					r += 256;
				pizels[q2] = colorMap[r];
			}
		}
	}

	public void setPixels(int x1, int y1, int x3, int y3,
			ColorModel cm, int[] newColor, int colSize, int rowSize) {
		int i_24_ = y1 + y3;
		int i_25_ = x1 + x3;
		for (int j = y1; j < i_24_; j++) {
			int p1 = colSize + (j - y1) * rowSize;
			int p2 = j * height;
			for (int k = x1; k < i_25_; k++) {
				int q1 = p1 + k - x1;
				int q2 = p2 + k;
				int r = newColor[q1];
				int blue = (r & 0xff0000) >> 16;
				int green = (r & 0xff00) >> 8;
				int red = r & 0xff;
				pizels[q2] = (byte) ((blue + green + red) / 3);
			}
		}
	}

	public void setProperties(Hashtable hashtable) {
		/* empty */
	}

	public int getWidth() {
		return height;
	}

	public int getHeight() {
		return width;
	}

	public int getPixel(int x, int y) {
		int pos = pizels[x + y * height];
		if (pos < 0)
			pos += 256;
		return pos;
	}

	public int[] getRow(int i) {
		int w = getWidth();
		for (int k = 0; k < w; k++)
			col[k] = getPixel(k, i);
		return col;
	}

	public int[] getCol(int i) {
		int h = getHeight();
		for (int k = 0; k < h; k++)
			row[k] = getPixel(i, k);
		return row;
	}
}
