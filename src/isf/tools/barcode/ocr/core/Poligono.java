/* i - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Polygon;
import java.awt.Rectangle;

class Poligono extends Polygon {
	Poligono(int[] xpoints, int[] ypoints, int npoints) {
		super(xpoints, ypoints, npoints);
	}

	boolean contiene(PointDouble p) {
		return contains((int) p.x, (int) p.y);
	}

	boolean contiene(Rectangle r) {
		Rectangle bound = getBounds();
		if (!bound.intersects(r))
			return false;
		Rectangle intersect = bound.intersection(r);
		if (intersect.equals(bound))
			return true;
		if (bound.contains(r.x, r.y)
				&& contains(r.x, r.y))
			return true;
		if (bound.contains(r.x + r.width, r.y)
				&& contains(r.x + r.width, r.y))
			return true;
		if (bound.contains(r.x, r.y + r.height)
				&& contains(r.x, r.y + r.height))
			return true;
		if (bound.contains(r.x + r.width, r.y
				+ r.height)
				&& contains(r.x + r.width, r.y
						+ r.height))
			return true;
		return false;
	}

	public String toString() {
		StringBuffer s = new StringBuffer();
		for (int i = 0; i < npoints; i++)
			s.append("  (" + xpoints[i] + "," + ypoints[i] + ")");
		return new String(s);
	}
}
