/* p - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

class p {
	private int a;

	private int _if;

	p(int i) {
		a = i;
		_if = 17 + i * 4;
	}

	int a(int i, int i_0_) {
		if (i >= _if - 2) {
			if (i_0_ < 9)
				return -1;
			if (i_0_ >= _if - 2)
				return -1;
			int i_1_ = i - (_if - 2);
			i_1_ += (i_0_ - 1) % 4 * 2;
			if (i_0_ >= _if - 4)
				return 8 + i_1_;
			if (i_0_ >= _if - 8)
				return 16 + i_1_;
			int i_2_ = (_if - i_0_ - 1) / 4;
			if (i_2_ % 2 == 0) {
				if (a == i_2_ - 1)
					return (i_2_ / 2 + 2 << 3) + i_1_;
				return -1;
			}
			return (i_2_ / 2 + 2 << 3) + i_1_;
		}
		if (i >= _if - 4) {
			if (i_0_ < 9)
				return -1;
			int i_3_ = i - (_if - 4);
			i_3_ += (i_0_ - 1) % 4 * 2;
			int i_4_ = (a - 1) / 2 + 3;
			int i_5_ = (_if - i_0_ - 1) / 4 + i_4_ + 1;
			return (i_5_ << 3) + i_3_;
		}
		if (i >= _if - 8) {
			if (i_0_ < 9)
				return -1;
			int i_6_ = i - (_if - 8);
			i_6_ = i_6_ + (i_0_ % 2 == 0 ? 4 : 0);
			int i_7_ = (a - 1) / 2 + 3;
			i_7_ += a + 2;
			int i_8_ = (_if - i_0_ - 1) / 2 + i_7_ + 1;
			return (i_8_ << 3) + i_6_;
		}
		if (i >= 9) {
			if (i_0_ == 6)
				return -1;
			int i_9_ = (i - 9) % 4;
			if (i_0_ < 6)
				i_9_ = i_9_ + (i_0_ % 2 == 0 ? 0 : 4);
			else
				i_9_ = i_9_ + (i_0_ % 2 == 0 ? 4 : 0);
			int i_10_ = (a - 1) / 2 + 3;
			i_10_ += a + 2;
			i_10_ += a * 2 + 4;
			int i_11_ = a - 1 - (i - 9) / 4;
			i_10_ += i_11_ / 2 * (15 + 4 * a) + i_11_ % 2 * (7 + 2 * a);
			int i_12_ = (_if - i_0_ - (i_0_ < 6 ? 2 : 1)) / 2 + i_10_ + 1;
			if (i > 12 && i_11_ % 2 == 0) {
				if (i_0_ >= _if - 2)
					return -1;
				i_12_--;
			}
			return (i_12_ << 3) + i_9_;
		}
		if (i >= 7) {
			if (i_0_ < 9)
				return -1;
			if (i_0_ >= _if - 8)
				return -1;
			int i_13_ = (i + 1) % 2;
			i_13_ += (i_0_ - 1) % 4 * 2;
			int i_14_ = (a - 1) / 2 + 3;
			i_14_ += a + 2;
			i_14_ += a * 2 + 4;
			int i_15_ = a;
			i_14_ += i_15_ / 2 * (15 + 4 * a) + i_15_ % 2 * (7 + 2 * a + 1);
			int i_16_ = (_if - i_0_ - 9) / 4 + i_14_ + 1;
			return (i_16_ << 3) + i_13_;
		}
		if (i == 6)
			return -1;
		if (i_0_ < 9)
			return -1;
		if (i_0_ >= _if - 8)
			return -1;
		int i_17_ = i % 2;
		i_17_ += (i_0_ - 1) % 4 * 2;
		int i_18_ = (a - 1) / 2 + 3;
		i_18_ += a + 2;
		i_18_ += a * 2 + 4;
		int i_19_ = a;
		i_18_ += i_19_ / 2 * (15 + 4 * a) + i_19_ % 2 * (7 + 2 * a + 1);
		i_18_ += a * ((5 - i) / 2) + a;
		int i_20_ = (_if - i_0_ - 9) / 4 + i_18_ + 1;
		return (i_20_ << 3) + i_17_;
	}
}
