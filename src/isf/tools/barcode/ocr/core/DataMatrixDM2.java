/* a0 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Iterator;

class DataMatrixDM2 extends Barcode {
	private static final boolean bx = false;

	private static final int bt = 24;

	private static final int br = 1;

	private static final double[] bp = new double[48];

	private static final double[] bB = new double[48];

	private static final double[] bC = new double[48];

	private int bv;

	private int bo;

	private int bA;

	private int bz;

	static final int bn = 3;

	static final int bu = 0;

	static final int bq = 1;

	static final int bw = 2;

	static final int bs = 4;

	private static final int by = 50;

	class a implements Iterator {
		private int a;

		private Iterator _if;

		a(Recognizer var_a1, int i, Rectangle rectangle, Point point) {
			int[][] is = a(var_a1, i, rectangle, point);
			int i_0_ = -1;
			int i_1_ = is[0].length;
			for (int i_2_ = 0; i_2_ < 48; i_2_++) {
				for (int i_3_ = 1; i_3_ < i_1_; i_3_++) {
					int i_4_ = is[i_2_][i_3_] * i_3_;
					if (i_4_ > i_0_) {
						i_0_ = i_4_;
						a = i_2_;
					}
				}
			}
			a %= 12;
			int[] is_5_ = a(var_a1, i, rectangle, point, 0, a);
			int[] is_6_ = a(var_a1, i, rectangle, point, 1, a);
			int[] is_7_ = a(var_a1, i, rectangle, point, 2, a);
			int[] is_8_ = a(var_a1, i, rectangle, point, 3, a);
			ArrayList arraylist = new ArrayList();
			for (int i_9_ = 1; i_9_ <= is_5_[0]; i_9_++) {
				int i_10_ = is_5_[i_9_];
				for (int i_11_ = 1; i_11_ <= is_7_[0]; i_11_++) {
					int i_12_ = is_7_[i_11_];
					int i_13_ = i_10_ + i_12_;
					for (int i_14_ = 1; i_14_ <= is_6_[0]; i_14_++) {
						int i_15_ = is_6_[i_14_];
						for (int i_16_ = 1; i_16_ <= is_8_[0]; i_16_++) {
							int i_17_ = is_8_[i_16_];
							int i_18_ = i_15_ + i_17_;
							double d = ((double) Math.max(i_13_, i_18_) / (double) Math
									.min(i_13_, i_18_));
							if ((!(d > 1.3) || !(d < 1.7)) && !(d > 4.8)) {
								int[] is_19_ = new int[4];
								is_19_[0] = i_10_;
								is_19_[1] = i_15_;
								is_19_[2] = i_12_;
								is_19_[3] = i_17_;
								arraylist.add(is_19_);
							}
						}
					}
				}
			}
			_if = arraylist.iterator();
		}

		private int[][] a(Recognizer var_a1, int i, Rectangle rectangle, Point point) {
			int i_20_ = rectangle.x;
			int i_21_ = rectangle.y;
			int i_22_ = rectangle.width;
			int i_23_ = rectangle.height;
			int i_24_ = i_20_ + rectangle.width;
			int i_25_ = i_21_ + rectangle.height;
			int i_26_ = point.x;
			int i_27_ = point.y;
			int i_28_ = i_26_ - i_20_;
			if (i_24_ - i_26_ > i_28_)
				i_28_ = i_24_ - i_26_;
			if (i_27_ - i_21_ > i_28_)
				i_28_ = i_27_ - i_21_;
			if (i_25_ - i_27_ > i_28_)
				i_28_ = i_25_ - i_27_;
			i_28_++;
			int[][] is = new int[48][i_28_];
			int i_29_ = -1;
			for (int i_30_ = i_21_; i_30_ < i_25_; i_30_++) {
				if (i_30_ == i_27_)
					i_29_ = 1;
				int i_31_ = -1;
				int i_32_ = i_20_;
				int i_33_ = i_24_;
				for (int i_34_ = i_32_; i_34_ < i_33_; i_34_++) {
					if (i_34_ == i_26_)
						i_31_ = 1;
					if (var_a1.a(i_34_, i_30_) < i) {
						boolean bool = var_a1.a(i_34_ + i_31_, i_30_) >= i;
						boolean bool_35_ = var_a1.a(i_34_, i_30_ + i_29_) >= i;
						if (bool || bool_35_) {
							for (int i_36_ = 0; i_36_ < 24; i_36_++) {
								double d = ((double) (i_34_ - i_26_)
										* DataMatrixDM2.bB[i_36_] + ((double) (i_30_ - i_27_) * DataMatrixDM2.bp[i_36_]));
								int i_37_ = i_36_;
								if (d < 0.0) {
									d = -d;
									i_37_ += 24;
								}
								int i_38_ = (int) d;
								if (i_38_ < i_28_)
									is[i_37_ / 1][i_38_]++;
							}
						}
					}
				}
			}
			return is;
		}

		private int[] a(Recognizer var_a1, int i, Rectangle rectangle, Point point,
				int i_39_, int i_40_) {
			int[] is = new int[10];
			int i_41_ = 0;
			int i_42_ = 0;
			double d = DataMatrixDM2.bB[i_40_];
			double d_43_ = DataMatrixDM2.bp[i_40_];
			int i_44_ = point.x;
			int i_45_ = point.y;
			int i_46_ = 3 * Math.max(rectangle.width, rectangle.height) / 2;
			int i_47_ = Math.min(rectangle.width, rectangle.height) / 4;
			Object object = null;
			double d_48_;
			double d_49_;
			double d_50_;
			double d_51_;
			switch (i_39_) {
			case 0:
				d_48_ = d;
				d_49_ = d_43_;
				d_50_ = d_43_;
				d_51_ = -d;
				break;
			case 1:
				d_48_ = -d_43_;
				d_49_ = d;
				d_50_ = d;
				d_51_ = d_43_;
				break;
			case 2:
				d_48_ = -d;
				d_49_ = -d_43_;
				d_50_ = d_43_;
				d_51_ = -d;
				break;
			case 3:
				d_48_ = d_43_;
				d_49_ = -d;
				d_50_ = d;
				d_51_ = d_43_;
				break;
			default:
				d_48_ = d_49_ = d_50_ = d_51_ = 0.0;
			}
			int i_52_ = 0;
			int i_53_ = 0;
			for (int i_54_ = 1; i_54_ < i_46_; i_54_++) {
				int i_55_ = 0;
				double d_56_ = (double) i_44_ + (double) i_54_ * d_48_;
				double d_57_ = (double) i_45_ + (double) i_54_ * d_49_;
				for (int i_58_ = -i_47_; i_58_ <= i_47_; i_58_++) {
					double d_59_ = d_56_ + (double) i_58_ * d_50_;
					double d_60_ = d_57_ + (double) i_58_ * d_51_;
					int i_61_ = (int) (d_59_ + 0.5);
					int i_62_ = (int) (d_60_ + 0.5);
					if (var_a1.a(i_61_, i_62_) < i)
						i_55_++;
				}
				int i_63_ = i_41_ / i_54_;
				if (i_54_ > 5) {
					if (i_52_ > i_63_ / 3 && i_55_ <= i_63_ / 3) {
						is[++i_42_] = i_54_;
						if (i_42_ == is.length - 1)
							break;
					}
					if (i_55_ <= i_63_ / 20) {
						if (++i_53_ > i_46_ / 10)
							break;
					} else
						i_53_ = 0;
				}
				i_41_ += i_55_;
				i_52_ = i_55_;
			}
			is[0] = i_42_;
			return is;
		}

		private int[] a(int[] is) {
			int[] is_64_ = new int[10];
			int i = is[0];
			int i_65_ = 0;
			int i_66_ = 10;
			for (int i_67_ = 1; i_67_ < i_66_; i_67_++)
				i += is[i_67_];
			for (int i_68_ = i_66_; i_68_ < is.length - 1; i_68_++) {
				int i_69_ = i / i_68_;
				if (is[i_68_] > i_69_ / 3) {
					if (is[i_68_ + 1] <= i_69_ / 3) {
						is_64_[++i_65_] = i_68_ + 1;
						if (i_65_ == is_64_.length - 1)
							break;
					}
				} else if (is[i_68_] < i_69_ / 20)
					break;
				i += is[i_68_];
			}
			is_64_[0] = i_65_;
			return is_64_;
		}

		int a() {
			return a;
		}

		public boolean hasNext() {
			return _if.hasNext();
		}

		public Object next() {
			return _if.next();
		}

		public void remove() {
			_if.remove();
		}
	}

	DataMatrixDM2(int i, int i_70_, int i_71_, PropertiesHolder var_c) {
		super(var_c);
		bv = i;
		bo = i_70_;
		bA = bz = 20;
		if (i_71_ > 10)
			bA = bz = 2 * i_71_;
		if (i < 300 && i_70_ < 300)
			bA = bz = 10;
	}

	int code() {
		return 5;
	}

	boolean isCheck() {
		return true;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	int _if(int i) {
		return -1;
	}

	String a(String string) {
		return "";
	}

	final int a(int i, int i_72_, int i_73_, int[] is, int i_74_, int i_75_) {
		int i_76_ = 2147483647;
		return i_76_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_77_) {
		return false;
	}

	void read(Recognizer var_a1, int i, ArrayList arraylist, Normalize var_a8, int i_78_,
			String string) {
		if (bv >= bA && bo >= bz) {
			int i_79_ = bo / bz;
			if (bo % bz > 1)
				i_79_++;
			int i_80_ = bv / bA;
			if (bv % bA > 1)
				i_80_++;
			int[][] is = new int[i_79_][i_80_];
			i = _if(var_a1, i, i_79_, i_80_, is, var_a8);
			int[][] is_81_ = new int[i_79_][i_80_];
			int i_82_ = Math.min(i_80_, i_79_);
			if (i_82_ > 10)
				i_82_ = 10;
			for (int i_83_ = 1; i_83_ <= i_82_; i_83_++)
				_if(is, i_83_, is_81_);
			Rectangle rectangle = new Rectangle(0, 0, bv, bo);
			int i_84_ = -1;
			int i_85_ = -1;
			ArrayList arraylist_86_ = new ArrayList();
			int i_87_ = 0;
			ArrayList arraylist_88_ = new ArrayList();
			int i_89_ = 1;
			while (i_89_-- > 0) {
				if (i_84_ != -1)
					is_81_[i_85_][i_84_] = 0;
				Point point = _if(is_81_);
				i_84_ = point.x;
				i_85_ = point.y;
				if (is_81_[i_85_][i_84_] == 0)
					break;
				Rectangle rectangle_90_ = _if(i_84_, i_85_, is, var_a8);
				if (rectangle_90_ != null) {
					if (rectangle_90_.width == 0 && rectangle_90_.height == 0)
						break;
					Iterator iterator = arraylist_88_.iterator();
					while_7_: do {
						Rectangle rectangle_91_;
						do {
							if (!iterator.hasNext())
								break while_7_;
							rectangle_91_ = (Rectangle) iterator.next();
						} while (!rectangle_91_.equals(rectangle_90_));
						iterator = null;
					} while (false);
					if (iterator != null) {
						arraylist_88_.add(rectangle_90_);
						Rectangle rectangle_92_ = new Rectangle(rectangle_90_.x
								* bA, rectangle_90_.y * bz, rectangle_90_.width
								* bA, rectangle_90_.height * bz);
						rectangle_92_ = rectangle_92_.intersection(rectangle);
						Point point_93_ = new Point(
								(rectangle_92_.x + rectangle_92_.width / 2),
								(rectangle_92_.y + rectangle_92_.height / 2));
						a var_a = new a(var_a1, i, rectangle_92_, point_93_);
						int i_94_ = var_a.a();
						ah var_ah = null;
						Poligono var_i = null;
						Object object = null;
						while (var_a.hasNext()) {
							int[] is_95_ = (int[]) var_a.next();
							if (is_95_[0] != 0 && is_95_[1] != 0
									&& is_95_[2] != 0 && is_95_[3] != 0) {
								var_i = _if(point_93_, i_94_, is_95_);
								var_i = _if(var_a1, i, var_i);
								if (var_i != null) {
									for (int i_96_ = 0; i_96_ < arraylist_86_
											.size(); i_96_++) {
										Polygon polygon = ((Polygon) arraylist_86_
												.get(i_96_));
										if (_if(polygon, var_i)) {
											var_i = null;
											break;
										}
									}
									if (var_i != null) {
										var_ah = new ah(var_i, var_a1, i, prop);
										if (var_ah != null
												&& (!var_ah._if() || var_ah.o
														.length() == 0)) {
											var_ah.a();
											var_ah = null;
										}
									}
									if (var_ah != null)
										break;
								}
							}
						}
						if (var_ah != null
								&& (string == null || string.length() == 0 || var_ah.o
										.startsWith(string))) {
							arraylist.add(new Recognized(var_ah.o, var_i, var_ah.k, 5,
									i, var_ah._long, prop));
							if (i_78_ > 0 && ++i_87_ >= i_78_)
								break;
							arraylist_86_.add(var_i);
							Rectangle rectangle_97_ = new Rectangle(0, 0, bA,
									bz);
							for (int i_98_ = 0; i_98_ < i_80_; i_98_++) {
								for (int i_99_ = 0; i_99_ < i_79_; i_99_++) {
									rectangle_97_.x = i_98_ * bA;
									rectangle_97_.y = i_99_ * bz;
									if (var_i.contiene(rectangle_97_))
										is[i_99_][i_98_] = 0;
								}
							}
							var_ah.a();
							Object object_100_ = null;
						}
					}
				}
			}
		}
	}

	private boolean _if(Polygon polygon, Polygon polygon_101_) {
		if (!polygon.getBounds().intersects(polygon_101_.getBounds()))
			return false;
		for (int i = 0; i < polygon.npoints; i++) {
			if (polygon_101_.contains(polygon.xpoints[i], polygon.ypoints[i]))
				return true;
		}
		for (int i = 0; i < polygon_101_.npoints; i++) {
			if (polygon.contains(polygon_101_.xpoints[i], polygon.ypoints[i]))
				return true;
		}
		return false;
	}

	private Poligono _if(Point point, int i, int[] is) {
		int[] is_102_ = new int[4];
		int[] is_103_ = new int[4];
		double d = bB[i];
		double d_104_ = bp[i];
		double d_105_ = (double) is[0] * d - (double) is[1] * d_104_;
		int i_106_;
		if (d_105_ < 0.0)
			i_106_ = (int) Math.floor(d_105_);
		else
			i_106_ = (int) Math.ceil(d_105_);
		is_102_[0] = point.x + i_106_;
		d_105_ = (double) is[0] * d_104_ + (double) is[1] * d;
		if (d_105_ < 0.0)
			i_106_ = (int) Math.floor(d_105_);
		else
			i_106_ = (int) Math.ceil(d_105_);
		is_103_[0] = point.y + i_106_;
		d_105_ = (double) -is[1] * d_104_ - (double) is[2] * d;
		if (d_105_ < 0.0)
			i_106_ = (int) Math.floor(d_105_);
		else
			i_106_ = (int) Math.ceil(d_105_);
		is_102_[1] = point.x + i_106_;
		d_105_ = (double) is[1] * d - (double) is[2] * d_104_;
		if (d_105_ < 0.0)
			i_106_ = (int) Math.floor(d_105_);
		else
			i_106_ = (int) Math.ceil(d_105_);
		is_103_[1] = point.y + i_106_;
		d_105_ = (double) -is[2] * d + (double) is[3] * d_104_;
		if (d_105_ < 0.0)
			i_106_ = (int) Math.floor(d_105_);
		else
			i_106_ = (int) Math.ceil(d_105_);
		is_102_[2] = point.x + i_106_;
		d_105_ = (double) -is[2] * d_104_ - (double) is[3] * d;
		if (d_105_ < 0.0)
			i_106_ = (int) Math.floor(d_105_);
		else
			i_106_ = (int) Math.ceil(d_105_);
		is_103_[2] = point.y + i_106_;
		d_105_ = (double) is[3] * d_104_ + (double) is[0] * d;
		if (d_105_ < 0.0)
			i_106_ = (int) Math.floor(d_105_);
		else
			i_106_ = (int) Math.ceil(d_105_);
		is_102_[3] = point.x + i_106_;
		d_105_ = (double) -is[3] * d + (double) is[0] * d_104_;
		if (d_105_ < 0.0)
			i_106_ = (int) Math.floor(d_105_);
		else
			i_106_ = (int) Math.ceil(d_105_);
		is_103_[3] = point.y + i_106_;
		return new Poligono(is_102_, is_103_, 4);
	}

	private int _if(Recognizer var_a1, int i, int i_107_, int i_108_, int[][] is,
			Normalize var_a8) {
		int i_109_ = 0;
		boolean bool = var_a8.isInitialized();
		for (int i_110_ = 0; i_110_ < i_107_; i_110_++) {
			for (int i_111_ = 0; i_111_ < i_108_; i_111_++) {
				int i_112_ = 0;
				int i_113_ = 0;
				for (int i_114_ = 0; i_114_ < bz; i_114_ += 3) {
					if (i_114_ == 0)
						i_113_ = i_110_ * bz;
					else
						i_113_ += 3;
					if (i_113_ >= bo - 1)
						break;
					for (int i_115_ = 0; i_115_ < bA; i_115_ += 3) {
						if (i_115_ == 0)
							i_109_ = i_111_ * bA;
						else
							i_109_ += 3;
						if (i_109_ >= bv - 1)
							break;
						i_112_++;
						int i_116_ = var_a1.a(i_109_, i_113_);
						if (!bool)
							var_a8.data[i_116_]++;
						if (i_116_ >= i)
							is[i_110_][i_111_] += 255;
					}
				}
				is[i_110_][i_111_] /= i_112_;
			}
		}
		if (!var_a8.isInitialized()) {
			var_a8.init();
			if (var_a8.isTwoNonZeroes() && var_a8.between(i) != i) {
				i = var_a8.between(i);
				for (int i_117_ = 0; i_117_ < i_107_; i_117_++) {
					for (int i_118_ = 0; i_118_ < i_108_; i_118_++)
						is[i_117_][i_118_] = 0;
				}
				i = _if(var_a1, i, i_107_, i_108_, is, var_a8);
			}
		}
		return i;
	}

	private void _if(int[][] is, int i, int[][] is_119_) {
		int i_120_ = is.length;
		int i_121_ = is[0].length;
		int[][] is_122_ = new int[i_120_ - i + 1][i_121_ - i + 1];
		int[] is_123_ = new int[4];
		int[] is_124_ = new int[4];
		int i_125_ = i * i;
		int i_126_ = i / 2;
		for (int i_127_ = 0; i_127_ <= i_120_ - i; i_127_++) {
			for (int i_128_ = 0; i_128_ <= i_121_ - i; i_128_++) {
				for (int i_129_ = 0; i_129_ < 4; i_129_++) {
					is_123_[i_129_] = 0;
					is_124_[i_129_] = 0;
				}
				for (int i_130_ = 0; i_130_ < i_126_; i_130_++) {
					for (int i_131_ = 0; i_131_ < i_126_; i_131_++) {
						is_123_[0] += is[i_127_ + i_130_][i_128_ + i_131_];
						is_124_[0]++;
					}
					for (int i_132_ = i_126_; i_132_ < i; i_132_++) {
						is_123_[1] += is[i_127_ + i_130_][i_128_ + i_132_];
						is_124_[1]++;
					}
				}
				for (int i_133_ = i_126_; i_133_ < i; i_133_++) {
					for (int i_134_ = 0; i_134_ < i / 2; i_134_++) {
						is_123_[2] += is[i_127_ + i_133_][i_128_ + i_134_];
						is_124_[2]++;
					}
					for (int i_135_ = i_126_; i_135_ < i; i_135_++) {
						is_123_[3] += is[i_127_ + i_133_][i_128_ + i_135_];
						is_124_[3]++;
					}
				}
				boolean bool = false;
				for (int i_136_ = 0; i_136_ < 4; i_136_++) {
					if (i != 1 || is_124_[i_136_] != 0) {
						int i_137_ = is_123_[i_136_] / is_124_[i_136_];
						if (i_137_ < 50 || i_137_ > 205) {
							bool = true;
							break;
						}
						is_122_[i_127_][i_128_] += is_123_[i_136_];
					}
				}
				if (bool)
					is_122_[i_127_][i_128_] = 0;
				else {
					is_122_[i_127_][i_128_] /= i_125_;
					if (is_122_[i_127_][i_128_] > 127)
						is_122_[i_127_][i_128_] = 255 - is_122_[i_127_][i_128_];
					int i_138_ = is_122_[i_127_][i_128_] / i_125_;
					for (int i_139_ = 0; i_139_ < i; i_139_++) {
						for (int i_140_ = 0; i_140_ < i; i_140_++)
							is_119_[i_127_ + i_139_][i_128_ + i_140_] += i_138_;
					}
				}
			}
		}
	}

	private Point _if(int[][] is) {
		int i = is[0].length;
		int i_141_ = is.length;
		int i_142_ = 0;
		int i_143_ = 0;
		int i_144_ = 0;
		for (int i_145_ = 0; i_145_ < i_141_; i_145_++) {
			for (int i_146_ = 0; i_146_ < i; i_146_++) {
				if (is[i_145_][i_146_] > i_142_)
					i_142_ = is[i_144_ = i_145_][i_143_ = i_146_];
			}
		}
		return new Point(i_143_, i_144_);
	}

	private Rectangle _if(int i, int i_147_, int[][] is, Normalize var_a8) {
		int i_148_ = is.length;
		int i_149_ = is[0].length;
		int i_150_ = 1;
		int i_151_ = 1;
		int i_152_;
		for (;;) {
			i_152_ = 0;
			for (int i_153_ = i_147_; i_153_ < i_147_ + i_151_; i_153_++) {
				for (int i_154_ = i; i_154_ < i + i_150_; i_154_++)
					i_152_ += is[i_153_][i_154_];
			}
			i_152_ /= i_151_ * i_150_;
			if (i_152_ > 127)
				i_152_ = 255 - i_152_;
			int[] is_155_ = new int[4];
			for (int i_156_ = i; i_156_ < i + i_150_; i_156_++) {
				if (i_147_ > 0)
					is_155_[3] += is[i_147_ - 1][i_156_];
				if (i_147_ + i_151_ < i_148_)
					is_155_[1] += is[i_147_ + i_151_][i_156_];
			}
			is_155_[3] /= i_150_;
			is_155_[1] /= i_150_;
			for (int i_157_ = i_147_; i_157_ < i_147_ + i_151_; i_157_++) {
				if (i > 0)
					is_155_[2] += is[i_157_][i - 1];
				if (i + i_150_ < i_149_)
					is_155_[0] += is[i_157_][i + i_150_];
			}
			is_155_[2] /= i_151_;
			is_155_[0] /= i_151_;
			for (int i_158_ = 0; i_158_ < 4; i_158_++) {
				if (is_155_[i_158_] > 127)
					is_155_[i_158_] = 255 - is_155_[i_158_];
			}
			int i_159_ = 0;
			int i_160_ = -1;
			for (int i_161_ = 0; i_161_ < 4; i_161_++) {
				if (is_155_[i_161_] > i_159_)
					i_159_ = is_155_[i_160_ = i_161_];
			}
			if (i_160_ == -1 || is_155_[i_160_] < 50)
				break;
			switch (i_160_) {
			case 0:
				i_150_++;
				break;
			case 1:
				i_151_++;
				break;
			case 2:
				i--;
				i_150_++;
				break;
			case 3:
				i_147_--;
				i_151_++;
				break;
			}
		}
		if (i_152_ < 64)
			return new Rectangle(0, 0, 0, 0);
		return new Rectangle(i, i_147_, i_150_, i_151_);
	}

	private Poligono _if(Recognizer var_a1, int i, Polygon polygon) {
		PointDouble var_ay = new PointDouble((double) polygon.xpoints[0],
				(double) polygon.ypoints[0]);
		PointDouble var_ay_162_ = new PointDouble((double) polygon.xpoints[1],
				(double) polygon.ypoints[1]);
		PointDouble var_ay_163_ = new PointDouble((double) polygon.xpoints[2],
				(double) polygon.ypoints[2]);
		double d = var_ay.mod(var_ay_162_);
		double d_164_ = var_ay_162_.mod(var_ay_163_);
		int i_165_ = Math.max((int) d, (int) d_164_);
		int[] is = new int[4];
		int[] is_166_ = new int[4];
		for (int i_167_ = 0; i_167_ < 4; i_167_++) {
			int i_168_ = i_165_ < 200 ? 3 : 4;
			int[] is_169_ = new int[4];
			int[] is_170_ = new int[4];
			int[] is_171_ = new int[4];
			int[] is_172_ = new int[4];
			int[] is_173_ = new int[4];
			int[] is_174_ = new int[4];
			int i_175_ = i_167_ == 3 ? 0 : i_167_ + 1;
			int i_176_ = i_175_ == 3 ? 0 : i_175_ + 1;
			int i_177_ = i_176_ == 3 ? 0 : i_176_ + 1;
			is_169_[0] = polygon.xpoints[i_167_];
			is_169_[1] = polygon.xpoints[i_167_]
					+ ((polygon.xpoints[i_175_] - polygon.xpoints[i_167_]) / i_168_);
			is_169_[2] = polygon.xpoints[i_167_]
					+ ((polygon.xpoints[i_176_] - polygon.xpoints[i_167_]) / i_168_);
			is_169_[3] = polygon.xpoints[i_167_]
					+ ((polygon.xpoints[i_177_] - polygon.xpoints[i_167_]) / i_168_);
			is_172_[0] = polygon.ypoints[i_167_];
			is_172_[1] = polygon.ypoints[i_167_]
					+ ((polygon.ypoints[i_175_] - polygon.ypoints[i_167_]) / i_168_);
			is_172_[2] = polygon.ypoints[i_167_]
					+ ((polygon.ypoints[i_176_] - polygon.ypoints[i_167_]) / i_168_);
			is_172_[3] = polygon.ypoints[i_167_]
					+ ((polygon.ypoints[i_177_] - polygon.ypoints[i_167_]) / i_168_);
			int i_178_ = (is_169_[2] - is_169_[0]) / 10;
			int i_179_ = (is_172_[2] - is_172_[0]) / 10;
			for (int i_180_ = 0; i_180_ < 4; i_180_++) {
				is_169_[i_180_] -= i_178_;
				is_172_[i_180_] -= i_179_;
			}
			is_170_[0] = is_169_[0];
			is_173_[0] = is_172_[0];
			is_170_[3] = (is_169_[0] + is_169_[1]) / 2;
			is_173_[3] = (is_172_[0] + is_172_[1]) / 2;
			is_170_[2] = (is_169_[3] + is_169_[2]) / 2;
			is_173_[2] = (is_172_[3] + is_172_[2]) / 2;
			is_170_[1] = is_169_[3];
			is_173_[1] = is_172_[3];
			is_171_[0] = is_169_[0];
			is_174_[0] = is_172_[0];
			is_171_[1] = is_169_[1];
			is_174_[1] = is_172_[1];
			is_171_[2] = (is_169_[1] + is_169_[2]) / 2;
			is_174_[2] = (is_172_[1] + is_172_[2]) / 2;
			is_171_[3] = (is_169_[0] + is_169_[3]) / 2;
			is_174_[3] = (is_172_[0] + is_172_[3]) / 2;
			Poligono var_i = new Poligono(is_170_, is_173_, 4);
			int i_181_ = (var_i.xpoints[2] + var_i.xpoints[3]) / 2;
			int i_182_ = (var_i.ypoints[2] + var_i.ypoints[3]) / 2;
			int i_183_ = polygon.xpoints[i_167_] - polygon.xpoints[i_177_];
			int i_184_ = polygon.ypoints[i_167_] - polygon.ypoints[i_177_];
			Poligono var_i_185_ = new Poligono(is_171_, is_174_, 4);
			i_183_ = polygon.xpoints[i_167_] - polygon.xpoints[i_175_];
			i_184_ = polygon.ypoints[i_167_] - polygon.ypoints[i_175_];
			PointDouble var_ay_186_ = a(var_a1, i, var_i_185_, i_181_, i_182_, i_183_,
					i_184_);
			if (var_ay_186_ == null)
				return null;
		}
		return null;
	}

	private PointDouble a(Recognizer var_a1, int i, Poligono var_i, int i_187_, int i_188_, int i_189_,
			int i_190_) {
		int i_191_ = 0;
		double d = Math.sqrt((double) (i_189_ * i_189_ + i_190_ * i_190_));
		double d_192_ = (double) i_190_ / d;
		double d_193_ = (double) i_189_ / d;
		Point point = new Point(var_i.xpoints[0], var_i.ypoints[0]);
		Point point_194_ = new Point(var_i.xpoints[1], var_i.ypoints[1]);
		Point point_195_ = new Point(var_i.xpoints[3], var_i.ypoints[3]);
		int i_196_ = (int) point.distance(point_195_);
		int i_197_ = (int) point.distance(point_194_) / 2;
		Object object = null;
		double d_198_ = d_193_;
		double d_199_ = d_192_;
		double d_200_ = d_192_;
		double d_201_ = -d_193_;
		int i_202_ = 0;
		int i_203_ = 0;
		int i_204_;
		for (i_204_ = 1; i_204_ < i_196_; i_204_++) {
			int i_205_ = 0;
			double d_206_ = (double) i_187_ + (double) i_204_ * d_198_;
			double d_207_ = (double) i_188_ + (double) i_204_ * d_199_;
			for (int i_208_ = -i_197_; i_208_ <= i_197_; i_208_++) {
				double d_209_ = d_206_ + (double) i_208_ * d_200_;
				double d_210_ = d_207_ + (double) i_208_ * d_201_;
				int i_211_ = (int) (d_209_ + 0.5);
				int i_212_ = (int) (d_210_ + 0.5);
				if (var_a1.a(i_211_, i_212_) < i)
					i_205_++;
			}
			int i_213_ = i_191_ / i_204_;
			if (i_204_ > 2) {
				if (i_202_ > i_213_ / 3 && i_205_ <= i_213_ / 3)
					break;
				if (i_205_ <= i_213_ / 20) {
					if (++i_203_ > i_196_ / 10)
						break;
				} else
					i_203_ = 0;
			}
			i_191_ += i_205_;
			i_202_ = i_205_;
		}
		if (i_204_ == i_196_)
			return null;
		return new PointDouble((double) i_204_ * d_198_, (double) i_204_ * d_199_);
	}

	static {
		for (int i = 0; i < 48; i++) {
			double d = 3.141592653589793 * (double) i / 24.0;
			bp[i] = Math.sin(d);
			bB[i] = Math.cos(d);
			bC[i] = Math.tan(d);
		}
		ah._try = 24;
		ah._if = bp;
		ah.p = bB;
		ah.n = bC;
	}
}
