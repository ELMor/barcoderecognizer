/* a2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

class RSSLTD extends Barcode {
	private static final boolean bN = false;

	private static final long[] bD = { 11111111113311L, 11111111123211L,
			11111111133111L, 11111112113211L, 11111112123111L, 11111113113111L,
			11111211113211L, 11111211123111L, 11111212113111L, 11111311113111L,
			11121111113211L, 11121111123111L, 11121112113111L, 11121211113111L,
			11131111113111L, 12111111113211L, 12111111123111L, 12111112113111L,
			12111211113111L, 12121111113111L, 13111111113111L, 11111111212311L,
			11111111222211L, 11111111232111L, 11111112212211L, 11111112222111L,
			11111113212111L, 11111211212211L, 11111211222111L, 11111212212111L,
			11111311212111L, 11121111212211L, 11121111222111L, 11121112212111L,
			11121211212111L, 11131111212111L, 12111111212211L, 12111111222111L,
			12111112212111L, 12111211212111L, 12121111212111L, 13111111212111L,
			11111111311311L, 11111111321211L, 11111112311211L, 11121111311211L,
			12111111311211L, 11111121112311L, 11111121122211L, 11111121132111L,
			11111122112211L, 11121121112211L, 11121121122111L, 11121122112111L,
			11121221112111L, 11131121112111L, 12111121112211L, 12111121122111L,
			12121121112111L, 11112111112311L, 11112111122211L, 11112111132111L,
			11112112112211L, 11112112122111L, 11112211112211L, 12112111112211L,
			12112111122111L, 12112112112111L, 12112211112111L, 12122111112111L,
			13112111112111L, 11211111112311L, 11211111122211L, 11211111132111L,
			11211112112211L, 11211112122111L, 11211113112111L, 11211211112211L,
			11211211122111L, 11221111112211L, 21111111122211L, 21111111132111L,
			21111112112211L, 21111112122111L, 21111113112111L, 21111211122111L,
			21111212112111L, 21121111122111L, 21111111221211L };

	private static final int[][] bG = {
			{ 1, 3, 9, 27, 81, 65, 17, 51, 64, 14, 42, 37, 22, 66 },
			{ 20, 60, 2, 6, 18, 54, 73, 41, 34, 13, 39, 28, 84, 74 } };

	private static HashMap bL = null;

	private ArrayList bE;

	private int[] bI = new int[14];

	private int[] bO = new int[14];

	private int[] bH = new int[12];

	private int[] bK = new int[12];

	private int[] bJ = new int[14];

	private int[] bM = new int[14];

	private int[] bF = new int[14];

	RSSLTD(PropertiesHolder var_c) {
		super(var_c);
		bE = new ArrayList();
	}

	int code() {
		return 19;
	}

	int _if(int i) {
		if (i == 27)
			return 27;
		return -1;
	}

	boolean isCheck() {
		return true;
	}

	double _else() {
		return 0.0;
	}

	double _for() {
		return 0.0;
	}

	void _do(int i) {
		/* empty */
	}

	final int a(int i, int i_0_, int i_1_, int[] is, int i_2_, int i_3_) {
		int i_4_ = 0;
		if (is[0] == 0)
			i_4_ = 2;
		int i_5_ = is[i_4_];
		for (int i_6_ = 1; i_6_ < 14; i_6_++)
			i_5_ += is[i_4_ + i_6_];
		int i_7_ = 2147483647;
		while (i_4_ + 14 <= i_1_) {
			if (is[i_4_ + 12] <= i_5_ / 12 && is[i_4_ + 12] >= i_5_ / 24
					&& is[i_4_ + 13] <= i_5_ / 12 && is[i_4_ + 13] >= i_5_ / 24) {
				int i_8_ = _if(is, i_4_);
				if (i_8_ >= 0) {
					SepInten var_d = this.readData(i, i_0_, is, i_4_, 14, i_2_, i_8_, true,
							i_3_);
					SepInten var_d_9_ = this.readData(i, i_0_, is, i_4_, 14, i_2_, i_8_,
							false, i_3_);
					if (var_d.a() == null || !var_d.a().contains(var_d_9_))
						var_d.a(var_d_9_);
					if (var_d_9_.a() == null || !var_d_9_.a().contains(var_d))
						var_d_9_.a(var_d);
					if (i_5_ < i_7_)
						i_7_ = i_5_;
				}
			}
			i_5_ -= is[i_4_];
			i_5_ -= is[i_4_ + 1];
			i_4_ += 2;
			i_5_ += is[i_4_ + 12];
			i_5_ += is[i_4_ + 13];
		}
		return i_7_;
	}

	boolean a(int i, int[] is, SepInten var_d, SepInten var_d_10_) {
		int i_11_ = var_d.c % 100 + var_d_10_.c % 100;
		i_11_ %= 89;
		if (i_11_ != var_d._try() || i_11_ != var_d_10_._try())
			return false;
		long l = (long) (var_d.c / 100);
		long l_12_ = (long) (var_d_10_.c / 100);
		long l_13_ = 2013571L * l + l_12_;
		if (l_13_ >= 2015133531096L)
			l_13_ -= 2015133531096L;
		String string = Long.toString(l_13_);
		ArrayList arraylist = getAL3();
		arraylist.add(new Recognized(string, 13, var_d, var_d_10_, code(), prop));
		return true;
	}

	void _try() {
		super._try();
		Iterator iterator = getAL3().iterator();
		while (iterator.hasNext()) {
			Recognized var_bf = (Recognized) iterator.next();
			SepInten var_d = var_bf.getSepIntensity();
			SepInten var_d_14_ = var_bf._new();
			_if(var_d, var_d_14_);
		}
	}

	private void _if(SepInten var_d, SepInten var_d_15_) {
		UserRect var_al = var_d.getUserRect();
		UserRect var_al_16_ = var_d_15_.getUserRect();
		var_al.resuma(var_al_16_);
		var_al_16_.resuma(var_al);
		_if(var_d, true);
		_if(var_d_15_, false);
	}

	private void _if(SepInten var_d, boolean bool) {
		double d = var_d._else();
		int i = var_d._do() * 28 / 18;
		int i_17_ = var_d.getDirection();
		double d_18_ = Math.sqrt(d * d + 1.0);
		double d_19_ = d / d_18_;
		double d_20_ = 1.0 / d_18_;
		int i_21_ = (int) ((double) i * d_20_ * d_20_);
		int i_22_ = i_21_;
		int i_23_ = (int) ((double) i * d_20_ * d_19_);
		switch (i_17_) {
		case 90:
			i_23_ = -i_23_;
			break;
		case 270:
			i_22_ = -i_22_;
			break;
		case 180: {
			int i_24_ = i_22_;
			i_22_ = -i_23_;
			i_23_ = i_24_;
			break;
		}
		case 360: {
			int i_25_ = i_22_;
			i_22_ = i_23_;
			i_23_ = -i_25_;
			break;
		}
		}
		if (bool) {
			i_22_ = -i_22_;
			i_23_ = -i_23_;
		}
		var_d.a(i_22_, i_23_);
	}

	void _if(Recognizer var_a1) {
		ArrayList arraylist = new ArrayList();
		Iterator iterator = al01.iterator();
		while (iterator.hasNext()) {
			SepInten var_d = (SepInten) iterator.next();
			if (_if(var_d, true, var_a1) == true)
				arraylist.add(var_d);
		}
		al01 = arraylist;
		arraylist = new ArrayList();
		Iterator iterator_26_ = al02.iterator();
		while (iterator_26_.hasNext()) {
			SepInten var_d = (SepInten) iterator_26_.next();
			if (_if(var_d, false, var_a1) == true)
				arraylist.add(var_d);
		}
		al02 = arraylist;
	}

	private boolean _if(SepInten var_d, boolean bool, Recognizer var_a1) {
		int i = var_d._do();
		if (!bool)
			i = -i;
		int i_27_ = var_d.getDirection();
		int i_28_ = var_d.getBWBound();
		PointDouble var_ay = var_d.getUserRect().getP1();
		PointDouble var_ay_29_ = var_d.getUserRect().getP2();
		double d = var_ay.getX();
		double d_30_ = var_ay.getY();
		double d_31_ = var_ay_29_.getX();
		double d_32_ = var_ay_29_.getY();
		PointDouble var_ay_33_ = null;
		PointDouble var_ay_34_ = null;
		switch (i_27_) {
		case 90:
			var_ay_33_ = new PointDouble(d + (double) i, d_30_);
			var_ay_34_ = new PointDouble(d_31_ + (double) i, d_32_);
			break;
		case 270:
			var_ay_33_ = new PointDouble(d - (double) i, d_30_);
			var_ay_34_ = new PointDouble(d_31_ - (double) i, d_32_);
			break;
		case 180:
			var_ay_33_ = new PointDouble(d, d_30_ + (double) i);
			var_ay_34_ = new PointDouble(d_31_, d_32_ + (double) i);
			break;
		case 360:
			var_ay_33_ = new PointDouble(d, d_30_ - (double) i);
			var_ay_34_ = new PointDouble(d_31_, d_32_ - (double) i);
			break;
		}
		PointDouble var_ay_35_ = var_d.getUserRect().mirrorPoint(var_ay_33_);
		PointDouble var_ay_36_ = var_d.getUserRect().mirrorPoint(var_ay_34_);
		int i_37_ = a(var_ay_35_, var_ay_36_, bool, var_d, var_a1);
		if (i_37_ == -1)
			return false;
		var_d.c = i_37_;
		return true;
	}

	private int a(PointDouble var_ay, PointDouble var_ay_38_, boolean bool, SepInten var_d, Recognizer var_a1) {
		int i = var_d.getDirection();
		Counters var_g = new Counters();
		double d = var_ay.getX();
		double d_39_ = var_ay.getY();
		double d_40_ = var_ay_38_.getX();
		double d_41_ = var_ay_38_.getY();
		if (i == 90 || i == 270) {
			double d_42_ = (d_40_ - d) / (d_41_ - d_39_);
			double d_43_ = d - d_39_ * d_42_;
			int i_44_ = bool ? -1 : 1;
			i_44_ = i_44_ * (i == 90 ? 1 : -1);
			int i_45_ = 0;
			int i_46_ = d_41_ > d_39_ ? 1 : -1;
			for (int i_47_ = (int) d_39_; i_47_ <= (int) d_41_; i_47_ += i_46_) {
				int i_48_ = (int) (d_43_ + (double) i_47_ * d_42_ + 0.5);
				int i_49_ = a(i_48_, i_44_, i_47_, i_45_, bool, var_a1, var_d);
				if (i_49_ != -1)
					var_g.incCounter(i_49_);
			}
		} else {
			double d_50_ = (d_41_ - d_39_) / (d_40_ - d);
			double d_51_ = d_39_ - d * d_50_;
			int i_52_ = bool ? -1 : 1;
			i_52_ = i_52_ * (i == 180 ? 1 : -1);
			int i_53_ = 0;
			int i_54_ = d_40_ > d ? 1 : -1;
			for (int i_55_ = (int) d; i_55_ <= (int) d_40_; i_55_ += i_54_) {
				int i_56_ = (int) (d_51_ + (double) i_55_ * d_50_ + 0.5);
				int i_57_ = a(i_55_, i_53_, i_56_, i_52_, bool, var_a1, var_d);
				if (i_57_ != -1)
					var_g.incCounter(i_57_);
			}
		}
		int i_58_;
		try {
			i_58_ = var_g.getMax();
		} catch (Exception exception) {
			i_58_ = -1;
		}
		return i_58_;
	}

	private int a(int i, int i_59_, int i_60_, int i_61_, boolean bool,
			Recognizer var_a1, SepInten var_d) {
		int i_62_ = var_d.getBWBound();
		int i_63_ = var_d._do();
		int i_64_ = bool ? 0 : 255;
		int i_65_ = 0;
		int i_66_ = var_a1.a(i, i_60_) < i_62_ ? 0 : 255;
		while_14_: do {
			if (i_66_ != i_64_) {
				do {
					i += i_59_;
					i_60_ += i_61_;
					i_66_ = var_a1.a(i, i_60_) < i_62_ ? 0 : 255;
					if (i_66_ == i_64_)
						break while_14_;
				} while (++i_65_ <= i_63_ / 8);
				return -1;
			}
			do {
				i_66_ = var_a1.a(i - i_59_, i_60_ - i_61_) < i_62_ ? 0 : 255;
				if (i_66_ != i_64_)
					break while_14_;
				i -= i_59_;
				i_60_ -= i_61_;
			} while (++i_65_ <= i_63_ / 8);
			return -1;
		} while (false);
		i_65_ = 0;
		int i_67_ = bool ? 13 : 0;
		bI[i_67_] = 0;
		int i_68_ = i_64_;
		for (;;) {
			i_66_ = var_a1.a(i, i_60_) < i_62_ ? 0 : 255;
			if (i_66_ != i_68_) {
				if (bool && --i_67_ < 0 || !bool && ++i_67_ > 13)
					break;
				bI[i_67_] = 1;
				i_68_ = i_66_;
			} else
				bI[i_67_]++;
			if (++i_65_ > 2 * i_63_)
				return -1;
			i += i_59_;
			i_60_ += i_61_;
		}
		int i_69_ = a(26, bool);
		return i_69_;
	}

	private int a(int i, boolean bool) {
		for (int i_70_ = 0; i_70_ < 14; i_70_++)
			bO[i_70_] = bI[i_70_];
		int i_71_ = bO[0];
		for (int i_72_ = 1; i_72_ < 14; i_72_++)
			i_71_ += bO[i_72_];
		for (int i_73_ = 0; i_73_ < 12; i_73_++) {
			bH[i_73_] = bO[i_73_] + bO[i_73_ + 1];
			bK[i_73_] = bH[i_73_] * i / i_71_;
			if (bH[i_73_] * i % i_71_ > i_71_ / 2)
				bK[i_73_]++;
			if (bK[i_73_] < 2 || bK[i_73_] > 9)
				return -1;
		}
		_for(i, bK, bO);
		int[] is = new int[7];
		int[] is_74_ = new int[7];
		int i_75_ = 0;
		int i_76_ = 0;
		for (int i_77_ = 0; i_77_ < 7; i_77_++) {
			is[i_77_] = bO[i_77_ * 2];
			is_74_[i_77_] = bO[i_77_ * 2 + 1];
			i_75_ += is[i_77_];
			i_76_ += is_74_[i_77_];
		}
		if (i_75_ % 2 == 0 || i_76_ % 2 == 0) {
			_do(i, bO, bI);
			i_75_ = 0;
			i_76_ = 0;
			for (int i_78_ = 0; i_78_ < 7; i_78_++) {
				is[i_78_] = bO[i_78_ * 2];
				is_74_[i_78_] = bO[i_78_ * 2 + 1];
				i_75_ += is[i_78_];
				i_76_ += is_74_[i_78_];
			}
			if (i_75_ % 2 == 0 || i_76_ % 2 == 0)
				return -1;
		}
		int i_79_ = -1;
		int i_80_;
		int i_81_;
		int i_82_;
		int i_83_;
		if (i_75_ == 17) {
			i_80_ = 6;
			i_81_ = 3;
			i_82_ = 0;
			i_83_ = 28;
		} else if (i_75_ == 13) {
			i_80_ = 5;
			i_81_ = 4;
			i_82_ = 183064;
			i_83_ = 728;
		} else if (i_75_ == 9) {
			i_80_ = 3;
			i_81_ = 6;
			i_82_ = 820064;
			i_83_ = 6454;
		} else if (i_75_ == 15) {
			i_80_ = 5;
			i_81_ = 4;
			i_82_ = 1000776;
			i_83_ = 203;
		} else if (i_75_ == 11) {
			i_80_ = 4;
			i_81_ = 5;
			i_82_ = 1491021;
			i_83_ = 2408;
		} else if (i_75_ == 19) {
			i_80_ = 8;
			i_81_ = 1;
			i_82_ = 1979845;
			i_83_ = 1;
		} else if (i_75_ == 7) {
			i_80_ = 1;
			i_81_ = 8;
			i_82_ = 1996939;
			i_83_ = 16632;
		} else
			return -1;
		int i_84_ = _new(is, 7, i_80_, 0);
		int i_85_ = _new(is_74_, 7, i_81_, 1);
		i_79_ = i_84_ * i_83_ + i_85_ + i_82_;
		if (i_79_ != -1) {
			int i_86_ = 0;
			int i_87_ = bool ? 0 : 1;
			for (int i_88_ = 0; i_88_ < 14; i_88_++)
				i_86_ += bO[i_88_] * bG[i_87_][i_88_];
			i_86_ %= 89;
			i_79_ = i_79_ * 100 + i_86_;
		}
		return i_79_;
	}

	private void _for(int i, int[] is, int[] is_89_) {
		int i_90_ = 10;
		int i_91_ = 1;
		int i_92_ = is_89_[0] = 1;
		for (int i_93_ = 1; i_93_ < 12; i_93_ += 2) {
			is_89_[i_93_] = is[i_93_ - 1] - is_89_[i_93_ - 1];
			is_89_[i_93_ + 1] = is[i_93_] - is_89_[i_93_];
			i_92_ += is_89_[i_93_] + is_89_[i_93_ + 1];
			if (is_89_[i_93_] < i_90_)
				i_90_ = is_89_[i_93_];
			if (is_89_[i_93_ + 1] < i_91_)
				i_91_ = is_89_[i_93_ + 1];
		}
		is_89_[13] = i - i_92_;
		if (is_89_[13] < i_90_)
			i_90_ = is_89_[13];
		int i_94_ = 0;
		if (i_90_ != 1)
			i_94_ = i_90_ - 1;
		if (i_94_ != 0) {
			for (int i_95_ = 0; i_95_ < 14; i_95_ += 2) {
				is_89_[i_95_] += i_94_;
				is_89_[i_95_ + 1] -= i_94_;
			}
		}
	}

	private void _do(int i, int[] is, int[] is_96_) {
		int i_97_ = 0;
		for (int i_98_ = 0; i_98_ < 14; i_98_++) {
			i_97_ += is_96_[i_98_];
			is_96_[i_98_] *= i;
		}
		for (int i_99_ = 0; i_99_ < 12; i_99_++) {
			int i_100_ = is_96_[i_99_] / i_97_;
			int i_101_ = is_96_[i_99_] % i_97_;
			if (i_100_ == 0 || i_101_ > i_97_ / 2) {
				i_100_++;
				i_101_ -= i_97_;
			}
			if (i_99_ < 11)
				is_96_[i_99_ + 1] += i_101_;
			is[i_99_] = i_100_;
		}
	}

	private int _new(int[] is, int i, int i_102_, int i_103_) {
		int i_104_ = 0;
		int i_105_ = 0;
		int i_107_;
		int i_106_ = i_107_ = 0;
		for (/**/; i_107_ < i; i_107_++)
			i_106_ += is[i_107_];
		for (int i_108_ = 0; i_108_ < i - 1; i_108_++) {
			int i_109_ = 1;
			i_105_ |= 1 << i_108_;
			while (i_109_ < is[i_108_]) {
				int i_110_ = _do(i_106_ - i_109_ - 1, i - i_108_ - 2);
				if (i_103_ == 0 && i_105_ == 0
						&& i_106_ - i_109_ - (i - i_108_ - 1) >= i - i_108_ - 1)
					i_110_ -= _do(i_106_ - i_109_ - (i - i_108_), i - i_108_
							- 2);
				if (i - i_108_ - 1 > 1) {
					int i_111_ = 0;
					for (int i_112_ = i_106_ - i_109_ - (i - i_108_ - 2); i_112_ > i_102_; i_112_--)
						i_111_ += _do(i_106_ - i_109_ - i_112_ - 1, i - i_108_
								- 3);
					i_110_ -= i_111_ * (i - 1 - i_108_);
				} else if (i_106_ - i_109_ > i_102_)
					i_110_--;
				i_104_ += i_110_;
				i_109_++;
				i_105_ &= 1 << i_108_ ^ 0xffffffff;
			}
			i_106_ -= i_109_;
		}
		return i_104_;
	}

	private int _do(int i, int i_113_) {
		int i_114_;
		int i_115_;
		if (i - i_113_ > i_113_) {
			i_114_ = i_113_;
			i_115_ = i - i_113_;
		} else {
			i_114_ = i - i_113_;
			i_115_ = i_113_;
		}
		int i_116_ = 1;
		int i_117_ = 1;
		for (int i_118_ = i; i_118_ > i_115_; i_118_--) {
			i_116_ *= i_118_;
			if (i_117_ <= i_114_) {
				i_116_ /= i_117_;
				i_117_++;
			}
		}
		for (/**/; i_117_ <= i_114_; i_117_++)
			i_116_ /= i_117_;
		return i_116_;
	}

	private final int _if(int[] is, int i) {
		int i_119_ = 0;
		for (int i_120_ = 0; i_120_ < 14; i_120_++) {
			bJ[i_120_] = is[i + i_120_] * 18;
			i_119_ += is[i + i_120_];
		}
		for (int i_121_ = 0; i_121_ < 14; i_121_++) {
			int i_122_ = is[i + i_121_];
			if (i_122_ < i_119_ / 24 || i_122_ > i_119_ / 4)
				return -1;
		}
		boolean bool = false;
		for (int i_123_ = 0; i_123_ < 14; i_123_++)
			bM[i_123_] = bJ[i_123_];
		for (int i_124_ = 0; i_124_ < 14; i_124_++) {
			int i_125_ = bM[i_124_] / i_119_;
			int i_126_ = bM[i_124_] % i_119_;
			if (i_125_ == 0 || i_126_ > i_119_ / 2) {
				i_125_++;
				i_126_ -= i_119_;
			}
			if (i_125_ >= 5) {
				bool = true;
				break;
			}
			if (i_125_ == 4) {
				i_125_ = 3;
				i_126_ += i_119_;
			}
			bF[i_124_] = i_126_;
			if (i_124_ < 13)
				bM[i_124_ + 1] += i_126_;
			bM[i_124_] = i_125_;
		}
		if (!bool) {
			long l = 0L;
			int i_127_ = -1;
			for (int i_128_ = 0; i_128_ < 14; i_128_++)
				l = l * 10L + (long) bM[i_128_];
			Object object = bL.get(new Long(l));
			if (object != null) {
				i_127_ = ((Integer) object).intValue();
				return i_127_;
			}
		}
		return -1;
	}

	String a(String string) {
		StringBuffer stringbuffer = new StringBuffer();
		int i = 13 - string.length();
		for (int i_129_ = 0; i_129_ < i; i_129_++)
			stringbuffer.append('0');
		stringbuffer.append(string);
		int i_130_ = 0;
		for (int i_131_ = 0; i_131_ < 13; i_131_++) {
			i = stringbuffer.charAt(i_131_) - 48;
			if (i_131_ % 2 == 0)
				i *= 3;
			i_130_ += i;
		}
		i_130_ %= 10;
		if (i_130_ != 0)
			i_130_ = 10 - i_130_;
		stringbuffer.append((char) (48 + i_130_));
		return new String(stringbuffer);
	}

	static {
		bL = new HashMap();
		for (int i = 0; i < bD.length; i++)
			bL.put(new Long(bD[i]), new Integer(i));
	}
}
