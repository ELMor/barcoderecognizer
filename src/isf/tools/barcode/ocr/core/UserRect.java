/* al - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Rectangle;

class UserRect {
	private PointDouble p1;

	private PointDouble p2;

	UserRect(double d, double d_0_, double d_1_, double d_2_) {
		p1 = new PointDouble(d, d_0_);
		p2 = new PointDouble(d_1_, d_2_);
	}

	PointDouble getP1() {
		return p1;
	}

	PointDouble getP2() {
		return p2;
	}

	double getP1x() {
		return p1.x;
	}

	void setP1x(double d) {
		p1.x = d;
	}

	double getP1y() {
		return p1.y;
	}

	void setP1y(double d) {
		p1.y = d;
	}

	double getP2x() {
		return p2.x;
	}

	void setP2x(double d) {
		p2.x = d;
	}

	double getP2y() {
		return p2.y;
	}

	void setP2y(double d) {
		p2.y = d;
	}

	Rectangle getRectangle() {
		return new Rectangle((int) getP1x(), (int) getP1y(),
				(int) (getP2x() - getP1x()), (int) (getP2y() - getP1y()));
	}

	PointDouble mirrorPoint(PointDouble var_ay) {
		double d = p1.x - p2.x;
		double d_3_ = p1.y - p2.y;
		double d_4_ = d * d;
		double d_5_ = d_3_ * d_3_;
		double d_6_ = ((d_5_ * p1.x + d_4_ * var_ay.x + d * d_3_
				* (var_ay.y - p1.y)) / (d_4_ + d_5_));
		double d_7_ = ((d_4_ * p1.y + d_5_ * var_ay.y + d * d_3_
				* (var_ay.x - p1.x)) / (d_4_ + d_5_));
		return new PointDouble(d_6_, d_7_);
	}

	double _if(PointDouble ptmp) {
		PointDouble p = mirrorPoint(ptmp);
		return p.mod(ptmp);
	}

	void sumVector(int i, int i_9_) {
		p1.x += (double) i;
		p1.y += (double) i_9_;
		p2.x += (double) i;
		p2.y += (double) i_9_;
	}

	void resuma(UserRect var_al_10_) {
		suma(var_al_10_.getP1());
		suma(var_al_10_.getP2());
	}

	private void suma(PointDouble var_ay) {
		PointDouble var_ay_11_ = mirrorPoint(var_ay);
		double d = var_ay_11_.mod(p1);
		double d_12_ = var_ay_11_.mod(p2);
		double d_13_ = p1.mod(p2);
		if (d > d_13_ && d > d_12_) {
			p2.x = var_ay_11_.x;
			p2.y = var_ay_11_.y;
		} else if (d_12_ > d_13_ && d_12_ > d) {
			p1.x = var_ay_11_.x;
			p1.y = var_ay_11_.y;
		}
	}
}
