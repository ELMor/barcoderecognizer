/* bf - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package isf.tools.barcode.ocr.core;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Recognized {
	private static final boolean f = false;
	
	private static final String[] barcodeNames = { "CODABAR", "CODE11", "CODE128",
			"CODE39", "CODE93", "DATAMATRIX", "EAN13", "EAN8", "I2OF5",
			"PATCH", "PDF417", "PLUS2", "PLUS5", "TELEPEN", "UPCA", "UPCE",
			"CODE32", "QR", "RSS14", "RSSLIMITED" };

	static final char _try = '\uffff';

	private int h;

	private PropertiesHolder prop;

	private String encoding;

	private int code;

	private SepInten sepIntensity;

	private SepInten n;

	private int numberOfProps;

	private int a;

	private Rectangle rect;

	private Poligono e;

	private Recognized supplemental;

	private HashMap holder;

	private int x;

	private StructuredAppend structAppend;

	Poligono poligono;

	public class StructuredAppend {
		public int seqId;

		public int position;

		public int total;

		StructuredAppend(int i, int i_0_, int i_1_) {
			seqId = i_1_;
			position = i;
			total = i_0_;
		}
	}

	Recognized(String string, int i, SepInten s1, SepInten s2, int codigo, PropertiesHolder p) {
		h = i;
		sepIntensity = s1;
		n = s2;
		code = codigo;
		numberOfProps = 1;
		supplemental = null;
		a = s1.getDirection();
		prop = p;
		holder = new HashMap();
		holder.put(string, new Integer(1));
		x = string.length();
	}

	Recognized(String enc, Poligono pol, int i, int codigo, int i_5_, int[] sappend, PropertiesHolder propiedades) {
		encoding = enc;
		rect = pol.getBounds();
		code = codigo;
		prop = propiedades;
		poligono = pol;
		h = -1;
		a = -1;
		sepIntensity = new SepInten(new UserRect((double) pol.xpoints[i],
				(double) pol.ypoints[i], (double) pol.xpoints[(i + 1) % 4],
				(double) pol.ypoints[(i + 1) % 4]), i_5_);
		n =new SepInten(new UserRect((double) pol.xpoints[(i + 2) % 4],
				(double) pol.ypoints[(i + 2) % 4],
				(double) pol.xpoints[(i + 3) % 4],
				(double) pol.ypoints[(i + 3) % 4]), i_5_);
		if (sappend != null)
			structAppend = new StructuredAppend(sappend[0], sappend[1], sappend[2]);
	}

	public Point[] endEdge() {
		Point[] points = new Point[2];
		UserRect var_al = n.getUserRect();
		PointDouble var_ay = var_al.getP1();
		points[0] = new Point((int) var_ay.x, (int) var_ay.y);
		var_ay = var_al.getP2();
		points[1] = new Point((int) var_ay.x, (int) var_ay.y);
		return points;
	}

	public Point[] stEdge() {
		Point[] points = new Point[2];
		UserRect var_al = sepIntensity.getUserRect();
		PointDouble var_ay = var_al.getP1();
		points[0] = new Point((int) var_ay.x, (int) var_ay.y);
		var_ay = var_al.getP2();
		points[1] = new Point((int) var_ay.x, (int) var_ay.y);
		return points;
	}

	public Rectangle getRectangle() {
		return rect;
	}

	Poligono _int() {
		return e;
	}

	public int _goto() {
		return h;
	}

	public int direction() {
		return a;
	}

	public String getEncoding() {
		return encoding;
	}

	public StructuredAppend getStructuredAppend() {
		return structAppend;
	}

	public Recognized getSupplemental() {
		return supplemental;
	}

	public String getBarCodeName() {
		return barcodeNames[code];
	}

	public int getBarcodeCode() {
		return code;
	}

	public boolean isSupplementalAvailable() {
		return supplemental != null;
	}

	public String toString() {
		int i = _goto();
		String string;
		if (i == -1)
			string = "";
		else if (i == getEncoding().length() - 1)
			string = "  (last char is";
		else if (i == getEncoding().length() - 2)
			string = "  (last two chars are";
		else
			string = "  (char at index " + i + " is";
		if (string.length() > 0)
			string += " checksum)";
		Object object = null;
		String scanDir;
		if (getBarcodeCode() == 5)
			scanDir = "";
		else {
			scanDir = "  scanned: ";
			if (direction() == 90)
				scanDir += "right";
			else if (direction() == 180)
				scanDir += "down";
			else if (direction() == 270)
				scanDir += "left";
			else if (direction() == 360)
				scanDir += "up";
		}
		String enc = getEncoding().indexOf('\n') == -1 ? "" : "\n";
		StructuredAppend sa = getStructuredAppend();
		String msg = "";
		if (sa != null)
			msg = ("\n  Structured Append: position = " + sa.position
					+ " of " + sa.total + " with sequence identifier: " + sa.seqId);
		return ("tasman.bars.BarCode  encoding: " + enc + getEncoding() + enc
				+ string + "  symbology: " + getBarCodeName() + "  supplemental: "
				+ isSupplementalAvailable() + scanDir + "  separatorIntensity: "
				+ sepIntensity.getBWBound() + "-" + n.getBWBound() + "\n" + "  boundingRect: "
				+ getRectangle().toString() + "  start edge: (" + stEdge()[0].x + ","
				+ stEdge()[0].y + ")-(" + stEdge()[1].x + "," + stEdge()[1].y + ")"
				+ "  end edge: (" + endEdge()[0].x + "," + endEdge()[0].y + ")-("
				+ endEdge()[1].x + "," + endEdge()[1].y + ")" + msg);
	}

	SepInten getSepIntensity() {
		return sepIntensity;
	}

	SepInten _new() {
		return n;
	}

	int getNumberOfProps() {
		return numberOfProps;
	}

	void setNumberOfProps(int i) {
		numberOfProps = i;
	}

	void setSupplemental(Recognized var_bf_9_) {
		supplemental = var_bf_9_;
	}

	void a(String string) {
		encoding = string;
	}

	void appendProp(String string) {
		numberOfProps++;
		Object val = holder.get(string);
		if (val == null)
			holder.put(string, new Integer(1));
		else
			holder.put(string, new Integer(((Integer) val).intValue() + 1));
	}

	boolean existProp(String name) {
		if (name.length() != x)
			return false;
		Iterator it = holder.keySet().iterator();
		int i = 0;
		while (it.hasNext()) {
			String key = (String) it.next();
			if (key.equals(name))
				return true;
			for (int c = 0; c < x; c++) {
				if (name.charAt(c) != '\uffff'
						&& key.charAt(c) != '\uffff'
						&& name.charAt(c) == key.charAt(c))
					i++;
			}
		}
		return i >= x / 2;
	}

	boolean a(Barcode var_aa) {
		Counters[] cs = new Counters[x];
		for (int i = 0; i < x; i++)
			cs[i] = new Counters();
		Iterator it = holder.keySet().iterator();
		while (it.hasNext()) {
			String string = (String) it.next();
			int i = ((Integer) holder.get(string)).intValue();
			for (int i_12_ = 0; i_12_ < x; i_12_++) {
				char c = string.charAt(i_12_);
				if (c != '\uffff') {
					for (int i_13_ = 0; i_13_ < i; i_13_++)
						cs[i_12_].incCounter(c);
				}
			}
		}
		boolean bool = false;
		boolean bool_14_ = true;
		StringBuffer stringbuffer = new StringBuffer(x);
		for (int i = 0; i < x; i++) {
			try {
				stringbuffer.append((char) cs[i].getMax());
			} catch (Exception exception) {
				bool_14_ = false;
			}
		}
		if (bool_14_ == true) {
			encoding = var_aa.a(new String(stringbuffer));
			if (encoding != null)
				bool = true;
		}
		if (!bool && var_aa.isCheck()) {
			it = holder.keySet().iterator();
			ArrayList arraylist = new ArrayList();
			while (it.hasNext()) {
				String string = (String) it.next();
				if (string.indexOf('\uffff') != -1 || var_aa.a(string) == null)
					arraylist.add(string);
			}
			it = arraylist.iterator();
			while (it.hasNext())
				holder.remove(it.next());
			if (!holder.isEmpty()) {
				int i = 0;
				it = holder.keySet().iterator();
				while (it.hasNext()) {
					String string = (String) it.next();
					int i_15_ = ((Integer) holder.get(string)).intValue();
					if (i_15_ > 10 && i_15_ > i) {
						i = i_15_;
						encoding = var_aa.a(string);
						bool = true;
					}
				}
			}
		}
		holder.clear();
		holder = null;
		return bool;
	}

	void a() {
		rect = sepIntensity.getRectangle().union(n.getRectangle());
		int[] is = new int[4];
		int[] is_16_ = new int[4];
		is[0] = (int) sepIntensity.getP1().getX();
		is_16_[0] = (int) sepIntensity.getP1().getY();
		is[1] = (int) n.getP1().getX();
		is_16_[1] = (int) n.getP1().getY();
		is[2] = (int) n.getP2().getX();
		is_16_[2] = (int) n.getP2().getY();
		is[3] = (int) sepIntensity.getP2().getX();
		is_16_[3] = (int) sepIntensity.getP2().getY();
		e = new Poligono(is, is_16_, 4);
	}

	boolean a(Recognized var_bf_17_) {
		if (sepIntensity == var_bf_17_.sepIntensity || n == var_bf_17_.n)
			return true;
		if (getBarcodeCode() == 9) {
			Rectangle rectangle = getRectangle();
			Rectangle rectangle_18_ = var_bf_17_.getRectangle();
			Point point = new Point(rectangle.x + rectangle.width / 2,
					rectangle.y + rectangle.height / 2);
			if (rectangle_18_.contains(point))
				return true;
		}
		if (sepIntensity._if(var_bf_17_.sepIntensity) && n._if(var_bf_17_.n))
			return true;
		if (sepIntensity._if(var_bf_17_.n) && n._if(var_bf_17_.sepIntensity))
			return true;
		return false;
	}
}
