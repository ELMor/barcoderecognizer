package isf.tools.barcode.ocr;
/* f - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import isf.tools.barcode.ocr.core.Settings;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Scrollbar;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class DialogSettings extends Dialog {
	private Frame parent;

	private TextField txtScanInt;

	private TextField txtBarsToRead;

	private Settings settings;

	public DialogSettings(Frame frame, Settings initSettings) {
		super(frame, "Settings", false);
		parent = frame;
		settings = initSettings;
		setResizable(false);
		setLayout(new BorderLayout());
		Panel panel = new Panel();
		panel.setLayout(new BorderLayout());
		Panel panel_0_ = new Panel();
		panel_0_.setLayout(new FlowLayout());
		panel_0_.add(new Label("scan interval"));
		panel_0_.add(txtScanInt = new TextField("" + settings.scanInterval, 3));
		txtScanInt.setEditable(false);
		panel_0_.add(new Label("-"));
		Scrollbar scrollbar = new Scrollbar(0, settings.scanInterval, 2, 1, 22);
		panel_0_.add(scrollbar);
		scrollbar.addAdjustmentListener(new AdjustmentListener() {
			public void adjustmentValueChanged(AdjustmentEvent adjustmentevent) {
				int i = adjustmentevent.getValue();
				txtScanInt.setText("" + i);
				settings.scanInterval = i;
			}
		});
		panel_0_.add(new Label("+"));
		panel.add(panel_0_, "North");
		TextArea textarea = (new TextArea(
				" 5 means scan every fifth pixel row or column of the image. Increasing this value may result in faster reading, but increases the probablility of not detecting narrow or poorly formed barcodes.",
				6, 50, 3));
		textarea.setEditable(false);
		panel.add(textarea, "Center");
		add(panel, "North");
		panel = new Panel();
		panel.setLayout(new BorderLayout());
		panel_0_ = new Panel();
		panel_0_.setLayout(new FlowLayout());
		panel_0_.add(new Label("bars to read"));
		panel_0_.add(txtBarsToRead = new TextField("" + settings.barsToRead, 3));
		txtBarsToRead.setEditable(false);
		panel_0_.add(new Label("-"));
		scrollbar = new Scrollbar(0, settings.barsToRead, 2, 1, 12);
		panel_0_.add(scrollbar);
		scrollbar.addAdjustmentListener(new AdjustmentListener() {
			public void adjustmentValueChanged(AdjustmentEvent adjustmentevent) {
				int i = adjustmentevent.getValue();
				txtBarsToRead.setText("" + i);
				settings.barsToRead = i;
			}
		});
		panel_0_.add(new Label("+"));
		panel.add(panel_0_, "North");
		textarea = (new TextArea(
				" If this number of barcodess is not found then the image is analyzed for color, contrast, and illumination variations. This can take some time. Only applies for images with more than two colors or 2-D barcodes.",
				5, 50, 3));
		textarea.setEditable(false);
		panel.add(textarea, "Center");
		add(panel, "Center");
		Panel panel_3_ = new Panel();
		panel_3_.setLayout(new GridLayout(1, 3));
		panel_3_.add(new Label(">>>", 1));
		Button button = new Button("OK");
		panel_3_.add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				dispose();
			}
		});
		panel_3_.add(new Label("<<<", 1));
		add(panel_3_, "South");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				dispose();
			}
		});
		pack();
		setLocation(0, 0);
	}

	public void dispose() {
		super.dispose();
		parent.toFront();
	}

	public Dimension getPreferredSize() {
		return parent.getPreferredSize();
	}
}
