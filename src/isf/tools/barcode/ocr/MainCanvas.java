package isf.tools.barcode.ocr;
/* c - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import isf.tools.barcode.ocr.core.Recognized;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.ScrollPane;
import java.util.StringTokenizer;


public class MainCanvas extends Canvas {
	private Image imagen = null;

	private Dimension _do;

	private Recognized[] recognized = null;

	MainCanvas() {
		_do = new Dimension(100, 100);
	}

	public Dimension getPreferredSize() {
		return _do;
	}

	public synchronized void setRecognized(Recognized[] r) {
		recognized = r;
	}

	public void paint(Graphics graphics) {
		FontMetrics fontmetrics = graphics.getFontMetrics();
		if (imagen != null)
			graphics.drawImage(imagen, 0, 0, this);
		MainCanvas var_c_0_ = this;
		synchronized (var_c_0_) {
			if (recognized != null) {
				graphics.setColor(Color.red);
				for (int i = 0; i < recognized.length; i++) {
					a(recognized[i], fontmetrics, graphics);
					if (recognized[i].isSupplementalAvailable())
						a(recognized[i].getSupplemental(), fontmetrics, graphics);
				}
			}
		}
	}

	private void a(Recognized var_bf, FontMetrics fontmetrics, Graphics graphics) {
		Point[] points = var_bf.stEdge();
		Point[] points_1_ = var_bf.endEdge();
		graphics.drawLine(points[0].x, points[0].y, points[1].x, points[1].y);
		graphics.drawLine(points_1_[0].x, points_1_[0].y, points_1_[1].x,
				points_1_[1].y);
		try {
			Rectangle rectangle = a(var_bf.getEncoding(), fontmetrics, graphics, var_bf
					.getRectangle(), null);
			a(var_bf.getEncoding(), fontmetrics, graphics, var_bf.getRectangle(), rectangle);
		} catch (ArrayIndexOutOfBoundsException arrayindexoutofboundsexception) {
			/* empty */
		}
	}

	private Rectangle a(String string, FontMetrics fontmetrics,
			Graphics graphics, Rectangle rectangle, Rectangle rectangle_2_) {
		float f = (float) (rectangle.width - 15);
		float f_3_;
		float f_4_;
		if (rectangle_2_ == null)
			f_3_ = f_4_ = 0.0F;
		else {
			graphics.clearRect(rectangle_2_.x - 1, rectangle_2_.y - 1,
					rectangle_2_.width + 2, rectangle_2_.height + 2);
			f_3_ = (float) rectangle_2_.x;
			f_4_ = (float) rectangle_2_.y;
		}
		StringTokenizer stringtokenizer = new StringTokenizer(string, "\r\n",
				true);
		String string_5_ = "";
		int i = 0;
		float f_6_ = 0.0F;
		while (stringtokenizer.hasMoreTokens()) {
			String string_7_ = string_5_;
			string_5_ = stringtokenizer.nextToken();
			if (string_5_.equals("\r")) {
				if (!string_7_.equals("\n"))
					i++;
				else
					string_5_ = "";
			} else if (string_5_.equals("\n")) {
				if (!string_7_.equals("\r"))
					i++;
				else
					string_5_ = "";
			} else {
				if (i > 1)
					f_4_ += (float) ((i - 1) * fontmetrics.getHeight());
				i = 0;
				f_4_ += (float) fontmetrics.getAscent();
				if (rectangle_2_ != null)
					graphics.drawString(string_5_, (int) f_3_, (int) f_4_);
				f_4_ += (float) (fontmetrics.getDescent() + fontmetrics
						.getLeading());
				if ((float) fontmetrics.stringWidth(string_5_) > f_6_)
					f_6_ = (float) fontmetrics.stringWidth(string_5_);
			}
		}
		int i_8_ = (int) f_6_;
		int i_9_ = (int) f_4_;
		int i_10_ = rectangle.x + (rectangle.width - i_8_) / 2;
		int i_11_ = rectangle.y + (rectangle.height - i_9_) / 2;
		return new Rectangle(i_10_, i_11_, i_8_, i_9_);
	}

	public void setImage(Image image) {
		if (imagen != null)
			imagen.flush();
		imagen = image;
		if (imagen != null)
			_do = new Dimension(imagen.getWidth(null), imagen.getHeight(null));
		((ScrollPane) getParent()).doLayout();
	}

	protected void finalize() throws Throwable {
		if (imagen != null)
			imagen.flush();
		super.finalize();
	}
}
