package isf.tools.barcode.ocr;
/* d - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import isf.tools.barcode.ocr.core.Settings;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class DialogDirections extends Dialog {
	private Frame parent;

	private Settings settings;

	public DialogDirections(Frame frame, Settings initSettings) {
		super(frame, "Directions", false);
		parent = frame;
		settings = initSettings;
		setResizable(false);
		setLayout(new BorderLayout());
		Panel panel = new Panel();
		panel.setLayout(new GridLayout(3, 3));
		panel.add(new Label(""));
		Checkbox checkbox = new Checkbox("up", settings.upScanDir);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.upScanDir = itemevent.getStateChange() == 1;
			}
		});
		panel.add(new Label(""));
		checkbox = new Checkbox("left", settings.leftScanDir);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.leftScanDir = itemevent.getStateChange() == 1;
			}
		});
		panel.add(new Label("Scan"));
		checkbox = new Checkbox("right", settings.rightScanDir);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.rightScanDir = itemevent.getStateChange() == 1;
			}
		});
		panel.add(new Label(""));
		checkbox = new Checkbox("down", settings.downScanDir);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.downScanDir = itemevent.getStateChange() == 1;
			}
		});
		panel.add(new Label(""));
		add(panel, "Center");
		panel = new Panel();
		panel.add(new Label("  "));
		add(panel, "West");
		Panel panel_4_ = new Panel();
		panel_4_.setLayout(new GridLayout(1, 3));
		panel_4_.add(new Label(">>>", 1));
		Button button = new Button("OK");
		panel_4_.add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				dispose();
			}
		});
		panel_4_.add(new Label("<<<", 1));
		add(panel_4_, "South");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				dispose();
			}
		});
		pack();
		setLocation(0, 0);
	}

	public void dispose() {
		super.dispose();
		parent.toFront();
	}

	public Dimension getPreferredSize() {
		return parent.getPreferredSize();
	}
}
