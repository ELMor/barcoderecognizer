package isf.tools.barcode.ocr;
/* a - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Dimension;
import java.awt.TextArea;

public class TxtArea extends TextArea {
	private Dimension a = new Dimension(100, 40);

	public TxtArea(String string, int i, int i_0_, int i_1_) {
		super(string, i, i_0_, i_1_);
	}

	public Dimension getPreferredSize() {
		return a;
	}

	public void append(String string) {
		if (getText().length() > 0)
			super.append("\n");
		super.append(string);
	}
}
