package isf.tools.barcode.ocr;
/* e - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import isf.tools.barcode.ocr.core.Settings;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class DialogBarcodes extends Dialog {
	private Frame parent;

	private Settings settings;

	public DialogBarcodes(Frame frame, Settings initSettings) {
		super(frame, "Barcodes", false);
		parent = frame;
		settings = initSettings;
		setResizable(false);
		setLayout(new BorderLayout());
		Panel panel = new Panel();
		panel.setLayout(new GridLayout(0, 3));
		panel.add(new Label("1-D :"));
		Checkbox checkbox = new Checkbox("Code 11", settings.code11);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.code11 = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("32", settings.code32);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.code32 = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("39", settings.code39);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.code39 = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("93", settings.code93);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.code93 = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("128", settings.code128);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.code128 = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("Codabar", settings.codabar);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.codabar = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("EAN13", settings.EAN13);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.EAN13 = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("EAN8", settings.EAN8);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.EAN8 = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("I2of5", settings.interleaved2of5);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.interleaved2of5 = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("Patch", settings.patch);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.patch = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("RSS-14", settings.rss14);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.rss14 = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("RSS Ltd.", settings.rssltd);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.rssltd = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("Telepen", settings.telepen);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.telepen = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("UPC A", settings.upcA);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.upcA = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("UPC E", settings.upcE);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.upcE = itemevent.getStateChange() == 1;
			}
		});
		Panel panel_15_ = new Panel();
		panel_15_.setLayout(new GridLayout(1, 2));
		checkbox = new Checkbox("+2", settings.plus2);
		panel_15_.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.plus2 = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("+5", settings.plus5);
		panel_15_.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.plus5 = itemevent.getStateChange() == 1;
			}
		});
		panel.add(panel_15_);
		checkbox = new Checkbox("check", settings.check);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.check = itemevent.getStateChange() == 1;
			}
		});
		panel.add(new Label("Stacked :"));
		checkbox = new Checkbox("PDF417", settings.pdf417);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.pdf417 = itemevent.getStateChange() == 1;
			}
		});
		panel.add(new Label(""));
		panel.add(new Label("2-D :"));
		checkbox = new Checkbox("DataMatrix", settings.dataMatrix);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.dataMatrix = itemevent.getStateChange() == 1;
			}
		});
		checkbox = new Checkbox("QR Code", settings.QRCode);
		panel.add(checkbox);
		checkbox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemevent) {
				settings.QRCode = itemevent.getStateChange() == 1;
			}
		});
		add(panel, "Center");
		panel = new Panel();
		panel.add(new Label(""));
		add(panel, "West");
		Panel panel_22_ = new Panel();
		panel_22_.setLayout(new GridLayout(1, 3));
		panel_22_.add(new Label(">>>", 1));
		Button button = new Button("OK");
		panel_22_.add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionevent) {
				dispose();
			}
		});
		panel_22_.add(new Label("<<<", 1));
		add(panel_22_, "South");
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				dispose();
			}
		});
		pack();
		setLocation(0, 0);
	}

	public void dispose() {
		super.dispose();
		parent.toFront();
	}

	public Dimension getPreferredSize() {
		return parent.getPreferredSize();
	}
}
