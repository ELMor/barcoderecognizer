// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   BarCode.java

package isf.util.codbarras;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.PrintStream;


public class BarCode extends Canvas
{

    public static final int BAR39 = 0;
    public static final int BAR39EXT = 1;
    public static final int INTERLEAVED25 = 2;
    public static final int CODE11 = 3;
    public static final int CODABAR = 4;
    public static final int MSI = 5;
    public static final int UPCA = 6;
    public static final int IND25 = 7;
    public static final int MAT25 = 8;
    public static final int CODE93 = 9;
    public static final int EAN13 = 10;
    public static final int EAN8 = 11;
    public static final int UPCE = 12;
    public static final int CODE128 = 13;
    public static final int CODE93EXT = 14;
    public static final int POSTNET = 15;
    public static final int EAN128 = 16;
    public static final int PDF417 = 30;
    public static final int DATAMATRIX = 31;
    public static final int MSI_CHECKSUM_10 = 0;
    public static final int MSI_CHECKSUM_11 = 1;
    public static final int MSI_CHECKSUM_11_10 = 2;
    public static final int MSI_CHECKSUM_10_10 = 3;
    public boolean textOnTop;
    public int barType;
    public String code;
    private String usedCodeSup;
    public boolean checkCharacter;
    public double postnetHeightTallBar;
    public double postnetHeightShortBar;
    public double leftMarginCM;
    public double topMarginCM;
    public String supplement;
    protected static final int d = 0;
    public boolean guardBars;
    public Color backColor;
    public String codeText;
    public int resolution;
    public double barHeightCM;
    public Font textFont;
    public Color fontColor;
    public Color barColor;
    public char UPCESytem;
    public char CODABARStartChar;
    public char CODABARStopChar;
    public boolean UPCEANSupplement2;
    public boolean UPCEANSupplement5;
    public char Code128Set;
    public int MSIChecksum;
    public double X;
    public double N;
    public double I;
    public double H;
    public double L;
    public int rotate;
    public double supSeparationCM;
    public double supHeight;
    public boolean processTilde;
    protected int currentX;
    protected int currentY;
    protected String set39[][] = {
        {
            "0", "nnnwwnwnn"
        }, {
            "1", "wnnwnnnnw"
        }, {
            "2", "nnwwnnnnw"
        }, {
            "3", "wnwwnnnnn"
        }, {
            "4", "nnnwwnnnw"
        }, {
            "5", "wnnwwnnnn"
        }, {
            "6", "nnwwwnnnn"
        }, {
            "7", "nnnwnnwnw"
        }, {
            "8", "wnnwnnwnn"
        }, {
            "9", "nnwwnnwnn"
        }, {
            "A", "wnnnnwnnw"
        }, {
            "B", "nnwnnwnnw"
        }, {
            "C", "wnwnnwnnn"
        }, {
            "D", "nnnnwwnnw"
        }, {
            "E", "wnnnwwnnn"
        }, {
            "F", "nnwnwwnnn"
        }, {
            "G", "nnnnnwwnw"
        }, {
            "H", "wnnnnwwnn"
        }, {
            "I", "nnwnnwwnn"
        }, {
            "J", "nnnnwwwnn"
        }, {
            "K", "wnnnnnnww"
        }, {
            "L", "nnwnnnnww"
        }, {
            "M", "wnwnnnnwn"
        }, {
            "N", "nnnnwnnww"
        }, {
            "O", "wnnnwnnwn"
        }, {
            "P", "nnwnwnnwn"
        }, {
            "Q", "nnnnnnwww"
        }, {
            "R", "wnnnnnwwn"
        }, {
            "S", "nnwnnnwwn"
        }, {
            "T", "nnnnwnwwn"
        }, {
            "U", "wwnnnnnnw"
        }, {
            "V", "nwwnnnnnw"
        }, {
            "W", "wwwnnnnnn"
        }, {
            "X", "nwnnwnnnw"
        }, {
            "Y", "wwnnwnnnn"
        }, {
            "Z", "nwwnwnnnn"
        }, {
            "-", "nwnnnnwnw"
        }, {
            ".", "wwnnnnwnn"
        }, {
            " ", "nwwnnnwnn"
        }, {
            "$", "nwnwnwnnn"
        }, {
            "/", "nwnwnnnwn"
        }, {
            "+", "nwnnnwnwn"
        }, {
            "%", "nnnwnwnwn"
        }, {
            "*", "nwnnwnwnn"
        }
    };
    protected String set25[][] = {
        {
            "0", "nnwwn"
        }, {
            "1", "wnnnw"
        }, {
            "2", "nwnnw"
        }, {
            "3", "wwnnn"
        }, {
            "4", "nnwnw"
        }, {
            "5", "wnwnn"
        }, {
            "6", "nwwnn"
        }, {
            "7", "nnnww"
        }, {
            "8", "wnnwn"
        }, {
            "9", "nwnwn"
        }
    };
    protected String setMSI[][] = {
        {
            "0", "nwnwnwnw"
        }, {
            "1", "nwnwnwwn"
        }, {
            "2", "nwnwwnnw"
        }, {
            "3", "nwnwwnwn"
        }, {
            "4", "nwwnnwnw"
        }, {
            "5", "nwwnnwwn"
        }, {
            "6", "nwwnwnnw"
        }, {
            "7", "nwwnwnwn"
        }, {
            "8", "wnnwnwnw"
        }, {
            "9", "wnnwnwwn"
        }
    };
    protected String set11[][] = {
        {
            "0", "nnnnw"
        }, {
            "1", "wnnnw"
        }, {
            "2", "nwnnw"
        }, {
            "3", "wwnnn"
        }, {
            "4", "nnwnw"
        }, {
            "5", "wnwnn"
        }, {
            "6", "nwwnn"
        }, {
            "7", "nnnww"
        }, {
            "8", "wnnwn"
        }, {
            "9", "wnnnn"
        }, {
            "-", "nnwnn"
        }
    };
    protected String setCODABAR[][] = {
        {
            "0", "nnnnnww"
        }, {
            "1", "nnnnwwn"
        }, {
            "2", "nnnwnnw"
        }, {
            "3", "wwnnnnn"
        }, {
            "4", "nnwnnwn"
        }, {
            "5", "wnnnnwn"
        }, {
            "6", "nwnnnnw"
        }, {
            "7", "nwnnwnn"
        }, {
            "8", "nwwnnnn"
        }, {
            "9", "wnnwnnn"
        }, {
            "-", "nnnwwnn"
        }, {
            "$", "nnwwnnn"
        }, {
            ":", "wnnnwnw"
        }, {
            "/", "wnwnnnw"
        }, {
            ".", "wnwnwnn"
        }, {
            "+", "nnwnwnw"
        }, {
            "A", "nnwwnwn"
        }, {
            "B", "nwnwnnw"
        }, {
            "C", "nnnwnww"
        }, {
            "D", "nnnwwwn"
        }
    };
    protected String set93[][] = {
        {
            "0", "131112"
        }, {
            "1", "111213"
        }, {
            "2", "111312"
        }, {
            "3", "111411"
        }, {
            "4", "121113"
        }, {
            "5", "121212"
        }, {
            "6", "121311"
        }, {
            "7", "111114"
        }, {
            "8", "131211"
        }, {
            "9", "141111"
        }, {
            "A", "211113"
        }, {
            "B", "211212"
        }, {
            "C", "211311"
        }, {
            "D", "221112"
        }, {
            "E", "221211"
        }, {
            "F", "231111"
        }, {
            "G", "112113"
        }, {
            "H", "112212"
        }, {
            "I", "112311"
        }, {
            "J", "122112"
        }, {
            "K", "132111"
        }, {
            "L", "111123"
        }, {
            "M", "111222"
        }, {
            "N", "111321"
        }, {
            "O", "121122"
        }, {
            "P", "131121"
        }, {
            "Q", "212112"
        }, {
            "R", "212211"
        }, {
            "S", "211122"
        }, {
            "T", "211221"
        }, {
            "U", "221121"
        }, {
            "V", "222111"
        }, {
            "W", "112122"
        }, {
            "X", "112221"
        }, {
            "Y", "112121"
        }, {
            "Z", "123111"
        }, {
            "-", "121131"
        }, {
            ".", "311112"
        }, {
            " ", "311211"
        }, {
            "$", "321111"
        }, {
            "/", "112131"
        }, {
            "+", "113121"
        }, {
            "%", "211131"
        }, {
            "_1", "121211"
        }, {
            "_2", "312111"
        }, {
            "_3", "311121"
        }, {
            "_4", "122211"
        }
    };
    protected String setUPCALeft[][] = {
        {
            "0", "3211"
        }, {
            "1", "2221"
        }, {
            "2", "2122"
        }, {
            "3", "1411"
        }, {
            "4", "1132"
        }, {
            "5", "1231"
        }, {
            "6", "1114"
        }, {
            "7", "1312"
        }, {
            "8", "1213"
        }, {
            "9", "3112"
        }
    };
    protected String setUPCARight[][] = {
        {
            "0", "3211"
        }, {
            "1", "2221"
        }, {
            "2", "2122"
        }, {
            "3", "1411"
        }, {
            "4", "1132"
        }, {
            "5", "1231"
        }, {
            "6", "1114"
        }, {
            "7", "1312"
        }, {
            "8", "1213"
        }, {
            "9", "3112"
        }
    };
    protected String setUPCEOdd[][] = {
        {
            "0", "3211"
        }, {
            "1", "2221"
        }, {
            "2", "2122"
        }, {
            "3", "1411"
        }, {
            "4", "1132"
        }, {
            "5", "1231"
        }, {
            "6", "1114"
        }, {
            "7", "1312"
        }, {
            "8", "1213"
        }, {
            "9", "3112"
        }
    };
    protected String setUPCEEven[][] = {
        {
            "0", "1123"
        }, {
            "1", "1222"
        }, {
            "2", "2212"
        }, {
            "3", "1141"
        }, {
            "4", "2311"
        }, {
            "5", "1321"
        }, {
            "6", "4111"
        }, {
            "7", "2131"
        }, {
            "8", "3121"
        }, {
            "9", "2113"
        }
    };
    protected String set39Ext[] = {
        "%U", "$A", "$B", "$C", "$D", "$E", "$F", "$G", "$H", "$I", 
        "$J", "$K", "$L", "$M", "$N", "$O", "$P", "$Q", "$R", "$S", 
        "$T", "$U", "$V", "$W", "$X", "$Y", "$Z", "%A", "%B", "%C", 
        "%D", "%E", " ", "/A", "/B", "/C", "/D", "/E", "/F", "/G", 
        "/H", "/I", "/J", "/K", "/L", "-", ".", "/O", "0", "1", 
        "2", "3", "4", "5", "6", "7", "8", "9", "/Z", "%F", 
        "%G", "%H", "%I", "%J", "%V", "A", "B", "C", "D", "E", 
        "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", 
        "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", 
        "Z", "%K", "%L", "%M", "%N", "%O", "%W", "+A", "+B", "+C", 
        "+D", "+E", "+F", "+G", "+H", "+I", "+J", "+K", "+L", "+M", 
        "+N", "+O", "+P", "+Q", "+R", "+S", "+T", "+U", "+V", "+W", 
        "+X", "+Y", "+Z", "%P", "%Q", "%R", "%S", "%T"
    };
    protected String set93Ext[] = {
        "_2U", "_1A", "_1B", "_1C", "_1D", "_1E", "_1F", "_1G", "_1H", "_1I", 
        "_1J", "_1K", "_1L", "_1M", "_1N", "_1O", "_1P", "_1Q", "_1R", "_1S", 
        "_1T", "_1U", "_1V", "_1W", "_1X", "_1Y", "_1Z", "_2A", "_2B", "_2C", 
        "_2D", "_2E", " ", "_3A", "_3B", "_3C", "_3D", "_3E", "_3F", "_3G", 
        "_3H", "_3I", "_3J", "_3K", "_3L", "-", ".", "_3O", "0", "1", 
        "2", "3", "4", "5", "6", "7", "8", "9", "_3Z", "_2F", 
        "_2G", "_2H", "_2I", "_2J", "_2V", "A", "B", "C", "D", "E", 
        "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", 
        "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", 
        "Z", "_2K", "_2L", "_2M", "_2N", "_2O", "_2W", "_4A", "_4B", "_4C", 
        "_4D", "_4E", "_4F", "_4G", "_4H", "_4I", "_4J", "_4K", "_4L", "_4M", 
        "_4N", "_4O", "_4P", "_4Q", "_4R", "_4S", "_4T", "_4U", "_4V", "_4W", 
        "_4X", "_4Y", "_4Z", "_2P", "_2Q", "_2R", "_2S", "_2T"
    };
    protected String UPCESystem0[] = {
        "EEEOOO", "EEOEOO", "EEOOEO", "EEOOOE", "EOEEOO", "EOOEEO", "EOOOEE", "EOEOEO", "EOEOOE", "EOOEOE"
    };
    protected String UPCESystem1[] = {
        "OOOEEE", "OOEOEE", "OOEEOE", "OOEEEO", "OEOOEE", "OEEOOE", "OEEEOO", "OEOEOE", "OEOEEO", "OEEOEO"
    };
    protected String setEANLeftA[][] = {
        {
            "0", "3211"
        }, {
            "1", "2221"
        }, {
            "2", "2122"
        }, {
            "3", "1411"
        }, {
            "4", "1132"
        }, {
            "5", "1231"
        }, {
            "6", "1114"
        }, {
            "7", "1312"
        }, {
            "8", "1213"
        }, {
            "9", "3112"
        }
    };
    protected String setEANLeftB[][] = {
        {
            "0", "1123"
        }, {
            "1", "1222"
        }, {
            "2", "2212"
        }, {
            "3", "1141"
        }, {
            "4", "2311"
        }, {
            "5", "1321"
        }, {
            "6", "4111"
        }, {
            "7", "2131"
        }, {
            "8", "3121"
        }, {
            "9", "2113"
        }
    };
    protected String setEANRight[][] = {
        {
            "0", "3211"
        }, {
            "1", "2221"
        }, {
            "2", "2122"
        }, {
            "3", "1411"
        }, {
            "4", "1132"
        }, {
            "5", "1231"
        }, {
            "6", "1114"
        }, {
            "7", "1312"
        }, {
            "8", "1213"
        }, {
            "9", "3112"
        }
    };
    protected String setEANCode[] = {
        "AAAAA", "ABABB", "ABBAB", "ABBBA", "BAABB", "BBAAB", "BBBAA", "BABAB", "BABBA", "BBABA"
    };
    protected String fiveSuplement[] = {
        "EEOOO", "EOEOO", "EOOEO", "EOOOE", "OEEOO", "OOEEO", "OOOEE", "OEOEO", "OEOOE", "OOEOE"
    };
    protected String set128[] = {
        "212222", "222122", "222221", "121223", "121322", "131222", "122213", "122312", "132212", "221213", 
        "221312", "231212", "112232", "122132", "122231", "113222", "123122", "123221", "223211", "221132", 
        "221231", "213212", "223112", "312131", "311222", "321122", "321221", "312212", "322112", "322211", 
        "212123", "212321", "232121", "111323", "131123", "131321", "112313", "132113", "132311", "211313", 
        "231113", "231311", "112133", "112331", "132131", "113123", "113321", "133121", "313121", "211331", 
        "231131", "213113", "213311", "213131", "311123", "311321", "331121", "312113", "312311", "332111", 
        "314111", "221411", "431111", "111224", "111422", "121124", "121421", "141122", "141221", "112214", 
        "112412", "122114", "122411", "142112", "142211", "241211", "221114", "413111", "241112", "134111", 
        "111242", "121142", "121241", "114212", "124112", "124211", "411212", "421112", "421211", "212141", 
        "214121", "412121", "111143", "111341", "131141", "114113", "114311", "411113", "411311", "113141", 
        "114131", "311141", "411131"
    };
    protected String set128A[] = {
        " ", "!", "\"", "#", "$", "%", "&", "'", "(", ")", 
        "*", "+", ",", "-", ".", "/", "0", "1", "2", "3", 
        "4", "5", "6", "7", "8", "9", ":", ";", "<", "=", 
        ">", "?", "@", "A", "B", "C", "D", "E", "F", "G", 
        "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", 
        "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[", 
        "\\", "]", "^", "_", "\0", "\001", "\002", "\003", "\004", "\005", 
        "\006", "\007", "\b", "\t", "\n", "\013", "\f", "\r", "\016", "\017", 
        "\020", "\021", "\022", "\023", "\024", "\025", "\026", "\027", "\030", "\031", 
        "\032", "\033", "\034", "\035", "\036", "\037", "_96", "_97", "_98", "_99", 
        "_100", "_101", "_102"
    };
    protected String set128B[] = {
        " ", "!", "\"", "#", "$", "%", "&", "'", "(", ")", 
        "*", "+", ",", "-", ".", "/", "0", "1", "2", "3", 
        "4", "5", "6", "7", "8", "9", ":", ";", "<", "=", 
        ">", "?", "@", "A", "B", "C", "D", "E", "F", "G", 
        "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", 
        "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[", 
        "\\", "]", "^", "_", "`", "a", "b", "c", "d", "e", 
        "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", 
        "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", 
        "z", "{", "_92", "}", "~", "_95", "_96", "_97", "_98", "_99", 
        "_100", "_101", "_102"
    };
    protected String set128C[] = {
        "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", 
        "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", 
        "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", 
        "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", 
        "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", 
        "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", 
        "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", 
        "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", 
        "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", 
        "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", 
        "_100", "_101", "_102"
    };
    protected String setPOSTNET[][] = {
        {
            "0", "11000"
        }, {
            "1", "00011"
        }, {
            "2", "00101"
        }, {
            "3", "00110"
        }, {
            "4", "01001"
        }, {
            "5", "01010"
        }, {
            "6", "01100"
        }, {
            "7", "10001"
        }, {
            "8", "10010"
        }, {
            "9", "10100"
        }
    };
    protected int leftMarginPixels;
    protected int topMarginPixels;
    protected int narrowBarPixels;
    protected int widthBarPixels;
    protected double narrowBarCM;
    protected double widthBarCM;
    protected int barHeightPixels;
    private int extraHeight;
    private int leftGuardBar;
    private int centerGuardBarStart;
    private int centerGuardBarEnd;
    private int rightGuardBar;
    private int endOfCode;
    private int startSuplement;
    private int endSuplement;
    private int suplementTopMargin;

    public BarCode()
    {
        textOnTop = false;
        barType = 0;
        code = "";
        usedCodeSup = "";
        checkCharacter = true;
        postnetHeightTallBar = 0.25D;
        postnetHeightShortBar = 0.125D;
        leftMarginCM = 0.5D;
        topMarginCM = 0.5D;
        supplement = "";
        guardBars = true;
        backColor = Color.white;
        codeText = "";
        resolution = 38;
        barHeightCM = 0.0D;
        textFont = new Font("Arial", 0, 11);
        fontColor = Color.black;
        barColor = Color.black;
        UPCESytem = '1';
        CODABARStartChar = 'A';
        CODABARStopChar = 'B';
        UPCEANSupplement2 = false;
        UPCEANSupplement5 = false;
        Code128Set = 'B';
        MSIChecksum = 0;
        X = 0.029999999999999999D;
        N = 2D;
        I = 1.0D;
        H = 0.45000000000000001D;
        L = 0.0D;
        rotate = 0;
        supSeparationCM = 0.5D;
        supHeight = 0.80000000000000004D;
        processTilde = false;
        currentX = 0;
        currentY = 0;
        leftMarginPixels = 0;
        topMarginPixels = 0;
        narrowBarPixels = 0;
        widthBarPixels = 0;
        narrowBarCM = 0.0D;
        widthBarCM = 0.0D;
        barHeightPixels = 0;
        extraHeight = 0;
        leftGuardBar = 0;
        centerGuardBarStart = 0;
        centerGuardBarEnd = 0;
        rightGuardBar = 0;
        endOfCode = 0;
    }

    public Dimension getPaintedArea()
    {
        int x = currentX;
        int y = currentY;
        if(rotate == 90 || rotate == 270)
        {
            y = currentX;
            x = currentY;
        }
        return new Dimension(x, y);
    }

    protected void addBar(Graphics g, int w, boolean black, int bartopmargin)
    {
        if(black)
        {
            g.setColor(barColor);
            g.fillRect(currentX, topMarginPixels + bartopmargin, w, (barHeightPixels + extraHeight) - bartopmargin);
        }
        currentX = currentX + w;
    }

    protected void paintPostNetChar(Graphics g, String pattern)
    {
        int shortBarPixels = (int)(postnetHeightShortBar * (double)resolution);
        int tallBarPixels = (int)(postnetHeightTallBar * (double)resolution);
        int diff = tallBarPixels - shortBarPixels;
        g.setColor(barColor);
        for(int i = 0; i < pattern.length(); i++)
        {
            char cBar = pattern.charAt(i);
            if(cBar == '0')
                g.fillRect(currentX, topMarginPixels + diff, narrowBarPixels, shortBarPixels + extraHeight);
            if(cBar == '1')
                g.fillRect(currentX, topMarginPixels, narrowBarPixels, tallBarPixels + extraHeight);
            currentX = currentX + narrowBarPixels;
            currentX = currentX + widthBarPixels;
        }

    }

    protected void paintPOSTNET(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        String codetmp = code;
        paintPostNetChar(g, "1");
        for(int i = code.length() - 1; i >= 0; i--)
        {
            String c = "" + code.charAt(i);
            sum += findChar(setPOSTNET, c);
        }

        int checksum = (int)mod(sum, 10D);
        if(checksum != 0)
            checksum = 10 - checksum;
        if(checkCharacter)
            codetmp = codetmp + (new Integer(checksum)).toString();
        for(int i = 0; i < codetmp.length(); i++)
        {
            String c = "" + codetmp.charAt(i);
            pos = findChar(setPOSTNET, c);
            paintPostNetChar(g, setPOSTNET[pos][1]);
        }

        paintPostNetChar(g, "1");
    }

    protected int findChar(String table[][], String c)
    {
        for(int i = 0; i < table.length; i++)
            if(c.compareTo(table[i][0]) == 0)
                return i;

        return -1;
    }

    protected void paintInterleaved25(Graphics g)
    {
        int pos = 0;
        int pos2 = 0;
        int sum = 0;
        String codetmp = code;
        paintChar(g, "bwbw", "nnnn");
        String l = "";
        if(mod(code.length(), 2D) == 0.0D && checkCharacter)
            codetmp = "0" + code;
        if(mod(code.length(), 2D) == 1.0D && !checkCharacter)
            codetmp = "0" + code;
        int sumeven = 0;
        int sumodd = 0;
        boolean even = true;
        for(int i = codetmp.length() - 1; i >= 0; i--)
        {
            String c = "" + codetmp.charAt(i);
            if(even)
                sumeven += findChar(set25, c);
            else
                sumodd += findChar(set25, c);
            even = !even;
        }

        int checksum = sumeven * 3 + sumodd;
        checksum = (int)mod(checksum, 10D);
        if(checksum != 0)
            checksum = 10 - checksum;
        if(checkCharacter)
            codetmp = codetmp + (new Integer(checksum)).toString();
        for(int i = 0; i < codetmp.length(); i += 2)
        {
            String c = "" + codetmp.charAt(i);
            String c2 = "" + codetmp.charAt(i + 1);
            pos = findChar(set25, c);
            pos2 = findChar(set25, c2);
            for(int j = 0; j < 5; j++)
            {
                paintChar(g, "b", "" + set25[pos][1].charAt(j));
                paintChar(g, "w", "" + set25[pos2][1].charAt(j));
            }

        }

        paintChar(g, "bwb", "wnn");
        codeText = codetmp;
    }

    protected void paintIND25(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        String codetmp = code;
        paintChar(g, "bwbwbw", "wwwwnw");
        int sumeven = 0;
        int sumodd = 0;
        boolean even = true;
        for(int i = codetmp.length() - 1; i >= 0; i--)
        {
            String c = "" + codetmp.charAt(i);
            if(even)
                sumeven += findChar(set25, c);
            else
                sumodd += findChar(set25, c);
            even = !even;
        }

        int checksum = sumeven * 3 + sumodd;
        checksum = (int)mod(checksum, 10D);
        if(checksum != 0)
            checksum = 10 - checksum;
        if(checkCharacter)
            codetmp = codetmp + (new Integer(checksum)).toString();
        for(int i = 0; i < codetmp.length(); i++)
        {
            String c = "" + codetmp.charAt(i);
            pos = findChar(set25, c);
            if(pos >= 0)
            {
                for(int j = 0; j < set25[pos][1].length(); j++)
                {
                    paintChar(g, "b", "" + set25[pos][1].charAt(j));
                    paintChar(g, "w", "w");
                }

            }
        }

        paintChar(g, "bwbwb", "wwnww");
    }

    protected String UPCEANCheck(String s)
    {
        boolean odd = true;
        int sumodd = 0;
        int sum = 0;
        for(int i = s.length() - 1; i >= 0; i--)
        {
            if(odd)
                sumodd += (new Integer("" + s.charAt(i))).intValue();
            else
                sum += (new Integer("" + s.charAt(i))).intValue();
            odd = !odd;
        }

        sum = sumodd * 3 + sum;
        int c = (int)mod(sum, 10D);
        if(c != 0)
            c = 10 - c;
        return "" + c;
    }

    protected void paintUPCA(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        if(code.length() == 11 && checkCharacter)
            code = code + UPCEANCheck(code);
        codeText = code;
        paintGuardChar(g, "bwb", "nnn", 0);
        leftGuardBar = currentX;
        for(int i = 0; i < code.length(); i++)
        {
            String c = "" + code.charAt(i);
            pos = -1;
            if(i <= 5)
            {
                pos = findChar(setUPCALeft, c);
                paintChar(g, "wbwb", setUPCALeft[pos][1]);
            } else
            {
                pos = findChar(setUPCARight, c);
                paintChar(g, "bwbw", setUPCARight[pos][1]);
            }
            if(i == 5)
            {
                centerGuardBarStart = currentX;
                paintGuardChar(g, "wbwbw", "nnnnn", 0);
                centerGuardBarEnd = currentX;
            }
        }

        rightGuardBar = currentX;
        paintGuardChar(g, "bwb", "nnn", 0);
        endOfCode = currentX;
        if(UPCEANSupplement2)
            paintSup2(g, code.substring(1, 3));
        else
        if(UPCEANSupplement5)
            paintSup5(g, code.substring(1, 6));
    }

    protected void paintEAN13(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        if(code.length() == 12 && checkCharacter)
        {
            code = code + UPCEANCheck(code);
            codeText = code;
        }
        if(code.length() < 13)
            return;
        paintGuardChar(g, "bwb", "nnn", 0);
        leftGuardBar = currentX;
        String sets = setEANCode[(new Integer("" + code.charAt(0))).intValue()];
        pos = findChar(setEANLeftA, "" + code.charAt(1));
        paintChar(g, "wbwb", setEANLeftA[pos][1]);
        for(int i = 2; i < 12; i++)
        {
            String c = "" + code.charAt(i);
            pos = -1;
            if(i <= 6)
            {
                String leftset[][] = setEANLeftA;
                if(sets.charAt(i - 2) == 'B')
                    leftset = setEANLeftB;
                pos = findChar(leftset, c);
                paintChar(g, "wbwb", leftset[pos][1]);
            } else
            {
                pos = findChar(setEANRight, c);
                paintChar(g, "bwbw", setEANRight[pos][1]);
            }
            if(i == 6)
            {
                centerGuardBarStart = currentX;
                paintGuardChar(g, "wbwbw", "nnnnn", 0);
                centerGuardBarEnd = currentX;
            }
        }

        pos = findChar(setEANRight, "" + code.charAt(12));
        paintChar(g, "bwbw", setEANRight[pos][1]);
        rightGuardBar = currentX;
        paintGuardChar(g, "bwb", "nnn", 0);
        endOfCode = currentX;
        if(UPCEANSupplement2)
            paintSup2(g, code.substring(2, 4));
        else
        if(UPCEANSupplement5)
            paintSup5(g, code.substring(2, 7));
    }

    private int findInArray(String s1[], String c)
    {
        for(int j = 0; j < s1.length; j++)
            if(s1[j].compareTo(c) == 0)
                return j;

        return -1;
    }

    private String convertCode128ControlChar(String c)
    {
        String FNC1 = "\312";
        String FNC2 = "\305";
        String FNC3 = "\304";
        String FNC4A = "\311";
        String FNC4B = "\310";
        if(c.compareTo(FNC1) == 0)
            c = "_102";
        if(c.compareTo(FNC4A) == 0)
            c = "_101";
        if(c.compareTo(FNC4B) == 0)
            c = "_100";
        if(c.compareTo(FNC2) == 0)
            c = "_97";
        if(c.compareTo(FNC3) == 0)
            c = "_96";
        return c;
    }

    private boolean isDigit(String c)
    {
        if(c.length() > 1)
            return false;
        else
            return c.charAt(0) >= '0' && c.charAt(0) <= '9';
    }

    private int getNextLowerCase(String s, int i)
    {
        for(int j = i; j < s.length(); j++)
            if(s.charAt(j) >= 'a' && s.charAt(j) <= 'z')
                return j;

        return 9999;
    }

    private int getNextControlChar(String s, int i)
    {
        for(int j = i; j < s.length(); j++)
            if(s.charAt(j) < ' ')
                return j;

        return 9999;
    }

    private boolean getEvenNumberDigits(String s, int i)
    {
        boolean even = true;
        for(int j = i; j < s.length(); j++)
            if(isDigit("" + s.charAt(j)))
                even = !even;
            else
                return even;

        return even;
    }

    private char calculateNextSet(String s, int i)
    {
        if(s.length() >= i + 4 && isDigit("" + s.charAt(i)) && isDigit("" + s.charAt(i + 1)) && isDigit("" + s.charAt(i + 2)) && isDigit("" + s.charAt(i + 3)))
            return 'C';
        return getNextControlChar(s, i) >= getNextLowerCase(s, i) ? 'B' : 'A';
    }

    protected void paintCode128(Graphics g)
    {
        int pos = 0;
        int check = 0;
        String set[] = set128A;
        if(Code128Set == 'B')
            set = set128B;
        if(Code128Set == 'C')
            set = set128C;
        int sum = 103;
        if(Code128Set == 'B')
        {
            set = set128B;
            sum = 104;
        }
        if(Code128Set == 'C')
        {
            set = set128C;
            sum = 105;
        }
        char currentSet = Code128Set;
        char previousSet = currentSet;
        if(Code128Set == 'B')
            paintChar(g, "bwbwbw", "211214");
        if(Code128Set == 'C')
            paintChar(g, "bwbwbw", "211232");
        if(Code128Set != 'B' && Code128Set != 'C')
            paintChar(g, "bwbwbw", "211412");
        int w = 1;
        for(int i = 0; i < code.length(); i++)
        {
            previousSet = currentSet;
            set = set128A;
            if(currentSet == 'B')
                set = set128B;
            if(currentSet == 'C')
                set = set128C;
            String c = "" + code.charAt(i);
            c = convertCode128ControlChar(c);
            if(currentSet == 'C')
            {
                boolean done = false;
                if(i < code.length() - 1 && isDigit("" + c) && isDigit("" + code.charAt(i + 1)))
                {
                    c = c + code.charAt(i + 1);
                    pos = findInArray(set128C, c);
                    if(pos >= 0)
                    {
                        paintChar(g, "bwbwbw", set128[pos]);
                        sum += pos * w;
                    }
                    done = true;
                    i++;
                }
                if(!done && !isDigit(c))
                {
                    pos = findInArray(set128C, c);
                    if(pos >= 0)
                    {
                        paintChar(g, "bwbwbw", set128[pos]);
                        sum += pos * w;
                        done = true;
                    }
                }
                if(!done && isDigit(c))
                {
                    currentSet = 'B';
                    if(i < code.length() - 1 && getNextControlChar(code, i + 1) < getNextLowerCase(code, i + 1))
                        currentSet = 'A';
                    pos = findInArray(set128C, "_100");
                    if(currentSet == 'A')
                        pos = findInArray(set128C, "_101");
                    sum += pos * w;
                    w++;
                    paintChar(g, "bwbwbw", set128[pos]);
                    pos = findInArray(set128B, c);
                    if(pos >= 0)
                    {
                        paintChar(g, "bwbwbw", set128[pos]);
                        sum += pos * w;
                    }
                    done = true;
                }
                if(!done && !isDigit(c))
                {
                    currentSet = calculateNextSet(code, i);
                    int pos2 = 0;
                    if(currentSet == 'A')
                        pos2 = findInArray(set, "_101");
                    if(currentSet == 'B')
                        pos2 = findInArray(set, "_100");
                    paintChar(g, "bwbwbw", set128[pos2]);
                    sum += pos2 * w;
                    i--;
                }
            } else
            if(calculateNextSet(code, i) == 'C')
            {
                i--;
                int pos2 = 0;
                pos2 = findInArray(set128B, "_99");
                if(currentSet == 'A')
                    pos2 = findInArray(set128A, "_99");
                paintChar(g, "bwbwbw", set128[pos2]);
                sum += pos2 * w;
                currentSet = 'C';
            } else
            {
                pos = findInArray(set, c);
                if(currentSet == 'A' && pos == -1 && findInArray(set128B, c) >= 0)
                {
                    int pos2 = 0;
                    if(getNextControlChar(code, i) < getNextLowerCase(code, i))
                    {
                        pos2 = findInArray(set, "_98");
                    } else
                    {
                        pos2 = findInArray(set, "_100");
                        currentSet = 'B';
                    }
                    paintChar(g, "bwbwbw", set128[pos2]);
                    sum += pos2 * w;
                    w++;
                    set = set128B;
                }
                if(currentSet == 'B' && pos == -1 && findInArray(set128A, c) >= 0)
                {
                    int pos2 = 0;
                    if(getNextControlChar(code, i) > getNextLowerCase(code, i))
                    {
                        pos2 = findInArray(set, "_98");
                    } else
                    {
                        pos2 = findInArray(set, "_101");
                        currentSet = 'A';
                    }
                    paintChar(g, "bwbwbw", set128[pos2]);
                    sum += pos2 * w;
                    w++;
                    set = set128A;
                }
                pos = findInArray(set, c);
                if(pos >= 0)
                {
                    paintChar(g, "bwbwbw", set128[pos]);
                    sum += pos * w;
                }
            }
            w++;
        }

        if(checkCharacter)
        {
            check = (int)mod(sum, 103D);
            paintChar(g, "bwbwbw", set128[check]);
        }
        paintChar(g, "bwbwbwb", "2331112");
    }

    protected void paintEAN128(Graphics g)
    {
        int pos = 0;
        int check = 0;
        if(barType == 16)
            Code128Set = 'C';
        String set[] = set128A;
        if(Code128Set == 'B')
            set = set128B;
        int sum = 103;
        if(Code128Set == 'B')
        {
            set = set128B;
            sum = 104;
        }
        if(Code128Set == 'C')
        {
            set = set128C;
            sum = 105;
        }
        char currentSet = Code128Set;
        char previousSet = currentSet;
        if(Code128Set == 'B')
            paintChar(g, "bwbwbw", "211214");
        if(Code128Set == 'C')
            paintChar(g, "bwbwbw", "211232");
        if(Code128Set != 'B' && Code128Set != 'C')
            paintChar(g, "bwbwbw", "211412");
        int w = 1;
        for(int i = 0; i < code.length(); i++)
        {
            previousSet = currentSet;
            set = set128A;
            if(currentSet == 'B')
                set = set128B;
            if(currentSet == 'C')
                set = set128C;
            String c = "" + code.charAt(i);
            c = convertCode128ControlChar(c);
            if(barType == 16 && i == 0)
            {
                pos = findInArray(set128C, "_102");
                paintChar(g, "bwbwbw", set128[pos]);
                sum += pos * w;
                w++;
            }
            if(currentSet == 'C')
            {
                boolean done = false;
                if(i < code.length() - 1 && isDigit("" + c) && isDigit("" + code.charAt(i + 1)))
                {
                    c = c + code.charAt(i + 1);
                    pos = findInArray(set128C, c);
                    if(pos >= 0)
                    {
                        paintChar(g, "bwbwbw", set128[pos]);
                        sum += pos * w;
                    }
                    done = true;
                    i++;
                }
                if(!done && !isDigit(c))
                {
                    pos = findInArray(set128C, c);
                    if(pos >= 0)
                    {
                        paintChar(g, "bwbwbw", set128[pos]);
                        sum += pos * w;
                        done = true;
                    }
                }
                if(!done && isDigit(c))
                {
                    currentSet = 'B';
                    if(i < code.length() - 1 && getNextControlChar(code, i + 1) < getNextLowerCase(code, i + 1))
                        currentSet = 'A';
                    pos = findInArray(set128C, "_100");
                    if(currentSet == 'A')
                        pos = findInArray(set128C, "_101");
                    sum += pos * w;
                    w++;
                    paintChar(g, "bwbwbw", set128[pos]);
                    pos = findInArray(set128B, c);
                    if(pos >= 0)
                    {
                        paintChar(g, "bwbwbw", set128[pos]);
                        sum += pos * w;
                    }
                    done = true;
                }
                if(!done && !isDigit(c))
                {
                    currentSet = calculateNextSet(code, i);
                    int pos2 = 0;
                    if(currentSet == 'A')
                        pos2 = findInArray(set, "_101");
                    if(currentSet == 'B')
                        pos2 = findInArray(set, "_100");
                    paintChar(g, "bwbwbw", set128[pos2]);
                    sum += pos2 * w;
                    i--;
                }
            } else
            if(calculateNextSet(code, i) == 'C')
            {
                i--;
                int pos2 = 0;
                pos2 = findInArray(set128B, "_99");
                if(currentSet == 'A')
                    pos2 = findInArray(set128A, "_99");
                paintChar(g, "bwbwbw", set128[pos2]);
                sum += pos2 * w;
                currentSet = 'C';
            } else
            {
                pos = findInArray(set, c);
                if(currentSet == 'A' && pos == -1 && findInArray(set128B, c) >= 0)
                {
                    int pos2 = 0;
                    if(getNextControlChar(code, i) < getNextLowerCase(code, i))
                    {
                        pos2 = findInArray(set, "_98");
                    } else
                    {
                        pos2 = findInArray(set, "_100");
                        currentSet = 'B';
                    }
                    paintChar(g, "bwbwbw", set128[pos2]);
                    sum += pos2 * w;
                    w++;
                    set = set128B;
                }
                if(currentSet == 'B' && pos == -1 && findInArray(set128A, c) >= 0)
                {
                    int pos2 = 0;
                    if(getNextControlChar(code, i) > getNextLowerCase(code, i))
                    {
                        pos2 = findInArray(set, "_98");
                    } else
                    {
                        pos2 = findInArray(set, "_101");
                        currentSet = 'A';
                    }
                    paintChar(g, "bwbwbw", set128[pos2]);
                    sum += pos2 * w;
                    w++;
                    set = set128A;
                }
                pos = findInArray(set, c);
                if(pos >= 0)
                {
                    paintChar(g, "bwbwbw", set128[pos]);
                    sum += pos * w;
                }
            }
            w++;
        }

        if(checkCharacter)
        {
            check = (int)mod(sum, 103D);
            paintChar(g, "bwbwbw", set128[check]);
        }
        paintChar(g, "bwbwbwb", "2331112");
    }

    protected void paintEAN8(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        if(code.length() == 7 && checkCharacter)
            code = code + UPCEANCheck(code);
        if(code.length() < 8)
            return;
        codeText = code;
        paintGuardChar(g, "bwb", "nnn", 0);
        leftGuardBar = currentX;
        for(int i = 0; i < 8; i++)
        {
            String c = "" + code.charAt(i);
            pos = -1;
            if(i <= 3)
            {
                pos = findChar(setEANLeftA, c);
                paintChar(g, "wbwb", setEANLeftA[pos][1]);
            } else
            {
                pos = findChar(setEANRight, c);
                paintChar(g, "bwbw", setEANRight[pos][1]);
            }
            if(i == 3)
            {
                centerGuardBarStart = currentX;
                paintGuardChar(g, "wbwbw", "nnnnn", 0);
                centerGuardBarEnd = currentX;
            }
        }

        rightGuardBar = currentX;
        paintGuardChar(g, "bwb", "nnn", 0);
        endOfCode = currentX;
        if(UPCEANSupplement2)
            paintSup2(g, code.substring(2, 4));
        else
        if(UPCEANSupplement5)
            paintSup5(g, code.substring(2, 7));
    }

    protected void paintUPCE(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        int checkchar = 0;
        String codetmp = "";
        if(code.length() == 11 && checkCharacter)
            code = code + UPCEANCheck(code);
        if(code.length() < 12)
            return;
        checkchar = (new Integer("" + code.charAt(11))).intValue();
        if(code.substring(3, 6).compareTo("000") == 0 || code.substring(3, 6).compareTo("100") == 0 || code.substring(3, 6).compareTo("200") == 0)
            codetmp = code.substring(1, 3) + code.substring(8, 11) + code.charAt(3);
        if(code.substring(3, 6).compareTo("300") == 0 || code.substring(3, 6).compareTo("400") == 0 || code.substring(3, 6).compareTo("500") == 0 || code.substring(3, 6).compareTo("600") == 0 || code.substring(3, 6).compareTo("700") == 0 || code.substring(3, 6).compareTo("800") == 0 || code.substring(3, 6).compareTo("900") == 0)
            codetmp = code.substring(1, 4) + code.substring(9, 11) + "3";
        if(code.substring(4, 6).compareTo("10") == 0 || code.substring(4, 6).compareTo("20") == 0 || code.substring(4, 6).compareTo("30") == 0 || code.substring(4, 6).compareTo("40") == 0 || code.substring(4, 6).compareTo("50") == 0 || code.substring(4, 6).compareTo("60") == 0 || code.substring(4, 6).compareTo("70") == 0 || code.substring(4, 6).compareTo("80") == 0 || code.substring(4, 6).compareTo("90") == 0)
            codetmp = code.substring(1, 5) + code.substring(10, 11) + "4";
        if(code.substring(5, 6).compareTo("0") != 0)
            codetmp = code.substring(1, 6) + code.substring(10, 11);
        codeText = codetmp;
        paintGuardChar(g, "bwb", "nnn", 0);
        leftGuardBar = currentX;
        String System = UPCESystem0[checkchar];
        if(UPCESytem == '1')
            System = UPCESystem1[checkchar];
        for(int i = 0; i < codetmp.length(); i++)
        {
            String c = "" + codetmp.charAt(i);
            pos = -1;
            String setLeft[][] = setUPCEOdd;
            if(System.charAt(i) == 'E')
                setLeft = setUPCEEven;
            pos = findChar(setLeft, c);
            String inverted = "";
            for(int j = 0; j < setLeft[pos][1].length(); j++)
                inverted = setLeft[pos][1].charAt(j) + inverted;

            paintChar(g, "wbwb", inverted);
        }

        rightGuardBar = currentX;
        paintGuardChar(g, "wbwbwb", "nnnnnn", 0);
        endOfCode = currentX;
        if(UPCEANSupplement2)
            paintSup2(g, codetmp.substring(0, 2));
        else
        if(UPCEANSupplement5)
            paintSup5(g, codetmp.substring(0, 5));
    }

    protected void paintSup2(Graphics g, String chars)
    {
        if(supplement.length() > 0)
            chars = supplement;
        suplementTopMargin = (int)((double)barHeightPixels * (1.0D - supHeight));
        if(usedCodeSup.length() == 0)
            usedCodeSup = chars;
        if(chars.length() != 2)
            return;
        currentX = (int)((double)currentX + (double)resolution * supSeparationCM);
        startSuplement = currentX;
        int i;
        try
        {
            i = Integer.valueOf(chars).intValue();
        }
        catch(Exception e)
        {
            i = 0;
        }
        String Parity = "OO";
        if(mod(i, 4D) == 1.0D)
            Parity = "OE";
        if(mod(i, 4D) == 2D)
            Parity = "EO";
        if(mod(i, 4D) == 3D)
            Parity = "EE";
        paintGuardChar(g, "bwb", "112", suplementTopMargin);
        String set[][] = setUPCEOdd;
        if(Parity.charAt(0) == 'E')
            set = setUPCEEven;
        int pos = findChar(set, "" + chars.charAt(0));
        paintGuardChar(g, "wbwb", set[pos][1], suplementTopMargin);
        paintGuardChar(g, "wb", "11", suplementTopMargin);
        set = setUPCEOdd;
        if(Parity.charAt(1) == 'E')
            set = setUPCEEven;
        pos = findChar(set, "" + chars.charAt(1));
        paintGuardChar(g, "wbwb", set[pos][1], suplementTopMargin);
        endSuplement = currentX;
    }

    protected void paintSup5(Graphics g, String chars)
    {
        if(supplement.length() > 0)
            chars = supplement;
        suplementTopMargin = (int)((double)barHeightPixels * (1.0D - supHeight));
        if(usedCodeSup.length() == 0)
            usedCodeSup = chars;
        if(chars.length() != 5)
            return;
        boolean odd = true;
        int sumodd = 0;
        int sum = 0;
        for(int i = chars.length() - 1; i >= 0; i--)
        {
            if(odd)
                sumodd += (new Integer("" + chars.charAt(i))).intValue();
            else
                sum += (new Integer("" + chars.charAt(i))).intValue();
            odd = !odd;
        }

        sum = sumodd * 3 + sum * 9;
        String sumstr = "" + sum;
        int c = (new Integer("" + sumstr.charAt(sumstr.length() - 1))).intValue();
        String Parity = fiveSuplement[c];
        currentX = (int)((double)currentX + (double)resolution * supSeparationCM);
        startSuplement = currentX;
        paintGuardChar(g, "bwb", "112", suplementTopMargin);
        String set[][] = null;
        for(int j = 0; j < 5; j++)
        {
            set = setUPCEOdd;
            if(Parity.charAt(j) == 'E')
                set = setUPCEEven;
            int pos = findChar(set, "" + chars.charAt(j));
            paintGuardChar(g, "wbwb", set[pos][1], suplementTopMargin);
            if(j < 4)
                paintGuardChar(g, "wb", "11", suplementTopMargin);
        }

        endSuplement = currentX;
    }

    protected void paintMAT25(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        String codetmp = code;
        paintChar(g, "bwbwbw", "wnnnnn");
        for(int i = 0; i < codetmp.length(); i++)
        {
            String c = "" + code.charAt(i);
            pos = findChar(set25, c);
            if(pos >= 0)
                paintChar(g, "bwbwbw", set25[pos][1] + "n");
        }

        paintChar(g, "bwbwbw", "wnnnnn");
    }

    protected void paintBAR39(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        paintChar(g, "bwbwbwbwb", set39[findChar(set39, "*")][1]);
        int inter = (int)(I * X * (double)resolution);
        if(inter == 0)
            inter = 1;
        currentX = currentX + inter;
        for(int i = 0; i < code.length(); i++)
        {
            String c = "" + code.charAt(i);
            pos = findChar(set39, c);
            if(pos > -1)
            {
                sum += pos;
                paintChar(g, "bwbwbwbwb", set39[pos][1]);
                currentX = currentX + inter;
            }
        }

        if(checkCharacter)
        {
            pos = (int)mod(sum, 43D);
            paintChar(g, "bwbwbwbwb", set39[pos][1]);
            currentX = currentX + inter;
            codeText = code + "" + set39[pos][0];
        }
        paintChar(g, "bwbwbwbwb", set39[findChar(set39, "*")][1]);
    }

    protected void paintCODE11(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        paintChar(g, "bwbwbw", "nnwwnn");
        int w = 1;
        sum = 0;
        for(int i = code.length() - 1; i >= 0; i--)
        {
            sum += findChar(set11, "" + code.charAt(i)) * w;
            if(++w == 11)
                w = 1;
        }

        int ch1 = (int)mod(sum, 11D);
        w = 2;
        sum = ch1;
        for(int i = code.length() - 1; i >= 0; i--)
        {
            sum += findChar(set11, "" + code.charAt(i)) * w;
            if(++w == 10)
                w = 1;
        }

        int ch2 = (int)mod(sum, 11D);
        for(int i = 0; i < code.length(); i++)
        {
            String c = "" + code.charAt(i);
            pos = findChar(set11, c);
            if(pos > -1)
                paintChar(g, "bwbwbw", set11[pos][1] + "n");
        }

        if(checkCharacter)
        {
            paintChar(g, "bwbwbw", set11[ch1][1] + "n");
            codeText = code + set11[ch1][0];
            if(code.length() > 10)
            {
                paintChar(g, "bwbwbw", set11[ch2][1] + "n");
                codeText = codeText + set11[ch2][0];
            }
        }
        paintChar(g, "bwbwb", "nnwwn");
    }

    protected void paintCODABAR(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        paintChar(g, "bwbwbwbw", setCODABAR[findChar(setCODABAR, "" + CODABARStartChar)][1] + "n");
        sum = findChar(setCODABAR, "" + CODABARStartChar) + findChar(setCODABAR, "" + CODABARStopChar);
        for(int i = code.length() - 1; i >= 0; i--)
            sum += findChar(setCODABAR, "" + code.charAt(i));

        int ch1 = (int)mod(sum, 16D);
        if(ch1 != 0)
            ch1 = 16 - ch1;
        for(int i = 0; i < code.length(); i++)
        {
            String c = "" + code.charAt(i);
            pos = findChar(setCODABAR, c);
            if(pos > -1)
                paintChar(g, "bwbwbwbw", setCODABAR[pos][1] + "n");
        }

        if(checkCharacter)
        {
            codeText = code + setCODABAR[ch1][0];
            paintChar(g, "bwbwbwbw", setCODABAR[ch1][1] + "n");
        }
        paintChar(g, "bwbwbwb", setCODABAR[findChar(setCODABAR, "" + CODABARStopChar)][1]);
    }

    protected int getMSIModule10(String code)
    {
        int sum = 0;
        sum = 0;
        String oddNumber = "";
        boolean odd = true;
        for(int i = code.length() - 1; i >= 0; i--)
        {
            if(!odd)
                sum += findChar(setMSI, "" + code.charAt(i));
            if(odd)
                oddNumber = findChar(setMSI, "" + code.charAt(i)) + oddNumber;
            odd = !odd;
        }

        oddNumber = "" + (new Long(oddNumber)).longValue() * 2L;
        for(int i = oddNumber.length() - 1; i >= 0; i--)
            sum += findChar(setMSI, "" + oddNumber.charAt(i));

        int ch1 = (int)mod(sum, 10D);
        if(ch1 != 0)
            ch1 = 10 - ch1;
        return ch1;
    }

    protected void paintMSI(Graphics g)
    {
        int pos = 0;
        int ch11 = 0;
        int sum = 0;
        int weight = 0;
        paintChar(g, "bw", "wn");
        weight = 2;
        for(int i = code.length() - 1; i >= 0; i--)
        {
            sum += findChar(setMSI, "" + code.charAt(i)) * weight;
            if(++weight == 8)
                weight = 2;
        }

        ch11 = (int)mod(sum, 11D);
        if(ch11 != 0)
            ch11 = 11 - ch11;
        int ch11_10 = getMSIModule10(code + setMSI[ch11][0]);
        int ch10 = getMSIModule10(code);
        int ch10_10 = getMSIModule10(code + setMSI[ch10][0]);
        for(int i = 0; i < code.length(); i++)
        {
            String c = "" + code.charAt(i);
            pos = findChar(setMSI, c);
            if(pos > -1)
                paintChar(g, "bwbwbwbw", setMSI[pos][1]);
        }

        if(checkCharacter)
        {
            if(MSIChecksum == 0)
            {
                paintChar(g, "bwbwbwbw", setMSI[ch10][1]);
                codeText = code + setMSI[ch10][0];
            }
            if(MSIChecksum == 1)
            {
                paintChar(g, "bwbwbwbw", setMSI[ch11][1]);
                codeText = code + setMSI[ch11][0];
            }
            if(MSIChecksum == 3)
            {
                paintChar(g, "bwbwbwbw", setMSI[ch10][1]);
                codeText = code + setMSI[ch10][0];
                paintChar(g, "bwbwbwbw", setMSI[ch10_10][1]);
                codeText = code + setMSI[ch10_10][0];
            }
            if(MSIChecksum == 2)
            {
                paintChar(g, "bwbwbwbw", setMSI[ch11][1]);
                codeText = code + setMSI[ch11][0];
                paintChar(g, "bwbwbwbw", setMSI[ch11_10][1]);
                codeText = code + setMSI[ch11_10][0];
            }
        }
        paintChar(g, "bwb", "nwn");
    }

    protected static double mod(double a, double b)
    {
        double f = a / b;
        double i = Math.round(f);
        if(i > f)
            i--;
        return a - b * i;
    }

    protected void paintBAR39Ext(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        paintChar(g, "bwbwbwbwb", set39[findChar(set39, "*")][1]);
        int inter = (int)(I * X * (double)resolution);
        if(inter == 0)
            inter = 1;
        currentX = currentX + inter;
        for(int i = 0; i < code.length(); i++)
        {
            byte b = (byte)code.charAt(i);
            if(b <= 128)
            {
                String encoded = set39Ext[b];
                for(int j = 0; j < encoded.length(); j++)
                {
                    String c = "" + encoded.charAt(j);
                    pos = findChar(set39, c);
                    if(pos > -1)
                    {
                        sum += pos;
                        paintChar(g, "bwbwbwbwb", set39[pos][1]);
                        currentX = currentX + inter;
                    }
                }

            }
        }

        if(checkCharacter)
        {
            pos = (int)mod(sum, 43D);
            paintChar(g, "bwbwbwbwb", set39[pos][1]);
            currentX = currentX + inter;
            codeText = code + "" + set39[pos][0];
        }
        paintChar(g, "bwbwbwbwb", set39[findChar(set39, "*")][1]);
    }

    protected void paintBAR93(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        int ch2 = 0;
        int ch1 = 0;
        paintChar(g, "bwbwbw", "111141");
        for(int i = 0; i < code.length(); i++)
        {
            String c = "" + code.charAt(i);
            pos = findChar(set93, c);
            if(pos > -1)
            {
                sum += pos;
                paintChar(g, "bwbwbw", set93[pos][1]);
            }
        }

        int w = 1;
        sum = 0;
        for(int i = code.length() - 1; i >= 0; i--)
        {
            sum += findChar(set93, "" + code.charAt(i)) * w;
            if(++w == 21)
                w = 1;
        }

        ch1 = (int)mod(sum, 47D);
        w = 2;
        sum = ch1;
        for(int i = code.length() - 1; i >= 0; i--)
        {
            sum += findChar(set93, "" + code.charAt(i)) * w;
            if(++w == 16)
                w = 1;
        }

        ch2 = (int)mod(sum, 47D);
        if(checkCharacter)
        {
            paintChar(g, "bwbwbw", set93[ch1][1]);
            paintChar(g, "bwbwbw", set93[ch2][1]);
            codeText = code + set93[ch1][0].charAt(0) + set93[ch2][0].charAt(0);
        }
        paintChar(g, "bwbwbwb", "1111411");
    }

    protected void paintBAR93Ext(Graphics g)
    {
        int pos = 0;
        int sum = 0;
        int ch2 = 0;
        int ch1 = 0;
        paintChar(g, "bwbwbw", "111141");
        for(int i = 0; i < code.length(); i++)
        {
            byte b = (byte)code.charAt(i);
            if(b <= 128)
            {
                String encoded = set93Ext[b];
                String c;
                if(encoded.length() == 3)
                {
                    c = "" + encoded.charAt(0) + encoded.charAt(1);
                    pos = findChar(set93, c);
                    paintChar(g, "bwbwbw", set93[pos][1]);
                    c = "" + encoded.charAt(2);
                } else
                {
                    c = "" + encoded.charAt(0);
                }
                pos = findChar(set93, c);
                sum += pos;
                paintChar(g, "bwbwbw", set93[pos][1]);
            }
        }

        int w = 1;
        sum = 0;
        for(int i = code.length() - 1; i >= 0; i--)
        {
            byte b = (byte)code.charAt(i);
            if(b <= 128)
            {
                String encoded = set93Ext[b];
                if(encoded.length() == 3)
                {
                    String c = "" + encoded.charAt(0) + encoded.charAt(1);
                    pos = findChar(set93, c);
                    sum += pos * (w + 1);
                    c = "" + encoded.charAt(2);
                    pos = findChar(set93, c);
                    sum += pos * w;
                    if(++w == 21)
                        w = 1;
                    if(++w == 21)
                        w = 1;
                } else
                {
                    String c = "" + encoded.charAt(0);
                    pos = findChar(set93, c);
                    sum += pos * w;
                    if(++w == 21)
                        w = 1;
                }
            }
        }

        ch1 = (int)mod(sum, 47D);
        w = 2;
        sum = ch1;
        for(int i = code.length() - 1; i >= 0; i--)
        {
            byte b = (byte)code.charAt(i);
            if(b <= 128)
            {
                String encoded = set93Ext[b];
                if(encoded.length() == 3)
                {
                    String c = "" + encoded.charAt(0) + encoded.charAt(1);
                    pos = findChar(set93, c);
                    sum += pos * (w + 1);
                    c = "" + encoded.charAt(2);
                    pos = findChar(set93, c);
                    sum += pos * w;
                    if(++w == 16)
                        w = 1;
                    if(++w == 16)
                        w = 1;
                } else
                {
                    String c = "" + encoded.charAt(0);
                    pos = findChar(set93, c);
                    sum += pos * w;
                    if(++w == 16)
                        w = 1;
                }
            }
        }

        ch2 = (int)mod(sum, 47D);
        if(checkCharacter)
        {
            paintChar(g, "bwbwbw", set93[ch1][1]);
            paintChar(g, "bwbwbw", set93[ch2][1]);
            codeText = code + set93[ch1][0].charAt(0) + set93[ch2][0].charAt(0);
        }
        paintChar(g, "bwbwbwb", "1111411");
    }

    protected void paintChar(Graphics g, String patternColor, String patternBars)
    {
        paintChar2(g, patternColor, patternBars, 0);
    }

    protected void paintChar2(Graphics g, String patternColor, String patternBars, int bartopmargin)
    {
        for(int i = 0; i < patternColor.length(); i++)
        {
            char cColor = patternColor.charAt(i);
            char cBar = patternBars.charAt(i);
            if(cBar == 'n')
                addBar(g, narrowBarPixels, cColor == 'b', bartopmargin);
            if(cBar == 'w')
                addBar(g, widthBarPixels, cColor == 'b', bartopmargin);
            if(cBar == '1')
                addBar(g, narrowBarPixels, cColor == 'b', bartopmargin);
            if(cBar == '2')
                addBar(g, (int)(narrowBarCM * (double)resolution * 2D), cColor == 'b', bartopmargin);
            if(cBar == '3')
                addBar(g, (int)(narrowBarCM * (double)resolution * 3D), cColor == 'b', bartopmargin);
            if(cBar == '4')
                addBar(g, (int)(narrowBarCM * (double)resolution * 4D), cColor == 'b', bartopmargin);
        }

    }

    protected void paintGuardChar(Graphics g, String patternColor, String patternBars, int bartopMargin)
    {
        if(textFont != null && guardBars)
        {
            g.setFont(textFont);
            extraHeight = g.getFontMetrics().getHeight();
        }
        paintChar2(g, patternColor, patternBars, bartopMargin);
        extraHeight = 0;
    }

    protected void calculateSizes()
    {
        int C = code.length();
        narrowBarCM = X;
        widthBarCM = X * N;
        if(barType == 2)
        {
            if(mod(C, 2D) == 0.0D && checkCharacter)
                C++;
            if(mod(C, 2D) == 1.0D && !checkCharacter)
                C++;
            if(checkCharacter)
                C++;
            L = (double)(C / 2) * (3D + 2D * N) * X + 7D * X;
        }
        if(barType == 6)
        {
            if(checkCharacter)
                C++;
            L = (double)(C * 7) * X + 11D * X;
        }
        if(barType == 10)
            L = (double)(C * 7) * X + 11D * X;
        if(barType == 11)
            L = (double)(C * 7) * X + 11D * X;
        if(barType == 13 || barType == 16)
        {
            if(checkCharacter)
                C++;
            if(Code128Set == 'C')
                L = (double)(11 * C + 35) * X;
            else
                L = (5.5D * (double)C + 35D) * X;
        }
        if(barType == 12)
            L = 42D * X + 9D * X;
        if(barType == 7)
        {
            if(checkCharacter)
                C++;
            L = (double)C * (3D + 2D * N) * X + 7D * X;
        }
        if(barType == 8)
        {
            if(checkCharacter)
                C++;
            L = (double)C * (3D + 2D * N) * X + 7D * X;
        }
        if(barType == 5)
        {
            if(checkCharacter)
                C++;
            L = (double)C * (4D + 4D * N) * X + (1.0D + N) * X + (2D + N) * X;
        }
        if(barType == 4)
        {
            if(checkCharacter)
                C++;
            L = (double)(C + 2) * (4D + 3D * N) * X;
        }
        if(barType == 3)
        {
            if(checkCharacter || code.length() > 10)
                C++;
            L = (double)(C + 2 + 1) * (3D + 2D * N) * X;
        }
        if(barType == 15)
        {
            if(checkCharacter)
                C++;
            L = X * 10D;
        }
        if(barType == 0)
        {
            if(checkCharacter)
                C++;
            L = (double)(C + 2) * (3D * N + 6D) * X + (double)(C + 1) * I * X;
        }
        if(barType == 1)
        {
            C = 0;
            if(checkCharacter)
                C++;
            for(int j = 0; j < code.length(); j++)
            {
                byte b = (byte)code.charAt(j);
                if(b <= 128)
                {
                    String encoded = set39Ext[b];
                    C += encoded.length();
                }
            }

            L = (double)(C + 2) * (3D * N + 6D) * X + (double)(C + 1) * I * X;
        }
        if(barType == 9 || barType == 14)
        {
            C = 0;
            if(checkCharacter)
                C++;
            for(int j = 0; j < code.length(); j++)
            {
                byte b = (byte)code.charAt(j);
                if(b <= 128)
                {
                    String encoded = set39Ext[b];
                    if(encoded.length() == 1)
                        C++;
                    else
                        C += 2;
                }
            }

            L = (double)(C + 2) * (9D * X) + (double)(C + 1) * I * X;
        }
        if(barHeightCM == 0.0D)
        {
            barHeightCM = L * H;
            if(barHeightCM < 0.625D)
                barHeightCM = 0.625D;
        }
        if(barHeightCM != 0.0D)
            barHeightPixels = (int)(barHeightCM * (double)resolution);
        if(narrowBarCM != 0.0D)
            narrowBarPixels = (int)(narrowBarCM * (double)resolution);
        if(widthBarCM != 0.0D)
            widthBarPixels = (int)(widthBarCM * (double)resolution);
        if(narrowBarPixels <= 0)
            narrowBarPixels = 1;
        if(widthBarPixels <= 1)
            widthBarPixels = 2;
    }

    public void paint(Graphics g2)
    {
        Graphics g = g2;
        Image im = null;
        if(rotate != 0)
        {
            String v = System.getProperty("java.version");
            if(v.indexOf("1.1") != 0)
            {
                RImageCreator imc = new RImageCreator();
                im = imc.getImage(getSize().width, getSize().height);
                g = imc.getGraphics();
            } else
            {
                if(getSize().width > getSize().height)
                    im = createImage(getSize().width, getSize().width);
                else
                    im = createImage(getSize().height, getSize().height);
                g = im.getGraphics();
            }
        }
        g2.setColor(backColor);
        g2.fillRect(0, 0, getSize().width, getSize().height);
        paintBasis(g);
        if(rotate != 0)
        {
            int maxw = currentX + leftMarginPixels;
            int maxh = currentY + topMarginPixels;
            Image imRotated = rotate(im, rotate, maxw, maxh);
            if(imRotated == null)
                g2.drawImage(im, 0, 0, null);
            else
                g2.drawImage(imRotated, 0, 0, null);
        }
    }

    protected void paintBasis(Graphics g)
    {
        codeText = "";
        calculateSizes();
        if(topMarginCM < 1.0D)
            topMarginCM = 1.0D;
        topMarginPixels = (int)(topMarginCM * (double)resolution);
        leftMarginPixels = (int)(leftMarginCM * (double)resolution);
        currentX = leftMarginPixels;
        g.setColor(backColor);
        int w = getSize().width;
        int h = getSize().height;
        int m = w;
        if(h > m)
            m = h;
        g.fillRect(0, 0, m, m);
        endOfCode = 0;
        String tmpCode = code;
        if(processTilde)
            code = applyTilde(code);
        if(barType == 3)
            paintCODE11(g);
        if(barType == 5)
            paintMSI(g);
        if(barType == 4)
            paintCODABAR(g);
        if(barType == 0)
            paintBAR39(g);
        if(barType == 1)
            paintBAR39Ext(g);
        if(barType == 2)
            paintInterleaved25(g);
        if(barType == 9)
            paintBAR93(g);
        if(barType == 11)
            paintEAN8(g);
        if(barType == 10)
            paintEAN13(g);
        if(barType == 6)
            paintUPCA(g);
        if(barType == 12)
            paintUPCE(g);
        if(barType == 13)
            paintCode128(g);
        if(barType == 16)
            paintEAN128(g);
        if(barType == 14)
            paintBAR93Ext(g);
        if(barType == 7)
            paintIND25(g);
        if(barType == 8)
            paintMAT25(g);
        if(barType == 15)
            paintPOSTNET(g);
        code = tmpCode;
        if(endOfCode == 0)
            endOfCode = currentX;
        if(codeText.length() == 0)
            codeText = code;
        g.setColor(fontColor);
        g.setFont(new Font("Arial", 0, 11));
        int TextH = g.getFontMetrics().getHeight();
        currentY = barHeightPixels + topMarginPixels;
        if(textFont != null)
        {
            g.setColor(fontColor);
            g.setFont(textFont);
            TextH = g.getFontMetrics().getHeight();
            int charW = g.getFontMetrics().stringWidth("X");
            if((UPCEANSupplement2 || UPCEANSupplement5) && (barType == 11 || barType == 6 || barType == 12 || barType == 10))
            {
                int groupCenterX = (endSuplement - startSuplement - g.getFontMetrics().stringWidth(usedCodeSup)) / 2;
                if(groupCenterX < 0)
                    groupCenterX = 0;
                g.drawString(usedCodeSup, startSuplement + groupCenterX, (topMarginPixels + suplementTopMargin) - 2);
            }
            int toCenterX;
            if(barType == 15)
            {
                toCenterX = (endOfCode - leftMarginPixels - g.getFontMetrics().stringWidth(codeText)) / 2;
                if(toCenterX < 0)
                    toCenterX = 0;
                g.drawString(codeText, leftMarginPixels + toCenterX, (int)(postnetHeightTallBar * (double)resolution + (double)TextH + 1.0D + (double)topMarginPixels));
                currentY = barHeightPixels + TextH + 1 + topMarginPixels;
                return;
            }
            if(barType == 10 && guardBars && codeText.length() >= 13)
            {
                int groupCenterX = 0;
                g.drawString(codeText.substring(0, 1), leftMarginPixels - charW, barHeightPixels + TextH + 1 + topMarginPixels);
                groupCenterX = (centerGuardBarStart - leftGuardBar - g.getFontMetrics().stringWidth(codeText.substring(1, 7))) / 2;
                if(groupCenterX < 0)
                    groupCenterX = 0;
                g.drawString(codeText.substring(1, 7), leftGuardBar + groupCenterX, barHeightPixels + TextH + 1 + topMarginPixels);
                groupCenterX = (rightGuardBar - centerGuardBarEnd - g.getFontMetrics().stringWidth(codeText.substring(7, 13))) / 2;
                if(groupCenterX < 0)
                    groupCenterX = 0;
                g.drawString(codeText.substring(7, 13), centerGuardBarEnd + groupCenterX, barHeightPixels + TextH + 1 + topMarginPixels);
                currentY = barHeightPixels + TextH + 1 + topMarginPixels;
                return;
            }
            if(barType == 6 && guardBars && codeText.length() >= 12)
            {
                int groupCenterX = 0;
                g.drawString(codeText.substring(0, 1), leftMarginPixels - charW, barHeightPixels + TextH + 1 + topMarginPixels);
                groupCenterX = (centerGuardBarStart - leftGuardBar - g.getFontMetrics().stringWidth(codeText.substring(1, 6))) / 2;
                if(groupCenterX < 0)
                    groupCenterX = 0;
                g.drawString(codeText.substring(1, 6), leftGuardBar + groupCenterX, barHeightPixels + TextH + 1 + topMarginPixels);
                groupCenterX = (rightGuardBar - centerGuardBarEnd - g.getFontMetrics().stringWidth(codeText.substring(6, 11))) / 2;
                if(groupCenterX < 0)
                    groupCenterX = 0;
                g.drawString(codeText.substring(6, 11), centerGuardBarEnd + groupCenterX, barHeightPixels + TextH + 1 + topMarginPixels);
                g.drawString(codeText.substring(11, 12), endOfCode + 3, barHeightPixels + TextH + 1 + topMarginPixels);
                currentY = barHeightPixels + TextH + 1 + topMarginPixels;
                return;
            }
            if(barType == 11 && guardBars && codeText.length() >= 8)
            {
                int groupCenterX = 0;
                groupCenterX = (centerGuardBarStart - leftGuardBar - g.getFontMetrics().stringWidth(codeText.substring(0, 4))) / 2;
                if(groupCenterX < 0)
                    groupCenterX = 0;
                g.drawString(codeText.substring(0, 4), leftGuardBar + groupCenterX, barHeightPixels + TextH + 1 + topMarginPixels);
                groupCenterX = (rightGuardBar - centerGuardBarEnd - g.getFontMetrics().stringWidth(codeText.substring(4, 8))) / 2;
                if(groupCenterX < 0)
                    groupCenterX = 0;
                g.drawString(codeText.substring(4, 8), centerGuardBarEnd + groupCenterX, barHeightPixels + TextH + 1 + topMarginPixels);
                currentY = barHeightPixels + TextH + 1 + topMarginPixels;
                return;
            }
            if(barType == 12 && guardBars)
            {
                int groupCenterX = 0;
                g.drawString(code.substring(0, 1), leftMarginPixels - charW, barHeightPixels + TextH + 1 + topMarginPixels);
                groupCenterX = (rightGuardBar - leftGuardBar - g.getFontMetrics().stringWidth(codeText)) / 2;
                if(groupCenterX < 0)
                    groupCenterX = 0;
                g.drawString(codeText, leftGuardBar + groupCenterX, barHeightPixels + TextH + 1 + topMarginPixels);
                currentY = barHeightPixels + TextH + 1 + topMarginPixels;
                return;
            }
            int groupCenterX = (endOfCode - leftMarginPixels - g.getFontMetrics().stringWidth(codeText)) / 2;
            if(groupCenterX < 0)
                groupCenterX = 0;
            if(!textOnTop)
                g.drawString(codeText, leftMarginPixels + groupCenterX, barHeightPixels + TextH + 1 + topMarginPixels);
            else
                g.drawString(codeText, leftMarginPixels + groupCenterX, TextH + 4);
            currentY = barHeightPixels + TextH + 1 + topMarginPixels;
        }
    }

    protected Image rotate(Image im, int angle, int maxw, int maxh)
    {
        int w = im.getWidth(null);
        int h = im.getHeight(null);
        if(maxw > w)
            maxw = w;
        if(maxh > h)
            maxh = h;
        int pixels[] = new int[w * h];
        int pixels2[] = new int[maxw * maxh];
        PixelGrabber pg = new PixelGrabber(im, 0, 0, w, h, pixels, 0, w);
        try
        {
            pg.grabPixels();
        }
        catch(InterruptedException e)
        {
            System.err.println("interrupted waiting for pixels!");
            return null;
        }
        if((pg.getStatus() & 0x80) != 0)
        {
            System.err.println("image fetch aborted or errored");
            return null;
        }
        if(angle == 90)
        {
            for(int i = 0; i < maxw; i++)
            {
                for(int j = 0; j < maxh; j++)
                    pixels2[maxh * (maxw - (i + 1)) + j] = pixels[j * w + i];

            }

            return Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(maxh, maxw, pixels2, 0, maxh));
        }
        if(angle == 180)
        {
            for(int i = 0; i < maxw; i++)
            {
                for(int j = 0; j < maxh; j++)
                    pixels2[(maxh - (j + 1)) * maxw + (maxw - (i + 1))] = pixels[j * w + i];

            }

            return Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(maxw, maxh, pixels2, 0, maxw));
        }
        if(angle == 270)
        {
            for(int i = 0; i < maxw; i++)
            {
                for(int j = 0; j < maxh; j++)
                    pixels2[maxh * i + (maxh - (j + 1))] = pixels[j * w + i];

            }

            return Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(maxh, maxw, pixels2, 0, maxh));
        } else
        {
            return null;
        }
    }

    private String applyTilde(String code)
    {
        int c = 0;
        int longi = code.length();
        String result = "";
        boolean done = false;
        for(int i = 0; i < longi; i++)
        {
            c = code.charAt(i);
            if(c == 126)
            {
                if(i < longi - 1)
                {
                    char nextc = code.charAt(i + 1);
                    if(nextc == '~')
                    {
                        result = result + '~';
                        i++;
                    } else
                    if(i < longi - 3)
                    {
                        String ascString = code.substring(i + 1, i + 4);
                        int asc = 0;
                        try
                        {
                            asc = (new Integer(ascString)).intValue();
                        }
                        catch(Exception e)
                        {
                            asc = 0;
                        }
                        if(asc > 255)
                            asc = 255;
                        result = result + (char)asc;
                        i += 3;
                    }
                }
            } else
            {
                result = result + (char)c;
            }
        }

        return result;
    }
}
