// Decompiled by Jad v1.5.8f. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: fieldsfirst 
// Source File Name:   RDataMatrixBean.java

package isf.util.codbarras.matrix;

import java.awt.Color;


public class RDataMatrixBean extends RDataMatrix
{

    public RDataMatrixBean()
    {
        barType = 31;
        reBuild = true;
    }

    public int getDotPixels()
    {
        return dotPixels;
    }

    public void setDotPixels(int i)
    {
        dotPixels = i;
    }

    public int getMargin()
    {
        return margin;
    }

    public void setMargin(int i)
    {
        margin = i;
    }

    public boolean getProcessTilde()
    {
        return processTilde;
    }

    public void setProcessTilde(boolean p)
    {
        processTilde = p;
    }

    public boolean getRebuild()
    {
        return reBuild;
    }

    public void setRebuild(boolean p)
    {
        reBuild = p;
    }

    public String getencoding()
    {
        if(encoding == RDataMatrix.E_ASCII)
            return "ASCII";
        if(encoding == RDataMatrix.E_C40)
            return "C40";
        if(encoding == RDataMatrix.E_TEXT)
            return "TEXT";
        if(encoding == RDataMatrix.E_BASE256)
            return "BASE256";
        if(encoding == RDataMatrix.E_AUTO)
            return "AUTO";
        else
            return "NONE";
    }

    public void setencoding(String e)
    {
        e = e.toUpperCase();
        if(e.compareTo("ASCII") == 0)
            encoding = RDataMatrix.E_ASCII;
        if(e.compareTo("C40") == 0)
            encoding = RDataMatrix.E_C40;
        if(e.compareTo("TEXT") == 0)
            encoding = RDataMatrix.E_TEXT;
        if(e.compareTo("BASE256") == 0)
            encoding = RDataMatrix.E_BASE256;
        if(e.compareTo("AUTO") == 0)
            encoding = RDataMatrix.E_AUTO;
        if(e.compareTo("NONE") == 0)
            encoding = RDataMatrix.E_NONE;
    }

    public String getpreferredFormat()
    {
        if(preferredFormat == -1)
            return "NONE";
        if(preferredFormat == 0)
            return "C10X10";
        if(preferredFormat == 1)
            return "C12X12";
        if(preferredFormat == 2)
            return "C14X14";
        if(preferredFormat == 3)
            return "C16X16";
        if(preferredFormat == 4)
            return "C18X18";
        if(preferredFormat == 5)
            return "C20X20";
        if(preferredFormat == 6)
            return "C22X22";
        if(preferredFormat == 7)
            return "C24X24";
        if(preferredFormat == 8)
            return "C26X26";
        if(preferredFormat == 9)
            return "C32X32";
        if(preferredFormat == 10)
            return "C36X36";
        if(preferredFormat == 11)
            return "C40X40";
        if(preferredFormat == 12)
            return "C44X44";
        if(preferredFormat == 13)
            return "C48X48";
        if(preferredFormat == 14)
            return "C52X52";
        if(preferredFormat == 15)
            return "C64X64";
        if(preferredFormat == 16)
            return "C72X72";
        if(preferredFormat == 17)
            return "C80X80";
        if(preferredFormat == 19)
            return "C96X96";
        if(preferredFormat == 20)
            return "C104X104";
        if(preferredFormat == 18)
            return "C88X88";
        if(preferredFormat == 21)
            return "C120X120";
        if(preferredFormat == 22)
            return "C132X132";
        if(preferredFormat == 23)
            return "C144X144";
        if(preferredFormat == 24)
            return "C8X18";
        if(preferredFormat == 25)
            return "C8X32";
        if(preferredFormat == 26)
            return "C12X26";
        if(preferredFormat == 27)
            return "C12X36";
        if(preferredFormat == 28)
            return "C16X36";
        if(preferredFormat == 29)
            return "C16X48";
        else
            return "NONE";
    }

    public void setpreferredFormat(String e)
    {
        e = e.toUpperCase();
        preferredFormat = -1;
        if(e.compareTo("C10X10") == 0)
            preferredFormat = 0;
        if(e.compareTo("C12X12") == 0)
            preferredFormat = 1;
        if(e.compareTo("C14X14") == 0)
            preferredFormat = 2;
        if(e.compareTo("C16X16") == 0)
            preferredFormat = 3;
        if(e.compareTo("C18X18") == 0)
            preferredFormat = 4;
        if(e.compareTo("C20X20") == 0)
            preferredFormat = 5;
        if(e.compareTo("C22X22") == 0)
            preferredFormat = 6;
        if(e.compareTo("C24X24") == 0)
            preferredFormat = 7;
        if(e.compareTo("C26X26") == 0)
            preferredFormat = 8;
        if(e.compareTo("C32X32") == 0)
            preferredFormat = 9;
        if(e.compareTo("C36X36") == 0)
            preferredFormat = 10;
        if(e.compareTo("C40X40") == 0)
            preferredFormat = 11;
        if(e.compareTo("C44X44") == 0)
            preferredFormat = 12;
        if(e.compareTo("C48X48") == 0)
            preferredFormat = 13;
        if(e.compareTo("C52X52") == 0)
            preferredFormat = 14;
        if(e.compareTo("C64X64") == 0)
            preferredFormat = 15;
        if(e.compareTo("C72X72") == 0)
            preferredFormat = 16;
        if(e.compareTo("C80X80") == 0)
            preferredFormat = 17;
        if(e.compareTo("C96X96") == 0)
            preferredFormat = 19;
        if(e.compareTo("C104X104") == 0)
            preferredFormat = 20;
        if(e.compareTo("C88X88") == 0)
            preferredFormat = 18;
        if(e.compareTo("C120X120") == 0)
            preferredFormat = 21;
        if(e.compareTo("C132X132") == 0)
            preferredFormat = 22;
        if(e.compareTo("C144X144") == 0)
            preferredFormat = 23;
        if(e.compareTo("C8X18") == 0)
            preferredFormat = 24;
        if(e.compareTo("C8X32") == 0)
            preferredFormat = 25;
        if(e.compareTo("C12X26") == 0)
            preferredFormat = 26;
        if(e.compareTo("C12X36") == 0)
            preferredFormat = 27;
        if(e.compareTo("C16X36") == 0)
            preferredFormat = 28;
        if(e.compareTo("C16X48") == 0)
            preferredFormat = 29;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String c)
    {
        code = c;
    }

    public double getLeftMarginCM()
    {
        return leftMarginCM;
    }

    public void setLeftMarginCM(double d)
    {
        leftMarginCM = d;
    }

    public double getTopMarginCM()
    {
        return topMarginCM;
    }

    public void setTopMarginCM(double d)
    {
        topMarginCM = d;
    }

    public Color getBackColor()
    {
        return backColor;
    }

    public void setBackColor(Color c)
    {
        backColor = c;
    }

    public int getResolution()
    {
        return resolution;
    }

    public void setResolution(int i)
    {
        resolution = i;
    }

    public Color getBarColor()
    {
        return barColor;
    }

    public void setBarColor(Color c)
    {
        barColor = c;
    }

    public int getRotate()
    {
        return rotate;
    }

    public void setRotate(int i)
    {
        rotate = i;
    }

    public String getName()
    {
        return "RDataMatrix";
    }
}
